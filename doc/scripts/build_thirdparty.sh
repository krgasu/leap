#!/bin/bash

# Source the scripts file
if ! . build_configure.sh 2>/dev/null ; then
 echo "Unable to find the config file" ; exit 1
fi

#================== SUPPORT FUNCTIONS ==============#
function CompileMessage {
echo "Compiling $1 will start in 2s"
sleep 1
echo "Compiling $1 will start in 1s"
}
function fetchFile {
    local FileSource="$1"
    local FileName="$2"

    # Fetch
    if [ ! -r "${FileName}" ]; then
      echo " Fetching ${FileSource}"
      if ! curl -kLo ${FileName} "${FileSource}"; then
        echo "error: Unable to fetch ${FileSource}" >&2
        exit 1
      fi
    else
      echo "${FileName} found on disk. Skipping."
    fi
}
function unpackFile {
    local FileName="$1"
    local FileDir="$2"

    #Check
    if [ -d ${FileDir} ]; then
      echo "${FileName} already unpacked. Skipping."
    else
      # Unpack
      echo "Unpacking: ${FileName} "
      if ! tar -zxC "${LIB_src}" -f "${FileName}"; then
        echo " error: Unpacking ${FileName} failed" >&2
        exit 1
      fi
     fi
}

#================= OPENMPI BUILD ==================#
function buildOPENMPI {
    echo "Checking for openmpi build..."

    cd $LIB_dir

    if [ -d ${openmpiBuildDir} ]; then
      echo "     found the build directory in ${openmpiBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${openmpiFileSource}" "${openmpiFileName}"
      unpackFile "${openmpiFileName}" "${openmpiFileDir}"

      mkdir ${openmpiBuildDir}

      cd ${openmpiFileDir}


      CompileMessage "OPENMPI"

      if ! ./configure  LDFLAGS=${LD_flag}                                                         \
                        CC="${cc_comp}" CFLAGS="${CC_flag}"                                        \
                        CXX="${xx_comp}" CXXFLAGS="${XX_flag}" F77="${ff_comp}" FFLAGS="${FF_flag}"\
                        FC="${f90_comp}" FCFLAGS="${FF_flag}"                                      \
                        --enable-fortran  ${host_opt}                                              \
                        --prefix=${openmpiBuildDir} ; then

        echo 'Failed to configure OPENMPI.'>&2 ; exit 1
      fi
      if ! make ; then
        echo 'Failed to compile OPENMPI.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install OPENMPI.'>&2 ; exit 1
      fi
    fi
}
#================== LAPACK BUILD ==================#
function buildLAPACK {
    echo "Checking for LAPACK/BLAS build..."
    cd $LIB_dir

    if [ -d ${lapackBuildDir} ]; then
      echo "     found the build directory in ${lapackBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${lapackFileSource}" "${lapackFileName}"
      unpackFile "${lapackFileName}" "${lapackFileDir}"

      mkdir ${lapackBuildDir}

      cd ${lapackFileDir}

      export CC=${cc_comp}
      export CFLAGS=${CC_flag}
      export CXX=${xx_comp}
      export CXXFLAGS=${XX_flag}
      export F77=${ff_comp}
      export FFLAGS=${FF_flag}
      export FC=${f90_comp}
      export FCFLAGS=${FF_flag}

      # Define a shortcut with 'sed'. Equivalent to 'sed -i' on non-MAC systems.
      sedi () { sed "$@" > foo.tmp; mv foo.tmp $2; }

      CompileMessage "LAPACK/BLAS"
      cp make.inc.example make.inc
      sedi 's/FORTRAN.*=.*/FORTRAN = '"${F77}"'/g' make.inc
      sedi 's/OPTS.*=.*/OPTS = '"${FFLAGS}"'/g' make.inc
      sedi 's/DRVOPTS.*=.*/DRVOPTS = $(OPTS)/g' make.inc
      sedi 's/NOOPT.*=.*/NOOPT = /g' make.inc
      sedi 's/# TIMER    = INT_CPU_TIME/TIMER = INT_CPU_TIME/g' make.inc
      sedi 's/LOADER.*=.*/LOADER = '"${F77}"'/g' make.inc
      sedi 's/LOADOPTS.*=.*/LOADOPTS = '"${FFLAGS}"'/g' make.inc
      sedi 's/BLASLIB.*=.*/BLASLIB = \.\.\/\.\.\/libblas\.a/g' make.inc
      sedi 's/LAPACKLIB.*=.*/LAPACKLIB = liblapack.a/g' make.inc
      if ! make blaslib; then
        echo "Failed to compile BLAS">&2 ; exit 1
      fi
      if ! make lapacklib; then
        echo "Failed to compile LAPACK">&2 ; exit 1
      fi
      cp libblas.a ${lapackBuildDir}
      cp liblapack.a ${lapackBuildDir}
    fi
}

#================== SUNDIALS BUILD ==================#
function buildSUNDIALS {
    echo "Checking for sundials build..."
    
    cd $LIB_dir
    
    if [ -d ${sundialsBuildDir} ]; then
      echo "     found the build directory in ${sundialsBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."
    
      fetchFile "${sundialsFileSource}" "${sundialsFileName}"
      unpackFile "${sundialsFileName}" "${sundialsFileDir}"
    
      mkdir ${sundialsBuildDir}
    
      cd ${sundialsBuildDir}
    
    
      CompileMessage "SUNDIALS"

      if ! cmake  -DCMAKE_INSTALL_PREFIX="${sundialsBuildDir}"           \
                  -DEXAMPLES_INSTALL_PATH="${sundialsBuildDir}/examples" \
    	          -DCMAKE_FORTRAN_COMPILER="${ff_comp}"                  \
    	          -DCMAKE_FORTRAN_FLAGS="${FF_flag}"                     \
    	          -DCMAKE_C_COMPILER="${cc_comp}"                        \
    	          -DCMAKE_C_FLAGS="${CC_flag}"                           \
    	          -DMPI_ENABLE="OFF"                                     \
    	          -DFCMIX_ENABLE="ON"                                    \
    	          -DLAPACK_ENABLE="ON"                                   \
    	          ${sundialsFileDir} ; then
    	echo 'Failed to configure SUNDIALS.'>&2 ; exit 1
      fi
      if ! make install ; then
    	echo 'Failed to install SUNDIALS.'>&2 ; exit 1
      fi
    fi
}


#================== FFTW3 BUILD ==================#
function buildFFTW3 {
    echo "Checking for FFTW3 build..."

    cd $LIB_dir

    if [ -d ${fftwBuildDir} ]; then
      echo "     found the build directory in ${fftwBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${fftwFileSource}" "${fftwFileName}"
      unpackFile "${fftwFileName}" "${fftwFileDir}"

      mkdir ${fftwBuildDir}

      cd ${fftwFileDir}

      CompileMessage "FFTW"
      if ! ./configure  LDFLAGS=${LD_flag}                                                         \
                        CC="${cc_comp}" CFLAGS="${CC_flag}"                                        \
                        CXX="${xx_comp}" CXXFLAGS="${XX_flag}" F77="${ff_comp}" FFLAGS="${FF_flag}"\
                        FC="${f90_comp}" FCFLAGS="${FF_flag}"                                      \
                        ${host_opt}                                                                \
                        --prefix=${fftwBuildDir} ; then
        echo 'Failed to configure FFTW.'>&2 ; exit 1
      fi
      if ! make ; then
        echo 'Failed to compile FFTW.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install FFTW.'>&2 ; exit 1
      fi
    fi
}


#================== HYPRE BUILD ==================#
function buildHYPRE {
    echo "Checking for hypre build..."

    cd $LIB_dir

    if [ -d ${hypreBuildDir} ]; then
      echo "     found the build directory in ${hypreBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${hypreFileSource}" "${hypreFileName}"
      unpackFile "${hypreFileName}" "${hypreFileDir}"

      mkdir ${hypreBuildDir}

      cd ${hypreFileDir}/src


      CompileMessage "HYPRE"

      if ! ./configure  LDFLAGS=${LD_flag}                                                        \
                        CC="${mpi_cc}" CFLAGS="${CC_flag}"                                        \
                        CXX="${mpi_xx}" CXXFLAGS="${XX_flag}" F77="${mpi_ff}" FFLAGS="${FF_flag}" \
                        FC="${mpi_f90}" FCFLAGS="${FF_flag}"                                      \
                        --enable-fortran  ${host_opt}                                             \
                        --enable-debug  --with-print-errors                                       \
                        --prefix=${hypreBuildDir} ; then

        echo 'Failed to configure HYPRE.'>&2 ; exit 1
      fi
      if ! make ; then
        echo 'Failed to compile HYPRE.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install HYPRE.'>&2 ; exit 1
      fi
    fi
}

#================== ZLIB BUILD ==================#
function buildZLIB {
    echo "Checking for ZLIB build..."

    cd $LIB_dir

    if [ -d ${zlibBuildDir} ]; then
      echo "     found the build directory in ${zlibBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${zlibFileSource}" "${zlibFileName}"
      unpackFile "${zlibFileName}" "${zlibFileDir}"

      mkdir ${zlibBuildDir}

      cd ${zlibFileDir}

      CompileMessage "ZLIB"
      if ! LDFLAGS=${LD_flag}                                                          \
           CC="${cc_comp}" CFLAGS="${CC_flag}"                                         \
           CXX="${xx_comp}" CXXFLAGS="${XX_flag}" F77="${ff_comp}" FFLAGS="${FF_flag}" \
           FC="${f90_comp}" FCFLAGS="${FF_flag}"                                       \
           ./configure   --prefix=${zlibBuildDir} ; then

        echo 'Failed to configure ZLIB.'>&2 ; exit 1
      fi
      if ! make clean install ; then
        echo 'Failed to make clean ZLIB.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install ZLIB.'>&2 ; exit 1
      fi


    fi
}

#================== SZIP BUILD ==================#
function buildSZIP {
    echo "Checking for SZIP build..."

    cd $LIB_dir

    if [ -d ${szipBuildDir} ]; then
      echo "     found the build directory in ${szipBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      fetchFile "${szipFileSource}" "${szipFileName}"
      unpackFile "${szipFileName}" "${szipFileDir}"

      mkdir ${szipBuildDir}

      cd ${szipFileDir}

      CompileMessage "SZIP"
      if ! ./configure  LDFLAGS=${LD_flag}                                                         \
                        CC="${cc_comp}" CFLAGS="${CC_flag}"                                        \
                        CXX="${xx_comp}" CXXFLAGS="${XX_flag}" F77="${ff_comp}" FFLAGS="${FF_flag}"\
                        FC="${f90_comp}" FCFLAGS="${FF_flag}"                                      \
                        ${host_opt}                                                                \
                        --prefix=${szipBuildDir} ; then

        echo 'Failed to configure SZIP.'>&2 ; exit 1
      fi
      if ! make clean install ; then
        echo 'Failed to make clean SZIP.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install SZIP.'>&2 ; exit 1
      fi


    fi
}

#================== HDF5 BUILD ==================#
function buildHDF5 {
    echo "Checking for HDF5 build..."
    cd $LIB_dir

    # Ask if user wants parallel or serial HDF5
    read -p "Do you want the parallel HDF5 feature activated [Y,N]?" -n 1 -r
    echo
    if [[ $REPLY  =~ ^[Yy]$ ]]; then
      hdf5_parallel="--enable-parallel"
    else
      hdf5_parallel=""
      hdf5BuildDir="${hdf5BuildDir}_serial"
    fi

    if [ -d ${hdf5BuildDir} ]; then
      echo "     found the build directory in ${hdf5BuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."

      echo ${hdf5FileSource}
      fetchFile "${hdf5FileSource}" "${hdf5FileName}"
      unpackFile "${hdf5FileName}" "${hdf5FileDir}"

      mkdir ${hdf5BuildDir}

      cd ${hdf5FileDir}

      CompileMessage "HDF5"
      if ! ./configure  LDFLAGS=${LD_flag} CPPFLAGS=${CPP_flag}                                    \
                        CC="${mpi_cc}" CFLAGS="${CC_flag}"                                         \
                        CXX="${mpi_xx}" CXXFLAGS="${XX_flag}"                                      \
                        F77="${mpi_ff}" FFLAGS="${FF_flag}"                                        \
                        FC="${mpi_f90}" FCFLAGS="${FF_flag}"                                       \
                        --enable-fortran --with-zlib=${zlibBuildDir} --with-szlib=${szipBuildDir}  \
                        ${host_opt} ${hdf5_parallel} --enable-static                               \
                        --prefix=${hdf5BuildDir} ; then
        echo 'Failed to configure HDF5.'>&2 ; exit 1
      fi
      if ! make ; then
        echo 'Failed to compile HDF5.'>&2 ; exit 1
      fi
      #if ! make check ; then
      #      echo 'Check failed: HDF5.'>&2 ; exit 1
      #fi
      if ! make install ; then
        echo 'Failed to install HDF5.'>&2 ; exit 1
      fi
    fi
}
#================== SILO BUILD ==================#
function buildSILO {
    cd $LIB_dir
    echo "Checking for SILO build..."

    if [ -d ${siloBuildDir} ]; then
      echo "     found the build directory in ${siloBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."
      fetchFile "${siloFileSource}" "${siloFileName}"
      unpackFile "${siloFileName}" "${siloFileDir}"

      mkdir ${siloBuildDir}

      cd ${siloFileDir}

      CompileMessage "SILO"

      if ! ./configure  LDFLAGS=${LD_flag}                                                         \
                        CC="${cc_comp}" CFLAGS="${CC_flag}"                                        \
                        CXX="${xx_comp}" CXXFLAGS="${XX_flag}" F77="${ff_comp}" FFLAGS="${FF_flag}"\
                        FC="${f90_comp}" FCFLAGS="${FF_flag}"                                      \
                        --enable-fortran --with-hdf5=${hdf5BuildDir}/include,${hdf5BuildDir}/lib   \
                        --with-zlib=${zlibBuildDir}/include,${zlibBuildDir}/lib                    \
                        --with-szlib=${szipBuildDir} --disable-silex                               \
                         ${host_opt}                                                               \
                        --prefix=${siloBuildDir} ; then
        echo 'Failed to configure SILO.'>&2 ; exit 1
      fi
      if ! make ; then
        echo 'Failed to compile SILO.'>&2 ; exit 1
      fi
      if ! make install ; then
        echo 'Failed to install SILO.'>&2 ; exit 1
      fi
    fi
}

#================== H5HUT BUILD ==================#
function buildH5HUT {
    cd $LIB_dir

    echo "Checking for H5HUT build..."
    if [ -d ${h5hutBuildDir} ]; then
      echo "     found the build directory in ${h5hutBuildDir}. Skipping."
    else
      echo "     unable to find the build directory. Proceeding to downloading and compiling."
      fetchFile "${h5hutFileSource}" "${h5hutFileName}"
      unpackFile "${h5hutFileName}" "${h5hutFileDir}"

      mkdir ${h5hutBuildDir}

      cd ${h5hutFileDir}-*

      CompileMessage "H5HUT"

      #./autogen.sh

      if ! ./autogen.sh; then
        echo 'Failed at autogen step.' >&2 ; exit 1
      fi

      if ! ./configure LDFLAGS=${LD_flag}                                              \
                       CC="${mpi_cc}" CFLAGS="${CC_flag}"                              \
                       CXX="${mpi_xx}" CXXFLAGS="${XX_flag}"                           \
                       FC="${mpi_f90}" FCFLAGS="${FF_flag}"                            \
                       --enable-fortran --enable-parallel                              \
                       --with-hdf5=${hdf5BuildDir}                                     \
                       --prefix=${h5hutBuildDir} ; then
      echo 'Failed to configure H5HUT.'>&2 ; exit 1
    fi
    if ! make ; then
      echo 'Failed to compile H5HUT.'>&2 ; exit 1
    fi
    if ! make install ; then
      echo 'Failed to install H5HUT.'>&2 ; exit 1
    fi

  fi
}

# Description
cat > "/dev/stderr" << EOF
#==============================================================#
#            Configuration: Please READ                        #
# You might change the directory where the libraries will be   #
# stored (LIB_dir). To update the links to the libraries, all  #
# you need to do is update the library version number!         #
# E.g.: The script below uses hdf5 1.8.13 but I checked online #
# and found the most recent version is 1.8.14 -> All I need to #
# do is change hdf5VerNum="1.8.14"                             #
#                                                              #
# HOW THE INSTALLER WORKS:                                     #
# -1- For every library, the installer will look if it can find#
#     a build directory (e.g.: lapackBuildDir). If it does     #
#     find one, it will assume that library is correctly       #
#     installed  and move on. Otherwise it will proceed to the #
#     next step                                                #
# -2- The installer will look for the tarball in LIB_dir.      #
#     If it can't find it, it will try to download it and      #
#     unpack it.                                               #
# -3- The installer will then proceed to configuring and       #
#     compiling the library.                                   #
#                                                              #
# It's possible to fine tune the compilers and flags used      #
# to build the libraries. This requires some understanding of  #
# what the packages do.                                        #
#                                                              #
# Contact: houssem                                             #
#==============================================================#
EOF

if ! read -n1 -rsp $'Press any key to continue or ctrl+c to exit.\n'; then
    exit 1
fi

#============== Create the directories ===============#
if [ ! -d "$LIB_dir" ]; then
  mkdir $LIB_dir
fi

if [ ! -d "${LIB_src}" ]; then
  mkdir ${LIB_src}
fi

#================ Check for Compilers ================#
echo "Checking for C compiler... ${cc_comp}"
if  ! command -v ${cc_comp} >/dev/null ; then
  echo "Error: ${cc_comp} is not a valid C compiler" >&2
fi

echo "Checking for C++ compiler... ${xx_comp}"
if  ! command -v ${cxx_comp} >/dev/null ; then
  echo "Error: ${cxx_comp} is not a valid C++ compiler" >&2
fi

echo "Checking for F77 compiler... ${ff_comp}"
if  ! command -v ${ff_comp} >/dev/null ; then
  echo "Error: ${ff_comp} is not a valid F77 compiler" >&2
fi

echo "Checking for F90 compiler... ${f90_comp}"
if  ! command -v ${f90_comp} >/dev/null ; then
  echo "Error: ${f90_comp} is not a valid F90 compiler" >&2
fi

cat > "/dev/stderr" << EOF
Compilers check... ok
EOF

for lib in "$@"; do
  if [ "$lib" = "OPENMPI" ]; then
	echo "Building OPENMPI"
	buildOPENMPI
  fi
  if [ "$lib" = "LAPACK" ]; then
	echo "Building LAPACK"
	buildLAPACK
  fi
  if [ "$lib" = "FFTW3" ]; then
	echo "Building FFTW3"
	buildFFTW3
  fi
  if [ "$lib" = "SUNDIALS" ]; then
	echo "Building SUNDIALS"
	buildSUNDIALS
  fi
  if [ "$lib" = "HYPRE" ]; then
	echo "Building HYPRE"
	buildHYPRE
  fi
  if [ "$lib" = "ZLIB" ]; then
	echo "Building ZLIB"
	buildZLIB
  fi
  if [ "$lib" = "SZIP" ]; then
	echo "Building SZIP"
	buildSZIP
  fi
  if [ "$lib" = "HDF5" ]; then
	echo "Building HDF5"
	buildHDF5
  fi
  if [ "$lib" = "H5HUT" ]; then
	echo "Building H5HUT"
	buildH5HUT
  fi
  if [ "$lib" = "SILO" ]; then
	echo "Building SILO"
	buildSILO
  fi
done

echo "Installation completed successfully !"
