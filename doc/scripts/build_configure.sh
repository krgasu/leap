#!/bin/bash

#================ Compilers and Flags ===============#
cc_comp="gcc"
xx_comp="g++"
ff_comp="gfortran"
f90_comp="gfortran"
mpi_cc="mpicc"
mpi_xx="mpicxx"
mpi_ff="mpif77"
mpi_f90="mpif90"
CC_flag="-O3 -ftree-vectorize"
XX_flag="-O3 -ftree-vectorize"
FF_flag="-O3 -ftree-vectorize"
LD_flag=""
CPP_flag=

#========== Target Installation Directory ===========#
LIB_dir="${HOME}/Codes/ThirdParty"
LIB_src="${LIB_dir}/src"

#================= Build Directories ================#
lapackBuildDir="${LIB_dir}/lapack"
fftwBuildDir="${LIB_dir}/fftw"
sundialsBuildDir="${LIB_dir}/sundials"
hdf5BuildDir="${LIB_dir}/hdf5"
szipBuildDir="${LIB_dir}/szip"
zlibBuildDir="${LIB_dir}/zlib"
siloBuildDir="${LIB_dir}/silo"
hypreBuildDir="${LIB_dir}/hypre"
h5hutBuildDir="${LIB_dir}/h5hut"

#================== Download Links ==================#
openmpiVerNum="3.1.1"
openmpiDirName="openmpi-${openmpiVerNum}"
openmpiFileName="${openmpiDirName}.tar.gz"
openmpiFileSource="http://www.open-mpi.org/software/ompi/v3.1/downloads/${openmpiFileName}"
openmpiFileDir="${LIB_src}/${openmpiDirName}"
openmpiBuildDir="${LIB_dir}/openmpi"

lapackVerNum="3.7.1"
lapackDirName="lapack-${lapackVerNum}"
lapackFileName="${lapackDirName}.tgz"
lapackFileSource="http://www.netlib.org/lapack/${lapackFileName}"
lapackFileDir="${LIB_src}/${lapackDirName}"

fftwVerNum="3.3.6-pl2"
fftwDirName="fftw-${fftwVerNum}"
fftwFileName="${fftwDirName}.tar.gz"
fftwFileSource="http://www.fftw.org/${fftwFileName}"
fftwFileDir="${LIB_src}/${fftwDirName}"
fftwBuildDir="${LIB_dir}/fftw"

sundialsVerNum="2.7.0"
sundialsDirName="sundials-${sundialsVerNum}"
sundialsFileName="${sundialsDirName}.tar.gz"
sundialsFileSource="https://computation.llnl.gov/projects/sundials/download/$sundialsFileName"
sundialsFileDir="${LIB_src}/${sundialsDirName}"

hdf5VerNum="1.10"
hdf5Patch="4"
hdf5DirName="hdf5-${hdf5VerNum}"
hdf5FileName="${hdf5DirName}.${hdf5Patch}.tar.gz"
hdf5FileSource="https://support.hdfgroup.org/ftp/HDF5/releases/${hdf5DirName}/${hdf5DirName}.${hdf5Patch}/src/${hdf5FileName}"
hdf5FileDir="${LIB_src}/${hdf5DirName}.${hdf5Patch}"

szipVerNum="2.1.1"
szipDirName="szip-${szipVerNum}"
szipFileName="${szipDirName}.tar.gz"
szipFileSource="https://support.hdfgroup.org/ftp/lib-external/szip/${szipVerNum}/src/${szipFileName}"
szipFileDir="${LIB_src}/${szipDirName}"

zlibVerNum="1.2.11"
zlibDirName="zlib-${zlibVerNum}"
zlibFileName="${zlibDirName}.tar.gz"
zlibFileSource="http://zlib.net/${zlibFileName}"
zlibFileDir="${LIB_src}/${zlibDirName}"

siloVerNum="4.10.2"
siloDirName="silo-${siloVerNum}"
siloFileName="${siloDirName}.tar.gz"
siloFileSource="http://wci.llnl.gov/content/assets/docs/simulation/computer-codes/silo/${siloDirName}/${siloFileName}"
siloFileDir="${LIB_src}/${siloDirName}"

hypreVerNum="2.11.2"
hypreDirName="hypre-${hypreVerNum}"
hypreFileName="${hypreDirName}.tar.gz"
hypreFileSource="https://computation.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods/download/$hypreFileName"
hypreFileDir="${LIB_src}/${hypreDirName}"

h5hutVerNum="2.0.0rc5"
h5hutDirName="H5hut-${h5hutVerNum}"
h5hutFileName="${h5hutDirName}.tar.gz"
h5hutFileSource="https://gitlab.psi.ch/H5hut/src/repository/archive.tar.gz?ref=H5hut-2.0.0rc5"
h5hutFileDir="${LIB_src}/src-${h5hutDirName}"
