---
project: LEAP
summary: LEAP is a Computational Fluid Dynamics (CFD) code developed by Prof. Mohamed Houssem Kasbaoui at Arizona State University. LEAP is designed to deliver robust and efficient algorithms for CFD applications involving laminar and turbulent single-phase/multiphase flows. -- https://kasbaoui.bitbucket.io
author: Mohamed Houssem Kasbaoui
author_description: School for Engineering of Matter Transport and Energy -- Arizona State University
twitter: https://twitter.com/prof_kasbaoui
email: houssem.kasbaoui@asu.edu
website: https://kasbaoui.bitbucket.io/
project_website: https://kasbaoui.bitbucket.io/
project_bitbucket: https://bitbucket.org/krgasu/leap/
project_url: https://kasbaoui.bitbucket.io/leap
license: gfdl
src_dir: ../src
incl_src: false
exclude_dir: ../src/tests
include: ../../ThirdParty/silo/include/
         ../../ThirdParty/h5hut/include/
docmark: !
docmark_alt: >
predocmark_alt: <
predocmark: *
display: public
         protected
         private
source: false
sort: permission-alpha
creation_date: %Y-%m-%d %H:%M %z
print_creation_date: true
version: v0.5.0
graph: true
coloured_edges: true
search: false
output_dir: ./html
media_dir: ./media

{!../README.md!}

---
