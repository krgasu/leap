#   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
#   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Solver parameters
# ------------------------------------------------------------------------------
Solver:                        CDIFS
Case:                          LID-DRIVEN CAVITY

Fluid density:                 1.0
Fluid viscosity:               1e-3
Gravity:                       0.0 0.0 0.0

Subiterations:                 1
Scheme order:                  2

HYPRE Solver:                  IJ-AMG-NONE
HYPRE Max Iter:                25
HYPRE Convergence Tol:         1e-8

# Timer parameters
# ------------------------------------------------------------------------------
Timestep:                      5.0e-3
Maximum time:                  30
Maximum iterations:            12000
Wall time:                     10
Frequency write restart data:  10.0
Frequency write output data:   4.0e-1
Output data:                   P Vel

# Block parameters
# ------------------------------------------------------------------------------
Domain size:                   1.0 1.0 1.5625e-2
Grid points:                   64 64 1
Ghost cells:                   1
Partition:                     2 2 1
Periodicity:                   .false. .false. .true.
Block file:                    block.hdf5

# Initial conditions parameters
# ------------------------------------------------------------------------------
Lid velocity:                  1.0
Fields IC file:                fields_ini.h5

# Run parameters
# ------------------------------------------------------------------------------
Fields write file:             fields.h5
Fields overwrite:              .false.
