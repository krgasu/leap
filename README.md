# Introduction

LEAP is a state-of-the-art, highly scalable, Computational Fluid Dynamics (CFD) code developed by Prof. Mohamed Houssem Kasbaoui at Arizona State University.
LEAP is designed to deliver robust and efficient algorithms for CFD applications involving laminar and turbulent single-phase/multiphase flows. The code is built bottom-up to leverage MPI parallelization and modern FORTRAN features (Object Oriented Programing, data polymorphism and code-reusability).

As a modern CFD package, LEAP offers the following advantages:

* **Flexible, Reusable, and Extendable Data Structures:**
Data structures are implemented with reusability and scalability in mind thanks to the new polymorphism and object oriented capabilities of Fortran 2008 and 2018. The two workhorses in LEAP are Eulerian and Lagrangian types.
    * The **Eulerian** data type (`eulerian_obj`) represents field quantities such as velocity field, pressure field, etc. The module leapEulerian defines a suite of data types and procedures that allows one to very easily define and work with new fields. Thanks to Object Oriented Fortran and polymorphism, things like allocation and parallel communications (for ghostcells and others) happen under the hood.
    * The **Lagrangian** data type (`lagrangian_obj`) represents lagrangian quantities such as tracers, inertial particles, immersed boundary markers. Abstract derived types are defined in the module leapLagrangian and can be easily extended to represent whatever Lagrangian quantity one desires. Here too, Object Oriented Fortran and polymorphism means that defining new types is made very easy with things like parallel communications and I/O directly inherited from the base type. Users can focus on implementing their physics, and worry less about the geeky MPI details or recoding basic procedures like interpolations.
    * The **Solver** data type (`solver_obj`) allows the user to easily define a new solver, implement any PDE using the provided finite-difference schemes (up to 6th order), advance the solution in time, and perform IO in single or double precision. There is no limit on how many different solvers can be implemented or any requirement on the equations to be solved.

* **High-Performance and Portrable I/O:**
By default, LEAP reads/writes files in the HDF5 file format, the gold standard for open-source, high-performance, and portable file format in scientific computing. HDF5 files can be explored using tools such as `h5dump` provided with HDF5 distributions. LEAP uses the H5HUT library to write The HDF5 files. These files can also be ingested by popular scientific visualization software such as VisIt or ParaView. In addition, LEAP can output files in the SILO file format. This is the preferred I/O driver for files intended for visualization using VisIt/ParaView.Additonal I/O drivers can be implmented upon request.

Solvers currently implemented in LEAP:

* **CDIFS:** Constant-Density-Incompressible-Flow-Solver. This solver solves the Navier-Stokes equations in 1D, 2D, or 3D on a Cartesian grid with staggered variable arrangment. Finite-volume schemes up to 6th order are available. Time advancement is based on a 2nd order iterative Crank-Nicolson scheme. Sample inputs for this solver are provided with the source code.
* **GRANS:** GRANular flow Solver. This solver is an implementation of dry collisional dynamics of granular material. This is an experimental solver.

In addition to providing the sovlers above, LEAP can be called as an external library to provide the following features:

* **Finite-Difference Schemes:** Fully conservative higher order finite difference schemes for incomressible flow.
* **Immersed Boundaries:** LEAP provides routines to implement Immersed Boundaries (IB) in a CFD code. The IB method in LEAP is based on the volume-filtering method. LEAP is able to read STL files and compute the forcing force that imposes the no-slip Boundary conditions
* **Resolved Particles:** A suite of procedures allow the definition, tracking and updating of resolved particles. LEAP manages all the communications, creation of ghost particles, and collisions. The Resolved Particles are NOT restricted to spheres only. LEAP offers hooks to define custom particle-particle collision models that allow implementations of arbitrarily shaped particle.
* **Point (Lagrangian) Particles:** LEAP provides data types and methods to defines clouds of point particles and the manage parallel communication using MPI.
* **I/O capabilities:** read/write files in H5HUT and SILO formats.

LEAP is currently under development. More features will be made available with time.

# License
LEAP is distributed open-source under GPLv3 license. See LICENSE.md.

# Compiling LEAP
LEAP requires a compiler compatible with Fortran 2008, such as Intel Fortran Compiler 18x or Gfortran 8x and higher.

The following libraries are required to run LEAP:

* An MPI distribution (recommended OpenMPI): This library handles the distributed memory aspects and allows the code to run in parallel on large computing systems.
* HDF5: This library provides a file format (hdf5) that is widely used for scientific data. It is designed to be scalable, and optimizes IO on distributed systems. There is a nice little tool called HDF View that provides a GUI to look into the meta data in the file.
* H5HUT: this is a wrapper around HDF5 that provides a high level API to make using HDF5 easier with block and Lagrangian data.
* SZIP: A compression library from the HDF group used by HDF5.
* ZLIB: Another compression library used by HDF5.
* SILO: A file format for point, structured, and unstructured meshes developed by the Lawrence Livermore National Laboratory.
* HYPRE: Scalable linear solvers and multigrid methods developed by the Lawrence Livermore National Laboratory.

Refer to the WIKI for instructions on how to compile the prerequisite libraries and LEAP.


# Documentation

Documentation for the most recent version is available here: [https://kasbaoui.bitbucket.io/leap](https://kasbaoui.bitbucket.io/leap).

Documentation is generated by the automatic documentation generator [FORD](http://fortranwiki.org/fortran/show/FORD).
To generate a local copy, navigate to the doc folder and create the documentation as following:

```
#!bash
cd /PATH/TO/LEAP/doc
ford leap-doc.md
```

You will find the documentation under
```
#!bash

/PATH/TO/LEAP/doc/html/index.html
```

Further details can be found in the project [WIKI](https://bitbucket.org/krgasu/leap/wiki)


# Support
If you need suport with compiling, running, developing applications based on LEAP, or if you need to report bugs contact Prof. Kasbaoui (<houssem.kasbaoui@asu.edu>).

