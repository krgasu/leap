!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module particles_resolved
  !>--------------------------------------------------------------------------
  ! Module: particles_resolved
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Methods to treat the particle phase as immersed boundaries in
  ! particle-resolved direct numerical simulations (PR-DNS)
  ! --------------------------------------------------------------------------
  use leapIO
  use leapKinds
  use leapLagrangian,      only : lagrangian_obj, lagrangian_set
  use leapEulerian,        only : eulerian_obj_r,eulerian_set
  use leapBlock,           only : block_obj
  use leapParallel,        only : parallel_obj
  use immersed_boundaries, only : marker_obj,marker_set
  use leapParser,          only : parser_obj
  use leapDiffOp,          only : op_obj
  use leapTimer,           only : timer_obj
  use leapMonitor,         only : monitor_set
  implicit none

  private
  public :: ResPart_set, ResPart_obj

  type,extends(lagrangian_obj) :: ResPart_obj
    !> An extended Lagrangian object that represents
    ! particle represented as an Immersed Boundary
    integer  :: s                                          !! A tag
    real(wp) :: d                                          !! Diameter of the particle
    real(wp) :: rho                                        !! Particle density
    real(wp) :: v(3)                                       !! Particle velocity
    real(wp) :: w(3)                                       !! Particle angular velocity
    real(wp) :: Fh(3)                                      !! Hydrodynamic force applied on particle
    real(wp) :: Th(3)                                      !! Hydrodynamic torque applied on particle
    real(wp) :: Fc(3)                                      !! Collision force applied on particle
    real(wp) :: Tc(3)                                      !! Collision torque applied on particle
    real(wp) :: pold(3)                                    !! Old particle position
    real(wp) :: vold(3)                                    !! Old particle velocity
    real(wp) :: wold(3)                                    !! Old particle angular velocity
    real(wp) :: Fhold(3)                                   !! Old hydrodynamic force
    real(wp) :: Thold(3)                                   !! Old hydrodynamic torque
    real(wp) :: Fcold(3)                                   !! Old collision force
    real(wp) :: Tcold(3)                                   !! Old collision torque
    contains
      procedure :: assign => ResPart_obj_assign
  end type ResPart_obj

  type, extends(lagrangian_set):: ResPart_set
    !> An extended Lagrgian set representing resolved particles
    type(marker_set)           :: ib                       !! Surface markers
    integer,allocatable        :: lookup(:)                !! Lookup array
    integer,allocatable        :: ranks(:)                 !! MPI ranks of the lagrangian objects
    ! ----
    type(timer_obj),   pointer :: timer     => null()      !! Timer utility
    type(parser_obj),  pointer :: parser    => null()      !! Parser for input file
    type(monitor_set), pointer :: monitors  => null()      !! Monitors to print to stdout and files
    type(op_obj),      pointer :: op        => null()      !! operators object
    real(wp)                   :: gravity(3) = 0.0_wp      !! Gravity
    real(wp)                   :: rhof       = 1.0_wp      !! Fluid density
    contains
      procedure :: SetReadFileName     => ResPart_set_SetReadFileName
      procedure :: Read                => ResPart_set_Read
      procedure :: SetWriteFileName    => ResPart_set_SetWriteFileName
      procedure :: Write               => ResPart_set_Write
      procedure :: Regroup             => ResPart_set_Regroup
      procedure :: UpdateLookup        => ResPart_set_UpdateLookup
      procedure :: SetFilterSize       => ResPart_set_SetFilterSize
      procedure :: Filter              => ResPart_set_Filter
      procedure :: SetMPIDataTypeParams=> ResPart_set_SetMPIDataTypeParams
      procedure :: SetObjectType       => ResPart_set_SetObjectType
      procedure :: lagrangian_set_Init => ResPart_set_Init
      procedure :: lagrangian_set_Final=> ResPart_set_Final
      procedure :: Prepare             => ResPart_set_Prepare
      procedure :: UpdateSDF           => ResPart_set_UpdateSDF
      procedure :: UpdateNormals       => ResPart_set_UpdateNormals
      procedure :: StoreOld            => ResPart_set_StoreOld
      procedure :: AdvanceCenters      => ResPart_set_AdvanceCenters
      procedure :: AdvanceMarkers      => ResPart_set_AdvanceMarkers
      procedure :: GetIBForcing        => ResPart_set_GetIBForcing
      procedure :: GetHydroForces      => ResPart_set_GetHydroForces
      procedure :: GetSurfaceStresses  => ResPart_set_GetSurfaceStresses
      procedure :: CreateMonitor       => ResPart_set_CreateMonitor
      procedure :: UpdateMonitor       => ResPart_set_UpdateMonitor
  end type ResPart_set

  contains
    ! IBparticle_obj methods
    ! ------------------------------------------------------
    subroutine ResPart_obj_assign(this,val)
      !> Assignment
      implicit none
      class(ResPart_obj),    intent(inout) :: this
      class(lagrangian_obj), intent(in)    :: val

      select type(val)
      type is (ResPart_obj)
        this%id   = val%id
        this%p    = val%p
        this%c    = val%c
        this%d    = val%d
        this%rho  = val%rho
        this%v    = val%v
        this%w    = val%w
        this%s    = val%s
        this%Fh   = val%Fh
        this%Th   = val%Th
        this%Fc   = val%Fc
        this%Tc   = val%Tc
        this%pold = val%pold
        this%vold = val%vold
        this%wold = val%wold
        this%Fhold= val%Fhold
        this%Thold= val%Thold
        this%Fcold= val%Fcold
        this%Tcold= val%Tcold
      end select

      return
    end subroutine ResPart_obj_assign

    ! ResPart_set methods
    ! ------------------------------------------------------
    subroutine ResPart_set_Init(this,name,block,parallel)
      !> Initialize the ResPart_set type.
      ! This subourtine replaces the inheritted lagrangian_init.
      class(ResPart_set),     intent(inout) :: this        !! Set of resolved partilces
      character(len=*),            intent(in)  :: name     !! Name of instance
      type(block_obj),      target,intent(in)  :: block    !! A block object
      type(parallel_obj),   target,intent(in)  :: parallel !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Allocate arrays
      allocate(this%count_proc(this%parallel%nproc))
      this%count_proc(:)=0

      ! Set name of variable
      this%name=trim(adjustl(name))

      ! Set sample type
      call this%SetObjectType()

      ! Initialize array with length zero
      this%count_ = 0
      call this%resize(this%count_)

      ! Create MPI type
      call this%CreateMPIType

      ! Initialize immersed boundary
      call this%ib%initialize(trim(adjustl(this%name))//'_markers',this%block,this%parallel)
      return
    end subroutine ResPart_set_Init
    subroutine ResPart_set_Final(this)
      !> Finalize the ResPart_set type.
      ! This subourtine replaces the inheritted lagrangian_final.
      class(ResPart_set),     intent(inout) :: this        !! Set of resolved partilces

      ! Remove all centroids
      call this%resize(0)

      ! Deallocate arrays
      if(allocated(this%count_proc)) deallocate(this%count_proc)
      if(allocated(this%ranks))      deallocate(this%ranks)
      if(allocated(this%lookup))     deallocate(this%lookup)

      ! Collision/Neighbor search tools
      if(allocated(this%objincell)) deallocate(this%objincell)
      if(allocated(this%neighbors)) then
        call this%FreeNeighborList()
        deallocate(this%neighbors)
      end if

      ! Finalize collision block, if used
      call this%cblock%finalize()

      ! Finalize IB
      call this%ib%finalize()

      ! Nullify pointers
      this%parallel => null()
      this%block    => null()

      return
    end subroutine ResPart_set_Final
    subroutine ResPart_set_SetObjectType(this)
      !> description
      implicit none
      class(ResPart_set), intent(inout) :: this            !! Set of resolved particles
      type(ResPart_obj) :: sample                          !! My sample
      allocate (this%sample,source=sample)
      return
    end subroutine ResPart_set_SetObjectType
    subroutine ResPart_set_SetReadFileName(this,name)
      !> Set the base name of file to write
      implicit none
      class(ResPart_set), intent(inout) :: this            !! Set of resolved particles
      character(len=*),   intent(in)    :: name            !! Name of file
      ! Work variables
      integer :: pos
      character(len=str64):: filename

      filename=name

      ! Figure out the base name, removing any extensions
      pos=scan(trim(adjustl(filename)),".",BACK=.true.)
      if (pos.gt.0) filename=filename(1:pos-1)

      this%read_file    = trim(adjustl(filename))//"_centers.h5"
      this%ib%read_file = trim(adjustl(filename))//"_markers.h5"

      return
    end subroutine ResPart_set_SetReadFileName
    subroutine ResPart_set_Read(this,iter,time)
      !> Read ResPart data from file
      implicit none
      class(ResPart_set), intent(inout) :: this            !! Set of resolved particles
      integer,            intent(out)   :: iter            !! Iteration at write
      real(wp),           intent(out)   :: time            !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5
      integer,        allocatable :: buffi(:)
      integer(leapI8),allocatable :: buffi8(:)
      real(wp),       allocatable :: buffr(:)

      ! Open the fi le
      call h5%initialize(trim(adjustl(this%read_file)),"R",this%parallel)

      ! Jump to last step
      call h5%LastTimeStep(iter,time)

      ! Get total number of point objects
      call h5%getnpoints(this%count)

      ! Distribute equally the data on each mpi rank
      associate(mpi=>this%parallel)
        this%count_=int(this%count/mpi%nproc)
        if (mpi%rank%mine.le.mod(this%count,mpi%nproc)) this%count_=this%count_+1
      end associate

      ! Resize the data array to contain read Lagrangian objects
      call this%resize(this%count_)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))
      ! Read the base Lagrangian object structure
      if (this%count_.eq.0) then
        call h5%read("id",  buffi8)
        call h5%read("x",   buffr )
        call h5%read("y",   buffr )
        call h5%read("z",   buffr )
        call h5%read("i",   buffi )
        call h5%read("j",   buffi )
        call h5%read("k",   buffi )
        call h5%read("u",   buffr )
        call h5%read("v",   buffr )
        call h5%read("w",   buffr )
        call h5%read("wx",  buffr )
        call h5%read("wy",  buffr )
        call h5%read("wz",  buffr )
        call h5%read("d",   buffr )
        call h5%read("s",   buffi )
        call h5%read("rho", buffr )
        call h5%read("Fhx", buffr )
        call h5%read("Fhy", buffr )
        call h5%read("Fhz", buffr )
        call h5%read("Thx", buffr )
        call h5%read("Thy", buffr )
        call h5%read("Thz", buffr )
        call h5%read("Fcx", buffr )
        call h5%read("Fcy", buffr )
        call h5%read("Fcz", buffr )
        call h5%read("Tcx", buffr )
        call h5%read("Tcy", buffr )
        call h5%read("Tcz", buffr )
      else
        select type (particles=>this%p)
        type is (ResPart_obj)
          call h5%read("id",  buffi8); particles(:)%id   = buffi8
          call h5%read("x",   buffr ); particles(:)%p(1) = buffr
          call h5%read("y",   buffr ); particles(:)%p(2) = buffr
          call h5%read("z",   buffr ); particles(:)%p(3) = buffr
          call h5%read("i",   buffi ); particles(:)%c(1) = buffi
          call h5%read("j",   buffi ); particles(:)%c(2) = buffi
          call h5%read("k",   buffi ); particles(:)%c(3) = buffi
          call h5%read("u",   buffr ); particles(:)%v(1) = buffr
          call h5%read("v",   buffr ); particles(:)%v(2) = buffr
          call h5%read("w",   buffr ); particles(:)%v(3) = buffr
          call h5%read("wx",  buffr ); particles(:)%w(1) = buffr
          call h5%read("wy",  buffr ); particles(:)%w(2) = buffr
          call h5%read("wz",  buffr ); particles(:)%w(3) = buffr
          call h5%read("d",   buffr ); particles(:)%d    = buffr
          call h5%read("s",   buffi ); particles(:)%s    = buffi
          call h5%read("rho", buffr ); particles(:)%rho  = buffr
          call h5%read("Fhx", buffr ); particles(:)%Fh(1)= buffr
          call h5%read("Fhy", buffr ); particles(:)%Fh(2)= buffr
          call h5%read("Fhz", buffr ); particles(:)%Fh(3)= buffr
          call h5%read("Thx", buffr ); particles(:)%Th(1)= buffr
          call h5%read("Thy", buffr ); particles(:)%Th(2)= buffr
          call h5%read("Thz", buffr ); particles(:)%Th(3)= buffr
          call h5%read("Fcx", buffr ); particles(:)%Fc(1)= buffr
          call h5%read("Fcy", buffr ); particles(:)%Fc(2)= buffr
          call h5%read("Fcz", buffr ); particles(:)%Fc(3)= buffr
          call h5%read("Tcx", buffr ); particles(:)%Tc(1)= buffr
          call h5%read("Tcy", buffr ); particles(:)%Tc(2)= buffr
          call h5%read("Tcz", buffr ); particles(:)%Tc(3)= buffr
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%finalize

      ! Clear IB data and reallocate
      call this%ib%resize(0)

      ! Read the associated IB data
      call this%ib%read(iter,time)

      ! Send Lagrangian centroids and markers to their mpiranks
      call this%communicate
      call this%ib%communicate

      ! Localize centroids and markers on the grid
      call this%localize
      call this%ib%localize

      ! Update lookup table
      call this%UpdateLookup

      return
    end subroutine ResPart_set_Read
    subroutine ResPart_set_SetWriteFileName(this,name)
      !> Set the base name of file to write
      implicit none
      class(ResPart_set), intent(inout) :: this            !! Set of resolved particles
      character(len=*),   intent(in)    :: name            !! Name of file
      ! Work variables
      integer :: pos
      character(len=str64):: filename

      filename=name

      ! Figure out the base name, removing any extensions
      pos=scan(trim(adjustl(filename)),".",BACK=.true.)
      if (pos.gt.0) filename=filename(1:pos-1)

      this%write_file    = trim(adjustl(filename))//"_centers.h5"
      this%ib%write_file = trim(adjustl(filename))//"_markers.h5"

      return
    end subroutine ResPart_set_SetWriteFileName
    subroutine ResPart_set_Write(this,iter,time)
      !> description
      implicit none
      class(ResPart_set), intent(inout) :: this            !! Set of resolved particles
      integer,            intent(in)    :: iter            !! Iteration at write
      real(wp),           intent(in)    :: time            !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5
      integer,        allocatable :: buffi(:)
      integer(leapI8),allocatable :: buffi8(:)
      real(wp),       allocatable :: buffr(:)

      ! Nothing to write, if empty
      call this%UpdateCount()
      if (this%count.eq.0) return

      ! Open the file
      if (this%overwrite) then
        call h5%initialize(trim(adjustl(this%write_file)),"W",this%parallel)
      else
        call h5%initialize(trim(adjustl(this%write_file)),"RW",this%parallel)
      end if

      ! Create a new time step
      call h5%NewTimeStep(iter,time)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))

      if (this%count_.eq.0) then
        call h5%write("id",  buffi8)
        call h5%write("x",   buffr )
        call h5%write("y",   buffr )
        call h5%write("z",   buffr )
        call h5%write("i",   buffi )
        call h5%write("j",   buffi )
        call h5%write("k",   buffi )
        call h5%write("u",   buffr )
        call h5%write("v",   buffr )
        call h5%write("w",   buffr )
        call h5%write("wx",  buffr )
        call h5%write("wy",  buffr )
        call h5%write("wz",  buffr )
        call h5%write("d",   buffr )
        call h5%write("s",   buffi )
        call h5%write("rho", buffr )
        call h5%write("Fhx", buffr )
        call h5%write("Fhy", buffr )
        call h5%write("Fhz", buffr )
        call h5%write("Thx", buffr )
        call h5%write("Thy", buffr )
        call h5%write("Thz", buffr )
        call h5%write("Fcx", buffr )
        call h5%write("Fcy", buffr )
        call h5%write("Fcz", buffr )
        call h5%write("Tcx", buffr )
        call h5%write("Tcy", buffr )
        call h5%write("Tcz", buffr )
      else
        ! Write the particle data
        select type (particles=>this%p)
        type is (ResPart_obj)
          buffi8=particles(1:this%count_)%id   ; call h5%write("id",  buffi8)
          buffr =particles(1:this%count_)%p(1) ; call h5%write("x",   buffr )
          buffr =particles(1:this%count_)%p(2) ; call h5%write("y",   buffr )
          buffr =particles(1:this%count_)%p(3) ; call h5%write("z",   buffr )
          buffi =particles(1:this%count_)%c(1) ; call h5%write("i",   buffi )
          buffi =particles(1:this%count_)%c(2) ; call h5%write("j",   buffi )
          buffi =particles(1:this%count_)%c(3) ; call h5%write("k",   buffi )
          buffr =particles(1:this%count_)%v(1) ; call h5%write("u",   buffr )
          buffr =particles(1:this%count_)%v(2) ; call h5%write("v",   buffr )
          buffr =particles(1:this%count_)%v(3) ; call h5%write("w",   buffr )
          buffr =particles(1:this%count_)%w(1) ; call h5%write("wx",  buffr )
          buffr =particles(1:this%count_)%w(2) ; call h5%write("wy",  buffr )
          buffr =particles(1:this%count_)%w(3) ; call h5%write("wz",  buffr )
          buffr =particles(1:this%count_)%d    ; call h5%write("d",   buffr )
          buffi =particles(1:this%count_)%s    ; call h5%write("s",   buffi )
          buffr =particles(1:this%count_)%rho  ; call h5%write("rho", buffr )
          buffr =particles(1:this%count_)%Fh(1); call h5%write("Fhx", buffr )
          buffr =particles(1:this%count_)%Fh(2); call h5%write("Fhy", buffr )
          buffr =particles(1:this%count_)%Fh(3); call h5%write("Fhz", buffr )
          buffr =particles(1:this%count_)%Th(1); call h5%write("Thx", buffr )
          buffr =particles(1:this%count_)%Th(2); call h5%write("Thy", buffr )
          buffr =particles(1:this%count_)%Th(3); call h5%write("Thz", buffr )
          buffr =particles(1:this%count_)%Fc(1); call h5%write("Fcx", buffr )
          buffr =particles(1:this%count_)%Fc(2); call h5%write("Fcy", buffr )
          buffr =particles(1:this%count_)%Fc(3); call h5%write("Fcz", buffr )
          buffr =particles(1:this%count_)%Tc(1); call h5%write("Tcx", buffr )
          buffr =particles(1:this%count_)%Tc(2); call h5%write("Tcy", buffr )
          buffr =particles(1:this%count_)%Tc(3); call h5%write("Tcz", buffr )
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%finalize

      ! Write the associated IB data
      call this%ib%write(iter,time)

      return
    end subroutine ResPart_set_Write
    subroutine ResPart_set_Regroup(this)
      !> Regroup markers with their respective centroids on
      ! the same MPI block
      implicit none
      class(ResPart_set), intent(inout):: this             !! Set of resolved particles
      integer, allocatable :: buf1d(:)
      ! Work variables
      integer :: n
      integer(kind=8):: id

      ! Update this%count
      call this%UpdateCount

      ! Allocate global arrays of centroids MPI ranks
      if (.not.allocated(this%ranks)) allocate(this%ranks(this%count))
      if (size(this%ranks).ne. this%count) then
        deallocate(this%ranks)
        allocate(this%ranks(this%count))
      end if

      allocate(buf1D(this%count))

      ! Set to zero
      this%ranks(:)=0
      ! Update Gloabl ranks
      do n=1,this%count_
        id=this%p(n)%id
        this%ranks(id)=this%GetOwnerRankByBlock(this%p(n))
      end do

      call this%parallel%sum(this%ranks,buf1D); this%ranks=buf1D

      select type (markers=>this%ib%p)
      type is (marker_obj)
        do n=1,this%ib%count_
          ! %s stores global id of centroid, %t stores owning MPI rank
          markers(n)%t=this%ranks(markers(n)%s)
        end do
      end select
      ! Communicate with specified particle locator
      call this%ib%communicate(ResPart_set_GetOwnerRankByRP)

      deallocate(buf1D)
      return
    end subroutine ResPart_set_Regroup
    function ResPart_set_GetOwnerRankByRP(this,marker) result (rank)
      !> Return MPI rank of the lagrangian centroid owning
      ! this marker.
      implicit none
      class(lagrangian_set),intent(inout):: this           !! Set of resolved particles
      class(lagrangian_obj),intent(in)   :: marker         !! A surface marker
      integer :: rank
      select type (marker)
      type is(marker_obj)
        rank=marker%t
      end select
      return
    end function ResPart_set_GetOwnerRankByRP
    subroutine ResPart_set_UpdateLookup(this)
      !> Update lookup array -- The lookup array returns
      ! the local (MPI rank) index of a centroid when
      ! given the global ID of that centroid
      implicit none
      class(ResPart_set),intent(inout):: this              !! Set of resolved particles
      ! Work variables
      integer :: n

      ! Allocate global lookup array
      if (.not.allocated(this%lookup)) allocate(this%lookup(this%count))
      if (size(this%lookup).ne. this%count) then
        deallocate(this%lookup)
        allocate(this%lookup(this%count))
      end if

      ! Update array
      this%lookup(:)=0
      select type (particles =>this%p)
      type is (ResPart_obj)
        do n=1,this%count_
          this%lookup(particles(n)%id)=n
        end do
      end select
      return
    end subroutine ResPart_set_UpdateLookup
    subroutine ResPart_set_SetFilterSize(this,l_filter)
      !> Filter a quantity to the Eulerian grid
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Set of resolved particles
      real(wp),            intent(in)    :: l_filter       !! Filter size
      ! Work variables
      real(wp) :: min_grid_spacing

      this%l_filter   =l_filter
      this%ib%l_filter=l_filter

      ! Get minimum grid_spacing
      min_grid_spacing=minval(this%block%dx)

      ! Adjust stencil size
      this%stib = 1 + 2*ceiling(l_filter/min_grid_spacing)
      this%ib%stib = this%stib

      ! Check there are enough ghost cells
      if (this%block%ngc.lt.this%stib/2) call this%parallel%stop('Insufficient number of ghost cells')

      return
    end subroutine ResPart_set_SetFilterSize
    subroutine ResPart_set_Filter(this,var,field)
      !> Filter a quantity to the Eulerian grid
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Set of resolved particles
      character(len=*),    intent(in)    :: var            !! Variable to compute
      type(eulerian_obj_r),intent(inout) :: field          !! Filtered quantity

      call this%ib%Filter(var,field)

      return
    end subroutine ResPart_set_Filter
    subroutine ResPart_set_SetMPIDataTypeParams(this,types,lengths,displacement)
      !> Set up parameters used when creating the MPI derived type
      use mpi_f08
      implicit none
      class(ResPart_set), intent(inout):: this             !! Set of resolved particles
      type(MPI_Datatype), allocatable, &
                           intent(out) :: types(:)         !! Array of types
      integer, allocatable,intent(out) :: lengths(:)       !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
               allocatable,intent(out) :: displacement(:)  !! Array of displacements
      ! Work variable
      integer :: N
      type(ResPart_obj) :: sample
      integer :: ierr

      associate(mpi=>this%parallel)
        ! Create the MPI structure
        N=19
        allocate(types(N),displacement(N),lengths(N))
        types( 1) = mpi%INT8    ; lengths( 1) = 1                  ! id     --  integer
        types( 2) = mpi%REAL_WP ; lengths( 2) = size(sample%p)     ! p      --  real*3
        types( 3) = mpi%INTEGER ; lengths( 3) = size(sample%c)     ! c      --  integer*3
        types( 4) = mpi%INTEGER ; lengths( 4) = 1                  ! s      --  integer
        types( 5) = mpi%REAL_WP ; lengths( 5) = 1                  ! d      --  real
        types( 6) = mpi%REAL_WP ; lengths( 6) = 1                  ! rho    --  real
        types( 7) = mpi%REAL_WP ; lengths( 7) = size(sample%v)     ! v      --  real*3
        types( 8) = mpi%REAL_WP ; lengths( 8) = size(sample%w)     ! w      --  real*3
        types( 9) = mpi%REAL_WP ; lengths( 9) = size(sample%Fh)    ! Fh     --  real*3
        types(10) = mpi%REAL_WP ; lengths(10) = size(sample%Th)    ! Th     --  real*3
        types(11) = mpi%REAL_WP ; lengths(11) = size(sample%Fc)    ! Fc     --  real*3
        types(12) = mpi%REAL_WP ; lengths(12) = size(sample%Tc)    ! Tc     --  real*3
        types(13) = mpi%REAL_WP ; lengths(13) = size(sample%pold)  ! pold   --  real*3
        types(14) = mpi%REAL_WP ; lengths(14) = size(sample%vold)  ! vold   --  real*3
        types(15) = mpi%REAL_WP ; lengths(15) = size(sample%wold)  ! wold   --  real*3
        types(16) = mpi%REAL_WP ; lengths(16) = size(sample%Fhold) ! Fhold  --  real*3
        types(17) = mpi%REAL_WP ; lengths(17) = size(sample%Thold) ! Thold  --  real*3
        types(18) = mpi%REAL_WP ; lengths(18) = size(sample%Fcold) ! Fcold  --  real*3
        types(19) = mpi%REAL_WP ; lengths(19) = size(sample%Tcold) ! Tcold  --  real*3
        ! Count the displacement for this structure
        call MPI_GET_ADDRESS(sample%id,      displacement( 1),ierr)
        call MPI_GET_ADDRESS(sample%p(1),    displacement( 2),ierr)
        call MPI_GET_ADDRESS(sample%c(1),    displacement( 3),ierr)
        call MPI_GET_ADDRESS(sample%s,       displacement( 4),ierr)
        call MPI_GET_ADDRESS(sample%d,       displacement( 5),ierr)
        call MPI_GET_ADDRESS(sample%rho,     displacement( 6),ierr)
        call MPI_GET_ADDRESS(sample%v(1),    displacement( 7),ierr)
        call MPI_GET_ADDRESS(sample%w(1),    displacement( 8),ierr)
        call MPI_GET_ADDRESS(sample%Fh(1),   displacement( 9),ierr)
        call MPI_GET_ADDRESS(sample%Th(1),   displacement(10),ierr)
        call MPI_GET_ADDRESS(sample%Fc(1),   displacement(11),ierr)
        call MPI_GET_ADDRESS(sample%Tc(1),   displacement(12),ierr)
        call MPI_GET_ADDRESS(sample%pold(1), displacement(13),ierr)
        call MPI_GET_ADDRESS(sample%vold(1), displacement(14),ierr)
        call MPI_GET_ADDRESS(sample%wold(1), displacement(15),ierr)
        call MPI_GET_ADDRESS(sample%Fhold(1),displacement(16),ierr)
        call MPI_GET_ADDRESS(sample%Thold(1),displacement(17),ierr)
        call MPI_GET_ADDRESS(sample%Fcold(1),displacement(18),ierr)
        call MPI_GET_ADDRESS(sample%Tcold(1),displacement(19),ierr)
        displacement = displacement - displacement( 1)
      end associate

      ! Do nothing for now
      return
    end subroutine ResPart_set_SetMPIDataTypeParams
    subroutine ResPart_set_Prepare(this,timer,parser,operators,monitors)
      !> Prepare ResPart_set for use with solvers
      implicit none
      class(ResPart_set),       intent(inout) :: this      !! Collection of Resolved Particles
      type(timer_obj),  target, intent(in)    :: timer     !! Timer utility
      type(parser_obj), target, intent(in)    :: parser    !! Parser for input file
      type(op_obj),     target, intent(in)    :: operators !! Operators object
      type(monitor_set),target, intent(in)    :: monitors  !! Monitors to print to stdout and files
      ! Work variables
      real(wp),parameter:: DefG(3)=[0.0_wp, 0.0_wp, 0.0_wp]
      real(wp):: dl
      integer :: kernel_interp,kernel_extrap
      character(len=str64) :: filename

      ! Associate pointers
      this%timer    => timer
      this%parser   => parser
      this%op       => operators
      this%monitors => monitors

      call this%parser%Get('RP read file',  filename )
      call this%SetReadFileName(filename)

      call this%parser%Get('RP write file', filename )
      call this%SetWriteFileName(filename)

      call this%parser%Get('RP overwrite', this%overwrite, default= .true. )

      ! Filtering parameters
      dl = (2.0_wp - 1.0e3_wp*epsilon(1.0_wp))*minval(this%block%dx)
      call this%parser%Get('RP filter half width',  this%l_filter, default = dl     )
      call this%parser%Get('RP interp kernel',      kernel_interp, default = 1      )
      call this%parser%Get('RP extrap kernel',      kernel_extrap, default = 1      )
      call this%   SetFilterKernel(kernel_interp,kernel_extrap)
      call this%ib%SetFilterKernel(kernel_interp,kernel_extrap)
      call this%   SetFilterSize(this%l_filter)

      ! Other physical parameters
      call this%parser%Get("Gravity",           this%gravity,      default = DefG   )
      call this%parser%Get("Fluid density",     this%rhof                           )

      ! Read resolved particle data
      call this%Read(this%timer%iter,this%timer%time)

      ! Send centroids and markers to their mpiranks
      call this%Communicate
      call this%ib%Communicate

      ! Localize centroids and markers on the grid
      call this%Localize
      call this%ib%Localize

      ! Update lookup table
      call this%UpdateLookup

      return
    end subroutine ResPart_set_Prepare
    subroutine ResPart_set_UpdateSDF(this,SA)
      !> Updates the Surface Density Function
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      type(eulerian_obj_r),intent(inout) :: SA
      ! work variables
      type(eulerian_obj_r):: tmp

      ! Initialize temporary field
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      ! Filter particle surface area
      call this%Filter('SA',tmp)

      ! Add to total SDF
      SA = SA + tmp

      ! Clear data
      call tmp%Finalize()
      return
    end subroutine ResPart_set_UpdateSDF
    subroutine ResPart_set_UpdateNormals(this,ibN)
      !> Updates the Normals field
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      type(eulerian_obj_r),intent(inout) :: ibN(3)
      ! work variables
      type(eulerian_obj_r):: tmp

      ! Initialize temporary field
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      ! Filter and add to total normals field
      call this%Filter('N1',tmp); ibN(1) = ibN(1) + tmp
      call this%Filter('N2',tmp); ibN(2) = ibN(2) + tmp
      call this%Filter('N3',tmp); ibN(3) = ibN(3) + tmp

      ! Clear data
      call tmp%Finalize()
      return
    end subroutine ResPart_set_UpdateNormals
    subroutine ResPart_set_StoreOld(this)
      !> Store values from previous timestep
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      ! Work variables
      integer :: n,m

       select type (particle =>this%p)
       type is (ResPart_obj)
         do n=1,this%count_
           ! Position, linear, and angular velocities
           particle(n)%pold  = particle(n)%p
           particle(n)%vold  = particle(n)%v
           particle(n)%wold  = particle(n)%w

           ! Hydrodynamic and collisional forces
           particle(n)%Fhold = particle(n)%Fh
           particle(n)%Thold = particle(n)%Th
           particle(n)%Fcold = particle(n)%Fc
           particle(n)%Tcold = particle(n)%Tc
         end do
       end select

       select type (markers=>this%ib%p)
       type is (marker_obj)
         do m=1,this%ib%count_
           markers(m)%pold = markers(m)%p
           markers(m)%vold = markers(m)%v
         end do
       end select
       return
    end subroutine ResPart_set_StoreOld
    subroutine ResPart_set_AdvanceCenters(this,dt)
      !> Advance centers to next timestep
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      real(wp),            intent(in)    :: dt             !! Timestep
      ! Work variables
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)
      integer :: n                                         !! iterators
      real(wp):: Mp                                        !! Particle mass
      real(wp):: Ip                                        !! Moment of inertia
      real(wp):: Fh_mid(3),Th_mid(3)                       !! Force and torque at midstep
      real(wp):: Fc_mid(3),Tc_mid(3)                       !! Force and torque at midstep

      select type (particle =>this%p)
      type is (ResPart_obj)
        do n=1,this%count_

          ! Compute mass and moment of inertia
          Mp  = particle(n)%rho*particle(n)%d**3/6.0_wp*Pi
          Ip  = Mp*particle(n)%d**2/10.0_wp

          ! Compute forces and torques at mid-timestep
          Fh_mid = (particle(n)%Fh + particle(n)%Fhold) * 0.5_wp
          Th_mid = (particle(n)%Th + particle(n)%Thold) * 0.5_wp
          Fc_mid = (particle(n)%Fc + particle(n)%Fcold) * 0.5_wp
          Tc_mid = (particle(n)%Tc + particle(n)%Tcold) * 0.5_wp

          ! Update linear velocity
          particle(n)%v = particle(n)%vold + dt*(Fc_mid+Fh_mid)/Mp &
                                           + dt*this%gravity       &
                                           - dt*this%rhof/particle(n)%rho*this%gravity

          ! Update angular velocity
          particle(n)%w = particle(n)%wold + dt*(Tc_mid+Th_mid)/Ip

          ! Update position
          particle(n)%p = particle(n)%pold + dt*0.5_wp*(particle(n)%v+particle(n)%vold)

        end do
      end select

      return
    end subroutine ResPart_set_AdvanceCenters
    subroutine ResPart_set_AdvanceMarkers(this,dt)
      !> Advance markers to next timestep
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      real(wp),            intent(in)    :: dt             !! Timestep
      ! Work variables
      integer        :: n,m
      integer(leapI8):: id
      real(wp)       :: rot_rate
      real(wp)       :: angle
      real(wp)       :: axis(3)
      real(wp)       :: disp(3)
      real(wp)       :: disp_old(3)
      real(wp)       :: a1,a2,a3

      select type (particle =>this%p)
      type is (ResPart_obj)
        select type(markers=>this%ib%p)
        type is (marker_obj)
          do m=1,this%ib%count_
            ! Get ID of parent centroid
            id=markers(m)%s

            ! Get local index of the centroid
            n = this%lookup(id)

            ! prepare rotation coefficients
            rot_rate = norm2(particle(n)%w)
            axis     = particle(n)%w/(rot_rate+epsilon(1.0_wp))
            angle    = dt*rot_rate
            a1       = cos(angle)
            a2       = sin(angle)
            a3       = (1.0_wp-cos(angle))

            ! TO-DO: Fix periodicity

            ! New rotated displacement
            disp_old = markers(m)%pold-particle(n)%pold
            disp = a1*disp_old                        &
                 + a2*cross_product(axis,disp_old)    &
                 + a3*dot_product(disp_old,axis)*axis

            ! Update position and velocity
            markers(m)%p = particle(n)%p + disp
            markers(m)%v = particle(n)%v + cross_product(particle(n)%w,disp)
            ! Update normals
            markers(m)%n = a1*markers(m)%n                        &
                         + a2*cross_product(axis,markers(m)%n)    &
                         + a3*dot_product(markers(m)%n,axis)*axis
          end do
        end select
      end select

      return
    end subroutine ResPart_set_AdvanceMarkers
    subroutine ResPart_set_GetHydroForces(this,P,U,V,W,ibVF,visc)
      !> Compute hydrodynamic force on particle
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      type(Eulerian_obj_r),intent(in)    :: P              !! Fluid pressure field
      type(Eulerian_obj_r),intent(in)    :: U              !! Fluid velocity field in 1-dir
      type(Eulerian_obj_r),intent(in)    :: V              !! Fluid velocity field in 2-dir
      type(Eulerian_obj_r),intent(in)    :: W              !! Fluid velocity field in 3-dir
      type(Eulerian_obj_r),intent(in)    :: ibVF           !! Solid volume fraction
      real(wp),            intent(in)    :: visc           !! Fluid viscosity
      ! work variable
      integer(leapI8):: id
      integer  :: n,m

      call this%GetSurfaceStresses(P,U,V,W,ibVF,visc)

      ! Bring markers that may be in other blocks
      ! to the one containing the centroid
      call this%Regroup()
      call this%UpdateLookup()

      ! Zero foces and torques
      select type (particles =>this%p)
      type is (ResPart_obj)
        do n=1,this%count_
          particles(n)%Fh=0.0_wp
          particles(n)%Th=0.0_wp
        end do
      end select

      ! Compute hydrodynamic forces exerted on particles
      select type (particles =>this%p)
      type is (ResPart_obj)
        select type(markers=>this%ib%p)
        type is (marker_obj)
          do m=1,this%ib%count_
            ! Get ID of parent centroid
            id=markers(m)%s
            ! Get local index of the centroid
            n = this%lookup(id)
            ! Update force applied on the resolved particle
            particles(n)%Fh = particles(n)%Fh + markers(m)%SA*markers(m)%f

            ! TO-DO: Treatment for periodicity

            ! Update torque applied on the resolved particle
            particles(n)%Th = particles(n)%Th + markers(m)%SA*cross_product(markers(m)%p-particles(n)%p,markers(m)%f)
          end do
        end select
      end select

      return
    end subroutine ResPart_set_GetHydroForces
    subroutine ResPart_set_GetSurfaceStresses(this,P,U,V,W,ibVF,visc)
      !> Compute hydrodynamic stresses on markers
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      type(Eulerian_obj_r),intent(in)    :: P              !! Fluid pressure field
      type(Eulerian_obj_r),intent(in)    :: U              !! Fluid velocity field in 1-dir
      type(Eulerian_obj_r),intent(in)    :: V              !! Fluid velocity field in 2-dir
      type(Eulerian_obj_r),intent(in)    :: W              !! Fluid velocity field in 3-dir
      type(Eulerian_obj_r),intent(in)    :: ibVF           !! Solid volume fraction
      real(wp),            intent(in)    :: visc           !! Fluid viscosity
      ! work variable
      type(Eulerian_obj_r) :: sig(6)                       !! Shear Stress tensor
      real(wp) :: F(this%stib,this%stib,this%stib)         !! Stencil data
      integer  :: slo(3), shi(3)                           !! Stencil bounds
      real(wp) :: pm(3)                                    !! Resolved pressure at marker location
      real(wp) :: sigm(6)                                  !! Resolved shear stress at marker location
      real(wp) :: vf                                       !! Fluid volume fraction at marker location
      integer  :: n,m

      ! Compute viscous stress tensor
      sig = this%op%StrainRate(U,V,W)

      do n=1,6
       sig(n) = sig(n)*(2.0_wp*visc)
      end do

      ! Get one-sided stresses at each marker
      select type (markers=>this%ib%p)
      type is (marker_obj)
        do m=1,this%ib%count_
          slo=markers(m)%c-this%stib/2
          shi=markers(m)%c+this%stib/2

          ! Interpolate pressure
          F(:,:,:) = P%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          pm       = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          ! Interpolate shear stress tensor
          do n=1,6
            F(:,:,:) = sig(n)%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
            sigm(n)  = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)
          end do

          ! Interpolate volume fraction
          F(:,:,:) = ibVF%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          vf       = 1.0_wp - markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          markers(m)%f = - vf*markers(m)%f  &
                         - Pm*markers(m)%n  &
                         + [dot_product(markers(m)%n,[sigm(1),sigm(4),sigm(6)]), &
                            dot_product(markers(m)%n,[sigm(4),sigm(2),sigm(5)]), &
                            dot_product(markers(m)%n,[sigm(6),sigm(5),sigm(3)])]
        end do
      end select

      ! TO-DO: replace convolution with trilinear interpolations
      return
    end subroutine ResPart_set_GetSurfaceStresses
    subroutine ResPart_set_GetIBForcing(this,Um,Vm,Wm,rhof,SA,ibF,dt)
      !> Compute the IB forcing
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      type(Eulerian_obj_r),intent(in)    :: Um             !! Velocity in 1-dir
      type(Eulerian_obj_r),intent(in)    :: Vm             !! Velocity in 2-dir
      type(Eulerian_obj_r),intent(in)    :: Wm             !! Velocity in 3-dir
      real(wp),            intent(in)    :: rhof           !! Fluid density
      type(Eulerian_obj_r),intent(in)    :: SA             !! Surface area
      type(Eulerian_obj_r),intent(inout) :: ibF(3)         !! IB forcing
      real(wp),            intent(in)    :: dt             !! Timestep
      ! work variable
      type(eulerian_obj_r):: tmp
      real(wp):: uf(3)
      integer :: shi(3), slo(3)                            !! Stencil extents
      real(wp):: F(this%stib,this%stib,this%stib)          !! Stencil data
      real(wp):: SDF
      integer :: m

      select type (markers=>this%ib%p)
      type is (marker_obj)
        do m=1,this%ib%count_
          slo=markers(m)%c-this%stib/2
          shi=markers(m)%c+this%stib/2

          ! Interpolate velocities
          F(:,:,:) = Um%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(1)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          F(:,:,:) = Vm%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(2)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          F(:,:,:) = Wm%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(3)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          ! Interpolate SDF
          F(:,:,:) = SA%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          SDF      = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          markers(m)%f = (1.0_wp/SDF)*rhof*(markers(m)%v-uf)/dt
        end do
      end select

      ! Compute forcing on mid points
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      call this%Filter('F1',tmp); ibF(1) = tmp + ibF(1)
      call this%Filter('F2',tmp); ibF(2) = tmp + ibF(2)
      call this%Filter('F3',tmp); ibF(3) = tmp + ibF(3)

      call tmp%Finalize()

      return
    end subroutine ResPart_set_GetIBForcing
    subroutine ResPart_set_CreateMonitor(this)
      !> Create monitor file for Resolved Particles
      implicit none
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles

      call this%monitors%create('ResPart',10,stdout=.false.      )
      call this%monitors%set   ('ResPart', 1,label='Iteration'   )
      call this%monitors%set   ('ResPart', 2,label='Time'        )
      call this%monitors%set   ('ResPart', 3,label='NbrCenters'  )
      call this%monitors%set   ('ResPart', 4,label='NbrMarkers'  )
      call this%monitors%set   ('ResPart', 5,label='avg|V1|'     )
      call this%monitors%set   ('ResPart', 6,label='avg|V2|'     )
      call this%monitors%set   ('ResPart', 7,label='avg|V3|'     )
      call this%monitors%set   ('ResPart', 8,label='avg|Fh1|'    )
      call this%monitors%set   ('ResPart', 9,label='avg|Fh2|'    )
      call this%monitors%set   ('ResPart',10,label='avg|Fh3|'    )

      return
    end subroutine ResPart_set_CreateMonitor
    subroutine ResPart_set_UpdateMonitor(this)
      class(ResPart_set),  intent(inout) :: this           !! Collection of Resolved Particles
      ! Work variables
      integer :: n
      real(wp):: Vp(3)
      real(wp):: Fh(3)
      real(wp):: buff(3)

      Vp = 0.0_wp
      Fh = 0.0_wp
      select type (centroid => this%p)
      type is (ResPart_obj)
        do n=1,this%count_
          ! Velocity
          Vp = Vp + centroid(n)%v
          ! Hydrodynamic force
          Fh = Fh + centroid(n)%Fh
        end do
      end select

      call this%parallel%sum(Vp, buff); Vp = buff/this%count
      call this%parallel%sum(Fh, buff); Fh = buff/this%count

      call this%monitors%set('ResPart', 1,this%timer%iter)
      call this%monitors%set('ResPart', 2,this%timer%time)
      call this%monitors%set('ResPart', 3,this%count     )
      call this%monitors%set('ResPart', 4,this%ib%count  )
      call this%monitors%set('ResPart', 5,Vp(1)          )
      call this%monitors%set('ResPart', 6,Vp(2)          )
      call this%monitors%set('ResPart', 7,Vp(3)          )
      call this%monitors%set('ResPart', 8,Fh(1)          )
      call this%monitors%set('ResPart', 9,Fh(2)          )
      call this%monitors%set('ResPart',10,Fh(3)          )
      return
    end subroutine ResPart_set_UpdateMonitor
    pure function cross_product(u,v) result(val)
      real(wp), intent(in) :: u(3)
      real(wp), intent(in) :: v(3)
      real(wp):: val(3)
      val(1) = u(2)*v(3)-u(3)*v(2)
      val(2) =-u(1)*v(3)+u(3)*v(1)
      val(3) = u(1)*v(2)-v(1)*u(2)
      return
    end function cross_product
end module particles_resolved
