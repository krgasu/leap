!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans) grans_WriteRestartData_smod
  contains
    module subroutine grans_obj_WriteRestartData(this)
      !> Write simulation data to disk required for restarts
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver

      ! Write Lagrangian data
      if (this%use_IB) call this%IB%write(this%timer%iter,this%timer%time)
      if (this%use_RP) call this%RP%write(this%timer%iter,this%timer%time)
      if (this%use_PP) call this%PP%write(this%timer%iter,this%timer%time)

      ! Write Eulerian data
      call this%fields%write(this%timer%iter,this%timer%time)
      return
    end subroutine grans_obj_WriteRestartData
end submodule grans_WriteRestartData_smod
