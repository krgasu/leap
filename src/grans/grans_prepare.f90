!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans) grans_PrepareSolver_smod
  contains
    module subroutine grans_obj_PrepareSolver(this)
      !> Prepare data for run
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver

      ! Set solver parameters
      call this%parser%get("Gravity",        this%gravity,      default = [0.0_wp, 0.0_wp, 0.0_wp])
      call this%parser%get("Subiterations",  this%subit_max,    default = 40)
      call this%parser%get("Convergence",    this%rel_error_max,default = 1.0e-6_wp)

      call this%parser%get('Use IB',         this%use_IB,  .false. )
      call this%parser%get('Use RP',         this%use_RP,  .false. )
      call this%parser%get('Use PP',         this%use_PP,  .false. )
      call this%parser%get('Use collisions', this%use_col, .false.)

      call grans_obj_PrepareSolver_block(this)
      call grans_obj_PrepareSolver_operators(this)
      call grans_obj_PrepareSolver_fields(this)

      if (this%use_IB)  call grans_obj_PrepareSolver_IB(this)
      if (this%use_RP)  call grans_obj_PrepareSolver_RP(this)
      if (this%use_PP)  call grans_obj_PrepareSolver_PP(this)
      if (this%use_col) call grans_obj_PrepareSolver_collision(this)

      call grans_obj_PrepareSolver_monitor(this)
      call grans_obj_PrepareSolver_output(this)

      ! Monitor and output initial conditions
      call this%monitor()
      call this%WriteOutputData()

      ! DEBUGGING
      call this%WriteRestartData()
      return
    end subroutine grans_obj_PrepareSolver

    subroutine grans_obj_PrepareSolver_IB(this)
      !> Prepare Immersed Boundary data before run
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver
      ! Work variables
      integer :: kernel_interp,kernel_extrap

      call this%IB%initialize('IB',this%block,this%parallel)
      call this%parser%get('IB read file',  this%IB%read_file         )
      call this%parser%get('IB write file', this%IB%write_file        )
      call this%parser%get('IB overwrite',  this%IB%overwrite, .true. )

      ! Read IB data
      call this%IB%read(this%timer%iter,this%timer%time)

      ! Send markers to their mpranks
      call this%IB%communicate

      ! Localize markers on the block
      call this%IB%localize

      ! Filtering parameters
      call this%parser%get('IB filter width',  this%IB%l_filter, 2.0_wp*this%block%dx(1))
      call this%parser%get('IB interp kernel', kernel_interp,    1               )
      call this%parser%get('IB extrap kernel', kernel_extrap,    1               )

      call this%IB%SetFilterSize(this%IB%l_filter)
      call this%IB%SetFilterKernel(kernel_interp,kernel_extrap)

      call this%IB%ComputeSolidVolFrac(this%ibVF,'IJ-AMG-NONE',1.0e-10_wp,30)

      return
    end subroutine grans_obj_PrepareSolver_IB
    subroutine grans_obj_PrepareSolver_RP(this)
      !> Prepare Resolved Particle data before run
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver
      ! Work variables
      integer :: pos
      integer :: kernel_interp,kernel_extrap

      ! Add specific Eulerian fields
      call this%fields%add('SA',  0,this%SA   )

      ! Initialize Resolved Particles
      call this%RP%initialize('rp',this%block,this%parallel)
      call this%parser%get('RP read file',  this%RP%read_file         )
      call this%parser%get('RP write file', this%RP%write_file        )
      call this%parser%get('RP overwrite',  this%RP%overwrite, .true. )

      ! Fix file names
      pos=scan(trim(adjustl(this%RP%read_file)),".",BACK=.true.)
      if (pos.gt.0) this%RP%read_file=this%RP%read_file(1:pos-1)
      this%RP%ib%read_file =trim(adjustl(this%RP%read_file))//"_markers.h5"
      this%RP%read_file    =trim(adjustl(this%RP%read_file))//".h5"

      pos=scan(trim(adjustl(this%RP%write_file)),".",BACK=.true.)
      if (pos.gt.0) this%RP%write_file=this%RP%write_file(1:pos-1)
      this%RP%ib%write_file=trim(adjustl(this%RP%write_file))//"_markers.h5"
      this%RP%write_file   =trim(adjustl(this%RP%write_file))//".h5"

      ! Read resolved particle data
      call this%RP%read(this%timer%iter,this%timer%time)

      ! Send centroids and markers to their mpranks
      call this%RP%communicate
      call this%RP%ib%communicate

      ! Localize centroids and markers on the grid
      call this%RP%localize
      call this%RP%ib%localize

      ! Update lookup table
      call this%RP%UpdateLookup

      ! Filtering parameters
      call this%parser%get('RP filter width',  this%RP%l_filter, this%block%dx(1))
      call this%parser%get('RP interp kernel', kernel_interp,    1               )
      call this%parser%get('RP extrap kernel', kernel_extrap,    1               )
      call this%RP%SetFilterKernel(kernel_interp,kernel_extrap)
      call this%RP%SetFilterSize(this%RP%l_filter)

      ! Filter initial fields
      call this%RP%filter('SA',this%SA)

      return
    end subroutine grans_obj_PrepareSolver_RP
    subroutine grans_obj_PrepareSolver_PP(this)
      !> Prepare Point Particle data before run
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver
      ! Work variables
      character(len=str64):: part_type
      integer :: kernel_interp,kernel_extrap

      ! Initialize Resolved Particles
      call this%parser%get("Particle type",    part_type)
      call this%PP%initialize('pp',this%block,this%parallel,part_type)

      call this%parser%get('PP read file',  this%PP%read_file         )
      call this%parser%get('PP write file', this%PP%write_file        )
      call this%parser%get('PP overwrite',  this%PP%overwrite, .true. )

      ! Read point particle data
      call this%PP%read(this%timer%iter,this%timer%time)

      ! Send particles to their mpranks
      call this%PP%communicate

      ! Localize particles on the block
      call this%PP%localize

      ! Filtering parameters
      call this%parser%get('PP filter width',  this%PP%l_filter, this%block%dx(1))
      call this%parser%get('PP interp kernel', kernel_interp,    1               )
      call this%parser%get('PP extrap kernel', kernel_extrap,    1               )
      call this%PP%SetFilterSize(this%PP%l_filter)
      call this%PP%SetFilterKernel(kernel_interp,kernel_extrap)

      return
    end subroutine grans_obj_PrepareSolver_PP
    subroutine grans_obj_PrepareSolver_block(this)
      !> Initialize and partition grid
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), Nb(3), ngc, ilo(3),ihi(3)
      logical :: periods(3)

      ! Get block information from input
      call this%parser%get("Domain size",  L )
      call this%parser%get("Grid points",  N )
      call this%parser%get("Partition",    Nb,      default = [1,1,1])
      call this%parser%get("Ghost cells",  ngc,     default = 1)
      call this%parser%get("Periodicity",  periods, default = [.false.,.false.,.false.])
      xlo=-0.5_wp*L; xhi= 0.5_wp*L
      ilo=[1,1,1];   ihi=N

      ! Initialize and partition blocks
      call this%block%initialize(xlo,xhi,ilo,ihi,ngc,this%parallel)
      call this%block%SetPeriodicity(periods)
      call this%block%partition(Nb)
      return
    end subroutine grans_obj_PrepareSolver_block
    subroutine grans_obj_PrepareSolver_operators(this)
      !> Initialize differential operators on
      ! current grid.
      use leapDiffOp
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      type(op_obj)     :: op
      integer          :: scheme_order
      character(str64) :: hypre_solver
      integer          :: hypre_max_it
      real(wp)         :: hypre_res_tol

      ! Get information from input
      call this%parser%Get("Scheme order",  scheme_order, default = 2)
      call op%initialize(this%block,this%parallel,order=scheme_order)

      ! Initialize HYPRE
      call this%parser%Get('VF solver',  hypre_solver , default="IJ-AMG-NONE")
      call this%parser%Get('VF max tol', hypre_res_tol, default=1.0e-10_wp   )
      call this%parser%Get('VF max it',  hypre_max_it , default=30           )

      call this%hypre%Initialize(this%block,this%parallel)
      call this%hypre%SelectSolver(hypre_solver,MaxTol=hypre_res_tol,MaxIt=hypre_max_it)

      ! Use differential operators to build the laplacian inside HYPRE
      call op%BuildLaplacian(this%hypre%mat,this%hypre%stm)

      ! Setup HYPRE system of equations
      call this%hypre%Setup()

      return
    end subroutine grans_obj_PrepareSolver_operators
    subroutine grans_obj_PrepareSolver_fields(this)
      !> Initialize fields used by this solver
      ! and read initial conditions.
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver

      call this%fields%initialize(this%block,this%parallel)

      call this%parser%get('Fields write file',  this%fields%write_file)
      call this%parser%get('Fields overwrite ',  this%fields%overwrite,.true.)

      ! Add each field to main container.
      ! This will allocate the underlying arrays
      ! and facilitate IO.
      call this%fields%add('Fpx', 1,this%Fp(1))
      call this%fields%add('Fpy', 2,this%Fp(2))
      call this%fields%add('Fpz', 3,this%Fp(3))
      call this%fields%add('ibVF',0,this%ibVF )
      call this%fields%add('PVF', 0,this%PVF  )

      ! Initialize particle and IB volume fractions
      this%PVF%cell  = 0.0_wp
      this%ibVF%cell = 0.0_wp

      return
    end subroutine grans_obj_PrepareSolver_fields
    subroutine grans_obj_PrepareSolver_monitor(this)
      !> Initialize monitors used by this solver.
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver

      ! Initialize monitors
      call this%monitors%initialize(this%parallel)

      ! Main monitor: writes to stdout
      call this%monitors%create('stdout',8,stdout=.true.       )
      call this%monitors%set   ('stdout',1,label='Iteration'   )
      call this%monitors%set   ('stdout',2,label='Time'        )
      call this%monitors%set   ('stdout',3,label='# particles' )
      call this%monitors%set   ('stdout',4,label='Subiter'     )
      call this%monitors%set   ('stdout',5,label='max CFL'     )
      call this%monitors%set   ('stdout',6,label='max|Vx|'     )
      call this%monitors%set   ('stdout',7,label='max|Vy|'     )
      call this%monitors%set   ('stdout',8,label='max|Vz|'     )

      ! Initialize additional monitors
      call this%monitors%create('hypre', 5,stdout=.false.      )
      call this%monitors%set   ('hypre', 1,label='Iteration'   )
      call this%monitors%set   ('hypre', 2,label='Time'        )
      call this%monitors%set   ('hypre', 3,label='Sub-iterions')
      call this%monitors%set   ('hypre', 4,label='Rel. error'  )
      call this%monitors%set   ('hypre', 5,label='intRHS'      )

      ! Write headers
      call this%monitors%print(print_labels=.true.)
      return
    end subroutine grans_obj_PrepareSolver_monitor
    subroutine grans_obj_PrepareSolver_output(this)
      !> Initialize output variables
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      character(len=str256) :: str
      integer :: count,n

      if (.not.this%parser%IsDefined("Output data")) return

      ! Count number of output
      call this%parser%get("Output data",  str )
      count = 0
      n = 1
      do while (n.ne.0)
        count = count + 1
        str=adjustl(str(n:len_trim(str)))
        n = scan(trim(str),' ')
      end do

      ! Read the outputs
      allocate(this%outputs(count))
      call this%parser%get("Output data",  this%outputs)

      ! Get names of fields to output
      ! Note: field names must match the actual names
      ! given in cdifs_obj_PrepareSolver_fields
      this%output_var = [character(str8)::]
      do n=1,size(this%outputs)
       select case (this%outputs(n))
       case ('Fp')
         this%output_var = [character(str8) :: this%output_var(:), 'Fpx', 'Fpy', 'Fpz']
       case ('ibVF')
         this%output_var = [character(str8) :: this%output_var(:), 'ibVF']
       case ('PVF')
         this%output_var = [character(str8) :: this%output_var(:), 'PVF']
       case default
         call this%parallel%stop("Unknown output "//this%outputs(n))
       end select
      end do

      return
    end subroutine grans_obj_PrepareSolver_output
    subroutine grans_obj_PrepareSolver_collision(this)
      !> Initialize collision components
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      real(wp):: ds,ds_default
      real(wp):: buf
      integer :: n

      ! Determine spacing for collision blocks
      ! - Default value
      ds_default = minval(this%block%dx)
      if (this%use_RP) then
        select type(center => this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            ds_default = max(ds_default,center(n)%d)
          end do
        end select
      end if
      if (this%use_PP) then
        select type(particle => this%PP%p)
        type is (particle_obj)
          do n=1,this%PP%count_
            ds_default = max(ds_default,particle(n)%d)
          end do
        end select
      end if
      call this%parallel%max(ds_default, buf); ds_default=buf
      ! - Parser value
      call this%parser%get('Collision grid spacing', ds,ds_default)

      if (this%use_IB) call this%IB%SetupCollisionBlock(ds, 1)
      if (this%use_RP) call this%RP%SetupCollisionBlock(ds, 1)
      if (this%use_PP) call this%PP%SetupCollisionBlock(ds, 1)

      call this%parser%get('Collision time',              this%tcol)
      call this%parser%get('Dry normal restitution coef', this%edry)
      call this%parser%get('Coulomb friction coef',       this%muc)

      ! Make sure the dt is small enough to properly resolve collisions
      this%timer%dt =min(this%timer%dt,this%tcol/8.0_wp)

      return
    end subroutine grans_obj_PrepareSolver_collision
end submodule grans_PrepareSolver_smod
