!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans) grans_module_smod
  contains
    module subroutine grans_obj_monitor(this)
      !> Compute and write monitored values
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver
      ! Work variables
      real(wp) :: maxCFL
      real(wp) :: maxV(3)
      integer  :: nbr
      real(wp) :: buf
      integer  :: n

      maxV   = 0.0_wp
      maxCFL = 0.0_wp
      nbr    = 0
      if (this%use_RP) then
        select type (center =>this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            maxCFL = max(maxCFL,this%timer%dt*norm2(center(n)%v)/center(n)%d)
            maxV(1)= max(maxV(1),abs(center(n)%v(1)))
            maxV(2)= max(maxV(2),abs(center(n)%v(2)))
            maxV(3)= max(maxV(3),abs(center(n)%v(3)))
          end do
        end select

        nbr = nbr + this%RP%count
      end if
      if (this%use_PP) then
        select type (particle =>this%PP%p)
        class is (particle_obj)
          do n=1,this%PP%count_
            maxCFL = max(maxCFL,this%timer%dt*norm2(particle(n)%v)/particle(n)%d)
            maxV(1)= max(maxV(1),abs(particle(n)%v(1)))
            maxV(2)= max(maxV(2),abs(particle(n)%v(2)))
            maxV(3)= max(maxV(3),abs(particle(n)%v(3)))
          end do
         end select

        nbr = nbr + this%PP%count
      end if

      call this%parallel%max(maxCFL, buf) ; maxCFL =buf
      call this%parallel%max(maxV(1),buf) ; maxV(1)=buf
      call this%parallel%max(maxV(2),buf) ; maxV(2)=buf
      call this%parallel%max(maxV(3),buf) ; maxV(3)=buf

      call this%monitors%set('stdout',1,this%timer%iter)
      call this%monitors%set('stdout',2,this%timer%time)
      call this%monitors%set('stdout',3,nbr            )
      call this%monitors%set('stdout',4,this%subit     )
      call this%monitors%set('stdout',5,maxCFL         )
      call this%monitors%set('stdout',6,maxV(1)        )
      call this%monitors%set('stdout',7,maxV(2)        )
      call this%monitors%set('stdout',8,maxV(3)        )

      call this%monitors%set('hypre', 1,this%timer%iter)
      call this%monitors%set('hypre', 2,this%timer%time)
      call this%monitors%set('hypre', 3,this%VF_it     )
      call this%monitors%set('hypre', 4,this%VF_rel    )
      call this%monitors%set('hypre', 5,this%intRHS    )
      call this%monitors%print()

      return
    end subroutine grans_obj_monitor
end submodule grans_module_smod
