!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module grans_cases
  !>--------------------------------------------------------------------------
  ! Module: grans_cases
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Simulation cases for GranS (Granular flow solver)
  !
  ! Note: Each new case must have:
  !    1) an interface to a module subroutine where block and other
  ! simulation data are defined.
  !    2) An implementation of this module subroutine in a submodule
  ! added in the src/cases folder.
  !    3) name of the case, and call to the module subroutine added
  ! to the subroutine case_obj_setup
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapEulerian
  use leapCases
  use particles_resolved
  use particles_point
  use immersed_boundaries
  implicit none

  private
  public :: grans_case_obj

  type,extends(case_obj) :: grans_case_obj
    !> Simulation case manager for the GRANS solvere
    contains
      procedure :: setup => case_obj_setup
  end type grans_case_obj

  interface
    module subroutine grans_homogeneous_cooling(this)
      !> Homogeneous Cooling
      implicit none
      class(grans_case_obj), intent(inout) :: this
    end subroutine grans_homogeneous_cooling
    module subroutine grans_headon_collision(this)
      !> Head-on Collision
      implicit none
      class(grans_case_obj), intent(inout) :: this
    end subroutine grans_headon_collision
    module subroutine grans_rotating_drum(this)
      !> Granular particles in a rotating drum.
      implicit none
      class(grans_case_obj), intent(inout) :: this
    end subroutine grans_rotating_drum
  end interface

  contains
    subroutine case_obj_setup(this)
      !> Calls appropriate case
      implicit none
      class(grans_case_obj), intent(inout) :: this

      select case (this%name)
      case ("HOMOGENEOUS COOLING")
        call grans_homogeneous_cooling(this)
      case ("HEADON COLLISION")
        call grans_headon_collision(this)
      case ("ROTATING DRUM")
        call grans_rotating_drum(this)
      case default
        call this%parallel%stop("GRANS: Unknown case")
      end select
      return
    end subroutine case_obj_setup
end module grans_cases
