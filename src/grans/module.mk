MODULE = grans
FILES = grans_cases.f90 grans.f90 grans_prepare.f90 grans_advance.f90 grans_monitor.f90 \
		grans_restart.f90 grans_output.f90
SRC += $(patsubst %, $(MODULE)/%, $(FILES))
