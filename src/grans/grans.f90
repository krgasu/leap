!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module grans
  !>--------------------------------------------------------------------------
  ! Module: GRANS
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Granular flow solver with Point and/or Resolved Particles
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapBlock
  use leapEulerian
  use leapHypre
  use leapSolver
  use leapTimer
  use leapMonitor
  use particles_resolved
  use particles_point
  use grans_cases
  use immersed_boundaries
  implicit none

  private
  public :: grans_obj

  type, extends(solver_obj):: grans_obj
    !> Granular flow solver with resolved particles
    !> Simulation case
    type(grans_case_obj):: case                            !! Case to run
    !> Geometry
    type(block_obj)     :: block                           !! Block information
    !> Mathematical tools
    type(hypre_obj)     :: hypre                           !! HYPRE Solvers
    !> Core data
    type(particle_set)  :: PP                              !! Point particles
    type(ResPart_set)   :: RP                              !! Resolved particles
    type(marker_set)    :: IB                              !! Immersed solids (walls)
    type(Eulerian_set)  :: fields                          !! Eulerian data container
    type(Eulerian_obj_r):: ibVF                            !! Solid volume fraction (walls)
    type(Eulerian_obj_r):: PVF                             !! Particle volume fraction
    type(Eulerian_obj_r):: SA                              !! Surface Area Density
    type(Eulerian_obj_r):: Fp(3)                           !! Particle Eulerian forcing
    !> Physical parameters
    real(wp)            :: gravity(3)=0.0_wp               !! Gravity
    !> Solver parameters
    integer             :: subit=0                         !! Solver sub-iteration
    integer             :: subit_max=40                    !! Maximum sub-iterations
    real(wp)            :: rel_error_max=1.0e-6_wp         !! Maximum relative error
    logical             :: use_col=.false.                 !! Collision parameter
    logical             :: use_RP=.false.                  !! Use Resolved Particles
    logical             :: use_PP=.false.                  !! Use Point Particles
    logical             :: use_IB=.false.                  !! Use Immersed Boundaries

    real(wp)            :: tcol                            !! Collision time
    real(wp)            :: edry                            !! Dry restitution coefficient
    real(wp)            :: muc                             !! Coulomb friction factor
    !> Output parmeters
    type(monitor_set)   :: monitors                        !! Monitors to print to stdout and files
    character(len=str8), &
            allocatable :: outputs(:)                      !! List of outputs
                                                           !! (one output can contain multiple variables)
    character(len=str8), &
            allocatable :: output_var(:)                   !! Names of variables to output
    !> Volume fraction solver
    character(len=:),allocatable  &
                        :: VF_solver                       !! Name of VF solver to use
    real(wp)            :: VF_MaxTol                       !! VF maximum relative tolerance
    integer             :: VF_MaxIT                        !! VF iterations
    integer             :: VF_it                           !! Sub-iterations for VF solves
    real(wp)            :: VF_rel                          !! Relative error at end of VF solves
    real(wp)            :: intRHS                          !! Magnitude of RHS in VF equation
    contains
      procedure :: Initialize           => grans_obj_Init
      procedure :: Finalize             => grans_obj_Final
      procedure :: SetInitialConditions => grans_obj_SetInitialConditions
      procedure :: PrepareSolver        => grans_obj_PrepareSolver
      procedure :: AdvanceSolution      => grans_obj_AdvanceSolution
      procedure :: Monitor              => grans_obj_Monitor
      procedure :: WriteRestartData     => grans_obj_WriteRestartData
      procedure :: WriteOutputData      => grans_obj_WriteOutputData
  end type grans_obj

  interface
    module subroutine grans_obj_PrepareSolver(this)
      !> Prepare data before run
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
    end subroutine grans_obj_PrepareSolver
    module subroutine grans_obj_AdvanceSolution(this)
      !> Advances solver from n to n+1
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
    end subroutine grans_obj_AdvanceSolution
    module subroutine grans_obj_Monitor(this)
      !> Analyze data and post to monitors
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
    end subroutine grans_obj_Monitor
    module subroutine grans_obj_WriteRestartData(this)
      !> Write restart data to disk
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
    end subroutine grans_obj_WriteRestartData
    module subroutine grans_obj_WriteOutputData(this)
      !> Process single-precision data for visualization
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
    end subroutine grans_obj_WriteOutputData
  end interface

  contains
    subroutine grans_obj_Init(this,timer,parallel,parser)
      !> Initialize the solver
      implicit none
      class(grans_obj),         intent(inout) :: this      !! The solver
      type(timer_obj),   target,intent(in)    :: timer     !! Timer utility
      type(parallel_obj),target,intent(in)    :: parallel  !! Parallel machinery
      type(parser_obj),  target,intent(in)    :: parser    !! Parser for input file

      ! Point to the master objects
      this%timer    => timer
      this%parallel => parallel
      this%parser   => parser

      return
    end subroutine grans_obj_Init
    subroutine grans_obj_SetInitialConditions(this)
      !> Set initial conditions at n=0
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver

      ! Initialize case
      call this%case%initialize(this%block,this%parallel,this%parser)
      ! Get case name
      call this%parser%get("Case", this%case%name)
      ! Setup the initial conditions
      call this%case%setup()
      ! Finalize case
      call this%case%finalize
      ! Finish run
      call this%timer%EndRun()
      return
    end subroutine grans_obj_SetInitialConditions
    subroutine grans_obj_Final(this)
      !> Deallocate data
      implicit none
      class(grans_obj), intent(inout) :: this             !! The solver

      this%timer    => null()
      this%parallel => null()
      this%parser   => null()

      if (allocated(this%outputs))    deallocate(this%outputs)
      if (allocated(this%output_var)) deallocate(this%output_var)

      call this%RP      %finalize()
      call this%PP      %finalize()
      call this%fields  %finalize()
      call this%fields  %finalize()
      call this%block   %finalize()
      call this%monitors%finalize()
      return
    end subroutine grans_obj_Final
end module grans
