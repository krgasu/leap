!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans) grans_AdvanceSolution_smod
  contains
    module subroutine grans_obj_AdvanceSolution(this)
      !> Update solution from n to n+1
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer(kind=8) :: id                                !! Particle ID
      integer         :: n,m                               !! Centroid and marker iterators
      real(wp)        :: pold(3)                           !! Old position
      real(wp)        :: Mp,Ip                             !! Mass and moment of intertia
      real(wp)        :: rel_error                         !! Relative error
      real(wp)        :: buf                               !! Buffer for parallel communication
      logical         :: done
      ! For displacement
      real(wp) :: rot_rate,angle
      real(wp) :: a1,a2,a3
      real(wp) :: axis(3)
      real(wp) :: disp(3), disp_old(3)
      real(wp), parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Start by saving solution from previous step
      call grans_obj_AdvanceSolution_StoreOld(this)

      ! Bring markers that may be in other blocks
      ! to the one containing the centroid
      if (this%use_RP) then
        call this%RP%regroup()
        call this%RP%UpdateLookup()
      end if

      ! Compute hydrodynamic forces and those
      ! other than collision
      call grans_obj_AdvanceSolution_ComputeHydrodynamicForces(this)

      associate (                                       &
        subit=>this%subit, subit_max=>this%subit_max,   &
        rel_error_max => this%rel_error_max,            &
        dt=>this%timer%dt)

        subit = 0; rel_error=0.0_wp; done=.false.
        do while (.not.done)
          ! Increment and zero relative error
          subit     = subit + 1
          rel_error = 0.0_wp

          ! Update collisin forces
          call grans_obj_AdvanceSolution_ComputeCollisionForces(this)

          ! Update centroid positons and velocities
          if (this%use_RP) then
            select type (center =>this%RP%p)
            type is (ResPart_obj)
              do n=1,this%RP%count_
                ! Store old position
                pold=center(n)%p

                ! Compute mass and moment of inertia
                Mp  = center(n)%rho*center(n)%d**3/6.0_wp*Pi
                Ip  = Mp*center(n)%d**2/10.0_wp

                ! Update momentum: add collision and hydrodynamic forces
                center(n)%v = center(n)%vold + dt*(center(n)%Fc+center(n)%Fh)/Mp

                ! Update angular velocity: add collision and hydrodynamic torques
                center(n)%w = center(n)%wold + dt*(center(n)%Tc+center(n)%Th)/Ip

                ! Update position
                center(n)%p = center(n)%pold + dt*center(n)%v

                ! Compute relative displacement
                rel_error = max(rel_error,norm2(center(n)%p-pold)/center(n)%d)
              end do
            end select
          end if
          if (this%use_PP) then
            select type (particle =>this%PP%p)
            class is (particle_obj)
              do n=1,this%PP%count_
                ! Store old position
                pold=particle(n)%p

                ! Compute mass and moment of inertia
                Mp  = particle(n)%rho*particle(n)%d**3/6.0_wp*Pi
                Ip  = Mp*particle(n)%d**2/10.0_wp

                ! Update momentum: add collision and hydrodynamic forces
                particle(n)%v = particle(n)%vold + dt*(particle(n)%Fc+particle(n)%Fh)/Mp

                ! Update angular velocity: add collision and hydrodynamic torques
                particle(n)%w = particle(n)%wold + dt*(particle(n)%Tc+particle(n)%Th)/Ip

                ! Update position
                particle(n)%p = particle(n)%pold + dt*particle(n)%v

                ! Compute relative displacement
                rel_error= max(rel_error,norm2(particle(n)%p-pold)/particle(n)%d)
              end do
            end select
          end if

          ! Synchronize across MPI ranks
          call this%parallel%max(rel_error,buf); rel_error=buf

          ! Exit condition
          if ((subit.eq.subit_max).or.rel_error.lt.rel_error_max) done=.true.
        end do

        ! Update marker positions and velocities
        if (this%use_RP) then
          select type (center =>this%RP%p)
          type is (ResPart_obj)
            select type(marker=>this%RP%ib%p)
            type is (marker_obj)
              do m=1,this%RP%ib%count_
                ! Get ID of parent centroid
                id=marker(m)%s
                ! Get local index of the centroid
                n = this%RP%lookup(id)

                ! prepare rotation coefficients
                rot_rate = norm2(center(n)%w)
                axis     = center(n)%w/(rot_rate+epsilon(1.0_wp))
                angle    = dt*rot_rate
                a1 = cos(angle)
                a2 = sin(angle)
                a3 = (1.0_wp-cos(angle))

                ! New rotated displacement
                disp_old = marker(m)%pold-center(n)%pold
                disp = a1*disp_old                        &
                     + a2*cross_product(axis,disp_old)    &
                     + a3*dot_product(disp_old,axis)*axis

                ! Update position and velocity
                marker(m)%p = center(n)%p + disp
                marker(m)%v = center(n)%v + cross_product(center(n)%w,disp)

                ! Update normals
                marker(m)%n = a1*marker(m)%n                        &
                            + a2*cross_product(axis,marker(m)%n)    &
                            + a3*dot_product(marker(m)%n,axis)*axis
              end do
            end select
          end select
        end if

        ! Apply Periodicity
        if (this%use_RP) then
          call this%RP%ApplyPeriodicity()
          call this%RP%ib%ApplyPeriodicity()
        end if
        if (this%use_PP) call this%PP%ApplyPeriodicity() 

        ! Communicate
        if (this%use_RP) then
          call this%RP%communicate()
          call this%RP%ib%communicate()
          call this%RP%localize()
          call this%RP%ib%localize()
        end if

        if (this%use_PP) then
          call this%PP%communicate()
          call this%PP%localize()
        end if

      end associate

      return
    end subroutine grans_obj_AdvanceSolution
    subroutine grans_obj_AdvanceSolution_StoreOld(this)
      !> Store old values
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer :: n,m

      if (this%use_RP) then
        select type (center =>this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            center(n)%pold = center(n)%p
            center(n)%vold = center(n)%v
            center(n)%Fcold= center(n)%Fc
            center(n)%Tcold= center(n)%Tc
            center(n)%Fhold= center(n)%Fh
            center(n)%Thold= center(n)%Th
          end do
        end select

        select type (markers=>this%RP%ib%p)
        type is (marker_obj)
          do m=1,this%RP%ib%count_
            markers(m)%pold= markers(m)%p
            markers(m)%vold= markers(m)%v
          end do
        end select

          ! Bring markers that may be in other blocks
          ! to the one containing the centroid
          call this%RP%regroup()

          ! Update lookup table
          call this%RP%UpdateLookup
      end if

      if (this%use_PP) then
        select type (particle =>this%PP%p)
        type is (particle_obj)
          do n=1,this%PP%count_
            particle(n)%pold = particle(n)%p
            particle(n)%vold = particle(n)%v
            particle(n)%Fcold= particle(n)%Fc
            particle(n)%Tcold= particle(n)%Tc
            particle(n)%Fhold= particle(n)%Fh
            particle(n)%Thold= particle(n)%Th
          end do
        end select
      end if
      return
    end subroutine grans_obj_AdvanceSolution_StoreOld
    subroutine grans_obj_AdvanceSolution_ComputeCollisionForces(this)
      !> Compute collision forces
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variable
      integer :: ic,jc,kc
      integer :: i,j,k
      integer :: key1,key2
      integer :: id1,id2
      integer :: n

      real(wp):: r1(3), r2(3)
      real(wp):: d1   , d2
      real(wp):: m1   , m2
      real(wp):: v1(3), v2(3)
      real(wp):: w1(3), w2(3)
      real(wp):: n2(3), Fc(3)
      integer :: ncol
      real(wp), parameter :: Pi=4.0_wp*atan(1.0_wp)
      ! Initialize collision force
      if (this%use_RP) then
        select type (center=>this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            center(n)%Fc = 0.0_wp
          end do
        end select
      end if
      if (this%use_PP) then
        select type (particle=>this%PP%p)
        type is (particle_obj)
          do n=1,this%PP%count_
            particle(n)%Fc = 0.0_wp
          end do
        end select
      end if

      if (.not.this%use_col) return

      ! Prepare collision lists
      if (this%use_IB) then
        call this%IB%UpdateGhostObjects(this%IB%cblock)
        call this%IB%UpdateNeighborList()
      end if
      if (this%use_RP) then
        call this%RP%UpdateGhostObjects(this%RP%cblock)
        call this%RP%UpdateNeighborList()
      end if
      if (this%use_PP) then
        call this%PP%UpdateGhostObjects(this%PP%cblock)
        call this%PP%UpdateNeighborList()
      end if

      ! Compute RP-RP collisions
      if (this%use_RP) then
        associate (lo =>this%RP%cblock%lo, hi=>this%RP%cblock%hi)
          ! Loop over bins
          select type (center=>this%RP%p)
          type is (ResPart_obj)
            ! Loop over collision grid
            do kc=lo(3),hi(3)
              do jc=lo(2),hi(2)
                do ic=lo(1),hi(1)
                  ! Loop over objects in this cell
                  do key1 = 1,this%RP%objincell(ic,jc,kc)
                    ! Get particle ID
                    call this%RP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                    r1 = center(id1)%p
                    d1 = center(id1)%d
                    m1 = center(id1)%rho*Pi/6.0_wp*center(id1)%d**3
                    v1 = center(id1)%v
                    w1 = center(id1)%w

                    ! Check collisions with neighbors
                    do k = kc-1,kc+1
                      do j = jc-1,jc+1
                        do i = ic-1,ic+1
                          do key2 = 1,this%RP%objincell(i,j,k)
                            call this%RP%neighbors(i,j,k)%get(key=key2,val=id2)

                            ! Skip collisions between same particle
                            if (abs(center(id1)%id).eq.abs(center(id2)%id)) cycle

                            r2 = center(id2)%p
                            d2 = center(id2)%d
                            m2 = center(id2)%rho*Pi/6.0_wp*center(id2)%d**3
                            v2 = center(id2)%v
                            w2 = center(id2)%w

                            center(id1)%Fc = center(id1)%Fc + DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end do
          end select
        end associate
      end if

      ! Compute PP-PP collisions
      if (this%use_PP) then
        associate (lo =>this%PP%cblock%lo, hi=>this%PP%cblock%hi)
        ! Loop over bins
          select type (particle=>this%PP%p)
          type is (particle_obj)
            ! Loop over collision grid
            do kc=lo(3),hi(3)
              do jc=lo(2),hi(2)
                do ic=lo(1),hi(1)
                  ! Loop over objects in this cell
                  do key1 = 1,this%PP%objincell(ic,jc,kc)
                    ! Get particle ID
                    call this%PP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                    r1 = particle(id1)%p
                    d1 = particle(id1)%d
                    m1 = particle(id1)%rho*Pi/6.0_wp*particle(id1)%d**3
                    v1 = particle(id1)%v
                    w1 = particle(id1)%w

                    ! Check collisions with neighbors
                    do k = kc-1,kc+1
                      do j = jc-1,jc+1
                        do i = ic-1,ic+1
                          do key2 = 1,this%PP%objincell(i,j,k)
                            call this%PP%neighbors(i,j,k)%get(key=key2,val=id2)

                            ! Skip collisions between same particle
                            if (abs(particle(id1)%id).eq.abs(particle(id2)%id)) cycle

                            r2 = particle(id2)%p
                            d2 = particle(id2)%d
                            m2 = particle(id2)%rho*Pi/6.0_wp*particle(id2)%d**3
                            v2 = particle(id2)%v
                            w2 = particle(id2)%w

                            particle(id1)%Fc = particle(id1)%Fc + DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end do
          end select
        end associate
      end if

      if (this%use_PP.and.this%use_RP) then
        ! Assuming RP and PP collision blocks are identical
        associate (lo =>this%PP%cblock%lo, hi=>this%PP%cblock%hi)
          select type(particle=>this%PP%p)
          type is (particle_obj)
            select type(center=>this%RP%p)
            type is(ResPart_obj)
              ! Loop over collision grid
              do kc=lo(3),hi(3)
                do jc=lo(2),hi(2)
                  do ic=lo(1),hi(1)

                    ! Loop over PP objects in this cell
                    do key1 = 1,this%PP%objincell(ic,jc,kc)
                      ! Get particle ID
                      call this%PP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                      r1 = particle(id1)%p
                      d1 = particle(id1)%d
                      m1 = particle(id1)%rho*Pi/6.0_wp*particle(id1)%d**3
                      v1 = particle(id1)%v
                      w1 = particle(id1)%w

                      ! Check collisions with neighbor RP objects
                      do k = kc-1,kc+1
                        do j = jc-1,jc+1
                          do i = ic-1,ic+1
                            do key2 = 1,this%RP%objincell(i,j,k)
                              call this%RP%neighbors(i,j,k)%get(key=key2,val=id2)

                              r2 = center(id2)%p
                              d2 = center(id2)%d
                              m2 = center(id2)%rho*Pi/6.0_wp*center(id2)%d**3
                              v2 = center(id2)%v
                              w2 = center(id2)%w

                              particle(id1)%Fc = particle(id1)%Fc + DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                            end do
                          end do
                        end do
                      end do
                    end do

                    ! Loop over RP objects in this cell
                    do key1 = 1,this%RP%objincell(ic,jc,kc)
                      ! Get particle ID
                      call this%RP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                      r1 = center(id1)%p
                      d1 = center(id1)%d
                      m1 = center(id1)%rho*Pi/6.0_wp*center(id1)%d**3
                      v1 = center(id1)%v
                      w1 = center(id1)%w

                      ! Check collisions with neighbor RP objects
                      do k = kc-1,kc+1
                        do j = jc-1,jc+1
                          do i = ic-1,ic+1
                            do key2 = 1,this%PP%objincell(i,j,k)
                              call this%PP%neighbors(i,j,k)%get(key=key2,val=id2)

                              r2 = particle(id2)%p
                              d2 = particle(id2)%d
                              m2 = particle(id2)%rho*Pi/6.0_wp*particle(id2)%d**3
                              v2 = particle(id2)%v
                              w2 = particle(id2)%w

                              center(id1)%Fc = center(id1)%Fc + DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                            end do
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end select
          end select
        end associate
      end if

      if (this%use_PP.and.this%use_IB) then
        ! Assuming RP and PP collision blocks are identical
        associate (lo =>this%PP%cblock%lo, hi=>this%PP%cblock%hi)
          select type(particle=>this%PP%p)
          type is (particle_obj)
            select type(triangle=>this%IB%p)
            type is(marker_obj)
              ! Loop over collision grid
              do kc=lo(3),hi(3)
                do jc=lo(2),hi(2)
                  do ic=lo(1),hi(1)

                    ! Loop over PP objects in this cell
                    do key1 = 1,this%PP%objincell(ic,jc,kc)
                      ! Get particle ID
                      call this%PP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                      r1 = particle(id1)%p
                      d1 = particle(id1)%d
                      m1 = particle(id1)%rho*Pi/6.0_wp*particle(id1)%d**3
                      v1 = particle(id1)%v
                      w1 = particle(id1)%w

                      ! Check collisions with neighbor IB triangles
                      do k = kc-1,kc+1
                        do j = jc-1,jc+1
                          do i = ic-1,ic+1
                            Fc = 0.0_wp
                            ncol = 0
                            do key2 = 1,this%IB%objincell(i,j,k)
                              call this%IB%neighbors(i,j,k)%get(key=key2,val=id2)

                              ! Static IB for now
                              r2 = triangle(id2)%p
                              n2 = triangle(id2)%n
                              v2 = 0.0_wp
                              w2 = 0.0_wp

                              if (0.5_wp*d1-abs(dot_product(r2-r1,n2)).gt.0.0_wp) then
                                Fc = Fc+DEM_PW_col(r1,r2,d1,m1,n2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                                ncol = ncol + 1
                              end if
                            end do
                            ncol = max(ncol,1)
                            particle(id1)%Fc = particle(id1)%Fc + Fc/real(ncol,wp)
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end select
          end select
        end associate
      end if
      ! Remove ghost objects and free neighbor lists
      if (this%use_IB) then
        call this%IB%recycle()
        call this%IB%FreeNeighborList()
      end if
      if (this%use_RP) then
        call this%RP%recycle()
        call this%RP%FreeNeighborList()
      end if
      if (this%use_PP) then
        call this%PP%recycle()
        call this%PP%FreeNeighborList()
      end if

      return
    end subroutine grans_obj_AdvanceSolution_ComputeCollisionForces
    subroutine grans_obj_AdvanceSolution_ComputeHydrodynamicForces(this)
      !> Compute hydrodynamic and other forces
      implicit none
      class(grans_obj), intent(inout) :: this              !! The solver
      ! Work variable
      real(wp):: Mp   ! Particle mass
      real(wp):: Ip   ! Particle moment of inertia
      integer :: n
      real(wp), parameter :: Pi=4.0_wp*atan(1.0_wp)

      if (this%use_RP) then
        select type (center =>this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            Mp  = center(n)%rho*center(n)%d**3/6.0_wp*Pi
            Ip  = Mp*center(n)%d**2/10.0_wp
            center(n)%Fh = Mp*this%gravity
            center(n)%Th = 0.0_wp
          end do
        end select
      end if

      if (this%use_PP) then
        select type (particle =>this%PP%p)
        class is (particle_obj)
          do n=1,this%PP%count_
            Mp  = particle(n)%rho*particle(n)%d**3/6.0_wp*Pi
            Ip  = Mp*particle(n)%d**2/10.0_wp
            particle(n)%Fh = Mp*this%gravity
            particle(n)%Th = 0.0_wp
          end do
        end select
      end if
      return
    end subroutine grans_obj_AdvanceSolution_ComputeHydrodynamicForces
    pure function cross_product(x,y) result(z)
      implicit none
      
      real(WP), dimension(3), intent(in) :: x,y
      real(WP), dimension(3)             :: z
      
      z(1)=x(2)*y(3)-x(3)*y(2)
      z(2)=x(3)*y(1)-x(1)*y(3)
      z(3)=x(1)*y(2)-x(2)*y(1)
      
      return
    end function cross_product
    pure function DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,tcol,edry,muc) result (val)
      implicit none
      real(wp), intent(in) :: r1(3), r2(3)  ! Position
      real(wp), intent(in) :: d1, d2        ! Diameter
      real(wp), intent(in) :: m1, m2        ! Masses of each particle
      real(wp), intent(in) :: v1(3), v2(3)  ! Velocities
      real(wp), intent(in) :: w1(3), w2(3)  ! Rotation rates
      real(wp), intent(in) :: tcol
      real(wp), intent(in) :: edry
      real(wp), intent(in) :: muc
      real(wp) :: val(3)

      ! Work variable
      real(wP) :: deltan        ! Overlap between particles
      real(wp) :: d12           ! Distance between centers
      real(wp) :: n12(3)        ! Unitary vector connecting the two centroids
      real(wp) :: t12(3)        ! Unitary tangential vector
      real(wp) :: v12(3)        ! Relative velocity at contact point
      real(wp) :: v12t(3)       ! Tangential relative velocity
      real(wp) :: me            ! Reduced mass
      real(wp) :: kn            ! Normal spring stiffness
      real(wp) :: etan          ! Normal dashpot coefficient
      real(wp) :: Fn,Ft         ! Normal and tangential forces
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Initialization
      val = 0.0_wp

      ! Distance between centers
      d12 = norm2(r1-r2)

      ! Compute overlap distance
      deltan= 0.5_wp*(d1+d2) -d12

      if (deltan.gt.0.0_wp) then
        ! deltan>0: The two particles overlap
        ! We proceed to dry collision calculation

        ! Unitary vector connecting the two centers
        n12 = (r2-r1)/d12

        ! Relative velocity at the contact point
        v12 = v1-v2 + 0.5_wp*d1*cross_product(w1,n12) &
                    + 0.5_wp*d2*cross_product(w2,n12)

        ! Compute normal collision force F^q_{ij,n}
        ! - Spring coefficient in normal direction
        me    = m1*m2/(m1+m2)
        kn   = me*(Pi**2+log(edry)**2)/tcol**2
        ! - Dashpot coefficient in normal direction
        etan = -2.0_wp*me*log(edry)/tcol
        ! - Normal force
        Fn    = -kn*deltan -etan*dot_product(v12,n12)

        ! Compute tangential collision force
        ! - Tangential relative velocity
        v12t  = v12 - dot_product(v12,n12)*n12
        t12   = v12t/(norm2(v12t)+epsilon(1.0_wp))
        ! - Tangential force
        Ft   = -muc*abs(Fn)

        val=Fn*n12 +Ft*t12
      end if

      return
    end function DEM_col
    pure function DEM_PW_col(r1,r2,d1,m1,n2,v1,v2,w1,w2,tcol,edry,muc) result (val)
      implicit none
      real(wp), intent(in) :: r1(3), r2(3)  ! Position
      real(wp), intent(in) :: d1            ! Diameter
      real(wp), intent(in) :: m1            ! Masses of each particle
      real(wp), intent(in) :: n2(3)         ! Wall normal
      real(wp), intent(in) :: v1(3), v2(3)  ! Velocities
      real(wp), intent(in) :: w1(3), w2(3)  ! Rotation rates
      real(wp), intent(in) :: tcol
      real(wp), intent(in) :: edry
      real(wp), intent(in) :: muc
      real(wp) :: val(3)

      ! Work variable
      real(wP) :: deltan        ! Overlap between particles
      real(wp) :: d12           ! Distance between centers
      real(wp) :: n12(3)        ! Unitary vector connecting the two centroids
      real(wp) :: t12(3)        ! Unitary tangential vector
      real(wp) :: v12(3)        ! Relative velocity at contact point
      real(wp) :: v12t(3)       ! Tangential relative velocity
      real(wp) :: me            ! Reduced mass
      real(wp) :: kn            ! Normal spring stiffness
      real(wp) :: etan          ! Normal dashpot coefficient
      real(wp) :: Fn,Ft         ! Normal and tangential forces
      real(wp) :: p(3)          ! Position of contact point
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Initialization
      val = 0.0_wp

      ! Distance between centers
      p = r1 - dot_product(r1-r2,n2)*n2

      d12 = norm2(r1-p)

      ! Compute overlap distance
      deltan= 0.5_wp*d1 -d12

      !if (deltan.gt.0.0_wp) then
        ! deltan>0: The two particles overlap
        ! We proceed to dry collision calculation

        ! Unitary vector connecting the two centers
        n12 = (p-r1)/d12

        ! Relative velocity at the contact point
        v12 = v1-v2 + 0.5_wp*d1*cross_product(w1,n12)

        ! Compute normal collision force F^q_{ij,n}
        ! - Spring coefficient in normal direction
        me    = m1
        kn   = me*(Pi**2+log(edry)**2)/tcol**2
        ! - Dashpot coefficient in normal direction
        etan = -2.0_wp*me*log(edry)/tcol
        ! - Normal force
        Fn    = -kn*deltan -etan*dot_product(v12,n12)

        ! Compute tangential collision force
        ! - Tangential relative velocity
        v12t  = v12 - dot_product(v12,n12)*n12
        t12   = v12t/(norm2(v12t)+epsilon(1.0_wp))
        ! - Tangential force
        Ft   = -muc*abs(Fn)

        val=Fn*n12 +Ft*t12
      !end if

      return
    end function DEM_PW_col
end submodule grans_AdvanceSolution_smod
