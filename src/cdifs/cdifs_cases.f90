!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module cdifs_cases
  !>--------------------------------------------------------------------------
  ! Module: cdifs_cases
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Simulation cases for CDIFS
  !
  ! Note: Each new case must have:
  !    1) an interface to a module subroutine where block and other
  ! simulation data are defined.
  !    2) An implementation of this module subroutine in a submodule
  ! added in the src/cases folder.
  !    3) name of the case, and call to the module subroutine added
  ! to the subroutine case_obj_setup
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapEulerian
  use leapCases
  implicit none

  private
  public :: cdifs_case_obj

  type,extends(case_obj) :: cdifs_case_obj
    !> Simulation case manager for the GRANS solvere
    contains
      procedure :: setup => case_obj_setup
  end type cdifs_case_obj

  interface
    module subroutine cdifs_taylor_green(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_taylor_green
    module subroutine cdifs_lamb_oseen(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_lamb_oseen
    module subroutine cdifs_vortex_dipole(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_vortex_dipole
    module subroutine cdifs_settling_sphere(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_settling_sphere
    module subroutine cdifs_rebound(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_rebound
    module subroutine cdifs_lid_driven_cavity(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_lid_driven_cavity
    module subroutine cdifs_vortex_impingement(this)
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
    end subroutine cdifs_vortex_impingement
  end interface
  contains
    subroutine case_obj_setup(this)
      !> Calls appropriate case
      implicit none
      class(cdifs_case_obj), intent(inout) :: this

      select case (this%name)
      case ("TAYLOR GREEN")
        call cdifs_taylor_green(this)
      case ("LAMB OSEEN")
        call cdifs_lamb_oseen(this)
      case ("VORTEX DIPOLE")
        call cdifs_vortex_dipole(this)
      case ("SETTLING SPHERE")
        call cdifs_settling_sphere(this)
      case ("REBOUND")
        call cdifs_rebound(this)
      case ("LID-DRIVEN CAVITY")
        call cdifs_lid_driven_cavity(this)
      case ("VORTEX IMPINGEMENT")
        call cdifs_vortex_impingement(this)
      case default
        call this%parallel%stop("CDIFS: Unknown case")
      end select
      return
    end subroutine case_obj_setup
end module cdifs_cases
