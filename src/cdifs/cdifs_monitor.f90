!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs) cdifs_Monitor_smod
  contains
    module subroutine cdifs_obj_Monitor(this)
      !> Compute and write monitored values
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      ! Monitor flow quantities
      call cdifs_obj_FlowUpdateMonitor(this)

      ! Monitor RP quantites
      if (this%use_RP) call this%RP%UpdateMonitor

      ! Print monitor
      call this%monitors%print()

      return
    end subroutine cdifs_obj_Monitor
    subroutine cdifs_obj_FlowUpdateMonitor(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      real(wp) :: maxdiv
      real(wp) :: maxV(3)
      integer  :: i,j,k
      real(wp) :: maxCFL(3)
      real(wp) :: buf

      associate (op=>this%op, lo=>this%block%lo,  hi=>this%block%hi,   &
        dxm=>this%block%dxm, dym=>this%block%dym, dzm=>this%block%dzm, &
        U=>this%U, V=>this%V, W=>this%W, dt=>this%timer%dt )

        ! Maximum velocities
        maxV = 0.0_wp
        maxV(1) = maxval(abs(U%cell(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3))))
        maxV(2) = maxval(abs(V%cell(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3))))
        maxV(3) = maxval(abs(W%cell(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3))))

        call this%parallel%max(maxV(1),buf); maxV(1)=buf
        call this%parallel%max(maxV(2),buf); maxV(2)=buf
        call this%parallel%max(maxV(3),buf); maxV(3)=buf

        ! Maximum divergence
        ! - Compute divergence field
        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
          this%divu%cell(i,j,k) = &
              dot_product(op%c_d1dx1(:,i),U%cell(i-op%st+1:i+op%st,j,k)) &
            + dot_product(op%c_d1dx2(:,j),V%cell(i,j-op%st+1:j+op%st,k)) &
            + dot_product(op%c_d1dx3(:,k),W%cell(i,j,k-op%st+1:k+op%st))
        end do
        ! - Get max value
        maxdiv = maxval(abs(this%divu%cell(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3))))
        call this%parallel%max(maxdiv,buf); maxdiv=buf

        ! Maximum CFL
        maxCFL = 0.0_wp
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              maxCFL(1) = max(maxCFL(1), U%cell(i,j,k)*dt/dxm(i))
              maxCFL(2) = max(maxCFL(2), V%cell(i,j,k)*dt/dym(j))
              maxCFL(3) = max(maxCFL(3), W%cell(i,j,k)*dt/dzm(k))
            end do
          end do
        end do
        call this%parallel%max(maxCFL(1),buf); maxCFL(1)=buf
        call this%parallel%max(maxCFL(2),buf); maxCFL(2)=buf
        call this%parallel%max(maxCFL(3),buf); maxCFL(3)=buf

        call this%monitors%set('stdout',1,this%timer%iter)
        call this%monitors%set('stdout',2,this%timer%time)
        call this%monitors%set('stdout',3,maxV(1)        )
        call this%monitors%set('stdout',4,maxV(2)        )
        call this%monitors%set('stdout',5,maxV(3)        )
        call this%monitors%set('stdout',6,maxval(maxCFL) )
        call this%monitors%set('stdout',7,maxdiv         )
        call this%monitors%set('stdout',8,this%hypre%it  )

        call this%monitors%set('Velocity',1,this%timer%iter)
        call this%monitors%set('Velocity',2,this%timer%time)
        call this%monitors%set('Velocity',3,maxV(1)        )
        call this%monitors%set('Velocity',4,maxV(2)        )
        call this%monitors%set('Velocity',5,maxV(3)        )
        call this%monitors%set('Velocity',6,maxval(maxCFL) )
        call this%monitors%set('Velocity',7,maxdiv         )
        call this%monitors%set('Velocity',8,this%hypre%it  )

      end associate
      return
    end subroutine cdifs_obj_FlowUpdateMonitor
end submodule cdifs_Monitor_smod
