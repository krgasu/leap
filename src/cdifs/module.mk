MODULE = cdifs
FILES = cdifs_cases.f90 cdifs.f90 cdifs_prepare.f90 cdifs_advance.f90 cdifs_monitor.f90 \
		cdifs_restart.f90 cdifs_output.f90
SRC += $(patsubst %, $(MODULE)/%, $(FILES))
