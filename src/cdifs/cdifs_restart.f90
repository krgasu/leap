!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs) cdifs_WriteRestartData_smod
  contains
    module subroutine cdifs_obj_WriteRestartData(this)
      class(cdifs_obj), intent(inout) :: this              !! The solver

      ! Write data required for restarts in working precision
      call this%fields%write(this%timer%iter,this%timer%time,[character(len=str8) :: 'U','V','W','P'])

      return
    end subroutine cdifs_obj_WriteRestartData
end submodule cdifs_WriteRestartData_smod
