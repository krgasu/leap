!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs) cdifs_WriteOutputData_smod
  contains
    module subroutine cdifs_obj_WriteOutputData(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer :: n

      if (.not.allocated(this%outputs)) return

      do n=1,size(this%outputs)
       select case (this%outputs(n))
       case ('P')
         ! Nothing to do
       case ('Vel')
         ! Compute velocities at cell-centers
         this%Um = this%op%intrp1(this%U)
         this%Vm = this%op%intrp2(this%V)
         this%Wm = this%op%intrp3(this%W)
       case ('DIV')
         this%divu = this%op%p_div(this%U,this%V,this%W)
         call this%divu%UpdateGhostCells
       case ('SA')
         ! Nothing to do
       case ('ibF')
         ! Nothing to do
       case ('ibN')
         ! Nothing to do
       case ('ibVF')
         ! Nothing to do
       end select
      end do

      ! Write data required for visualization in single precision
      call this%fields%WriteSilo(this%timer%iter,this%timer%time,this%output_var)
      return
    end subroutine cdifs_obj_WriteOutputData
end submodule cdifs_WriteOutputData_smod
