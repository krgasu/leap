!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs) cdifs_AdvanceSolution_smod
  contains
    module subroutine cdifs_obj_AdvanceSolution(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer :: it
      integer :: itp

      ! Store old states
      this%Uold = this%U
      this%Vold = this%V
      this%Wold = this%W
      if (this%use_RP) call this%RP%StoreOld()

      ! Perform sub-iterations
      it = 0
      do while (it.lt.this%subit)
        it = it +1

        ! Compute the hydrodynamic forces exerted on particles
        if (this%use_RP) call this%RP%GetHydroForces(this%P,this%U,this%V,this%W,this%ibVF,this%visc)

        ! Incrementally advance the particles while updating the collision forces
        do itp=1,this%maxitp
          if (this%use_col) call cdifs_obj_UpdateCollisions(this)
          if (this%use_RP)  call this%RP%AdvanceCenters(this%timer%dt/real(this%maxitp,wp))
        end do

        if (this%use_RP) call this%RP%AdvanceMarkers(this%timer%dt)

        ! Treatment for periodicity
        if (this%use_RP) call this%RP%ApplyPeriodicity
        if (this%use_RP) call this%RP%ib%ApplyPeriodicity

        ! Send centroids and markers to their mpiranks
        if (this%use_RP) call this%RP%Communicate
        if (this%use_RP) call this%RP%ib%Communicate

        ! Localize centroids and markers on the grid
        if (this%use_RP) call this%RP%Localize
        if (this%use_RP) call this%RP%ib%Localize

        ! Update solid volume fraction
        if (this%use_RP) then
          ! Update normals
          this%ibN(1) = 0.0_wp; this%ibN(2) = 0.0_wp; this%ibN(3) = 0.0_wp
          call this%RP%UpdateNormals(this%ibN)
          if (this%use_IB) call this%IB%UpdateNormals(this%ibN)

          this%rhs = this%op%div('rhs',this%ibN(1),this%ibN(2),this%ibN(3))*(-1.0_wp)
          call this%op%ApplyLaplacianDC(this%rhs,this%bcs,'ibVF')
          call this%hypre%SetRHS(this%rhs)
          call this%hypre%Solve(this%ibVF)
        end if

        call cdifs_obj_AdvanceSolution_Predictor(this)

        if (this%use_IB &
        .or.this%use_RP) call cdifs_obj_AdvanceSolution_IBForcing(this)

        call cdifs_obj_AdvanceSolution_Corrector(this)
      end do
      return
    end subroutine cdifs_obj_AdvanceSolution
    subroutine cdifs_obj_UpdateCollisions(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variable
      integer :: ic,jc,kc
      integer :: i,j,k
      integer :: key1,key2
      integer :: id1,id2
      integer :: n

      real(wp):: r1(3), r2(3)
      real(wp):: d1   , d2
      real(wp):: m1   , m2
      real(wp):: v1(3), v2(3)
      real(wp):: w1(3), w2(3)
      real(wp):: n2(3), Fc(3)
      integer :: ncol
      real(wp), parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Initialize collision force
      if (this%use_RP) then
        select type (center=>this%RP%p)
        type is (ResPart_obj)
          do n=1,this%RP%count_
            center(n)%Fcold = center(n)%Fc
            center(n)%Fc    = 0.0_wp
          end do
        end select
      end if

      ! Prepare collision lists
      if (this%use_IB) then
        call this%IB%UpdateGhostObjects(this%IB%cblock)
        call this%IB%UpdateNeighborList()
      end if
      if (this%use_RP) then
        call this%RP%UpdateGhostObjects(this%RP%cblock)
        call this%RP%UpdateNeighborList()
      end if

      ! Compute RP-RP collisions
      if (this%use_RP) then
        associate (lo =>this%RP%cblock%lo, hi=>this%RP%cblock%hi)
          ! Loop over bins
          select type (center=>this%RP%p)
          type is (ResPart_obj)
            ! Loop over collision grid
            do kc=lo(3),hi(3)
              do jc=lo(2),hi(2)
                do ic=lo(1),hi(1)
                  ! Loop over objects in this cell
                  do key1 = 1,this%RP%objincell(ic,jc,kc)
                    ! Get particle ID
                    call this%RP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                    r1 = center(id1)%p
                    d1 = center(id1)%d
                    m1 = center(id1)%rho*Pi/6.0_wp*center(id1)%d**3
                    v1 = center(id1)%v
                    w1 = center(id1)%w

                    ! Check collisions with neighbors
                    do k = kc-1,kc+1
                      do j = jc-1,jc+1
                        do i = ic-1,ic+1
                          do key2 = 1,this%RP%objincell(i,j,k)
                            call this%RP%neighbors(i,j,k)%get(key=key2,val=id2)

                            ! Skip collisions between same particle
                            if (abs(center(id1)%id).eq.abs(center(id2)%id)) cycle

                            r2 = center(id2)%p
                            d2 = center(id2)%d
                            m2 = center(id2)%rho*Pi/6.0_wp*center(id2)%d**3
                            v2 = center(id2)%v
                            w2 = center(id2)%w

                            center(id1)%Fc = center(id1)%Fc + DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end do
          end select
        end associate
      end if


      if (this%use_RP.and.this%use_IB) then
        associate (lo =>this%RP%cblock%lo, hi=>this%RP%cblock%hi)
          select type(particle=>this%RP%p)
          type is (ResPart_obj)
            select type(triangle=>this%IB%p)
            type is(marker_obj)
              ! Loop over collision grid
              do kc=lo(3),hi(3)
                do jc=lo(2),hi(2)
                  do ic=lo(1),hi(1)

                    ! Loop over RP objects in this cell
                    do key1 = 1,this%RP%objincell(ic,jc,kc)
                      ! Get particle ID
                      call this%RP%neighbors(ic,jc,kc)%get(key=key1,val=id1)

                      r1 = particle(id1)%p
                      d1 = particle(id1)%d
                      m1 = particle(id1)%rho*Pi/6.0_wp*particle(id1)%d**3
                      v1 = particle(id1)%v
                      w1 = particle(id1)%w

                      ! Check collisions with neighbor IB triangles
                      do k = kc-1,kc+1
                        do j = jc-1,jc+1
                          do i = ic-1,ic+1
                            Fc = 0.0_wp
                            ncol = 0
                            do key2 = 1,this%IB%objincell(i,j,k)
                              call this%IB%neighbors(i,j,k)%get(key=key2,val=id2)

                              ! Static IB for now
                              r2 = triangle(id2)%p
                              n2 = triangle(id2)%n
                              v2 = 0.0_wp
                              w2 = 0.0_wp

                              if (0.5_wp*d1-abs(dot_product(r2-r1,n2)).gt.0.0_wp) then
                                Fc = Fc+DEM_PW_col(r1,r2,d1,m1,n2,v1,v2,w1,w2,this%tcol,this%edry,this%muc)
                                ncol = ncol + 1
                              end if
                            end do
                            ncol = max(ncol,1)
                            particle(id1)%Fc = particle(id1)%Fc + Fc/real(ncol,wp)
                          end do
                        end do
                      end do
                    end do

                  end do
                end do
              end do
            end select
          end select
        end associate
      end if
      ! Remove ghost objects and free neighbor lists
      if (this%use_IB) then
        call this%IB%Recycle()
        call this%IB%FreeNeighborList()
      end if
      if (this%use_RP) then
        call this%RP%Recycle()
        call this%RP%FreeNeighborList()
      end if
      return
    end subroutine cdifs_obj_UpdateCollisions
    subroutine cdifs_obj_AdvanceSolution_Predictor(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      associate ( U=>this%U, V=>this%V, W=>this%W, P=>this%P,       &
         Uold => this%Uold, Vold => this%Vold, Wold => this%Wold,   &
         resU => this%resU, resV => this%resV, resW => this%resW,   &
         rho => this%rho, visc => this%visc, bcs => this%bcs,       &
         dt=>this%timer%dt, op=>this%op)

        ! Predictor step
        !-------------------------
        U = (U+Uold)*0.5_wp; V = (V+Vold)*0.5_wp; W = (W+Wold)*0.5_wp
        resU = cdifs_AdvanceSolution_get_velocity_rhs_U(U,V,W,P,rho,visc,op,this%maskU)
        resV = cdifs_AdvanceSolution_get_velocity_rhs_V(U,V,W,P,rho,visc,op,this%maskV)
        resW = cdifs_AdvanceSolution_get_velocity_rhs_W(U,V,W,P,rho,visc,op,this%maskW)

        ! Buid new velocity: U_{k+1}^{n+1}
        U = Uold + resU*dt
        V = Vold + resV*dt
        W = Wold + resW*dt

        call U%UpdateGhostCells;    call V%UpdateGhostCells;    call W%UpdateGhostCells
        call bcs%UpdateBoundary(U); call bcs%UpdateBoundary(V); call bcs%UpdateBoundary(W)
      end associate
      return
    end subroutine cdifs_obj_AdvanceSolution_Predictor
    subroutine cdifs_obj_AdvanceSolution_IBForcing(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variable
      integer :: i,j,k

      ! Build forcing
      associate ( U =>this%U, V=>this%V, W=>this%W, rho=>this%rho, &
        Um=>this%Um, Vm=>this%Vm, Wm=>this%Wm, SA => this%SA,      &
        ibF => this%ibF, op=>this%op, dt=>this%timer%dt)

        ! Update SDF
        SA = 0.0_wp
        if (this%use_RP) call this%RP%UpdateSDF(SA)
        if (this%use_IB) call this%IB%UpdateSDF(SA)

        ! Interpolate velocities
        Um = op%intrp1(U); call Um%UpdateGhostCells
        Vm = op%intrp2(V); call Vm%UpdateGhostCells
        Wm = op%intrp3(W); call Wm%UpdateGhostCells

        ! Recompute the forcing
        ibF(1) = 0.0_wp; ibF(2) = 0.0_wp; ibF(3) = 0.0_wp
        if (this%use_RP) call this%RP%GetIBForcing(Um,Vm,Wm,rho,SA,ibF,dt)
        if (this%use_IB) call this%IB%GetIBForcing(Um,Vm,Wm,rho,SA,ibF,dt)
      end associate

      ! Apply forcing
      associate ( U=>this%U, V=>this%V, W=>this%W, ibF=>this%ibF,   &
        lo => this%block%lo, hi => this%block%hi, op=>this%op,      &
        rho => this%rho, bcs => this%bcs, dt=>this%timer%dt)

        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskU%cell(i,j,k)>0)
          U%cell(i,j,k) = U%cell(i,j,k) + dt/rho * dot_product(op%c_intrp1m(:,i),ibF(1)%cell(i-op%st:i+op%st-1,j,k))
        end do
        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskV%cell(i,j,k)>0)
          V%cell(i,j,k) = V%cell(i,j,k) + dt/rho * dot_product(op%c_intrp2m(:,j),ibF(2)%cell(i,j-op%st:j+op%st-1,k))
        end do
        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskW%cell(i,j,k)>0)
          W%cell(i,j,k) = W%cell(i,j,k) + dt/rho * dot_product(op%c_intrp3m(:,k),ibF(3)%cell(i,j,k-op%st:k+op%st-1))
        end do

        call U%UpdateGhostCells;    call V%UpdateGhostCells;    call W%UpdateGhostCells
        call bcs%UpdateBoundary(U); call bcs%UpdateBoundary(V); call bcs%UpdateBoundary(W)
      end associate

      return
    end subroutine cdifs_obj_AdvanceSolution_IBForcing
    subroutine cdifs_obj_AdvanceSolution_Corrector(this)
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer :: i,j,k

      ! Get shortcuts
      associate ( U=>this%U, V=>this%V, W=>this%W, P=>this%P,       &
        dP=>this%dP, rhs=>this%rhs, hypre=>this%hypre, op=>this%op, &
        bcs=>this%bcs, dt=>this%timer%dt, rho=>this%rho)

        rhs = op%p_div(U,V,W)*(rho/dt)

        call hypre%SetRHS(rhs)
        call hypre%Solve(dP)
        call dP%UpdateGhostCells

        dP%cell = dP%cell - dP%mean()

        ! Update pressure
        !-------------------------
        P = P + dP
        call bcs%UpdateBoundary(P)
      end associate

      associate ( U=>this%U, V=>this%V, W=>this%W, dP=>this%dP,     &
        lo=>this%block%lo, hi=>this%block%hi, op=>this%op,          &
        rho=>this%rho, bcs=>this%bcs, dt=>this%timer%dt)

        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskU%cell(i,j,k)>0)
          U%cell(i,j,k) = U%cell(i,j,k) - dt/rho * dot_product(op%c_d1dx1m(:,i),dP%cell(i-op%st:i+op%st-1,j,k))
        end do

        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskV%cell(i,j,k)>0)
          V%cell(i,j,k) = V%cell(i,j,k) - dt/rho * dot_product(op%c_d1dx2m(:,j),dP%cell(i,j-op%st:j+op%st-1,k))
        end do

        do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),this%maskW%cell(i,j,k)>0)
          W%cell(i,j,k) = W%cell(i,j,k) - dt/rho * dot_product(op%c_d1dx3m(:,k),dP%cell(i,j,k-op%st:k+op%st-1))
        end do

        call U%UpdateGhostCells;    call V%UpdateGhostCells;    call W%UpdateGhostCells
        call bcs%UpdateBoundary(U); call bcs%UpdateBoundary(V); call bcs%UpdateBoundary(W)
      end associate
      return
    end subroutine cdifs_obj_AdvanceSolution_Corrector

    function cdifs_AdvanceSolution_get_velocity_rhs_U(U,V,W,P,rho,visc,op,maskU) result(res)
      implicit none
      type(eulerian_obj_r), intent(in)   :: U,V,W,P
      real(wp),             intent(in)   :: rho
      real(wp),             intent(in)   :: visc
      type(op_obj),         intent(inout):: op
      type(eulerian_obj_i), intent(in)   :: maskU
      type(eulerian_obj_r) :: res
      ! Work variables
      integer :: i,j,k
      integer :: n,kk
      integer :: is, js, ks
      real(wp):: Flux
      real(wp):: Uinterp, Vinterp, Winterp

      call res%initialize('rhs',op%block,op%parallel,stag=1)

      res = 0.0_wp

      associate ( &
        lo => op%block%lo, hi => op%block%hi, x => op%block%x, &
        xm => op%block%xm, y => op%block%y, ym => op%block%ym, &
        z  => op%block%z,  zm => op%block%zm)

        do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),maskU%cell(i,j,k)>0)

          res%cell(i,j,k) = 0.0_wp

          ! Flux in 1-dir
          do n = -op%st,op%st-1
            ! Shift
            is = i+n; js = j; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx1 (:,is),U%cell(is-op%st+1:is+op%st,js,ks)) &
                 - P%cell(is,js,ks)/rho
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx1m(n,i)*Flux

            ! Convective: convUU
            Uinterp = dot_product(op%c_intrp1 (:,is),    U%cell(is-op%st+1:is+op%st,js,ks))
            do kk = 1, op%st
              Flux = -Uinterp * dot_product(op%morinishi_int(:,kk),U%cell(is-op%st+1:is+op%st,js,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st+1,kk)/(xm(i) - xm(i-1)) * Flux
            end do
          end do

          ! Flux in 2-dir
          do n= -op%st+1,op%st
            ! Shift
            is = i; js = j+n; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx2m(:,js),U%cell(is,js-op%st:js+op%st-1,ks))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx2 (n,j)*Flux

            ! Convective: convVU
            Vinterp = dot_product(op%c_intrp1m(:,is),    V%cell(is-op%st:is+op%st-1,js,ks))
            do kk = 1, op%st
              Flux = -Vinterp * dot_product(op%morinishi_int(:,kk),U%cell(is,js-op%st:js+op%st-1,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(y (j+1)-y (j  )) * Flux
            end do
          end do

          ! Flux in 3-dir
          do n= -op%st+1,op%st
            ! Shift
            is = i; js = j; ks = k+n

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx3m(:,ks),U%cell(is,js,ks-op%st:ks+op%st-1))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx3 (n,k)*Flux

            ! Convective: convWU
            Winterp = dot_product(op%c_intrp1m(:,is),    W%cell(is-op%st:is+op%st-1,js,ks))
            do kk = 1, op%st
              Flux = -Winterp * dot_product(op%morinishi_int(:,kk),U%cell(is,js,ks-op%st:ks+op%st-1))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(z (k+1)-z (k  )) * Flux
            end do
          end do
        end do
      end associate

      return
    end function cdifs_AdvanceSolution_get_velocity_rhs_U
    function cdifs_AdvanceSolution_get_velocity_rhs_V(U,V,W,P,rho,visc,op,maskV) result(res)
      implicit none
      type(eulerian_obj_r), intent(in)   :: U,V,W,P
      real(wp),             intent(in)   :: rho
      real(wp),             intent(in)   :: visc
      type(op_obj),         intent(inout):: op
      type(eulerian_obj_i), intent(in)   :: maskV
      type(eulerian_obj_r) :: res
      ! Work variables
      integer :: i,j,k
      integer :: n,kk
      integer :: is, js, ks
      real(wp):: Flux
      real(wp):: Uinterp, Vinterp, Winterp

      call res%initialize('rhs',op%block,op%parallel,stag=2)

      res = 0.0_wp

      associate ( &
        lo => op%block%lo, hi => op%block%hi, x => op%block%x, &
        xm => op%block%xm, y => op%block%y, ym => op%block%ym, &
        z  => op%block%z,  zm => op%block%zm)

        do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),maskV%cell(i,j,k)>0)

          res%cell(i,j,k) = 0.0_wp

          ! Flux in 1-dir
          do n = -op%st+1,op%st
            ! Shift
            is = i+n; js = j; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx1m(:,is),V%cell(is-op%st:is+op%st-1,js,ks))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx1 (n,i)*Flux

            ! Convective: convUV
            Uinterp = dot_product(op%c_intrp2m(:,js),    U%cell(is,js-op%st:js+op%st-1,ks))
            do kk = 1, op%st
              Flux = -Uinterp * dot_product(op%morinishi_int(:,kk),V%cell(is-op%st:is+op%st-1,js,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(x(i+1) - x(i)) * Flux
            end do
          end do

          ! Flux in 2-dir
          do n= -op%st,op%st-1
            ! Shift
            is = i; js = j+n; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx2 (:,js),V%cell(is,js-op%st+1:js+op%st,ks)) &
                 - P%cell(is,js,ks)/rho
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx2m(n,j)*Flux

            ! Convective: convVV
            Vinterp = dot_product(op%c_intrp2 (:,js),V%cell(is,js-op%st+1:js+op%st,ks))
            do kk = 1, op%st
              Flux = -Vinterp * dot_product(op%morinishi_int(:,kk),V%cell(is,js-op%st+1:js+op%st,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st+1,kk)/(ym(j)-ym(j-1)) * Flux
            end do
          end do

          ! Flux in 3-dir
          do n= -op%st+1,op%st
            ! Shift
            is = i; js = j; ks = k+n

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx3m(:,ks),V%cell(is,js,ks-op%st:ks+op%st-1))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx3 (n,k)*Flux

            ! Convective: convWU
            Winterp = dot_product(op%c_intrp2m(:,js),    W%cell(is,js-op%st:js+op%st-1,ks))
            do kk = 1, op%st
              Flux = -Winterp * dot_product(op%morinishi_int(:,kk),V%cell(is,js,ks-op%st:ks+op%st-1))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(z (k+1)-z (k  )) * Flux
            end do
          end do
        end do
      end associate

      return
    end function cdifs_AdvanceSolution_get_velocity_rhs_V
    function cdifs_AdvanceSolution_get_velocity_rhs_W(U,V,W,P,rho,visc,op,maskW) result(res)
      implicit none
      type(eulerian_obj_r), intent(in)   :: U,V,W,P
      real(wp),             intent(in)   :: rho
      real(wp),             intent(in)   :: visc
      type(op_obj),         intent(inout):: op
      type(eulerian_obj_i), intent(in)   :: maskW
      type(eulerian_obj_r) :: res
      ! Work variables
      integer :: i,j,k
      integer :: n,kk
      integer :: is, js, ks
      real(wp):: Flux
      real(wp):: Uinterp, Vinterp, Winterp

      call res%initialize('rhs',op%block,op%parallel,stag=3)

      res = 0.0_wp

      associate ( &
        lo => op%block%lo, hi => op%block%hi, x => op%block%x, &
        xm => op%block%xm, y => op%block%y, ym => op%block%ym, &
        z  => op%block%z,  zm => op%block%zm)

        do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1),maskW%cell(i,j,k)>0)

          res%cell(i,j,k) = 0.0_wp

          ! Flux in 1-dir
          do n = -op%st+1,op%st
            ! Shift
            is = i+n; js = j; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx1m(:,is),W%cell(is-op%st:is+op%st-1,js,ks))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx1 (n,i)*Flux

            ! Convective: convUW
            Uinterp = dot_product(op%c_intrp3m(:,ks),    U%cell(is,js,ks-op%st:ks+op%st-1))
            do kk = 1, op%st
              Flux = -Uinterp * dot_product(op%morinishi_int(:,kk),W%cell(is-op%st:is+op%st-1,js,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(x(i+1) - x(i)) * Flux
            end do
          end do

          ! Flux in 2-dir
          do n= -op%st+1,op%st
            ! Shift
            is = i; js = j+n; ks = k

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx2m(:,js),W%cell(is,js-op%st:js+op%st-1,ks))
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx2 (n,j)*Flux

            ! Convective: convVW
            Vinterp = dot_product(op%c_intrp3m(:,ks),    V%cell(is,js,ks-op%st:ks+op%st-1))
            do kk = 1, op%st
              Flux = -Vinterp * dot_product(op%morinishi_int(:,kk),W%cell(is,js-op%st:js+op%st-1,ks))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st,kk)/(y (j+1)-y (j  )) * Flux
            end do
          end do

          ! Flux in 3-dir
          do n= -op%st,op%st-1
            ! Shift
            is = i; js = j; ks = k+n

            ! Viscous
            Flux = visc/rho*dot_product(op%c_d1dx3 (:,ks),W%cell(is,js,ks-op%st+1:ks+op%st)) &
                 - P%cell(is,js,ks)/rho
            res%cell(i,j,k) = res%cell(i,j,k) + op%c_d1dx3m(n,k)*Flux

            ! Convective: convWW
            Winterp = dot_product(op%c_intrp3 (:,ks),W%cell(is,js,ks-op%st+1:ks+op%st))
            do kk = 1, op%st
              Flux = -Winterp * dot_product(op%morinishi_int(:,kk),W%cell(is,js,ks-op%st+1:ks+op%st))
              res%cell(i,j,k) = res%cell(i,j,k) + op%alpha(kk)*op%morinishi_ddx(n+op%st+1,kk)/(zm(k)-zm(k-1)) * Flux
            end do
          end do

        end do
      end associate
      return
    end function cdifs_AdvanceSolution_get_velocity_rhs_W
    pure function DEM_col(r1,r2,d1,d2,m1,m2,v1,v2,w1,w2,tcol,edry,muc) result (val)
      implicit none
      real(wp), intent(in) :: r1(3), r2(3)  ! Position
      real(wp), intent(in) :: d1, d2        ! Diameter
      real(wp), intent(in) :: m1, m2        ! Masses of each particle
      real(wp), intent(in) :: v1(3), v2(3)  ! Velocities
      real(wp), intent(in) :: w1(3), w2(3)  ! Rotation rates
      real(wp), intent(in) :: tcol          ! Collision time
      real(wp), intent(in) :: edry          ! Normal dry restitution coef.
      real(wp), intent(in) :: muc           ! Coulomb factor
      real(wp) :: val(3)

      ! Work variable
      real(wP) :: deltan                    ! Overlap between particles
      real(wp) :: d12                       ! Distance between centers
      real(wp) :: n12(3)                    ! Unitary vector connecting the two centroids
      real(wp) :: t12(3)                    ! Unitary tangential vector
      real(wp) :: v12(3)                    ! Relative velocity at contact point
      real(wp) :: v12t(3)                   ! Tangential relative velocity
      real(wp) :: me                        ! Reduced mass
      real(wp) :: kn                        ! Normal spring stiffness
      real(wp) :: etan                      ! Normal dashpot coefficient
      real(wp) :: Fn,Ft                     ! Normal and tangential forces
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Initialization
      val = 0.0_wp

      ! Distance between centers
      d12 = norm2(r1-r2)

      ! Compute overlap distance
      deltan= 0.5_wp*(d1+d2) -d12

      if (deltan.gt.0.0_wp) then
        ! deltan>0: The two particles overlap
        ! We proceed to dry collision calculation

        ! Unitary vector connecting the two centers
        n12 = (r2-r1)/d12

        ! Relative velocity at the contact point
        v12 = v1-v2 + 0.5_wp*d1*cross_product(w1,n12) &
                    + 0.5_wp*d2*cross_product(w2,n12)

        ! Compute normal collision force F^q_{ij,n}
        ! - Spring coefficient in normal direction
        me    = m1*m2/(m1+m2)
        kn   = me*(Pi**2+log(edry)**2)/tcol**2
        ! - Dashpot coefficient in normal direction
        etan = -2.0_wp*me*log(edry)/tcol
        ! - Normal force
        Fn    = -kn*deltan -etan*dot_product(v12,n12)

        ! Compute tangential collision force
        ! - Tangential relative velocity
        v12t  = v12 - dot_product(v12,n12)*n12
        t12   = v12t/(norm2(v12t)+epsilon(1.0_wp))
        ! - Tangential force
        Ft   = -muc*abs(Fn)

        val=Fn*n12 +Ft*t12
      end if

      return
    end function DEM_col
    pure function DEM_PW_col(r1,r2,d1,m1,n2,v1,v2,w1,w2,tcol,edry,muc) result (val)
      implicit none
      real(wp), intent(in) :: r1(3), r2(3)  ! Position
      real(wp), intent(in) :: d1            ! Diameter
      real(wp), intent(in) :: m1            ! Masses of each particle
      real(wp), intent(in) :: n2(3)         ! Wall normal
      real(wp), intent(in) :: v1(3), v2(3)  ! Velocities
      real(wp), intent(in) :: w1(3), w2(3)  ! Rotation rates
      real(wp), intent(in) :: tcol          ! Collision time
      real(wp), intent(in) :: edry          ! Normal dry restitution coef.
      real(wp), intent(in) :: muc           ! Coulomb factor
      real(wp) :: val(3)

      ! Work variable
      real(wP) :: deltan                    ! Overlap between particles
      real(wp) :: d12                       ! Distance between centers
      real(wp) :: n12(3)                    ! Unitary vector connecting the two centroids
      real(wp) :: t12(3)                    ! Unitary tangential vector
      real(wp) :: v12(3)                    ! Relative velocity at contact point
      real(wp) :: v12t(3)                   ! Tangential relative velocity
      real(wp) :: me                        ! Reduced mass
      real(wp) :: kn                        ! Normal spring stiffness
      real(wp) :: etan                      ! Normal dashpot coefficient
      real(wp) :: Fn,Ft                     ! Normal and tangential forces
      real(wp) :: p(3)                      ! Position of contact point
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Initialization
      val = 0.0_wp

      ! Distance between centers
      p = r1 - dot_product(r1-r2,n2)*n2

      d12 = norm2(r1-p)

      ! Compute overlap distance
      deltan= 0.5_wp*d1 -d12

      !if (deltan.gt.0.0_wp) then
        ! deltan>0: The two particles overlap
        ! We proceed to dry collision calculation

        ! Unitary vector connecting the two centers
        n12 = (p-r1)/d12

        ! Relative velocity at the contact point
        v12 = v1-v2 + 0.5_wp*d1*cross_product(w1,n12)

        ! Compute normal collision force F^q_{ij,n}
        ! - Spring coefficient in normal direction
        me    = m1
        kn   = me*(Pi**2+log(edry)**2)/tcol**2
        ! - Dashpot coefficient in normal direction
        etan = -2.0_wp*me*log(edry)/tcol
        ! - Normal force
        Fn    = -kn*deltan -etan*dot_product(v12,n12)

        ! Compute tangential collision force
        ! - Tangential relative velocity
        v12t  = v12 - dot_product(v12,n12)*n12
        t12   = v12t/(norm2(v12t)+epsilon(1.0_wp))
        ! - Tangential force
        Ft   = -muc*abs(Fn)

        val=Fn*n12 +Ft*t12
      !end if

      return
    end function DEM_PW_col
    pure function cross_product(x,y) result(z)
      implicit none
      real(wp), intent(in) :: x(3),y(3)
      real(wp)             :: z(3)
      z(1)=x(2)*y(3)-x(3)*y(2)
      z(2)=x(3)*y(1)-x(1)*y(3)
      z(3)=x(1)*y(2)-x(2)*y(1)
      return
    end function cross_product
end submodule cdifs_AdvanceSolution_smod
