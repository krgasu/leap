!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs) cdifs_PrepareSolver_smod
  contains
    module subroutine cdifs_obj_PrepareSolver(this)
      !> Prepare data before run
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      real(wp) :: ds

      ! Set solver parameters
      call this%parser%Get("Fluid density",   this%rho                        )
      call this%parser%Get("Fluid viscosity", this%visc                       )
      call this%parser%Get("Gravity",         this%gravity, default =         &
                                                      [0.0_wp, 0.0_wp, 0.0_wp])
      call this%parser%Get("Subiterations",   this%subit,   default = 2       )
      call this%parser%Get('Use ResPart',     this%use_RP,  default = .false. )
      call this%parser%Get('Use IB',          this%use_IB,  default = .false. )
      call this%parser%Get('Use collisions',  this%use_col, default = .false. )

      call cdifs_obj_PrepareSolverBlock(this)
      call cdifs_obj_PrepareSolverOperators(this)
      call cdifs_obj_PrepareSolverFields(this)
      call cdifs_obj_PrepareSolverBCS(this)

      ! Activate other components
      ! - Initialize Resolved Particles
      if (this%use_RP) then 
        call this%RP%Initialize('ResPart',this%block,this%parallel)
        call this%RP%Prepare(this%timer,this%parser,this%op,this%monitors)
      end if

      ! - Initialize Immersed Boundaries
      if (this%use_IB) then
        call this%IB%Initialize('IB',     this%block,this%parallel)
        call this%IB%Prepare(this%timer,this%parser,this%op,this%monitors)
      end if

      ! - Compute initial surface density function and solid volume fraction
      if ( this%use_RP .or. this%use_IB) then
        ! Update SDF
        this%SA = 0.0_wp
        if (this%use_RP) call this%RP%UpdateSDF(this%SA)
        if (this%use_IB) call this%IB%UpdateSDF(this%SA)

        ! Update normals
        this%ibN(1) = 0.0_wp; this%ibN(2) = 0.0_wp; this%ibN(3) = 0.0_wp
        if (this%use_RP) call this%RP%UpdateNormals(this%ibN)
        if (this%use_IB) call this%IB%UpdateNormals(this%ibN)

        this%rhs = this%op%div('rhs',this%ibN(1),this%ibN(2),this%ibN(3))*(-1.0_wp)
        call this%op%ApplyLaplacianDC(this%rhs,this%bcs,'ibVF')
        call this%hypre%SetRHS(this%rhs)
        call this%hypre%Solve(this%ibVF)
      end if

      ! - Initialize collision utilities
      if (this%use_col) then
          call this%parser%Get('Collision grid spacing',      ds                       )
          call this%parser%Get('Collision substeps',          this%maxitp, default = 1 )
          call this%parser%Get('Collision time',              this%tcol                )
          call this%parser%Get('Dry normal restitution coef', this%edry                )
          call this%parser%Get('Coulomb friction coef',       this%muc                 )
          if (this%use_IB) call this%IB%SetupCollisionBlock(ds,1)
          if (this%use_RP) call this%RP%SetupCollisionBlock(ds,1)
      end if

      ! I/O utilities
      call cdifs_obj_PrepareSolverMonitor(this)
      call cdifs_obj_PrepareSolverOutput(this)

      ! Monitor and output initial conditions
      call this%Monitor()
      call this%WriteOutputData()

      return
    end subroutine cdifs_obj_PrepareSolver

    subroutine cdifs_obj_PrepareSolverBlock(this)
      !> Initialize and partition grid
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer :: Nb(3)
      integer :: ngc
      character(len=str64) :: filename

      call this%parser%Get("Block file",   filename, default = 'block.hdf5' )
      call this%parser%Get("Partition",    Nb,       default = [1,1,1]      )
      call this%parser%Get("Ghost cells",  ngc,      default = 1            )

      ! Initialize, read, and partition block
      call this%block%Initialize(ngc,this%parallel)
      call this%block%Read(filename)
      call this%block%Partition(Nb)

      return
    end subroutine cdifs_obj_PrepareSolverBlock
    subroutine cdifs_obj_PrepareSolverOperators(this)
      !> Initialize differential operators on current grid.
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      integer          :: scheme_order
      character(str64) :: hypre_solver
      integer          :: hypre_max_it
      real(wp)         :: hypre_res_tol

      ! Get information from input
      call this%parser%Get("Scheme order",          scheme_order,  default=2)
      call this%parser%Get("HYPRE Solver",          hypre_solver,  default="IJ-AMG-NONE")
      call this%parser%Get("HYPRE Max Iter",        hypre_max_it,  default=30           )
      call this%parser%Get("HYPRE Convergence Tol", hypre_res_tol, default=1.0e-10_wp   )

      ! Initialize operators
      call this%op%Initialize(this%block,this%parallel,order=scheme_order)

      ! Initialize HYPRE
      call this%hypre%Initialize(this%block,this%parallel)
      call this%hypre%SelectSolver(hypre_solver,MaxTol=hypre_res_tol,MaxIt=hypre_max_it)

      ! Use differential operators to build the laplacian inside HYPRE
      call this%op%BuildLaplacian(this%hypre%mat,this%hypre%stm)

      ! Setup HYPRE system of equations
      call this%hypre%setup()
      return
    end subroutine cdifs_obj_PrepareSolverOperators
    subroutine cdifs_obj_PrepareSolverFields(this)
      !> Initialize fields used by this solver
      ! and read initial conditions.
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      call this%fields%initialize(this%block,this%parallel)

      call this%parser%Get('Fields IC file',     this%fields%read_file )
      call this%parser%Get('Fields write file',  this%fields%write_file)
      call this%parser%Get('Fields overwrite ',  this%fields%overwrite, default=.true.)

      ! Add each field to main container.
      ! This will allocate the underlying arrays
      ! and facilitate IO.
      call this%fields%Add('U',      1, this%U     )
      call this%fields%Add('V',      2, this%V     )
      call this%fields%Add('W',      3, this%W     )
      call this%fields%Add('P',      0, this%P     )
      call this%fields%Add('dP',     0, this%dP    )
      call this%fields%Add('Uold',   1, this%Uold  )
      call this%fields%Add('Vold',   2, this%Vold  )
      call this%fields%Add('Wold',   3, this%Wold  )
      call this%fields%Add('resU',   1, this%resU  )
      call this%fields%Add('resV',   2, this%resV  )
      call this%fields%Add('resW',   3, this%resW  )
      call this%fields%Add('rhs',    0, this%rhs   )
      call this%fields%Add('div',    0, this%divu  )
      call this%fields%Add('Um',     0, this%Um    )
      call this%fields%Add('Vm',     0, this%Vm    )
      call this%fields%Add('Wm',     0, this%Wm    )
      call this%fields%Add('maskU',  1, this%maskU )
      call this%fields%Add('maskV',  2, this%maskV )
      call this%fields%Add('maskW',  3, this%maskW )

      if (this%use_RP .or. this%use_IB) then
        call this%fields%Add('SA',   0, this%SA    )
        call this%fields%Add('ibVF', 0, this%ibVF  )
        call this%fields%Add('ibF1', 0, this%ibF(1))
        call this%fields%Add('ibF2', 0, this%ibF(2))
        call this%fields%Add('ibF3', 0, this%ibF(3))
        call this%fields%Add('ibN1', 0, this%ibN(1))
        call this%fields%Add('ibN2', 0, this%ibN(2))
        call this%fields%Add('ibN3', 0, this%ibN(3))
      end if

      ! Read initial conditions from disk
      call this%fields%Read(this%timer%iter,this%timer%time,[character(len=str8) :: 'U','V','W','P'])

      return
    end subroutine cdifs_obj_PrepareSolverFields
    subroutine cdifs_obj_PrepareSolverBCS(this)
      !> Initialize boundary conditions
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      real(wp)  :: time
      integer   :: iter
      character(len=str64) :: filename

      call this%parser%Get("BC file",   filename, default = 'bcs.hdf5' )

      ! Initialize and read boundary conditions
      call this%bcs%Initialize(this%block,this%parallel)
      call this%bcs%Read(iter,time,filename)

      ! Create masks
      call this%bcs%BuildMask('U',this%maskU)
      call this%bcs%BuildMask('V',this%maskV)
      call this%bcs%BuildMask('W',this%maskW)

      ! Update boundary conditions for U,V,W,P
      call this%bcs%UpdateBoundary(this%U)
      call this%bcs%UpdateBoundary(this%V)
      call this%bcs%UpdateBoundary(this%W)
      call this%bcs%UpdateBoundary(this%P)
      return
    end subroutine cdifs_obj_PrepareSolverBCS
    subroutine cdifs_obj_PrepareSolverMonitor(this)
      !> Initialize monitors used by this solver.
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      ! Initialize monitors
      call this%monitors%initialize(this%parallel)

      ! Main monitor: writes to stdout
      call this%monitors%Create('stdout',8,stdout=.true.       )
      call this%monitors%Set   ('stdout',1,label='Iteration'   )
      call this%monitors%Set   ('stdout',2,label='Time'        )
      call this%monitors%Set   ('stdout',3,label='max|Vx|'     )
      call this%monitors%Set   ('stdout',4,label='max|Vy|'     )
      call this%monitors%Set   ('stdout',5,label='max|Vz|'     )
      call this%monitors%Set   ('stdout',6,label='CFL'         )
      call this%monitors%Set   ('stdout',7,label='max|div(V)|' )
      call this%monitors%Set   ('stdout',8,label='Nbr P iter'  )

      ! Additional monitors: write to files
      call this%monitors%Create('Velocity',8,stdout=.false.      )
      call this%monitors%Set   ('Velocity',1,label='Iteration'   )
      call this%monitors%Set   ('Velocity',2,label='Time'        )
      call this%monitors%Set   ('Velocity',3,label='max|Vx|'     )
      call this%monitors%Set   ('Velocity',4,label='max|Vy|'     )
      call this%monitors%Set   ('Velocity',5,label='max|Vz|'     )
      call this%monitors%Set   ('Velocity',6,label='CFL'         )
      call this%monitors%Set   ('Velocity',7,label='max|div(V)|' )
      call this%monitors%Set   ('Velocity',8,label='Nbr P iter'  )

      if (this%use_RP) call this%RP%CreateMonitor()

      ! Write headers
      call this%monitors%Print(print_labels=.true.)
      return
    end subroutine cdifs_obj_PrepareSolverMonitor
    subroutine cdifs_obj_PrepareSolverOutput(this)
      !> Initialize the visualization outputs
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
      ! Work variables
      character(len=str256) :: str
      integer               :: count
      integer               :: n

      if (.not.this%parser%IsDefined("Output data")) return

      ! Parse the output string
      ! Count number of outputs
      call this%parser%Get("Output data",  str )
      count = 0
      n     = 1
      do while (n.ne.0)
        count = count + 1
        str   = adjustl(str(n:len_trim(str)))
        n     = scan(trim(str),' ')
      end do

      ! Read the outputs
      allocate(this%outputs(count))
      call this%parser%Get("Output data",  this%outputs)

      ! Get names of fields to output
      ! Note: field names must match the actual names
      ! given in cdifs_obj_PrepareSolverFields
      this%output_var = [character(str8)::]
      do n=1,size(this%outputs)
       select case (this%outputs(n))
       case ('Vel')
         this%output_var = [character(str8) :: this%output_var(:), 'Um', 'Vm', 'Wm']
       case ('P')
         this%output_var = [character(str8) :: this%output_var(:), 'P']
       case ('DIV')
         this%output_var = [character(str8) :: this%output_var(:), 'div']
       case ('SA')
         if (this%use_RP .or. this%use_IB) &
           this%output_var = [character(str8) :: this%output_var(:), 'SA']
       case ('ibF')
         if (this%use_RP .or. this%use_IB) &
           this%output_var = [character(str8) :: this%output_var(:), 'ibF1', 'ibF2', 'ibF3']
       case ('ibN')
         if (this%use_RP .or. this%use_IB) &
           this%output_var = [character(str8) :: this%output_var(:), 'ibN1', 'ibN2', 'ibN3']
       case ('ibVF')
         if (this%use_RP .or. this%use_IB) &
           this%output_var = [character(str8) :: this%output_var(:), 'ibVF']
       case default
         call this%parallel%Stop("Unknown output variables in CDIFS: "//this%outputs(n))
       end select
      end do
      return
    end subroutine cdifs_obj_PrepareSolverOutput
end submodule cdifs_PrepareSolver_smod
