!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module cdifs
  !>--------------------------------------------------------------------------
  ! Module: CDIFS
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Constant Density Incompressible Flow Solver.
  ! Available Features:
  !   - HYPRE solvers for the pressure-Poisson equation
  !   - Immersed Boundaries
  !   - Resolved Particles
  !   x Lagrangian point-particles
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapBlock
  use leapEulerian
  use leapSolver
  use leapTimer
  use leapMonitor
  use leapDiffOp
  use leapHypre
  use leapBC
  use cdifs_cases
  use particles_resolved
  use immersed_boundaries
  implicit none

  private
  public :: cdifs_obj

  type, extends(solver_obj):: cdifs_obj
    !> Constant Density Incompressible Flow Solver
    !> Simulation case
    type(cdifs_case_obj):: case                            !! Case to run
    !> Geometry
    type(block_obj)     :: block                           !! Block information
    !> Mathematical tools
    type(op_obj)        :: op                              !! Differential Operators
    type(bc_set)        :: bcs                             !! Boundary conditions
    type(hypre_obj)     :: hypre                           !! HYPRE Solvers
    !> Core fields
    type(Eulerian_set)  :: fields                          !! Eulerian data container
    type(Eulerian_obj_r):: U,V,W                           !! Fluid velocity
    type(Eulerian_obj_r):: P                               !! Fluid pressure
    type(Eulerian_obj_r):: dP                              !! Pressure correction
    !> Optional components
    logical             :: use_RP      = .false.           !! Use Resolved Particles
    logical             :: use_IB      = .false.           !! Use Immersed Boundaries
    logical             :: use_col     = .false.           !! Use collisions
    type(ResPart_set)   :: RP                              !! Resolved particles
    type(marker_set)    :: IB                              !! Immersed boundary (walls)
    type(Eulerian_obj_r):: SA                              !! Surface density function
    type(Eulerian_obj_r):: ibVF                            !! Solid volume fraction
    type(Eulerian_obj_r):: ibF(3)                          !! Immersed boundary force
    type(Eulerian_obj_r):: ibN(3)                          !! IB normals field
    !> Additional fields
    type(Eulerian_obj_r):: Uold,Vold,Wold                  !! Old fluid velocity (at n)
    type(Eulerian_obj_r):: resU,resV,resW                  !! Velocity residual
    type(Eulerian_obj_r):: rhs                             !! Right-hand side
    type(Eulerian_obj_r):: divu                            !! Divergence field
    type(Eulerian_obj_r):: Um,Vm,Wm                        !! Velocities at mid points
    !> Masks
    type(Eulerian_obj_i):: maskU
    type(Eulerian_obj_i):: maskV
    type(Eulerian_obj_i):: maskW
    !> Output parmeters
    type(monitor_set)   :: monitors                        !! Monitors to print to stdout and files
    character(len=str8), &
            allocatable :: outputs(:)                      !! List of outputs
                                                           !! (one output can contain multiple variables)
    character(len=str8), &
            allocatable :: output_var(:)                   !! Names of variables to output
    !> Flow parameters
    real(wp)            :: rho         = 1.0_wp            !! Fluid density
    real(wp)            :: visc        = 1.0_wp            !! Fluid viscosity
    real(wp)            :: gravity(3)  = 0.0_wp            !! Gravity
    !> Solver parameters
    integer             :: subit       = 2                 !! Solver sub-iteration
    !> Collision parameters
    real(wp)            :: tcol                            !! Collision time
    real(wp)            :: edry                            !! Dry restitution coefficient
    real(wp)            :: muc                             !! Coulomb friction coefficient
    integer             :: maxitp     = 1                  !! Collision substeps
    contains
      procedure :: Initialize           => cdifs_obj_Init
      procedure :: Finalize             => cdifs_obj_Final
      procedure :: SetInitialConditions => cdifs_obj_SetInitialConditions
      procedure :: PrepareSolver        => cdifs_obj_PrepareSolver
      procedure :: AdvanceSolution      => cdifs_obj_AdvanceSolution
      procedure :: Monitor              => cdifs_obj_Monitor
      procedure :: WriteRestartData     => cdifs_obj_WriteRestartData
      procedure :: WriteOutputData      => cdifs_obj_WriteOutputData
  end type cdifs_obj
  interface
    module subroutine cdifs_obj_PrepareSolver(this)
      !> Prepare data before run
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
    end subroutine cdifs_obj_PrepareSolver
    module subroutine cdifs_obj_AdvanceSolution(this)
      !> Advances solution from n to n+1
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
    end subroutine cdifs_obj_AdvanceSolution
    module subroutine cdifs_obj_Monitor(this)
      !> Analyze data and post to monitors
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
    end subroutine cdifs_obj_Monitor
    module subroutine cdifs_obj_WriteRestartData(this)
      !> Write restart data to disk
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
    end subroutine cdifs_obj_WriteRestartData
    module subroutine cdifs_obj_WriteOutputData(this)
      !> Process single-precision data for visualization
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver
    end subroutine cdifs_obj_WriteOutputData
  end interface

  contains
    subroutine cdifs_obj_Init(this,timer,parallel,parser)
      !> Initialize the solver
      implicit none
      class(cdifs_obj),         intent(inout) :: this      !! The solver
      type(timer_obj),   target,intent(in)    :: timer     !! Timer utility
      type(parallel_obj),target,intent(in)    :: parallel  !! Parallel machinery
      type(parser_obj),  target,intent(in)    :: parser    !! Parser for input file

      ! Point to the master objects
      this%timer    => timer
      this%parallel => parallel
      this%parser   => parser

      return
    end subroutine cdifs_obj_Init
    subroutine cdifs_obj_SetInitialConditions(this)
      !> Set initial conditions at n=0
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      ! Initialize case
      call this%case%Initialize(this%block,this%parallel,this%parser)

      ! Get case name
      call this%parser%Get("Case", this%case%name)

      ! Setup the initial conditions
      call this%case%Setup()

      ! Finalize case
      call this%case%Finalize

      ! Finish run
      call this%timer%EndRun()

      return
    end subroutine cdifs_obj_SetInitialConditions
    subroutine cdifs_obj_Final(this)
      ! Finalize solver
      implicit none
      class(cdifs_obj), intent(inout) :: this              !! The solver

      this%timer    => null()
      this%parallel => null()
      this%parser   => null()

      if (allocated(this%outputs))    deallocate(this%outputs)
      if (allocated(this%output_var)) deallocate(this%output_var)

      call this%RP       % Finalize()
      call this%IB       % Finalize()
      call this%hypre    % Finalize()
      call this%op       % Finalize()
      call this%fields   % Finalize()
      call this%bcs      % Finalize()
      call this%block    % Finalize()
      call this%monitors % Finalize()
      return
    end subroutine cdifs_obj_Final
end module cdifs
