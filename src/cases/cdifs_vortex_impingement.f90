!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs_cases) vortex_impingement_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Vortex impingement
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: CDIFS
  ! Description: Vortex dipole impinging on bottom wall
  ! References:
  ! --------------------------------------------------------------------------
  contains
    module subroutine cdifs_vortex_impingement(this)
      !> Define block info and initial conditions
      ! for this case
      implicit none
      class(cdifs_case_obj), intent(inout) :: this

      ! Set the block info
      call cdifs_vortex_impingement_block(this)

      ! Set the initial fields
      call cdifs_vortex_impingement_fields(this)

      ! Set boundary conditions
      call cdifs_vortex_impingement_bcs(this)

      return
    end subroutine cdifs_vortex_impingement
    subroutine cdifs_vortex_impingement_block(this)
      use leapBlock
      !> Set the block parameters
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3)
      integer :: N(3)
      integer :: ngc
      integer :: Nb(3)
      real(wp):: r
      integer :: i,j,k,dir
      character(str64) :: filename

      call this%parser%Get("Domain size",       L  )
      call this%parser%Get("Grid points",       N  )
      call this%parser%Get("Ghost cells",       ngc)
      call this%parser%Get("Partition",         Nb )
      call this%parser%Get("Stretching factor", r  )


      ! Initialize the main block
      call this%block%Initialize(ngc,this%parallel)

      ! Setup the domain periodicity
      call this%block%SetPeriodicity([.true.,.false.,.true.])

      ! Create a grid, locally refined at the wall
      ! ------------------------------------------
      associate (lo =>this%block%lo, hi=>this%block%hi)
        ! Setup bounds
        lo = [1,1,1]
        hi = N

        ! Initialize axes
        do dir=1,3
          call this%block%axis(dir)%Initialize(lo(dir),hi(dir),ngc)
        end do
        call this%block%SetConveniencePointers()
      end associate

      associate (x=>this%block%x, y=>this%block%y, z=>this%block%z, &
        lo =>this%block%lo, hi=>this%block%hi)
        ! Uniform x-axis
        do concurrent (i=lo(1):hi(1)+1)
          x(i)= (i-lo(1))*L(1)/real(hi(1)-lo(1)+1,wp)
        end do

        ! Stretched y-axis
        do concurrent (j=lo(2):hi(2)+1)
          y(j)= (j-lo(2))*L(2)/real(hi(2)-lo(2)+1,wp)
          y(j)= L(2)*(1.0_wp - tanh(r*(L(2)-y(j))/L(2))/tanh(r))
        end do

        ! Uniform z-axis
        do concurrent (k=lo(3):hi(3)+1)
          z(k)= (k-lo(3))*L(3)/real(hi(3)-lo(3)+1,wp)
        end do
      end associate

      ! Partition block for parallel initializations
      call this%block%Partition(Nb)

      ! Write block to disk
      call this%parser%Get('Block file',filename)
      call this%block%Write(filename)

      return
    end subroutine cdifs_vortex_impingement_block
    subroutine cdifs_vortex_impingement_fields(this)
      !> Set the flow fields
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(Eulerian_set)   :: fields
      type(eulerian_obj_r) :: U, V, W, P
      character(str64)     :: filename
      real(wp):: L(3)
      real(wp):: a
      real(wp):: Ud
      real(wp):: rho
      real(wp):: mu
      real(wp):: Rey
      real(wp):: r
      real(wp):: Ur, Ut
      real(wp):: costheta
      real(wp):: sintheta
      real(wp):: center(3)
      integer :: i,j,k
      real(wp),parameter:: twoPi=8.0_wp*atan(1.0_wp)

      ! Bessel firt root and constant C
      real(wp),parameter :: a1 = 3.8317059702075123115_wp
      real(wp) :: C

      ! Get parameters from input file
      call this%parser%Get("Domain size",     L     )
      call this%parser%Get('Dipole radius',   a     )
      call this%parser%Get('Dipole velocity', Ud    )
      call this%parser%Get('Dipole center',   center)
      call this%parser%Get("Fluid density",   rho   )
      call this%parser%Get("Fluid viscosity", mu    )

      ! Initialize fields
      call fields%Initialize(this%block,this%parallel)

      ! Add fields to structure (this will allocate data)
      call fields%Add('U', 1, U )
      call fields%Add('V', 2, V )
      call fields%Add('W', 3, W )
      call fields%Add('P', 0, P )

      associate (lo => this%block%lo, hi=> this%block%hi,         &
          x =>this%block%x , y =>this%block%y, z => this%block%z, &
          xm=>this%block%xm, ym=>this%block%ym,zm=> this%block%zm)

        U = 0.0_wp
        V = 0.0_wp

        ! Compute the constant
        C = 2.0_wp/J0(a1)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              ! U - velocity
              r = sqrt((x(i)-center(1))**2+(ym(j)-center(2))**2)
              costheta= (ym(j)-center(2))/(r+epsilon(1.0_wp))
              sintheta= (x (i)-center(1))/(r+epsilon(1.0_wp))

              if ((r/a).le.1.0_wp) then
                r = a1*r/a
                Ur = Ud * ( C*J1(r)/(r+epsilon(1.0_wp)) - 1.0_WP ) * costheta
                Ut = Ud * ( 1.0_WP - C*(J0(r)-J1(r)/(r+epsilon(1.0_wp))) ) * sintheta
              else
                Ur = - Ud/(r/a)**2 * costheta
                Ut = - Ud/(r/a)**2 * sintheta
              end if
              U%cell(i,j,k) = Ur*sintheta+Ut*costheta

              ! V - velocity
              r = sqrt((xm(i)-center(1))**2+(y(j)-center(2))**2)
              costheta= (y (j)-center(2))/(r+epsilon(1.0_wp))
              sintheta= (xm(i)-center(1))/(r+epsilon(1.0_wp))


              if ((r/a).le.1.0_wp) then
                r = a1*r/a
                Ur = Ud * ( C*J1(r)/(r+epsilon(1.0_wp)) - 1.0_WP ) * costheta
                Ut = Ud * ( 1.0_WP - C*(J0(r)-J1(r)/(r+epsilon(1.0_wp))) ) * sintheta
              else
                Ur = - Ud/(r/a)**2 * costheta
                Ut = - Ud/(r/a)**2 * sintheta
              end if
              V%cell(i,j,k) = Ur*costheta-Ut*sintheta
            end do
          end do
        end do

        W = 0.0_wp
        P = 0.0_wp

        ! Reynolds number
        Rey = rho*Ud*a/(mu+epsilon(1.0_wp))
        write(*,*) "Reynolds number = ", Rey
      end associate

      ! Write data to disk
      call this%parser%Get('Fields IC file',  filename)
      call fields%SetWriteFileName(filename)
      call fields%Write(0,0.0_wp)
      call fields%WriteSilo(0,0.0_wp)

      ! Clear data
      call fields%Finalize()
      return
    end subroutine cdifs_vortex_impingement_fields
    subroutine cdifs_vortex_impingement_bcs(this)
      !> Set boundary conditions
      use leapBC
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(bc_set)         :: bcs
      real(wp)             :: xhi(3),xlo(3),L(3)

      ! Initialize utility that handles boundary conditions
      call bcs%Initialize(this%block,this%parallel)

      ! Get the lid velocity
      call this%parser%Get("Domain size", L )

      ! Setup regions where boundary conditions apply
      ! - Bottom boundary
      xlo = [0.0_wp, 0.0_wp, 0.0_wp]
      xhi = [L(1),   0.0_wp, L(3)  ]
      call bcs%Add('Bottom', xlo, xhi, normal = '+x2')

      ! - Top boundary
      xlo = [0.0_wp, L(2),   0.0_wp]
      xhi = [L(1),   L(2),   L(3)  ]
      call bcs%Add('Top', xlo, xhi, normal = '-x2')

      call bcs%SetBC('Top',    BC_OUTFLOW)
      call bcs%SetBC('Bottom', BC_WALL   )

      ! Write boundary conditions
      call bcs%Write(0,0.0_wp)

      ! Clear data
      call bcs%Finalize()
      return
    end subroutine cdifs_vortex_impingement_bcs
    pure function J0(x) result(val)
      !> Bessel function J0(x)
      implicit none
      real(wp), intent(in) :: x
      real(wp)             :: val
      ! work variables
      real(wp) :: ax,xx,z,y
      real(wp), parameter :: r(6) = [ 57568490574.0_wp,-13362590354.0_wp, 651619640.7_wp,&
                                     -11214424.18_wp,   77392.33017_wp,  -184.9052456_wp]
      real(wp), parameter :: s(6) = [ 57568490411.0_wp, 1029532985.0_wp, 9494680.718_wp, &
                                      59272.64853_wp,   267.8532712_wp,  1.0_wp]
      real(wp), parameter :: p(5) = [ 1.0_wp,-0.1098628627E-2_wp, 0.2734510407E-4_wp,&
                                             -0.2073370639E-5_wp, 0.2093887211E-6_wp]
      real(wp), parameter :: q(5) = [-.1562499995E-1_wp,.1430488765E-3_wp,-.6911147651E-5_wp,&
                                      .7621095161E-6_wp,-.934945152E-7_wp]

      if (abs(x).lt.8.0_wp) then
         y   = x**2
         val = (r(1)+y*(r(2)+y*(r(3)+y*(r(4)+y*(r(5)+y*r(6)))))) / &
               (s(1)+y*(s(2)+y*(s(3)+y*(s(4)+y*(s(5)+y*s(6))))))
      else
         ax  = abs(x)
         z   = 8.0_wp/ax
         y   = z**2
         xx  = ax-.785398164_wp
         val = sqrt(.636619772_wp/ax)*(cos(xx)*(p(1)+y*(p(2)+y*(p(3)+y*(p(4)+y*p(5))))) - &
               z*sin(xx)*(q(1)+y*(q(2)+y*(q(3)+y*(q(4)+y*q(5))))))
      end if
      return
    end function J0
    pure function J1(x) result(val)
      !> Bessel function J1(x)
      implicit none
      real(wp), intent(in) :: x
      real(wp)             :: val
      ! work variables
      real(wp) :: ax,xx,z,y
      real(wp), parameter :: r(6) = [ 72362614232.0_wp,-7895059235.0_wp, 242396853.1_wp, &
                                     -2972611.439_wp,   15704.48260_wp, -30.16036606_wp]
      real(wp), parameter :: s(6) = [144725228442.0_wp, 2300535178.0_wp, 18583304.74_wp, &
                                     99447.43394_wp,    376.9991397_wp,  1.0_wp]
      real(wp), parameter :: p(5) = [1.0_wp, 0.183105E-2_wp,     -0.3516396496E-4_wp, &
                                             0.2457520174E-5_wp, -0.240337019E-6_wp]
      real(wp), parameter :: q(5) = [ .04687499995_wp,-.2002690873E-3_wp,.8449199096E-5_wp,&
                                     -.88228987E-6_wp,.105787412E-6_wp]

      if (abs(x).lt.8.0_wp) then
         y   = x**2
         val = x*(r(1)+y*(r(2)+y*(r(3)+y*(r(4)+y*(r(5)+y*r(6)))))) / &
               (s(1)+y*(s(2)+y*(s(3)+y*(s(4)+y*(s(5)+y*s(6))))))
      else
         ax  = abs(x)
         z   = 8.0_wp/ax
         y   = z**2
         xx  = ax-2.356194491_wp
         val = sqrt(.636619772_wp/ax)*(cos(xx)*(p(1)+y*(p(2)+y*(p(3)+y*(p(4)+y*p(5))))) - &
               z*sin(xx)*(q(1)+y*(q(2)+y*(q(3)+y*(q(4)+y*q(5))))))*sign(1.0_wp,x)
      end if
      return
    end function J1
end submodule vortex_impingement_smod


