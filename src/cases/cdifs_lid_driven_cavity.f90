!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs_cases) cavity_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Lid-driven cavity problem
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: CDIFS
  ! Description: Flow in cavity driven by a sliding lid
  ! References:
  ! Ghia, Ghia, and Shin (1982), "High-Re solutions for incompressible flow 
  ! using the Navier-Stokes equations and a multigrid method", Journal of 
  ! Computational Physics, Vol. 48, pp. 387-411.
  ! --------------------------------------------------------------------------
  contains
    module subroutine cdifs_lid_driven_cavity(this)
      !> Define block info and initial conditions
      ! for this case
      implicit none
      class(cdifs_case_obj), intent(inout) :: this

      ! Set the block info
      call cdifs_lid_driven_cavity_block(this)

      ! Set the initial fields
      call cdifs_lid_driven_cavity_fields(this)

      ! Set boundary conditions
      call cdifs_lid_driven_cavity_bcs(this)

      return
    end subroutine cdifs_lid_driven_cavity
    subroutine cdifs_lid_driven_cavity_block(this)
      use leapBlock
      !> Set the block parameters
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), ngc, ilo(3),ihi(3), Nb(3)
      character(str64) :: filename

      call this%parser%Get("Domain size", L  )
      call this%parser%Get("Grid points", N  )
      call this%parser%Get("Ghost cells", ngc)
      call this%parser%Get("Partition",   Nb )

      ! Domain extents
      xlo=-0.5_wp*L ; xhi= 0.5_wp*L
      ilo=[1,1,1] ; ihi=N

      ! Initialize the main block
      call this%block%Initialize(ngc,this%parallel)

      ! Setup the domain periodicity
      call this%block%SetPeriodicity([.false.,.false.,.true.])

      ! Create a uniform block
      call this%block%SetupUniformGrid(xlo,xhi,ilo,ihi)

      ! Partition block for parallel initializations
      call this%block%Partition(Nb)

      ! Write block to disk
      call this%parser%Get('Block file',filename)
      call this%block%Write(filename)

      return
    end subroutine cdifs_lid_driven_cavity_block
    subroutine cdifs_lid_driven_cavity_fields(this)
      !> Set the flow fields
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(Eulerian_set)   :: fields
      type(eulerian_obj_r) :: U, V, W, P
      character(str64)     :: filename


      ! Initialize fields
      call fields%Initialize(this%block,this%parallel)

      ! Add fields to structure (this will allocate data)
      call fields%Add('U', 1, U )
      call fields%Add('V', 2, V )
      call fields%Add('W', 3, W )
      call fields%Add('P', 0, P )

      U = 0.0_wp
      V = 0.0_wp
      W = 0.0_wp
      P = 0.0_wp

      ! Write data to disk
      call this%parser%Get('Fields IC file',  filename)
      call fields%SetWriteFileName(filename)
      call fields%Write(0,0.0_wp)

      ! Clear data
      call fields%finalize()
      return
    end subroutine cdifs_lid_driven_cavity_fields
    subroutine cdifs_lid_driven_cavity_bcs(this)
      !> Set boundary conditions
      use leapBC
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(bc_set)         :: bcs
      type(extent_obj)     :: extents
      integer              :: i,j,k
      real(wp)             :: xhi(3),xlo(3),L(3)
      real(wp)             :: Ulid

      real(wp), pointer    :: Ubc(:,:,:)
      real(wp), pointer    :: Vbc(:,:,:)
      real(wp), pointer    :: Wbc(:,:,:)

      ! Initialize utility that handles boundary conditions
      call bcs%Initialize(this%block,this%parallel)

      ! Get the lid velocity
      call this%parser%Get("Lid velocity", Ulid, default = 1.0_wp  )

      call this%parser%Get("Domain size", L )

      ! Setup regions where boundary conditions apply
      ! - Left boundary
      xlo = [-0.5_wp*L(1),-0.5_wp*L(2),-0.5_wp*L(3)]
      xhi = [-0.5_wp*L(1), 0.5_wp*L(2), 0.5_wp*L(3)]
      call bcs%Add('Left', xlo, xhi, normal = '+x1')

      ! - Right boundary
      xlo = [ 0.5_wp*L(1),-0.5_wp*L(2),-0.5_wp*L(3)]
      xhi = [ 0.5_wp*L(1), 0.5_wp*L(2), 0.5_wp*L(3)]
      call bcs%Add('Right', xlo, xhi, normal = '-x1')

      ! - Bottom boundary
      xlo = [-0.5_wp*L(1),-0.5_wp*L(2),-0.5_wp*L(3)]
      xhi = [ 0.5_wp*L(1),-0.5_wp*L(2), 0.5_wp*L(3)]
      call bcs%Add('Bottom', xlo, xhi, normal = '+x2')

      ! - Top boundary
      xlo = [-0.5_wp*L(1), 0.5_wp*L(2),-0.5_wp*L(3)]
      xhi = [ 0.5_wp*L(1), 0.5_wp*L(2), 0.5_wp*L(3)]
      call bcs%Add('Top', xlo, xhi, normal = '-x2')

      call bcs%SetBC('Top',    BC_INFLOW)
      call bcs%SetBC('Bottom', BC_WALL  )
      call bcs%SetBC('Left',   BC_WALL  )
      call bcs%SetBC('Right',  BC_WALL  )

      ! Set the inflow values
      extents = bcs%GetExtents('Top')
      call bcs%GetBCPointer('Top','U',Ubc)
      call bcs%GetBCPointer('Top','V',Vbc)
      call bcs%GetBCPointer('Top','W',Wbc)
      do k=extents%lo(3),extents%hi(3)
        do j=extents%lo(2),extents%hi(2)
          do i=extents%lo(1),extents%hi(1)
            Ubc(i,j,k) = Ulid
            Vbc(i,j,k) = 0.0_wp
            Wbc(i,j,k) = 0.0_wp
          end do
        end do
      end do

      ! Write boundary conditions
      call bcs%Write(0,0.0_wp)

      ! Clear data
      Ubc => null()
      Vbc => null()
      Wbc => null()
      call bcs%Finalize()
      return
    end subroutine cdifs_lid_driven_cavity_bcs
end submodule cavity_smod

