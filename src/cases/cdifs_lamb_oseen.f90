!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs_cases) lamb_oseen_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Lamb Oseen
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: CDIFS
  ! Description: Lamb-Oseen vortex
  ! References:
  ! Shuai, Shuai, and M. Houssem Kasbaoui. 2022. “Accelerated Decay of a
  ! Lamb–Oseen Vortex Tube Laden with Inertial Particles in Eulerian–Lagrangian 
  ! Simulations.” Journal of Fluid Mechanics 936.
  ! --------------------------------------------------------------------------
  contains
    module subroutine cdifs_lamb_oseen(this)
      !> Define block info and initial conditions
      ! for this case
      implicit none
      class(cdifs_case_obj), intent(inout) :: this

      ! Serial initialization
      if (this%parallel%nproc.ne.1) &
        call this%parallel%Stop("Case requires serial run")

      ! Set the block info
      call cdifs_lamb_oseen_block(this)

      ! Set the initial fields
      call cdifs_lamb_oseen_fields(this)

      ! Set boundary conditions
      call cdifs_lamb_oseen_bcs(this)

      return
    end subroutine cdifs_lamb_oseen
    subroutine cdifs_lamb_oseen_block(this)
      !> Set the block parameters
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), ngc, ilo(3),ihi(3)
      character(str64) :: filename

      call this%parser%Get("Domain size", L  )
      call this%parser%Get("Grid points", N  )
      call this%parser%Get("Ghost cells", ngc)

      ! Domain extents
      xlo=-0.5_wp*L ; xhi= 0.5_wp*L
      ilo=[1,1,1] ; ihi=N

      ! Initialize the main block
      call this%block%Initialize(ngc,this%parallel)

      ! Setup the domain periodicity
      call this%block%SetPeriodicity([.true.,.true.,.true.])

      ! Create a uniform block
      call this%block%SetupUniformGrid(xlo,xhi,ilo,ihi)

      ! Partition block for parallel initializations
      call this%block%Partition([1,1,1])

      ! Write block to disk
      call this%parser%Get('Block file',filename)
      call this%block%Write(filename)

      return
    end subroutine cdifs_lamb_oseen_block
    subroutine cdifs_lamb_oseen_fields(this)
      !> Set the block parameters
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(Eulerian_set)   :: fields
      type(eulerian_obj_r) :: U, V, W, P
      real(wp):: L(3)
      real(wp):: radius
      real(wp):: circ
      real(wp):: rho
      real(wp):: mu
      real(wp):: Rey
      real(wp):: r
      real(wp):: sgn
      real(wp):: c(2)
      real(wp):: coef
      integer :: i,j,k,n
      real(wp),parameter:: twoPi=8.0_wp*atan(1.0_wp)

      ! Initialize fields
      call fields%Initialize(this%block,this%parallel)
      call this%parser%Get('Fields IC file',  fields%write_file)

      call this%parser%Get("Domain size",     L     )
      call this%parser%Get('Vortex radius',   radius)
      call this%parser%Get('Circulation',     circ  )
      call this%parser%Get("Fluid density",   rho   )
      call this%parser%Get("Fluid viscosity", mu    )

      ! Add fields to structure (this will allocate data)
      call fields%Add('U', 1, U )
      call fields%Add('V', 2, V )
      call fields%Add('W', 3, W )
      call fields%Add('P', 0, P )


      associate (lo => this%block%lo, hi=> this%block%hi,          &
          x =>this%block%x , y =>this%block%y,  z => this%block%z, &
          xm=>this%block%xm, ym=>this%block%ym, zm=> this%block%zm)

        do n=1,4
          select case (n)
          case (1)
            c(1) =  L(1)/4.0_wp
            c(2) =  L(2)/4.0_wp
          case (2)
            c(1) = -L(1)/4.0_wp
            c(2) =  L(2)/4.0_wp
          case (3)
            c(1) = -L(1)/4.0_wp
            c(2) = -L(2)/4.0_wp
          case (4)
            c(1) =  L(1)/4.0_wp
            c(2) = -L(2)/4.0_wp
          end select

          sgn=(-1)**n
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                ! U component
                r = sqrt((x(i)-c(1))**2+(ym(j)-c(2))**2)
                coef = sgn*circ/twoPi/(r+epsilon(1.0_wp))*(1.0_WP-exp(-r**2/radius**2))
                U%cell(i,j,k) = U%cell(i,j,k) - coef*(ym(j)-c(2))/(r+epsilon(1.0_wp))

                ! V component
                r = sqrt((xm(i)-c(1))**2+(y(j)-c(2))**2)
                coef = sgn*circ/twoPi/(r+epsilon(1.0_wp))*(1.0_WP-exp(-r**2/radius**2))
                V%cell(i,j,k) = V%cell(i,j,k) + coef*(xm(i)-c(1))/(r+epsilon(1.0_wp))
              end do
            end do
          end do
        end do

        W = 0.0_wp
        P = 0.0_wp

        ! Reynolds number
        Rey = (circ/twoPi/radius)*radius*rho/(mu+epsilon(1.0_wp))
        write(*,*) "Reynolds number = ", Rey
        write(*,*) "Vortex time     = ", twoPi*radius**2/circ
        write(*,*) "radius/dx       = ", radius/(this%block%dx(1))
        write(*,*) "radius/dy       = ", radius/(this%block%dx(2))
        write(*,*) "radius/dz       = ", radius/(this%block%dx(3))
      end associate

      ! Write data to disk
      call fields%Write(0,0.0_wp)

      ! Clear data
      call fields%Finalize()
      return
    end subroutine cdifs_lamb_oseen_fields
    subroutine cdifs_lamb_oseen_bcs(this)
      !> Set boundary conditions
      use leapBC
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(bc_set)         :: bcs

      ! Initialize utility that handles boundary conditions
      call bcs%Initialize(this%block,this%parallel)

      ! Fully-periodic, nothing to do

      ! Write boundary conditions
      call bcs%Write(0,0.0_wp)

      ! Clear data
      call bcs%Finalize()
      return
    end subroutine cdifs_lamb_oseen_bcs
end submodule lamb_oseen_smod

