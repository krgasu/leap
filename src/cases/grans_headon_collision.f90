!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans_cases) headon_collision_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Head-on Collision
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: GRANS
  ! Description: Head-on Collision. Performs head-on collisions between
  ! point particles (PP) and/or resolved particles (RP).
  ! References:
  ! --------------------------------------------------------------------------
  contains
    module subroutine grans_headon_collision(this)
      !> Setup block parameters and initial conditions
      ! for this case
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      logical :: use_RP = .false.
      logical :: use_PP = .false.

      ! Serial initialization
      if (this%parallel%nproc.ne.1) &
        call this%parallel%stop("Case requires serial run")

      ! Set the block info
      call grans_headon_collision_block(this)

      ! Set remaining data
      call this%parser%get("Use RP", use_RP, .false.  )
      call this%parser%get("Use PP", use_PP, .false.  )

      if (use_RP) call grans_headon_collision_RP(this)
      if (use_PP) call grans_headon_collision_PP(this)
      return
    end subroutine grans_headon_collision
    subroutine grans_headon_collision_block(this)
      !> Setup the simulation block
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), ngc, ilo(3),ihi(3)

      call this%parser%get("Domain size", L  )
      call this%parser%get("Grid points", N  )
      call this%parser%get("Ghost cells", ngc)

      ! Domain extents
      xlo=-0.5_wp*L ; xhi= 0.5_wp*L
      ilo=(/1,1,1/) ; ihi=N

      ! Initialize a uniform grid on this domain
      call this%block%initialize(xlo,xhi,ilo,ihi,ngc,this%parallel)
      call this%block%SetPeriodicity([.true.,.true.,.true.])
      call this%block%partition([1,1,1])

      return
    end subroutine grans_headon_collision_block
    subroutine grans_headon_collision_RP(this)
      !> Setup the resolved particles
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      type(ResPart_set) :: RP
      real(wp):: dp
      real(wp):: rhop
      real(wp):: dl
      integer :: n
      integer :: pos
      character(len=str64):: ini_file

      ! Physical variables
      ! -------------------------- !
      call this%parser%get("Particle diameter",  dp)
      call this%parser%get("Particle density", rhop)
      call this%parser%get("Facet size",          dl, 0.5_wp*minval(this%block%dx))

      ! Initialize Resolved Particle data
      call RP%initialize('RP',this%block,this%parallel)

      ! Activate 2 particles
      call RP%resize(2)

      ! Place particles in head-on collisions
      select type (particle => RP%p)
      type is (ResPart_obj)
        do concurrent ( n=1:RP%count_ )
          ! Particle global ID
          particle(n)%id = int(n, kind=8)
          ! Particle diameter
          particle(n)%d  = dp
          ! Particle position
          particle(n)%p=  (2.0_wp*real(n-1,wp)-1.0_wp)*particle(n)%d
          ! Particle velocity
          particle(n)%v= -(2.0_wp*real(n-1,wp)-1.0_wp)
          ! Particle density
          particle(n)%rho= rhop
          ! Force and Torque on particle
          particle(n)%Fh = 0.0_wp
          particle(n)%Th = 0.0_wp
          particle(n)%Fc = 0.0_wp
          particle(n)%Tc = 0.0_wp
        end do
      end select

      ! Define surface markers
      call grans_headon_collision_markers(RP,dl)

      ! IO variables
      ! -------------------------- !
      call this%parser%get('RP IC file',ini_file)

      pos=scan(trim(adjustl(ini_file)),".",BACK=.true.)
      if (pos.gt.0) ini_file=ini_file(1:pos-1)
      RP%write_file   =trim(adjustl(ini_file))//".h5"
      RP%ib%write_file=trim(adjustl(ini_file))//"_markers.h5"

      ! Apply periodicity
      call RP%ApplyPeriodicity
      call RP%ib%ApplyPeriodicity

      ! Write and finalize
      call RP%write(0,0.0_wp)
      call RP%finalize()
      return
    end subroutine grans_headon_collision_RP
    subroutine grans_headon_collision_PP(this)
      !> Setup the point particles
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      type(particle_set) :: PP
      real(wp):: dp
      real(wp):: rhop
      integer :: n
      character(len=str64):: part_type
      character(len=str64):: ini_file

      ! Physical variables
      ! -------------------------- !
      call this%parser%get("Particle diameter", dp       )
      call this%parser%get("Particle density",  rhop     )
      call this%parser%get("Particle type",     part_type, 'default')

      ! Initialize Resolved Particle data
      call PP%initialize('PP',this%block,this%parallel,part_type)

      ! Activate 2 particles
      call PP%resize(2)

      ! Place particles in head-on collisions
      select type (particle => PP%p)
      class is (particle_obj)
        do concurrent ( n=1:PP%count_ )
          ! Particle global ID
          particle(n)%id   = int(n, kind=8)
          ! Particle diameter
          particle(n)%d    = dp
          ! Particle position
          particle(n)%p(1) =  (2.0_wp*real(n-1,wp)-1.0_wp)*particle(n)%d
          particle(n)%p(2) = -(2.0_wp*real(n-1,wp)-1.0_wp)*particle(n)%d
          particle(n)%p(3) = -(2.0_wp*real(n-1,wp)-1.0_wp)*particle(n)%d
          ! Particle velocity
          particle(n)%v(1) = -(2.0_wp*real(n-1,wp)-1.0_wp)
          particle(n)%v(2) =  (2.0_wp*real(n-1,wp)-1.0_wp)
          particle(n)%v(3) =  (2.0_wp*real(n-1,wp)-1.0_wp)
          ! Particle density
          particle(n)%rho  = rhop
          ! Force and Torque on particle
          particle(n)%Fh   = 0.0_wp
          particle(n)%Th   = 0.0_wp
          particle(n)%Fc   = 0.0_wp
          particle(n)%Tc   = 0.0_wp
        end do
      end select

      ! IO variables
      ! -------------------------- !
      call this%parser%get('PP IC file',ini_file)
      PP%write_file = trim(adjustl(ini_file))

      ! Write and finalize
      call PP%write(0,0.0_wp)
      call PP%finalize()
      return
    end subroutine grans_headon_collision_PP
    subroutine grans_headon_collision_markers(RP,dl)
      !> Seed markers on the resolved particles
      implicit none
      type(ResPart_set), intent(inout) :: RP
      real(wP),          intent(in)    :: dl
      ! Work variables
      real(wp) :: dSA                                      !! Infinitesimal Surface area
      real(wp) :: radius                                   !! Infinitesimal Surface area
      real(wp) :: d_phi                                    !! Infinitesimal phi angle (0 -> 2pi)
      real(wp),allocatable :: phi(:)                       !! phi angle (0 -> 2pi)
      real(wp),allocatable :: theta(:)                     !! theta angle (0 -> pi)
      integer  :: n_phi
      integer  :: n_theta
      real(wp) :: phi_mid
      real(wp) :: theta_mid
      integer  :: i,j,old_count,n
      integer(kind=8)  :: facetID
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Place markers at center of facet
      select type (particle => RP%p)
      type is (ResPart_obj)
        do n=1,RP%count_

          ! Get radius
          radius = particle(n)%d*0.5_wp

          ! Setup target surface area and phi angle
          dSA=dl**2

          ! Theta angle
          n_phi   = ceiling(2.0_wp*Pi/(dl/radius))
          n_theta = ceiling(4.0*Pi*radius**2/dSA/n_phi)

          ! Create spherical grid
          allocate(phi  (n_phi+1)  )
          allocate(theta(n_theta+1))

          d_phi=2.0_wp*Pi/real(n_phi,wp)
          do i=1,n_phi+1
              phi(i)= (i-1)*d_phi
          end do
          do j=1,n_theta+1
              theta(j)= acos(max(-1.0_wp, 1.0_wp -(j-1)*dSA/(radius**2*d_phi)))
          end do
          theta(n_theta+1)= Pi

          old_count=RP%ib%count_
          call RP%ib%resize(old_count+n_phi*n_theta)
          select type (markers => RP%ib%p)
          type is(marker_obj)

            do i=1,n_phi
              do j=1,n_theta
                 ! Facet ID
                 facetID = old_count+n_theta*(i-1)+j
                 markers(facetID)%id=facetID

                 ! Position
                 phi_mid   = 0.5_wp*(phi(i+1)+phi(i))
                 theta_mid = 0.5_wp*(theta(j+1)+theta(j))

                 markers(facetID)%p(1)= particle(n)%p(1)+ radius*sin(theta_mid)*cos(phi_mid)
                 markers(facetID)%p(2)= particle(n)%p(2)+ radius*sin(theta_mid)*sin(phi_mid)
                 markers(facetID)%p(3)= particle(n)%p(3)+ radius*cos(theta_mid)

                 ! Surface area
                 markers(facetID)%SA = radius**2*d_phi *      &
                                     ( cos(theta(j))-cos(theta(j+1)))

                 ! Normal vector
                 markers(facetID)%n = (/ sin(theta_mid)*cos(phi_mid), &
                                         sin(theta_mid)*sin(phi_mid), &
                                         cos(theta_mid)/)
                 ! Velocity
                 markers(facetID)%v = particle(n)%v

                 ! Tag
                 markers(facetID)%s = int(particle(n)%id,leapI4)

                 ! Force
                 markers(facetID)%f = 0.0_wp

              end do
            end do
          end select
          deallocate(phi)
          deallocate(theta)
        end do
      end select

      return
    end subroutine grans_headon_collision_markers
end submodule headon_collision_smod

