!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (cdifs_cases) settling_sphere_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Settling sphere
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: CDIFS
  ! Description: A sphere settling in a quiscent flow
  ! References:
  ! Dave, H., Herrmann, M. & Kasbaoui, M. H. The volume-filtering immersed
  ! boundary method. Journal of Computational Physics 487, 112136 (2023).
  ! --------------------------------------------------------------------------
  contains
    module subroutine cdifs_settling_sphere(this)
      !> Define block info and initial conditions
      ! for this case
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      logical :: use_RP
      logical :: use_IB

      ! Set the block info
      call cdifs_settling_sphere_block(this)

      ! Set the initial fields
      call cdifs_settling_sphere_fields(this)

      ! Set resolved particles
      call this%parser%Get('Use ResPart',  use_RP, .false. )
      if (use_RP) call cdifs_settling_sphere_ResPart(this)

      ! Set the immersed boundaries
      call this%parser%Get('Use IB',       use_IB, .false. )
      if (use_IB) call cdifs_settling_sphere_IB(this)

      ! Set boundary conditions
      call cdifs_settling_sphere_bcs(this)

      return
    end subroutine cdifs_settling_sphere
    subroutine cdifs_settling_sphere_block(this)
      !> Set the block parameters
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), ngc, ilo(3),ihi(3), Nb(3)
      character(str64) :: filename

      call this%parser%Get("Domain size", L  )
      call this%parser%Get("Grid points", N  )
      call this%parser%Get("Ghost cells", ngc)
      call this%parser%Get("Partition",   Nb )

      ! Domain extents
      xlo=-0.5_wp*L ; xhi= 0.5_wp*L
      ilo=[1,1,1] ; ihi=N

      ! Initialize the main block
      call this%block%Initialize(ngc,this%parallel)

      ! Setup the domain periodicity
      call this%block%SetPeriodicity([.false.,.true.,.false.])

      ! Create a uniform block
      call this%block%SetupUniformGrid(xlo,xhi,ilo,ihi)

      ! Partition block for parallel initializations
      call this%block%Partition(Nb)

      ! Write block to disk
      call this%parser%Get('Block file',filename)
      call this%block%Write(filename)

      return
    end subroutine cdifs_settling_sphere_block
    subroutine cdifs_settling_sphere_fields(this)
      !> Set the flow fields
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(Eulerian_set)   :: fields
      type(eulerian_obj_r) :: U, V, W, P
      character(str64)     :: filename


      ! Initialize fields
      call fields%Initialize(this%block,this%parallel)

      ! Add fields to structure (this will allocate data)
      call fields%Add('U', 1, U )
      call fields%Add('V', 2, V )
      call fields%Add('W', 3, W )
      call fields%Add('P', 0, P )

      U = 0.0_wp
      V = 0.0_wp
      W = 0.0_wp
      P = 0.0_wp

      ! Write data to disk
      call this%parser%Get('Fields IC file',  filename)
      call fields%SetWriteFileName(filename)
      call fields%write(0,0.0_wp)

      ! Clear data
      call fields%Finalize()
      return
    end subroutine cdifs_settling_sphere_fields
    subroutine cdifs_settling_sphere_ResPart(this)
      !> Set the flow fields
      use particles_resolved
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(ResPart_set) :: RP                              !! Resolved particles
      real(wp)          :: diam
      real(wp)          :: rhop
      real(wp)          :: dl
      character(str64)  :: filename

      ! Initialze resolved particles
      call RP%Initialize('ResPart',this%block,this%parallel)
      call this%parser%Get("Particle diameter", diam)
      call this%parser%Get("Particle density",  rhop)


      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        ! Activate 1 particle
        call RP%Resize(1)

        dl=0.5_wp*minval(this%block%dx)

        select type(particle => RP%p)
        type is (ResPart_obj)
          ! Particle globabl ID
          particle(1)%id   = int(1,kind=8)
          ! Diameter
          particle(1)%d    = diam
          ! Position
          particle(1)%p(1) = 0.0_wp
          particle(1)%p(2) = this%block%pmax(2)-5.0_wp*diam
          particle(1)%p(3) = 0.0_wp
          ! Velocity
          particle(1)%v    = 0.0_wp
          ! Angular velocity
          particle(1)%w    = 0.0_wp
          ! Density
          particle(1)%rho  = rhop
          ! Zero force and torque
          particle(1)%Fh   = 0.0_wp
          particle(1)%Th   = 0.0_wp
          particle(1)%Fc   = 0.0_wp
          particle(1)%Tc   = 0.0_wp

          ! Add surface markers
          call add_markers(RP,particle(1)%id,particle(1)%p,particle(1)%v,particle(1)%d/2.0_wp,dl)
        end select

      end if

      ! Treatment for periodicity
      call RP%ApplyPeriodicity
      call RP%ib%ApplyPeriodicity

      ! Send to the right rank
      call RP%Communicate
      call RP%ib%Communicate

      ! Localize centers and markers on grid
      call RP%Localize
      call RP%ib%Localize

      ! Write data to disk
      call this%parser%Get('RP IC file', filename   )
      call RP%SetWriteFileName(filename)
      call RP%Write(0,0.0_WP)

      ! Finalize
      call RP%Finalize
    end subroutine cdifs_settling_sphere_ResPart
    subroutine cdifs_settling_sphere_IB(this)
      !> Set the immersed boundaries
      use immersed_boundaries
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      real(wp)          :: Lb                              !! Channel width
      type(marker_set)  :: IB                              !! Immersed boundary (walls)
      character(str64)  :: filename
      real(wp)          :: hm
      integer           :: ns, nh
      real(wp)          :: ds, dh, Hb
      integer           :: i,j,m

      ! Initialze resolved particles
      call IB%Initialize('IB',this%block,this%parallel)
      call this%parser%Get("Channel width", Lb)

      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        ! Count the number of markers
        ns = ceiling(Lb/(0.5_wp*minval(this%block%dx)))
        ds = Lb/real(ns,wp)

        Hb = (this%block%pmax(2)-this%block%pmin(2))
        nh = ceiling(Hb/(0.5_wp*minval(this%block%dx)))
        dh = Hb/real(nh,wp)

        call IB%Resize(4*nh*ns)

        select type (markers=>IB%p)
        type is (marker_obj)
          ! Add box walls
          m = 0
          do j=1,nh
            ! Height at this location
            hm = (real(j,wp)+0.5_wp)*dh + this%block%pmin(2)
            do i=1,ns
              ! Side at z=-Lb/2.0_wp
              m = m + 1
              markers(m)%id = m
              markers(m)%p  = [-Lb/2.0_wp, hm,-Lb/2.0_wp] &
                            + [(real(i,wp)+0.5_wp)*ds, 0.0_wp,0.0_wp]
              markers(m)%SA = ds*dh
              markers(m)%v  = 0.0_wp
              markers(m)%n  = [ 0.0_wp, 0.0_wp, 1.0_wp]
              ! Side at z=+Lb/2.0_wp
              m = m + 1
              markers(m)%id = m
              markers(m)%p  = [-Lb/2.0_wp, hm, Lb/2.0_wp] &
                            + [(real(i,wp)+0.5_wp)*ds, 0.0_wp,0.0_wp]
              markers(m)%SA = ds*dh
              markers(m)%v  = 0.0_wp
              markers(m)%n  = [ 0.0_wp, 0.0_wp,-1.0_wp]
              ! Side at x=-Lb/2.0_wp
              m = m + 1
              markers(m)%id = m
              markers(m)%p  = [-Lb/2.0_wp, hm,-Lb/2.0_wp] &
                            + [0.0_wp,0.0_wp,(real(i,wp)+0.5_wp)*ds]
              markers(m)%SA = ds*dh
              markers(m)%v  = 0.0_wp
              markers(m)%n  = [ 1.0_wp, 0.0_wp, 0.0_wp]
              ! Side at x=+Lb/2.0_wp
              m = m + 1
              markers(m)%id = m
              markers(m)%p  = [ Lb/2.0_wp, hm,-Lb/2.0_wp] &
                            + [0.0_wp,0.0_wp,(real(i,wp)+0.5_wp)*ds]
              markers(m)%SA = ds*dh
              markers(m)%v  = 0.0_wp
              markers(m)%n  = [-1.0_wp, 0.0_wp, 0.0_wp]
            end do
          end do

        end select
      end if

      ! Treatment for periodicity
      call IB%ApplyPeriodicity

      ! Send to the right rank
      call IB%Communicate

      ! Localize marker points
      call IB%Localize

      ! Write data to disk
      call this%parser%Get('IB IC file', filename   )
      call IB%SetWriteFileName(filename)
      call IB%Write(0,0.0_WP)

      ! Finalize
      call IB%Finalize
    end subroutine cdifs_settling_sphere_IB
    subroutine cdifs_settling_sphere_bcs(this)
      !> Set boundary conditions
      use leapBC
      implicit none
      class(cdifs_case_obj), intent(inout) :: this
      ! Work variables
      type(bc_set)         :: bcs
      real(wp)             :: xhi(3),xlo(3)
      real(wp), pointer    :: BCVal(:,:,:)
      logical              :: use_IB

      ! Initialize utility that handles boundary conditions
      call bcs%Initialize(this%block,this%parallel)

      ! Setup regions where boundary conditions apply
      associate (pmin => this%block%pmin, pmax => this%block%pmax)

        xlo = [pmin(1),pmin(2),pmin(3)]
        xhi = [pmin(1),pmax(2),pmax(3)]
        call bcs%Add('xL', xlo, xhi, normal = '+x1')

        xlo = [pmax(1),pmin(2),pmin(3)]
        xhi = [pmax(1),pmax(2),pmax(3)]
        call bcs%Add('xR', xlo, xhi, normal = '-x1')

        xlo = [pmin(1),pmin(2),pmax(3)]
        xhi = [pmax(1),pmax(2),pmax(3)]
        call bcs%Add('zR', xlo, xhi, normal = '-x3')

        xlo = [pmin(1),pmin(2),pmin(3)]
        xhi = [pmax(1),pmax(2),pmin(3)]
        call bcs%Add('zL', xlo, xhi, normal = '+x3')

      end associate

      call bcs%SetBC('xL', BC_OUTFLOW)
      call bcs%SetBC('xR', BC_OUTFLOW)
      call bcs%SetBC('zL', BC_OUTFLOW)
      call bcs%SetBC('zR', BC_OUTFLOW)

      call bcs%SetBC('xL', BC_DIRICHLET,'ibVF')
      call bcs%SetBC('xR', BC_DIRICHLET,'ibVF')
      call bcs%SetBC('zL', BC_DIRICHLET,'ibVF')
      call bcs%SetBC('zR', BC_DIRICHLET,'ibVF')

      call this%parser%Get('Use IB',       use_IB, .false. )
      if (.not.use_IB) then
        call bcs%GetBCPointer('xL','ibVF',BCVal); BCVal=0.0_wp
        call bcs%GetBCPointer('xR','ibVF',BCVal); BCVal=0.0_wp
        call bcs%GetBCPointer('zL','ibVF',BCVal); BCVal=0.0_wp
        call bcs%GetBCPointer('zR','ibVF',BCVal); BCVal=0.0_wp
      else
        call bcs%GetBCPointer('xL','ibVF',BCVal); BCVal=1.0_wp
        call bcs%GetBCPointer('xR','ibVF',BCVal); BCVal=1.0_wp
        call bcs%GetBCPointer('zL','ibVF',BCVal); BCVal=1.0_wp
        call bcs%GetBCPointer('zR','ibVF',BCVal); BCVal=1.0_wp
      end if

      ! Write boundary conditions
      call bcs%Write(0,0.0_wp)

      ! Clear data
      call bcs%Finalize()
      return
    end subroutine cdifs_settling_sphere_bcs
    subroutine add_markers(RP,id,centroid,vel,radius,facet_size)
      use particles_resolved
      use immersed_boundaries, only : marker_obj,marker_set
      implicit none
      type(ResPart_set),intent(inout) :: RP                !! Resolved particles
      integer(kind=8),  intent(in)    :: id                !! ID of parent resolved particle
      real(wp),         intent(in)    :: centroid(3)       !! Position of centroid
      real(wp),         intent(in)    :: vel(3)            !! Velocity of centroid
      real(wp),         intent(in)    :: radius            !! Particle radius
      real(wp),         intent(in)    :: facet_size        !! Characteristic facet size
      ! Work variables
      real(wp) :: dSA                                      !! Infinitesimal Surface area
      real(wp) :: d_phi                                    !! Infinitesimal phi angle (0 -> 2pi)
      real(wp) :: d_theta                                  !! Infinitesimal theta angle (0 -> pi)
      real(wp),allocatable :: theta(:)                     !! theta angle (0 -> pi)
      integer  :: n_theta
      integer  :: n_phi
      real(wp) :: phi_mid
      real(wp) :: theta_mid
      integer  :: i,j
      integer  :: element_count
      integer(kind=8) :: facetID
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp)

      ! Setup target surface area and phi angle
      dSA=facet_size**2

      ! Split the lattitude into segments of size=facet_size
      n_theta = ceiling(Pi*radius/facet_size)
      d_theta = Pi/real(n_theta,wp)

      ! Create the nodal values for the lattitude angle
      allocate(theta(n_theta+1))
      do i=1,n_theta+1
        theta(i)= real(i-1,wp)*d_theta
      end do

      ! Count the total number of elements to seed
      element_count = 0
      do i=1,n_theta
        theta_mid = 0.5_wp*(theta(i)+theta(i+1))

        ! Split the longitude such that the area of each
        ! element is approximately equal to facet_size^2
        d_phi = dSA/(radius**2*(cos(theta(i))-cos(theta(i+1)) ))

        ! Add to total number of elements
        element_count = element_count + ceiling(2.0_wp*Pi/d_phi)
      end do
      ! Create n_phi*n_theta markers
      call RP%ib%Resize(RP%ib%count_ + element_count)

      ! Place markers at center of facets
      facetID = RP%ib%count_ - element_count
      select type(markers =>RP%ib%p)
      type is (marker_obj)
        do i=1,n_theta

          ! Lattitude angle at mid point
          theta_mid = 0.5_wp*(theta(i)+theta(i+1))

          ! Discretize the longitudinal direction
          d_phi = dSA/(radius**2*(cos(theta(i))-cos(theta(i+1)) ))
          n_phi = ceiling(2.0_wp*Pi/d_phi)
          d_phi = 2.0_wp*Pi/real(n_phi,wp)

          do j=1,n_phi

            ! Longitude angle at mid point
            phi_mid = 0.5_wp*(real(j-1,wp)+real(j,wp))*d_phi

            ! Facet ID
            facetID = facetID + int(1,kind=8)
            markers(facetID)%id = facetID

            ! Position
            markers(facetID)%p(1) = centroid(1) + radius*sin(theta_mid)*cos(phi_mid)
            markers(facetID)%p(2) = centroid(2) + radius*sin(theta_mid)*sin(phi_mid)
            markers(facetID)%p(3) = centroid(3) + radius*cos(theta_mid)

            ! Surface area
            markers(facetID)%SA = radius**2*d_phi *(cos(theta(i))-cos(theta(i+1)))

            ! Normal vector
            markers(facetID)%n = [sin(theta_mid)*cos(phi_mid),sin(theta_mid)*sin(phi_mid),cos(theta_mid)]

            ! Velocity
            markers(facetID)%v = vel

            ! Tag
            markers(facetID)%s = int(id,kind=4)

            ! Force
            markers(facetID)%f = 0.0_wp

          end do
        end do
      end select

      deallocate(theta)

      return
    end subroutine add_markers
end submodule settling_sphere_smod

