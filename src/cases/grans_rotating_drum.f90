!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (grans_cases) rotating_drum_smod
  !>--------------------------------------------------------------------------
  ! Submodule: Rotating drum
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Solver: GRANS
  ! Description: Granular particles in a rotating drum.
  ! References:
  ! Sack, A., and Pöschel, T., 2016, “Dissipation of Energy by Dry Granular 
  ! Matter in a Rotating Cylinder,” Sci Rep, 6(1), p. 26833.
  ! --------------------------------------------------------------------------
  contains
    module subroutine grans_rotating_drum(this)
      !> Setup block parameters and initial conditions
      ! for this case
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      logical :: use_IB = .false.
      logical :: use_PP = .false.

      ! Serial initialization
      if (this%parallel%nproc.ne.1) &
        call this%parallel%stop("Case requires serial run")

      ! Set the block info
      call grans_rotating_drum_block(this)

      ! Set remaining data
      call this%parser%get("Use IB", use_IB, .true.   )
      call this%parser%get("Use PP", use_PP, .true.   )

      if (use_IB) call grans_rotating_drum_IB(this)
      if (use_PP) call grans_rotating_drum_PP(this)
      return
    end subroutine grans_rotating_drum
    subroutine grans_rotating_drum_block(this)
      !> Setup the simulation block
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      real(wp):: L(3),xlo(3),xhi(3)
      integer :: N(3), ngc, ilo(3),ihi(3)

      call this%parser%get("Domain size", L  )
      call this%parser%get("Grid points", N  )
      call this%parser%get("Ghost cells", ngc)

      ! Domain extents
      xlo=-0.5_wp*L ; xhi= 0.5_wp*L
      ilo=(/1,1,1/) ; ihi=N

      ! Initialize a uniform grid on this domain
      call this%block%initialize(xlo,xhi,ilo,ihi,ngc,this%parallel)
      call this%block%SetPeriodicity([.true.,.true.,.true.])
      call this%block%partition([1,1,1])

      return
    end subroutine grans_rotating_drum_block
    subroutine grans_rotating_drum_IB(this)
      !> Setup the immersed boundary
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      type(marker_set) :: IB
      real(wp):: L(3)     ! Domain size
      real(WP):: diam     ! Diameter of cylinder
      real(WP):: d_theta  ! angular element size
      integer :: n_axis   ! count of elements in the axis direction
      integer :: n_theta  ! count of angular elements
      integer :: i,j,id   ! Iterator
      real(wp):: dl
      character(len=str_medium):: ini_file
      real(WP), parameter :: Pi=4.0*atan(1.0_wp)

      ! Physical variable
      ! -------------------------- !
      call this%parser%get("Drum diameter",      Diam )
      call this%parser%get("Facet size",         dl, 0.5_wp*minval(this%block%dx))

      ! Initialize IB data
      call IB%initialize('IB',this%block,this%parallel)

      ! Create markers
      L       = this%block%xhi -this%block%xlo
      n_theta = ceiling(Pi*Diam/dl)
      d_theta = 2.0_wp*Pi/real(n_theta,wp)
      n_axis  = max(1,floor(L(3)/dl))
      dl      = L(3)/real(n_axis,wp)
      call IB%resize(n_theta*n_axis)

      ! Cylinder body
      select type (markers=>IB%p)
      type is (marker_obj)
        do i=1,n_theta
          do j=1,n_axis
            ! ID
            id = n_axis*(i-1)+j
            markers(id)%id = int(id,kind=8)
            ! Position
            markers(id)%p(1)= 0.0_wp+0.5_wp*Diam*cos((i-1)*d_theta + d_theta/2.0_wp)
            markers(id)%p(2)= 0.0_wp+0.5_wp*Diam*sin((i-1)*d_theta + d_theta/2.0_wp)
            markers(id)%p(3)= 0.0_wp+(j-1)*dl + dl/2.0_wp - 0.5_wp*L(3)
            ! Surface Area
            markers(id)%SA  = 0.5_wp*Diam*d_theta*dl
            ! Normal
            markers(id)%n(1)= -cos((i-1)*d_theta + d_theta/2.0_wp)
            markers(id)%n(2)= -sin((i-1)*d_theta + d_theta/2.0_wp)
            markers(id)%n(3)= 0.0_wp
            ! velocity
            markers(id)%v = 0.0_wp
          end do
        end do
      end select

      ! IO variables
      ! -------------------------- !
      call this%parser%get('IB IC file',ini_file)
      IB%write_file=trim(adjustl(ini_file))

      ! Write and finalize
      call IB%write(0,0.0_wp)
      call IB%finalize()
      return
    end subroutine grans_rotating_drum_IB
    subroutine grans_rotating_drum_PP(this)
      !> Setup the point particles
      implicit none
      class(grans_case_obj), intent(inout) :: this
      ! Work variables
      type(particle_set) :: PP
      integer :: Np
      real(wp):: dp
      real(wp):: xlo(3)
      real(wp):: xhi(3)
      real(wp):: rhop
      real(wp):: rand(3)
      real(wp):: Diam
      integer :: n
      character(len=str64):: part_type
      character(len=str64):: ini_file

      ! Physical variables
      ! -------------------------- !
      call this%parser%get("Number of particles", Np       )
      call this%parser%get("Drum diameter",       Diam     )
      call this%parser%get("Particle diameter",   dp       )
      call this%parser%get("Particle density",    rhop     )
      call this%parser%get("Particle type",       part_type, 'default')

      ! Initialize Resolved Particle data
      call PP%initialize('PP',this%block,this%parallel,part_type)

      ! Activate 2 particles
      call PP%resize(Np)

      xlo= [-0.5_wp*Diam/sqrt(2.0_wp)+0.5_wp*dp,-0.5_wp*Diam/sqrt(2.0_wp)+0.5_wp*dp,this%block%xlo(3)]
      xhi= [ 0.5_wp*Diam/sqrt(2.0_wp)-0.5_wp*dp, 0.5_wp*Diam/sqrt(2.0_wp)-0.5_wp*dp,this%block%xhi(3)]
      ! Place particles in head-on collisions
      select type (particle => PP%p)
      class is (particle_obj)
        do n=1,PP%count_
          ! Particle global ID
          particle(n)%id = int(n, kind=8)
          ! Particle diameter
          particle(n)%d  = dp
          ! Particle position
          call random_number(rand)
          particle(n)%p = xlo+rand*(xhi-xlo)
          ! Particle velocity
          call random_number(rand)
          particle(n)%v  = 2.0_wp*(rand-0.5_wp)
          ! Particle density
          particle(n)%rho= rhop
          ! Force and Torque on particle
          particle(n)%Fh = 0.0_wp
          particle(n)%Th = 0.0_wp
          particle(n)%Fc = 0.0_wp
          particle(n)%Tc = 0.0_wp
        end do
      end select

      ! Apply periodicity
      call PP%ApplyPeriodicity

      ! IO variables
      ! -------------------------- !
      call this%parser%get('PP IC file',ini_file)
      PP%write_file = trim(adjustl(ini_file))

      ! Write and finalize
      call PP%write(0,0.0_wp)
      call PP%finalize()
      return
    end subroutine grans_rotating_drum_PP
end submodule rotating_drum_smod

