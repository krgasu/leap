!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2021 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !>--------------------------------------------------------------------------
  ! Program: LEAP
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Main leap program: runs specified solvers
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapSolver
  use leapParser
  use leapParallel
  use leapTimer
  use leapCli
  use grans
  use cdifs
  implicit none
  type(cli_obj)                  :: cli                    !! Command line utility
  type(parallel_obj)             :: parallel               !! Utility that interfaces with MPI API
  type(parser_obj)               :: parser                 !! Parses input files
  character(len=64)              :: solver_name            !! Name of solver to be used
  class(solver_obj), allocatable :: solver                 !! Solver to run
  type(timer_obj)                :: timer                  !! Timing utility
  character(len=3)               :: mode                   !! Execution mode: initial conditions or run

  ! Initialize parser
  call parser%Initialize()

  ! Parse input file
  call parser%ParseFile()

  ! Initialize parallel environment
  call parallel%Initialize()

  ! Initialize timer
  call timer%Initialize(parser)

  ! Get solver info
  call parser%Get("Solver",solver_name)

  select case (trim(adjustl(solver_name)))
  case ("grans", "Grans", "GRANS")
    ! GranS: Granular flow solver
    allocate(grans_obj:: solver)
  case ("cdifs", "Cdifs", "CDIFS")
    ! CDIFS: Constant Density Incompressible Flow Solver
    allocate(cdifs_obj:: solver)
  case default
    call parallel%stop("LEAP: Unknown solver")
  end select

  ! Initialize solver
  call solver%Initialize(timer,parallel,parser)

  ! Determine if we need to set initial
  ! conditions, or run the solver
  call cli%Get('mode',mode,default="RUN")
  select case (mode)
  case ("ini", "Ini", "INI")
    call solver%SetInitialConditions()
  case ("run", "Run", "RUN")
    call solver%PrepareSolver()
  case default
    call parallel%stop("LEAP: invalid execution mode")
  end select

  ! Advance solution
  do while ( .not. timer%Done() )
    ! Update timer to new time step
    call timer%StepForward()
    ! Advance solution from n to n+1
    call solver%AdvanceSolution()
    ! Write monitor files
    call solver%Monitor()
    ! Write restart data
    if ( timer%TimeToWriteRestartData()) call solver%WriteRestartData()
    ! Write output data
    if ( timer%TimeToWriteOutputData())  call solver%WriteOutputData()
  end do

  ! Finalize
  call solver%finalize()
  call timer%finalize()
  call parser%finalize()
  call parallel%finalize()
end program main
