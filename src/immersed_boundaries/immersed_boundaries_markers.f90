!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module immersed_boundaries_markers
  !>--------------------------------------------------------------------------
  ! Module: immersed_boundaries_markers
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Treatment of surface markers for direct-forcing immersed boundary
  ! methods
  ! --------------------------------------------------------------------------
  use leapIO
  use leapKinds
  use leapLagrangian,      only : lagrangian_obj, lagrangian_set
  use leapEulerian,        only : eulerian_obj_r,eulerian_set
  use leapBlock,           only : block_obj
  use leapParallel,        only : parallel_obj
  use leapHypre,           only : hypre_obj
  use leapParser,          only : parser_obj
  use leapDiffOp,          only : op_obj
  use leapTimer,           only : timer_obj 
  use leapMonitor,         only : monitor_set
  implicit none
  private
  public :: marker_obj, marker_set

  type, extends (lagrangian_obj) :: marker_obj
    !> An extended Lagrangian object that represents a
    ! marker placed at the centroid of a triangular element
    ! on the surface of an immersed body
    integer  :: s                                          !! A tag
    integer  :: t                                          !! Another tag
    real(WP) :: SA                                         !! Sufrace area
    real(WP) :: n(3)                                       !! normal at the marker
    real(WP) :: v(3)                                       !! marker velocity
    real(WP) :: f(3)                                       !! marker forcing
    real(WP) :: pold(3)                                    !! Old position
    real(WP) :: vold(3)                                    !! Old velocity
    contains
      procedure :: Info                => marker_obj_Info
      procedure :: assign              => marker_obj_assign
  end type marker_obj

  type,extends(lagrangian_set) :: marker_set
    !> A collection of IB markers
    type(timer_obj),   pointer :: timer     => null()      !! Timer utility
    type(parser_obj),  pointer :: parser    => null()      !! Parser for input file
    type(monitor_set), pointer :: monitors  => null()      !! Monitors to print to stdout and files
    type(op_obj),      pointer :: op        => null()      !! operators object
    contains
      procedure :: CoM                 => marker_set_CoM
      procedure :: Filter              => marker_set_Filter
      procedure :: ComputeSolidVolFrac => marker_set_ComputeSolidVolFrac
      procedure :: Read                => marker_set_Read
      procedure :: Write               => marker_set_Write
      procedure :: LoadSTL             => marker_set_LoadSTL
      procedure :: SetMPIDataTypeParams=> marker_set_SetMPIDataTypeParams
      procedure :: SetObjectType       => marker_set_SetObjectType

      procedure :: Prepare             => marker_set_Prepare
      procedure :: UpdateSDF           => marker_set_UpdateSDF
      procedure :: UpdateNormals       => marker_set_UpdateNormals
      procedure :: GetIBForcing        => marker_set_GetIBForcing
  end type marker_set

  contains

    ! marker_obj methods
    ! ------------------------------------------------------
    subroutine marker_obj_Info(this)
      !> Print information about this marker
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(marker_obj), intent(inout)    :: this          !! A marker_obj object
      write(stdout,*) "id    :",this%id
      write(stdout,*) "p     :",this%p
      write(stdout,*) "c     :",this%c
      write(stdout,*) "s     :",this%s
      write(stdout,*) "t     :",this%t
      write(stdout,*) "SA    :",this%SA
      write(stdout,*) "n     :",this%n
      write(stdout,*) "v     :",this%v
      write(stdout,*) "f     :",this%f
      write(stdout,*) "pold  :",this%pold
      write(stdout,*) "vold  :",this%vold
      return
    end subroutine marker_obj_Info
    subroutine marker_obj_assign(this,val)
      !> Assignment
      implicit none
      class(marker_obj),     intent(inout) :: this         !! A marker_obj object
      class(lagrangian_obj), intent(in)    :: val          !! A marker_obj object
      select type (val)
      class is (marker_obj)
        this%id  =val%id
        this%p   =val%p
        this%c   =val%c
        this%s   =val%s
        this%t   =val%t
        this%SA  =val%SA
        this%n   =val%n
        this%v   =val%v
        this%f   =val%f
        this%pold=val%pold
        this%vold=val%vold
      end select
      return
    end subroutine marker_obj_assign

    ! marker_set methods
    ! ------------------------------------------------------
    subroutine marker_set_SetObjectType(this)
      !> Set the sample type used in allocation
      ! of polymorphic variables
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      type(marker_obj) :: sample                           !! My sample
      allocate(this%sample,source=sample)
      return
    end subroutine marker_set_SetObjectType
    function marker_set_CoM(this) result(CoM)
      !> Find the center of mass
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      real(wp)                         :: CoM(3)           !! Position of center of mass
      ! Work variables
      real(wp):: CoM_(3),SA_,SA

      SA_=0.0_wp
      CoM_=0.0_wp
      if (this%count_.gt.0) then
        select type(markers=>this%p)
        type is (marker_obj)
          SA_   = sum(markers(1:this%count_)%SA)
          CoM_(1)=sum(markers(1:this%count_)%SA*markers(1:this%count_)%p(1))
          CoM_(2)=sum(markers(1:this%count_)%SA*markers(1:this%count_)%p(2))
          CoM_(3)=sum(markers(1:this%count_)%SA*markers(1:this%count_)%p(3))
        end select
      end if
      call this%parallel%sum(SA_    ,SA    )
      call this%parallel%sum(CoM_(1),CoM(1))
      call this%parallel%sum(CoM_(2),CoM(2))
      call this%parallel%sum(CoM_(3),CoM(3))
      CoM=CoM/SA
      return
    end function marker_set_CoM
    subroutine marker_set_Filter(this,var,field)
      !> Compute a filtered quantity on the
      ! eulerian grid
      implicit none
      class(marker_set),   intent(inout) :: this           !! Set of ib markers
      character(len=*),    intent(in)    :: var            !! Variable to compute
      type(eulerian_obj_r),intent(inout) :: field          !! Filtered quantity
      ! Work variables
      real(wp),allocatable :: bump(:,:,:)
      real(wp):: coef
      integer :: slo(3),shi(3)
      integer :: n,i,j,k

      ! Initialize field
      field%cell=0.0_wp
      select type(markers=>this%p)
      type is (marker_obj)
        select case(trim(adjustl(var)))
        case ('SA')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('V1')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%v(1)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('V2')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%v(2)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('V3')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%v(3)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('F1')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%f(1)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('F2')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%f(2)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('F3')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%f(3)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('N1')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%n(1)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('N2')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%n(2)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('N3')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*markers(n)%n(3)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('V.N')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*sum(markers(n)%v(:)*markers(n)%n(:))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC11')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(1)*markers(n)%n(1)+markers(n)%v(1)*markers(n)%n(1))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC22')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(2)*markers(n)%n(2)+markers(n)%v(2)*markers(n)%n(2))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC33')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(3)*markers(n)%n(3)+markers(n)%v(3)*markers(n)%n(3))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC12')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(1)*markers(n)%n(2)+markers(n)%v(2)*markers(n)%n(1))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC13')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(1)*markers(n)%n(3)+markers(n)%v(3)*markers(n)%n(1))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case ('VISC23')

          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=markers(n)%c-this%stib/2
            shi=markers(n)%c+this%stib/2
            call markers(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=markers(n)%SA*(markers(n)%v(2)*markers(n)%n(3)+markers(n)%v(3)*markers(n)%n(2))
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        end select
      end select

      ! Free memory
      if (allocated(bump))      deallocate(bump)

      ! Update ghostcells
      call field%AddUpGhostCells

      return
    end subroutine marker_set_Filter
    subroutine marker_set_ComputeSolidVolFrac (this,VF,solver,MaxTol,MaxIt,RelaxType,Rel,It,intRHS)
      !> Compute the solid volume fraction on the mesh
      use leapDiffOp
      implicit none
      class(marker_set),   intent(inout) :: this           !! Set of ib markers
      type(eulerian_obj_r),intent(inout) :: VF             !! Volume fraction
      character(len=*),    intent(in)    :: solver         !! Name of solver to be used
      real(wp),            intent(in)    :: MaxTol         !! Maximum relative tolerance
      integer,             intent(in)    :: MaxIt          !! Maximum number of subiterations
      integer,optional,    intent(in)    :: RelaxType      !! Relaxation type
      real(wp),optional,   intent(out)   :: Rel            !! Relative error at end of solve
      integer,optional,    intent(out)   :: It             !! Number of iterations performed
      real(wp),optional,   intent(out)   :: intRHS         !! Magnitude of RHS
      ! Work variables
      type(hypre_obj)      :: hypre                        !! Hypre solvers
      type(op_obj)         :: op
      type(eulerian_obj_r) :: N1,  N2,  N3

      integer :: i,j,k
      real(wp):: dx,dy,dz,Vol,buffr

      ! Initialize HYPRE
      ! -------------------------- !
      call hypre%initialize(this%block,this%parallel)

      ! Chose solver to use
      if (present(RelaxType)) then
        call hypre%SelectSolver(solver,MaxTol=MaxTol,MaxIt=MaxIt,RelaxType=RelaxType)
      else
        call hypre%SelectSolver(solver, MaxTol=MaxTol,MaxIt=MaxIt)
      end if

      ! Setup the solver
      call hypre%setup()

      ! Setting up right hand side
      ! -------------------------- !
      call N1%initialize('N1',this%block,this%parallel,0)
      call N2%initialize('N2',this%block,this%parallel,0)
      call N3%initialize('N3',this%block,this%parallel,0)

      ! Filter normals on mesh
      call this%filter('N1',N1)
      call this%filter('N2',N2)
      call this%filter('N3',N3)

      ! Compute the divergence
      call op%initialize(this%block,this%parallel)
      VF=op%div(VF%name,N1,N2,N3)
      VF%cell = - VF%cell

      call N1%finalize()
      call N2%finalize()
      call N3%finalize()

      ! Setting up right hand side
      ! -------------------------- !
      if (present(intRHS)) then
        intRHS = 0.0_wp
        Vol    = 0.0_wp
        do k=this%block%lo(3),this%block%hi(3)
          do j=this%block%lo(2),this%block%hi(2)
            do i=this%block%lo(1),this%block%hi(1)
              dx = this%block%x(i+1)-this%block%x(i)
              dy = this%block%y(j+1)-this%block%y(j)
              dz = this%block%z(k+1)-this%block%z(k)
              intRHS = intRHS + VF%cell(i,j,k)* dx*dy*dz
              Vol= Vol + dx*dy*dz
            end do
          end do
        end do
        call this%parallel%sum(Vol,    buffr); Vol   = buffr
        call this%parallel%sum(intRHS, buffr); intRHS= buffr/Vol
      end if

      call hypre%SetRHS(VF)

      ! Solve Poisson equation
      ! -------------------------- !
      call hypre%Solve(VF)

      ! Update ghostcells
      call VF%UpdateGhostCells

      if (present(Rel)) Rel=hypre%rel
      if (present(It))  It=hypre%it

      ! Throw warning if appropriate
      if (hypre%rel > MaxTol .and. this%parallel%rank%mine.eq.this%parallel%rank%root) &
            write(*,fmt='(a,ES12.5)') "Warning: Volume Fraction solver did not converge -- Rel. Res.=",hypre%rel

      ! Finalize
      call hypre%finalize()
      call op%finalize()
      return
   end subroutine marker_set_ComputeSolidVolFrac
    subroutine marker_set_LoadSTL(this,STL_file)
      !> Load markers from a binary STL.
      ! This is a serial routine.
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      character(len=*),  intent(in)    :: STL_file         !! Variable to compute
      ! Work variables
      integer          :: facets_count                     !! Number of facets in the STL file
      real(leapSP)     :: v1(3),v2(3),v3(3),a(3),b(3)      !! verticies of the facet
      character(len=80):: cbuf                             !! Character buffer
      character(len=2) :: padd                             !! Padding in STL file
      integer :: n
      integer :: fid,ierr
      logical :: ok


      ! Update the global count of markers
      call this%recycle
      associate (mpi => this%parallel)
        if (mpi%rank%mine.eq.mpi%rank%root) then
          ! Check file is there and open it
          inquire(file=trim(adjustl(STL_file)),exist=ok)
          if (.not.ok) call mpi%stop('Unable to find the STL file: '//trim(adjustl(STL_file)))

          open(newunit = fid, file   = trim(adjustl(STL_file)),&
                              status = 'old',                  &
                              action = 'read',                 &
                              access = 'stream',               &
                              form   = 'unformatted',          &
                              iostat = ierr)
          if (ierr.ne.0) call mpi%stop('Unable to open the STL file: '//trim(adjustl(STL_file)))

          ! Read header record
          ! -------------------------- !
          read (fid) cbuf
          read (fid) facets_count

          ! Add new ib markers
          ! -------------------------- !
          call this%resize(this%count_+facets_count)

          ! Re-read and store the facets
          ! -------------------------- !
          select type(markers=>this%p)
          type is (marker_obj)
            do n =this%count_-facets_count+1,this%count_
              ! Assign ID to markers
              ! -------------------------- !
               markers(n)%id = this%count + n - (this%count_-facets_count)
              ! Read normal
              ! -------------------------- !
              read (fid) a
              markers(n)%n=real(a,WP)
              ! Read vertecies
              ! -------------------------- !
              read (fid) v1
              read (fid) v2
              read (fid) v3

              ! Place a marker at the centroid
              markers(n)%p = reaL((v1+v2+v3)/3.0_wp,WP)

              ! Assign the surface area of the facet to the marker
              a = v2-v1
              b = v3-v1
              markers(n)%SA= real(0.5_WP * sqrt(          &
                            (a(2)*b(3)-a(3)*b(2))**2     &
                          + (a(3)*b(1)-a(1)*b(3))**2     &
                          + (a(1)*b(2)-a(2)*b(1))**2),WP)

              ! Read padding
              read (fid) padd
            end do
          end select

          close(fid)
        end if
      end associate

      call this%recycle
      return
    end subroutine marker_set_LoadSTL

    ! Deferred lagrangian_set methods
    ! ------------------------------------------------------
    subroutine marker_set_SetMPIDataTypeParams(this,types,lengths,displacement)
      !> Set up parameters used when creating the MPI derived type
      use mpi_f08
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      type(MPI_Datatype), allocatable, &
                           intent(out) :: types(:)         !! Array of types
      integer, allocatable,intent(out) :: lengths(:)       !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
               allocatable,intent(out) :: displacement(:)  !! Array of displacements
      ! Work variable
      integer :: N
      type(marker_obj) :: sample
      integer :: ierr

      associate(mpi=>this%parallel)
        !! Create the MPI structure
        N=11
        allocate(types(N),displacement(N),lengths(N))
        types( 1) = mpi%INT8    ; lengths( 1) = 1                 ! id     --  integer
        types( 2) = mpi%REAL_WP ; lengths( 2) = size(sample%p)    ! p      --  real*3
        types( 3) = mpi%INTEGER ; lengths( 3) = size(sample%c)    ! c      --  integer*3
        types( 4) = mpi%INTEGER ; lengths( 4) = 1                 ! s      --  integer
        types( 5) = mpi%INTEGER ; lengths( 5) = 1                 ! t      --  integer
        types( 6) = mpi%REAL_WP ; lengths( 6) = 1                 ! SA     --  real
        types( 7) = mpi%REAL_WP ; lengths( 7) = size(sample%n)    ! n      --  real*3
        types( 8) = mpi%REAL_WP ; lengths( 8) = size(sample%v)    ! v      --  real*3
        types( 9) = mpi%REAL_WP ; lengths( 9) = size(sample%f)    ! f      --  real*3
        types(10) = mpi%REAL_WP ; lengths(10) = size(sample%pold) ! pold   --  real*3
        types(11) = mpi%REAL_WP ; lengths(11) = size(sample%vold) ! vold   --  real*3
        ! Count the displacement for this structure
        call MPI_GET_ADDRESS(sample%id,     displacement( 1),ierr)
        call MPI_GET_ADDRESS(sample%p(1),   displacement( 2),ierr)
        call MPI_GET_ADDRESS(sample%c(1),   displacement( 3),ierr)
        call MPI_GET_ADDRESS(sample%s,      displacement( 4),ierr)
        call MPI_GET_ADDRESS(sample%t,      displacement( 5),ierr)
        call MPI_GET_ADDRESS(sample%SA,     displacement( 6),ierr)
        call MPI_GET_ADDRESS(sample%n(1),   displacement( 7),ierr)
        call MPI_GET_ADDRESS(sample%v(1),   displacement( 8),ierr)
        call MPI_GET_ADDRESS(sample%f(1),   displacement( 9),ierr)
        call MPI_GET_ADDRESS(sample%pold(1),displacement(10),ierr)
        call MPI_GET_ADDRESS(sample%vold(1),displacement(11),ierr)
        displacement = displacement - displacement( 1)
      end associate
      return
    end subroutine marker_set_SetMPIDataTypeParams
    subroutine marker_set_Read(this,iter,time)
      !> Read marker data from file in parallel
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      integer,             intent(out) :: iter             !! Iteration at write
      real(wp),            intent(out) :: time             !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer,          allocatable :: buffi(:)
      integer(leapI8),  allocatable :: buffi8(:)
      real(wp),         allocatable :: buffr(:)

      ! Open the file
      call h5%initialize(trim(adjustl(this%read_file)),"R",this%parallel)

      ! Jump to last step
      call h5%LastTimeStep(iter,time)

      ! Get total number of point objects
      call h5%getnpoints(this%count)

      ! Distribute equally the data on each mpi rank
      associate(mpi=>this%parallel)
        this%count_=int(this%count/mpi%nproc)
        if (mpi%rank%mine.le.mod(this%count,mpi%nproc)) this%count_=this%count_+1
      end associate

      ! Resize the data array to contain read Lagrangian objects
      call this%resize(this%count_)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))
      ! Read the base Lagrangian object structure
      if (this%count_.eq.0) then
        call h5%read("id",buffi8)
        call h5%read("x", buffr )
        call h5%read("y", buffr )
        call h5%read("z", buffr )
        call h5%read("i", buffi )
        call h5%read("j", buffi )
        call h5%read("k", buffi )
        call h5%read("s", buffi )
        call h5%read("SA",buffr )
        call h5%read("nx",buffr )
        call h5%read("ny",buffr )
        call h5%read("nz",buffr )
        call h5%read("vx",buffr )
        call h5%read("vy",buffr )
        call h5%read("vz",buffr )
        call h5%read("fx",buffr )
        call h5%read("fy",buffr )
        call h5%read("fz",buffr )
      else
        select type (markers=>this%p)
        type is (marker_obj)
          call h5%read("id",buffi8); markers(:)%id   = buffi8
          call h5%read("x", buffr ); markers(:)%p(1) = buffr
          call h5%read("y", buffr ); markers(:)%p(2) = buffr
          call h5%read("z", buffr ); markers(:)%p(3) = buffr
          call h5%read("i", buffi ); markers(:)%c(1) = buffi
          call h5%read("j", buffi ); markers(:)%c(2) = buffi
          call h5%read("k", buffi ); markers(:)%c(3) = buffi
          call h5%read("s", buffi ); markers(:)%s    = buffi
          call h5%read("SA",buffr ); markers(:)%SA   = buffr
          call h5%read("nx",buffr ); markers(:)%n(1) = buffr
          call h5%read("ny",buffr ); markers(:)%n(2) = buffr
          call h5%read("nz",buffr ); markers(:)%n(3) = buffr
          call h5%read("vx",buffr ); markers(:)%v(1) = buffr
          call h5%read("vy",buffr ); markers(:)%v(2) = buffr
          call h5%read("vz",buffr ); markers(:)%v(3) = buffr
          call h5%read("fx",buffr ); markers(:)%f(1) = buffr
          call h5%read("fy",buffr ); markers(:)%f(2) = buffr
          call h5%read("fz",buffr ); markers(:)%f(3) = buffr
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%finalize

      return
    end subroutine marker_set_Read
    subroutine marker_set_Write(this,iter,time)
      !> Write marker data to file in parallel
      implicit none
      class(marker_set), intent(inout) :: this             !! Set of ib markers
      integer,             intent(in)  :: iter             !! Iteration at write
      real(wp),            intent(in)  :: time             !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer,          allocatable :: buffi(:)
      integer(leapI8),  allocatable :: buffi8(:)
      real(wp),         allocatable :: buffr(:)

      ! Nothing to write, if empty
      call this%UpdateCount()
      if (this%count.eq.0) return

      ! Open the file
      if (this%overwrite) then
        call h5%initialize(trim(adjustl(this%write_file)),"W",this%parallel)
      else
        call h5%initialize(trim(adjustl(this%write_file)),"RW",this%parallel)
      end if

      ! Create a new time step
      call h5%NewTimeStep(iter,time)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))

      ! Write the marker data
      if (this%count_.eq.0) then
        call h5%write("id",buffi8)
        call h5%write("x", buffr )
        call h5%write("y", buffr )
        call h5%write("z", buffr )
        call h5%write("i", buffi )
        call h5%write("j", buffi )
        call h5%write("k", buffi )
        call h5%write("s", buffi )
        call h5%write("SA",buffr )
        call h5%write("nx",buffr )
        call h5%write("ny",buffr )
        call h5%write("nz",buffr )
        call h5%write("vx",buffr )
        call h5%write("vy",buffr )
        call h5%write("vz",buffr )
        call h5%write("fx",buffr )
        call h5%write("fy",buffr )
        call h5%write("fz",buffr )
      else
        select type (markers=>this%p)
        type is (marker_obj)
          buffi8=markers(1:this%count_)%id   ; call h5%write("id",buffi8)
          buffr =markers(1:this%count_)%p(1) ; call h5%write("x", buffr )
          buffr =markers(1:this%count_)%p(2) ; call h5%write("y", buffr )
          buffr =markers(1:this%count_)%p(3) ; call h5%write("z", buffr )
          buffi =markers(1:this%count_)%c(1) ; call h5%write("i", buffi )
          buffi =markers(1:this%count_)%c(2) ; call h5%write("j", buffi )
          buffi =markers(1:this%count_)%c(3) ; call h5%write("k", buffi )
          buffi =markers(1:this%count_)%s    ; call h5%write("s", buffi )
          buffr =markers(1:this%count_)%SA   ; call h5%write("SA",buffr )
          buffr =markers(1:this%count_)%n(1) ; call h5%write("nx",buffr )
          buffr =markers(1:this%count_)%n(2) ; call h5%write("ny",buffr )
          buffr =markers(1:this%count_)%n(3) ; call h5%write("nz",buffr )
          buffr =markers(1:this%count_)%v(1) ; call h5%write("vx",buffr )
          buffr =markers(1:this%count_)%v(2) ; call h5%write("vy",buffr )
          buffr =markers(1:this%count_)%v(3) ; call h5%write("vz",buffr )
          buffr =markers(1:this%count_)%f(1) ; call h5%write("fx",buffr )
          buffr =markers(1:this%count_)%f(2) ; call h5%write("fy",buffr )
          buffr =markers(1:this%count_)%f(3) ; call h5%write("fz",buffr )
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%finalize

      return
    end subroutine marker_set_Write
    subroutine marker_set_Prepare(this,timer,parser,operators,monitors)
      !> Prepare marker_set for use with solvers
      implicit none
      class(marker_set),        intent(inout) :: this      !! Immersed boundary
      type(timer_obj),  target, intent(in)    :: timer     !! Timer utility
      type(parser_obj), target, intent(in)    :: parser    !! Parser for input file
      type(op_obj),     target, intent(in)    :: operators !! Operators object
      type(monitor_set),target, intent(in)    :: monitors  !! Monitors to print to stdout and files
      ! Work variables
      real(wp):: dl
      integer :: kernel_interp,kernel_extrap
      character(len=str64) :: filename

      ! Associate pointers
      this%timer    => timer
      this%parser   => parser
      this%op       => operators
      this%monitors => monitors

      call this%parser%Get('IB read file',  filename )
      call this%SetReadFileName(filename)

      call this%parser%Get('IB write file', filename )
      call this%SetWriteFileName(filename)

      call this%parser%Get('IB overwrite', this%overwrite, default = .true. )

      ! Filtering parameters
      dl = (2.0_wp - 1.0e3_wp*epsilon(1.0_wp))*minval(this%block%dx)
      call this%parser%Get('IB filter half width',  this%l_filter, default = dl     )
      call this%parser%Get('IB interp kernel',      kernel_interp, default = 1      )
      call this%parser%Get('IB extrap kernel',      kernel_extrap, default = 1      )
      call this%SetFilterKernel(kernel_interp,kernel_extrap)
      call this%SetFilterSize(this%l_filter)

      ! Read resolved particle data
      call this%Read(this%timer%iter,this%timer%time)

      ! Send markers to their mpiranks
      call this%Communicate

      ! Localize centroids and markers on the grid
      call this%Localize

      return
    end subroutine marker_set_Prepare
    subroutine marker_set_UpdateSDF(this,SA)
      !> Updates the Surface Density Function
      implicit none
      class(marker_set),   intent(inout) :: this           !! Immersed boundary
      type(eulerian_obj_r),intent(inout) :: SA
      ! work variables
      type(eulerian_obj_r):: tmp

      ! Initialize temporary field
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      ! Filter particle surface area
      call this%Filter('SA',tmp)

      ! Add to total SDF
      SA = SA + tmp

      ! Clear data
      call tmp%Finalize()
      return
    end subroutine marker_set_UpdateSDF
    subroutine marker_set_UpdateNormals(this,ibN)
      !> Updates the Normals field
      implicit none
      class(marker_set),   intent(inout) :: this           !! Immersed boundary
      type(eulerian_obj_r),intent(inout) :: ibN(3)
      ! work variables
      type(eulerian_obj_r):: tmp

      ! Initialize temporary field
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      ! Filter and add to total normals field
      call this%Filter('N1',tmp); ibN(1) = ibN(1) + tmp
      call this%Filter('N2',tmp); ibN(2) = ibN(2) + tmp
      call this%Filter('N3',tmp); ibN(3) = ibN(3) + tmp

      ! Clear data
      call tmp%Finalize()
      return
    end subroutine marker_set_UpdateNormals
    subroutine marker_set_GetIBForcing(this,Um,Vm,Wm,rhof,SA,ibF,dt)
      !> Compute the IB forcing
      implicit none
      class(marker_set),   intent(inout) :: this           !! Collection of Resolved Particles
      type(Eulerian_obj_r),intent(in)    :: Um             !! Velocity in 1-dir
      type(Eulerian_obj_r),intent(in)    :: Vm             !! Velocity in 2-dir
      type(Eulerian_obj_r),intent(in)    :: Wm             !! Velocity in 3-dir
      real(wp),            intent(in)    :: rhof           !! Fluid density
      type(Eulerian_obj_r),intent(in)    :: SA             !! Surface area
      type(Eulerian_obj_r),intent(inout) :: ibF(3)         !! IB forcing
      real(wp),            intent(in)    :: dt             !! Timestep
      ! work variable
      type(eulerian_obj_r):: tmp
      real(wp):: uf(3)
      integer :: shi(3), slo(3)                            !! Stencil extents
      real(wp):: F(this%stib,this%stib,this%stib)          !! Stencil data
      real(wp):: SDF
      integer :: m
   
      select type (markers=>this%p)
      type is (marker_obj)
        do m=1,this%count_
          slo=markers(m)%c-this%stib/2
          shi=markers(m)%c+this%stib/2

          ! Interpolate velocities
          F(:,:,:) = Um%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(1)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          F(:,:,:) = Vm%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(2)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          F(:,:,:) = Wm%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          uf(3)    = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          ! Interpolate SDF
          F(:,:,:) = SA%cell(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3))
          SDF      = markers(m)%interpolate(this%l_filter,slo,shi,this%block,this%g1in,F)

          markers(m)%f = (1.0_wp/SDF)*rhof*(markers(m)%v-uf)/dt
        end do
      end select

      ! Compute forcing on mid points
      call tmp%Initialize('tmp',this%block,this%parallel,0)

      call this%Filter('F1',tmp); ibF(1) = tmp + ibF(1)
      call this%Filter('F2',tmp); ibF(2) = tmp + ibF(2)
      call this%Filter('F3',tmp); ibF(3) = tmp + ibF(3)

      call tmp%Finalize()

      return
    end subroutine marker_set_GetIBForcing
end module immersed_boundaries_markers
