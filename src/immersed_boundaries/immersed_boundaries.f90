!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module immersed_boundaries
  !>--------------------------------------------------------------------------
  ! Module: immersed_boundaries
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Numerical methods for immersed boundaries
  ! --------------------------------------------------------------------------
  use immersed_boundaries_markers, only : marker_obj, marker_set
  use immersed_boundaries_solids,  only : solid_obj, solid_set
  implicit none
end module immersed_boundaries
