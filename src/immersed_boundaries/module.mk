MODULE = immersed_boundaries
FILES = immersed_boundaries_markers.f90 immersed_boundaries_solids.f90 immersed_boundaries.f90
SRC += $(patsubst %, $(MODULE)/%, $(FILES))
