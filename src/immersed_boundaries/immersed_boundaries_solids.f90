!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module immersed_boundaries_solids
  !>--------------------------------------------------------------------------
  ! Module: immersed_boundaries_solids
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! tools to represent a collection of immersed solids using surface
  ! markers
  ! --------------------------------------------------------------------------
  use leapIO
  use leapKinds
  use leapLagrangian
  use leapEulerian
  use leapBlock
  use leapParallel
  use immersed_boundaries_markers
  implicit none
  private
  public :: solid_obj,solid_set

  type, extends (marker_set) :: solid_obj
    !> An object that represents a solid
    real(WP) :: xc(3)                                      !! position of center of mass
    real(WP) :: vc(3)                                      !! velocity of center of mass
    real(WP) :: Ac(3)                                      !! Angluar velocity of center of mass
    integer  :: motion=0                                   !! Type of motion
    real(WP) :: fr=0.0_WP                                  !! Frequency of oscillation
  end type solid_obj

  type :: solid_set
    !> Collection of solid_obj
    type(solid_obj), allocatable :: p(:)                   !! Array of solid_obj
    integer :: count =0                                    !! Total count of solid_obj
    character(len=:),allocatable :: name                   !! Name of the collection of solid_obj
    ! IO
    character(len=str64)         :: read_file              !! file to read
    character(len=str64)         :: write_file             !! file to write
    logical                      :: overwrite=.true.       !! Switch to overwrite IO files

    type(block_obj),    pointer :: block   =>null()        !! Associated block structure
    type(parallel_obj), pointer :: parallel=>null()        !! Associated parallel structure
    contains
      procedure :: Initialize          => solid_set_Init
      procedure :: Finalize            => solid_set_Final
      procedure :: SetWriteFileName    => solid_set_SetWriteFileName
      procedure :: SetReadFileName     => solid_set_SetReadFileName
      procedure :: SetOverwrite        => solid_set_SetOverwrite
      procedure :: Read                => solid_set_Read
      procedure :: Write               => solid_set_Write
      procedure :: Communicate         => solid_set_Communicate
      procedure :: Localize            => solid_set_Localize
      procedure :: SetFilterKernel     => solid_set_SetFilterKernel
      procedure :: SetFilterSize       => solid_set_SetFilterSize
      procedure :: Filter              => solid_set_Filter
  end type
  contains
    ! solid_set methods
    ! ------------------------------------------------------
    subroutine solid_set_Init(this,name,nobj,block,parallel)
      !> Initialize a collection of solid_obj
      implicit none
      class(solid_set),           intent(inout) :: this    !! A collection of solid_obj
      character(len=*),           intent(in)    :: name    !! Name of variable
      integer,                    intent(in)    :: nobj    !! Number of solid_obj
      type(block_obj),    target, intent(in)    :: block   !! A block object
      type(parallel_obj), target, intent(in)    :: parallel!! parallel structure from main program
      ! Work variables
      integer :: n
      character(len=str64)  :: my_name

      ! Associate
      this%block => block
      this%parallel => parallel

      this%name      =trim(adjustl(name))

      ! Set default values for IO files
      call this%SetWriteFileName(this%name)
      call this%SetReadFileName(this%name)
      call this%SetOverwrite(.true.)

      ! Set number of solids
      this%count=nobj
      allocate(this%p(this%count))

      ! Store name
      do n=1,this%count
        write(my_name,fmt='(a,i6.6)') trim(adjustl(name))//'_',n
        call this%p(n)%Initialize(trim(adjustl(my_name)), this%block,this%parallel)
      end do

      return
    end subroutine solid_set_Init
    subroutine solid_set_SetWriteFileName(this,name)
      !> Set the names of files to write
      implicit none
      class(solid_set),           intent(inout) :: this    !! A collection of solid_obj
      character(len=*),           intent(in)    :: name    !! Name of file
      ! Work variables
      integer :: pos,n

      ! Store the base name with no extensions
      pos=scan(trim(adjustl(name)),".",BACK=.true.)
      if (pos.gt.0) then
        this%write_file=name(1:pos-1)
      else
        this%write_file=name
      end if

      do n=1,this%count
        write(this%p(n)%write_file,fmt='(a,i6.6,a)')trim(adjustl(this%write_file))//'_',n,'.h5'
      end do

      this%write_file=trim(adjustl(this%write_file))//'.h5'
      return
    end subroutine solid_set_SetWriteFileName
    subroutine solid_set_SetReadFileName(this,name)
      !> Set the names of files to read
      class(solid_set),           intent(inout) :: this    !! A collection of solid_obj
      character(len=*),           intent(in)    :: name    !! Name of file
      ! Work variables
      integer :: pos, n
      ! Store the base name with no extensions
      pos=scan(trim(adjustl(name)),".",BACK=.true.)
      if (pos.gt.0) then
        this%read_file=name(1:pos-1)
      else
        this%read_file=name
      end if
      do n=1,this%count
        write(this%p(n)%read_file,fmt='(a,i6.6,a)') trim(adjustl(this%read_file))//'_',n,'.h5'
      end do

      this%read_file=trim(adjustl(this%read_file))//'.h5'
      return
    end subroutine solid_set_SetReadFileName
    subroutine solid_set_SetOverwrite(this,overwrite)
      !> Set file overwritting
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      logical,          intent(in)    :: overwrite         !! Toggle
      ! Work variables
      integer :: n
      this%overwrite=overwrite
      do n=1,this%count
        this%p(n)%overwrite=this%overwrite
      end do
      return
    end subroutine solid_set_SetOverwrite
    subroutine solid_set_Final(this)
      !> Finalize
      implicit none
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      ! Work variable
      integer :: n

      ! Nullify pointers
      this%block    => null()
      this%parallel => null()

      ! Finalize all solid_obj
      if (allocated(this%p)) then
        do n=1,this%count
          call this%p(n)%Finalize
        end do
        deallocate(this%p)
      end if

      return
    end subroutine solid_set_Final
    subroutine solid_set_Read(this,iter,time)
      !> Read all solid_obj
      implicit none
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      integer,          intent(out)   :: iter              !! Iteration at write
      real(wp),         intent(out)   :: time              !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer :: n

      do n=1,this%count
        call this%p(n)%read(iter,time)
        ! Add some attributes
        call h5%Initialize(trim(adjustl(this%p(n)%read_file)),"RW",this%parallel)
        call h5%ReadAttributes('Motion',this%p(n)%motion)
        call h5%ReadAttributes('X_c',this%p(n)%xc)
        call h5%ReadAttributes('V_c',this%p(n)%vc)
        call h5%ReadAttributes('A_c',this%p(n)%ac)
        call h5%ReadAttributes('Freq',this%p(n)%fr)
        call h5%Finalize
      end do

      return
    end subroutine solid_set_Read
    subroutine solid_set_Write(this,iter,time)
      !> Write all solid_obj
      implicit none
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      integer,          intent(in)    :: iter              !! Iteration at write
      real(wp),         intent(in)    :: time              !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer :: n

      do n=1,this%count
        call this%p(n)%write(iter,time)

        ! Add some attributes
        call h5%Initialize(trim(adjustl(this%p(n)%write_file)),"RW",this%parallel)
        call h5%WriteAttributes('Motion',this%p(n)%motion)
        call h5%WriteAttributes('X_c',this%p(n)%xc)
        call h5%WriteAttributes('V_c',this%p(n)%vc)
        call h5%WriteAttributes('A_c',this%p(n)%Ac)
        call h5%WriteAttributes('Freq',this%p(n)%fr)
        call h5%Finalize

      end do

      return
    end subroutine solid_set_Write
    subroutine solid_set_Communicate(this)
      !> Communicate markers contained in all solid_obj
      implicit none
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      ! Work variables
      integer :: n

      do n=1,this%count
        call this%p(n)%communicate
      end do

      return
    end subroutine solid_set_Communicate
    subroutine solid_set_Localize(this)
      !> Localize markers on the gril for all solid_obj
      implicit none
      class(solid_set), intent(inout) :: this              !! A collection of solid_obj
      ! Work variables
      integer :: n

      do n=1,this%count
        call this%p(n)%Localize
      end do

      return
    end subroutine solid_set_Localize
    subroutine solid_set_SetFilterKernel(this,kernel_interp,kernel_extrap)
      !> Select interp/extrap kernels
      implicit none
      class(solid_set), intent(inout)    :: this           !! A collection of solid_obj
      integer,          intent(in)       :: kernel_interp  !! Filter kernel for interpolations
      integer,          intent(in)       :: kernel_extrap  !! Filter kernel for extrapolations
      ! Work variable
      integer :: n
      if (allocated(this%p)) then
        do n=1,this%count
          call this%p(n)%SetFilterKernel(kernel_interp,kernel_extrap)
        end do
      end if
      return
    end subroutine solid_set_SetFilterKernel
    subroutine solid_set_SetFilterSize(this,l_filter)
      !> Change filter size to desired value
      implicit none
      class(solid_set), intent(inout)    :: this           !! A collection of solid_obj
      real(wp),            intent(in)    :: l_filter       !! Filter size
      ! Work variable
      integer :: n
      if (allocated(this%p)) then
        do n=1,this%count
          call this%p(n)%SetFilterSize(l_filter)
        end do
      end if
      return
    end subroutine solid_set_SetFilterSize
    subroutine solid_set_Filter(this,var,field)
      !> Filter a quantity to the Eulerian grid
      implicit none
      class(solid_set), intent(inout)    :: this           !! A collection of solid_obj
      character(len=*),    intent(in)    :: var            !! Variable to compute
      type(eulerian_obj_r),intent(inout) :: field          !! Filtered quantity
      ! Work variables
      type(eulerian_obj_r) :: my_field
      integer :: n

      ! Initialize value to zero
      field%cell=0.0_WP

      ! Create a temporary Eulerian structure, cell centered
      call my_field%Initialize('tmp',this%block,this%parallel,0)
      do n=1,this%count

        ! Filter quantity
        call this%p(n)%filter(var,my_field)

        ! Add up cell values
        field%cell=field%cell+my_field%cell
      end do

      ! Finalize tmp variable
      call my_field%Finalize
      return
    end subroutine solid_set_Filter
end module immersed_boundaries_solids
