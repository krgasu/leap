!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test hash table
  use leapKinds
  use leapUtils, only: hashtbl_obj
  !use hashtbl
  use iso_fortran_env, only: stderr => error_unit,&
                             stdout => output_unit
  implicit none

  type(hashtbl_obj) :: tbl(4)
  integer           :: keys(20)
  integer           :: sub_keys(5,4)
  integer           :: i
  integer           :: val(4)
  integer           :: indices(4,20)
  integer           :: error
  logical           :: test_successful=.false.

  keys = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
  sub_keys(:,1) = [2,6,10,14,15]
  sub_keys(:,2) = [1,9,11,19,20]
  sub_keys(:,3) = [3,4,8, 17,12]
  sub_keys(:,4) = [5,7,13,16,18]
  indices(:, 1) = [-1, 1,-1,-1]
  indices(:, 2) = [ 1,-1,-1,-1]
  indices(:, 3) = [-1,-1, 1,-1]
  indices(:, 4) = [-1,-1, 2,-1]
  indices(:, 5) = [-1,-1,-1, 1]
  indices(:, 6) = [ 2,-1,-1,-1]
  indices(:, 7) = [-1,-1,-1, 2]
  indices(:, 8) = [-1,-1, 3,-1]
  indices(:, 9) = [-1, 2,-1,-1]
  indices(:,10) = [ 3,-1,-1,-1]
  indices(:,11) = [-1, 3,-1,-1]
  indices(:,12) = [-1,-1, 5,-1]
  indices(:,13) = [-1,-1,-1, 3]
  indices(:,14) = [ 4,-1,-1,-1]
  indices(:,15) = [ 5,-1,-1,-1]
  indices(:,16) = [-1,-1,-1, 4]
  indices(:,17) = [-1,-1, 4,-1]
  indices(:,18) = [-1,-1,-1, 5]
  indices(:,19) = [-1, 4,-1,-1]
  indices(:,20) = [-1, 5,-1,-1]

  call tbl(1)%initialize(5)
  call tbl(2)%initialize(5)
  call tbl(3)%initialize(5)
  call tbl(4)%initialize(5)

  ! Update the hashtable
  do i=1,5
    call tbl(1)%put(key=sub_keys(i,1),val=i)
    call tbl(2)%put(key=sub_keys(i,2),val=i)
    call tbl(3)%put(key=sub_keys(i,3),val=i)
    call tbl(4)%put(key=sub_keys(i,4),val=i)
  end do

  ! Inquire hashtable
  error = 0
  do i=1,size(keys)
    call tbl(1)%get(key=keys(i),val=val(1)); 
    call tbl(2)%get(key=keys(i),val=val(2)); 
    call tbl(3)%get(key=keys(i),val=val(3)); 
    call tbl(4)%get(key=keys(i),val=val(4)); 

    !print*, 'Index of',keys(i),'in sub_keys:',norm2(real(val-indices(:,keys(i)),wp))
    error = error + abs(val(1)-indices(1,keys(i)))
    error = error + abs(val(2)-indices(2,keys(i)))
    error = error + abs(val(3)-indices(3,keys(i)))
    error = error + abs(val(4)-indices(4,keys(i)))
  end do

  if (error.eq.0) test_successful = .true.

  if (test_successful) then
    write(stdout, fmt='(a)') "Test passed."
  else
    write(stderr, fmt='(a)') "Test failed."
    error stop
  end if

end program main

