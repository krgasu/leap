!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Block module
  ! Make sure blocks are generated correctly
  ! in serial and parallel
  use leapKinds
  use leapParallel
  use leapIO
  use leapBlock
  use mpi_f08
  implicit none
  type(parallel_obj)   :: parallel
  type(block_obj)      :: block
  integer              :: nx(3)
  integer              :: ngc
  real(wp)             :: L(3)
  integer              :: Nb(3)
  real(wp)             :: buff,volume
  integer              :: i,j,k
  logical              :: test_successful=.false.

  ! Launch parallel environment
  call parallel%initialize

  ! Domain size
  L  = [1.0_wp, 1.0_wp, 1.0_wp]

  ! Domain resolution
  nx = [24,24,2]

  ! Partition the domain
  select case (parallel%nproc)
  case (1)
    Nb = [1,1,1]
  case (2)
    Nb = [2,1,1]
  case (3)
    Nb = [3,1,1]
  case (4)
    Nb = [2,2,1]
  case default
    call parallel%stop("Run this test with 1 to 4 mpi ranks")
  end select

  ! Ghost cells
  ngc= 2

  ! Initialize block
  call block%initialize(ngc,parallel)
  call block%SetPeriodicity([.true.,.false.,.true.])
  call block%SetupUniformGrid([0.0_wp,0.0_wp,0.0_wp],L,[1,1,1],nx)
  call block%partition(Nb)

  ! Compute total volume
  associate (x => block%x, y => block%y, z => block%z)
    buff=0.0_wp
    do k=block%lo(3),block%hi(3)
      do j=block%lo(2),block%hi(2)
        do i=block%lo(1),block%hi(1)
          buff= buff + (x(i+1)-x(i))*(y(j+1)-y(j))*(z(k+1)-z(k))
        end do
      end do
    end do
  end associate
  call parallel%sum(buff,volume)

  ! Make sure the sum of the cell volumes equals the total volume
  if (abs(volume-L(1)*L(2)*L(3)).lt.1e-12_wp) test_successful=.true.


  if (.not.test_successful) then
    if (parallel%rank%mine.eq.parallel%rank%root) then
      write(*,*) "Sum of cell volumes:",volume
      write(*,*) "Total volume:", L(1)*L(2)*L(3)
    end if
    call block%info
  end if

  do k=1,parallel%nproc
    if (parallel%rank%mine.eq.k) write(*,fmt='(a,i2,a)') ' --- Block on MPI rank ',parallel%rank%mine,' ---'
    if (parallel%rank%mine.eq.k) call block%Info()
    call MPI_BARRIER(parallel%comm%g,i)
  end do

  if (parallel%rank%mine.eq.parallel%rank%root) &
    write(*,'(a,l1)') 'Is test successful? ', test_successful

  ! Finalize block structure
  call block%finalize()
  call parallel%finalize
end program main
