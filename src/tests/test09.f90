!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test IO with SILO
  ! Create fields and write them using SILO writer
  use leapKinds
  use leapParallel
  use leapBlock
  use leapIO
  use leapEulerian
  use iso_fortran_env, only: stderr => error_unit,&
                             stdout => output_unit
  implicit none
  type(parallel_obj)   :: parallel
  type(block_obj)      :: block
  type(eulerian_set)   :: data
  type(eulerian_obj_r) :: field0,field1,field2,field3
  integer              :: Nb(3)
  real(wp)             :: L(3)
  integer              :: nx(3)
  real(wp)             :: kx,ky,kz
  real(wp),parameter   :: twoPi=8.0_wp*atan(1.0_wp)
  integer              :: i,j,k
  real(wp)             :: time
  integer              :: iter
  logical              :: test_successful=.false.

  ! Initializations
  call parallel%initialize

  ! Domain size
  L(1)=1.0_wp
  L(2)=1.0_wp
  L(3)=1.0_wp

  ! Domain resolution
  nx = (/24,24,12/)

  ! Partition the domain
  select case (parallel%nproc)
  case (1)
    Nb(1)=1; Nb(2)=1; Nb(3)=1
  case (2)
    Nb(1)=2; Nb(2)=1; Nb(3)=1
  case (4)
    Nb(1)=2; Nb(2)=2; Nb(3)=1
  case default
    call parallel%stop("Run this test with 1, 2 or 4 mpi ranks")
  end select

  ! Initialize blocks and topo
  call block%initialize((/0.0_wp,0.0_wp,0.0_wp/),L, &
                        (/1,1,1/),nx,2,parallel)
  call block%partition(Nb)

  ! Intialize the flow fields
  call data%initialize(block,parallel)
  call data%add('field0',0,field0)
  call data%add('field1',1,field1)
  call data%add('field2',2,field2)
  call data%add('field3',3,field3)
  data%write_file="data"

  iter=0
  time=0.0_wp
  associate (xm => block%xm, ym => block%ym, zm => block%zm, &
             x  => block%x , y  => block%y , z  => block%z , &
             lo => block%lo, hi => block%hi, ngc=> block%ngc)
    ! Initialize field
    kx= twoPi/L(1)
    ky= twoPi/L(2)
    kz= twoPi/L(3)
    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          field0%cell(i,j,k) = cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k))
          field1%cell(i,j,k) = cos(kx*x (i))*cos(ky*ym(j))*sin(kz*zm(k))
          field2%cell(i,j,k) = cos(kx*xm(i))*sin(ky*y (j))*cos(kz*zm(k))
          field3%cell(i,j,k) = sin(kx*xm(i))*cos(ky*ym(j))*cos(kz*z (k))
        end do
      end do
    end do
  end associate


  call data%WriteSilo(iter,time,[character(str8)::'field0','field1','field2','field3'])

  test_successful = .true.

  ! Finalize
  call data%finalize
  call block%finalize
  call parallel%finalize

  if (test_successful) then
    if (parallel%rank%mine.eq.parallel%rank%root) write(stdout, fmt='(a)') "Test passed."
  else
    if (parallel%rank%mine.eq.parallel%rank%root) write(stderr, fmt='(a)') "Test failed."
    error stop
  end if
end program main
