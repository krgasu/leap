!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test HYPRE GPU solver
  use leapKinds
  use leapCli
  use leapParallel
  use leapBlock
  use leapEulerian
  use leapHypre
  use leapDiffOp
  use immersed_boundaries
  use leapLagrangian, only: KERNEL_TRIANGLE
  use mpi_f08
  use iso_fortran_env, only: stderr => error_unit,&
                             stdout => output_unit
  implicit none
  type(cli_obj)        :: cli
  type(block_obj)      :: block
  type(parallel_obj)   :: parallel
  !> Case: 1 = sphere, 2 = 2D cylinder
  integer              :: case_id
  integer, parameter   :: CASE_SPHERE   = 1
  integer, parameter   :: CASE_CYLINDER = 2
  !> Geometry
  real(wp)             :: L(3)
  real(wp)             :: xlo(3)
  real(wp)             :: xhi(3)
  integer              :: ilo(3)
  integer              :: ihi(3)
  integer              :: nx(3)
  integer              :: ngc=2
  integer              :: Nb(3)
  !> Immmersed Boundary
  type(eulerian_set)   :: data
  type(solid_set)      :: solid
  type(eulerian_obj_r) :: N1,N2,N3
  real(WP)             :: radius
  real(wp),parameter   :: Pi=4.0_wp*atan(1.0_wp)
  !> Poisson equation
  type(hypre_obj)      :: hypre
  type(op_obj)         :: op
  type(eulerian_obj_r) :: rhs,sol
  integer              :: solver_id
  !> Postprocessing
  real(wp)             :: dvol
  real(wp)             :: volume
  real(wp)             :: volume_ref
  real(wp)             :: volume_err
  logical              :: test_successful=.false.
  real(wp)             :: buf
  !> Iterators
  integer              :: i,j,k


  ! Command line options: HYPRE solver and number
  ! of grid points per direction
  call cli%get('s',  solver_id,default=1    )
  call cli%get('nx', nx(1),    default=32   )
  call cli%get('c',  case_id,  default=1    )

  ! Launch parallel environment
  call parallel%initialize

  ! Domain initialization
  ! -------------------------- !
  select case (case_id)
  case (CASE_SPHERE)
    ! Sphere case
    ! -------------------------- !
    ! Domain size
    L(1)=1.0_wp
    L(2)=1.0_wp
    L(3)=1.0_wp

    ! Sphere radius
    radius = L(1)/4.0_wp

    ! Uniform domain resolution
    nx = nx(1)

    ! Enforce decomposition
    select case (parallel%nproc)
    case (1)
      Nb(1)= 1;  Nb(2)=1;  Nb(3)=1
    case (2)
      Nb(1)= 2;  Nb(2)=1;  Nb(3)=1
    case (3)
      Nb(1)= 3;  Nb(2)=1;  Nb(3)=1
    case (4)
      Nb(1)= 2;  Nb(2)=2;  Nb(3)=1
    case (16)
      Nb(1)= 2;  Nb(2)=2;  Nb(3)=4
    case (18)
      Nb(1)= 3;  Nb(2)=3;  Nb(3)=2
    case default
      call parallel%stop("Run this test with 1, 2, 3, 4, 16, or 18 mpi ranks")
    end select
  case (CASE_CYLINDER)
    ! 2D cylinder case
    ! -------------------------- !
    L(1)=1.0_wp
    L(2)=1.0_wp
    L(3)=L(1)/real(nx(1),wp)

    ! Cylinder radius
    radius = L(1)/4.0_wp

    ! 2D uniform domain
    nx = [nx(1),nx(1),1]

    ! Enforce decomposition
    select case (parallel%nproc)
    case (1)
      Nb(1)= 1;  Nb(2)=1;  Nb(3)=1
    case (2)
      Nb(1)= 2;  Nb(2)=1;  Nb(3)=1
    case (3)
      Nb(1)= 3;  Nb(2)=1;  Nb(3)=1
    case (4)
      Nb(1)= 2;  Nb(2)=2;  Nb(3)=1
    case default
      call parallel%stop("Run this test with 1, 2, 3, 4 mpi ranks")
    end select
  end select

  ! Ghost cells
  ngc = 2

  ! Initialize block
  xlo=-L/2.0_wp; ilo=1
  xhi= L/2.0_wp; ihi=nx
  call block%initialize(xlo,xhi,ilo,ihi,ngc,parallel)

  ! Set periodicity
  select case (case_id)
  case (CASE_SPHERE)
    call block%SetPeriodicity([.false.,.false.,.false.])
  case (CASE_CYLINDER)
    call block%SetPeriodicity([.false.,.false.,.true.])
  end select

  ! Set parallel partition
  call block%partition(Nb)

  ! Initialize operators
  ! -------------------------- !
  call op%initialize(block,parallel)

  ! Immersed boundary initialization
  ! -------------------------- !
  call solid%initialize('solid',1,block,parallel)
  call solid%SetWriteFileName('solid.h5')

  ! Generate sphere/cylinder
  ! -------------------------- !
  if (parallel%rank%mine.eq.parallel%rank%root) then
    select case (case_id)
    case (CASE_SPHERE)
      call create_sphere(solid,radius)
    case (CASE_CYLINDER)
      call create_cylinder(solid,radius,L(3))
    end select
  end if

  call solid%communicate()
  call solid%localize()

  call solid%write(0,0.0_wp)

  ! Initialize field data
  ! -------------------------- !
  call data%initialize(block,parallel)
  call data%add('nx',  0,N1 )
  call data%add('ny',  0,N2 )
  call data%add('nz',  0,N3 )
  call data%add('div', 0,rhs)
  call data%add('sol', 0,sol)

  ! Setup filter width
  call solid%SetFilterKernel(KERNEL_TRIANGLE,KERNEL_TRIANGLE)
  call solid%SetFilterSize(block%dx(1)*1.5_wp)

  ! Filter normals on grid
  call solid%filter('N1',N1)
  call solid%filter('N2',N2)
  call solid%filter('N3',N3)

  ! Compute -div(N1,N2,N3)
  rhs=op%div('div',N1,N2,N3)
  rhs%cell = - rhs%cell

  ! Solve using HYPRE
  ! -------------------------- !
  call hypre%initialize(block,parallel)
  select case (solver_id)
  case (1)
    call hypre%SelectSolver('IJ-AMG-NONE',MaxTol=1.0e-10_wp,MaxIt=30 )
  case (2)
    call hypre%SelectSolver('IJ-PCG-DS',  MaxTol=1.0e-10_wp,MaxIt=500)
  case (3)
    call hypre%SelectSolver('IJ-PCG-AMG', MaxTol=1.0e-10_wp,MaxIt=500)
  case (4)
    call hypre%SelectSolver('PCG', MaxTol=1.0e-10_wp,MaxIt=100)
  case (5)
    call hypre%SelectSolver('PFMG',MaxTol=1.0e-10_wp,MaxIt=100,RelaxType=1)
  case (6)
    call hypre%SelectSolver('SMG', MaxTol=1.0e-10_wp,MaxIt=100)
  case default
    write(*,*) "Uknown HYPRE solver"
    write(stderr, fmt='(a)') "Test failed."
    error stop
  end select
  ! Setup hypre internals
  call hypre%Setup()

  ! Send right hand side to hypre
  call hypre%SetRHS(rhs)

  ! Compute solution: corresponding to solid volume fraction
  call hypre%Solve(sol)

  data%write_file="data.h5"
  call data%write(0,0.0_wp)

  ! Compute solid volume from solution
  volume=0.0_wp
  do k=block%lo(3),block%hi(3)
    do j=block%lo(2),block%hi(2)
      do i=block%lo(1),block%hi(1)
        dvol = block%dxm(i)*block%dym(j)*block%dzm(k)
        volume= volume + sol%cell(i,j,k)* dvol
      end do
    end do
  end do

  call parallel%sum(volume,buf); volume=buf

  ! Reference volume
  select case (case_id)
  case (CASE_SPHERE)
    ! Volume of a sphere
    volume_ref=4.0_wp/3.0_wp*Pi*radius**3
  case (CASE_CYLINDER)
    ! Volume of a cylinder
    volume_ref=L(3)*Pi*radius**2
  end select

  ! Volume relative error
  volume_err= abs(volume-volume_ref)/volume_ref

  if (parallel%rank%mine.eq.parallel%rank%root) then
    write(stdout,fmt='(a18,es12.4)') 'HYPRE residual:   ', hypre%rel
    write(stdout,fmt='(a18,i12)'   ) 'HYPRE iterations: ', hypre%it
    write(stdout,fmt='(a18,es12.4)') 'Exact volume:     ', volume_ref
    write(stdout,fmt='(a18,es12.4)') 'Computed volume:  ', volume
    write(stdout,fmt='(a18,es12.4)') 'Volume rel. error:', volume_err
  end if

  if (volume_err.lt. 1.0e-1_wp) test_successful=.true.

  ! Finalizations
  call hypre%finalize
  call op%finalize
  call solid%finalize
  call data%finalize
  call block%finalize
  call parallel%finalize

  if (test_successful) then
    if (parallel%rank%mine.eq.parallel%rank%root) write(stdout, fmt='(a)') "Test passed."
  else
    if (parallel%rank%mine.eq.parallel%rank%root) write(stderr, fmt='(a)') "Test failed."
    error stop
  end if
  contains
    subroutine create_sphere(my_solid,my_radius)
      implicit none
      type(solid_set),intent(inout):: my_solid
      real(WP),       intent(in)   :: my_radius
      integer(kind=8)      :: id
      real(WP)             :: facet_size
      real(wp)             :: dSA
      real(wp)             :: d_phi
      real(wp),allocatable :: phi(:)
      real(wp),allocatable :: theta(:)
      integer              :: n_phi
      integer              :: n_theta
      real(wp)             :: phi_mid
      real(wp)             :: theta_mid
      real(wp)             :: shift

      ! Place sphere at center of domain
      ! -------------------------- !
      shift = -my_radius+epsilon(1.0_wp)
      my_solid%p(1)%xc=0.0_wp + [shift,shift,0.0_wp]
      my_solid%p(1)%vc=0.0_wp
      my_solid%p(1)%Ac=0.0_wp

      ! Discretize surface
      ! -------------------------- !
      facet_size=block%dx(1)/2.0_wp

      !Setup target surface area and phi angle
      dSA=facet_size**2

      ! Theta angle
      n_phi   = ceiling(2.0_wp*Pi/(facet_size/my_radius))
      n_theta = ceiling(4.0*Pi*my_radius**2/dSA/n_phi)

      ! Create spherical grid
      allocate(phi  (n_phi+1)  )
      allocate(theta(n_theta+1))

      d_phi=2.0_wp*Pi/real(n_phi,wp)
      do i=1,n_phi+1
        phi(i)= (i-1)*d_phi
      end do
      do j=1,n_theta+1
        theta(j)= acos(max(-1.0_wp, 1.0_wp - (j-1)*dSA/(my_radius**2*d_phi)))
      end do
      theta(n_theta+1)= Pi

      ! Create n_phi*n_theta markers
      call my_solid%p(1)%resize(n_phi*n_theta)

      ! Place markers at center of facet
      select type(markers =>my_solid%p(1)%p)
      type is (marker_obj)

        do i=1,n_phi
          do j=1,n_theta
            ! Facet ID
            id = n_theta*(i-1)+j
            markers(id)%id=id

            ! Position
            phi_mid   = 0.5_wp*(phi(i+1)+phi(i))
            theta_mid = 0.5_wp*(theta(j+1)+theta(j))

            markers(id)%p(1)= my_solid%p(1)%xc(1)+ my_radius*sin(theta_mid)*cos(phi_mid)
            markers(id)%p(2)= my_solid%p(1)%xc(2)+ my_radius*sin(theta_mid)*sin(phi_mid)
            markers(id)%p(3)= my_solid%p(1)%xc(3)+ my_radius*cos(theta_mid)

            ! Surface area
            markers(id)%SA = my_radius**2*d_phi *         &
                         ( cos(theta(j))-cos(theta(j+1)))

            ! Normal vector
            markers(id)%n = (/ sin(theta_mid)*cos(phi_mid), &
                               sin(theta_mid)*sin(phi_mid), &
                               cos(theta_mid)/)
            ! Velocity
            markers(id)%v = 0.0_wp
          end do
        end do
      end select
      deallocate(phi)
      deallocate(theta)
      return
    end subroutine create_sphere
    subroutine create_cylinder(my_solid,my_radius,Lc)
      implicit none
      type(solid_set),intent(inout):: my_solid
      real(WP),       intent(in)   :: my_radius
      real(WP),       intent(in)   :: Lc
      integer(kind=8)      :: id
      real(WP)             :: facet_size
      integer              :: n_phi
      real(wp)             :: d_phi
      integer              :: n_axis
      real(wp)             :: d_axis
      real(wp)             :: phi_mid
      real(wp)             :: shift

      ! Place cylinder at center of domain
      ! -------------------------- !
      shift = -my_radius+epsilon(1.0_wp)
      my_solid%p(1)%xc=0.0_wp + [shift,shift,0.0_wp]
      my_solid%p(1)%vc=0.0_wp
      my_solid%p(1)%Ac=0.0_wp

      ! Discretize surface
      ! -------------------------- !
      facet_size=block%dx(1)/2.0_wp

      n_phi=ceiling(2.0_wp*Pi*my_radius/facet_size)
      d_phi=2.0_wp*Pi/real(n_phi,wp)

      n_axis= max(1,floor(Lc/facet_size))
      d_axis= Lc/real(n_axis,wp)

      ! Create markers
      call my_solid%p(1)%resize(n_phi*n_axis)

      ! Place markers at center of facet
      select type(markers =>my_solid%p(1)%p)
      type is (marker_obj)

        do i=1,n_phi
          do j=1,n_axis
            ! Facet ID
            id = n_axis*(i-1)+j
            markers(id)%id=int(id,kind=8)

            ! Position
            phi_mid   = (i-1)*d_phi + d_phi/2.0_wp

            markers(id)%p(1)= my_solid%p(1)%xc(1)+ my_radius*cos(phi_mid)
            markers(id)%p(2)= my_solid%p(1)%xc(2)+ my_radius*sin(phi_mid)
            markers(id)%p(3)= my_solid%p(1)%xc(3)+ (j-1)*d_axis + d_axis/2.0_wp - Lc/2.0_wp

            ! Surface area
            markers(id)%SA = my_radius*d_phi*d_axis

            ! Normal vector
            markers(id)%n = [cos(phi_mid), sin(phi_mid), 0.0_wp]

            ! Velocity
            markers(id)%v = 0.0_wp
          end do
        end do
      end select
      return
    end subroutine create_cylinder
end program main
