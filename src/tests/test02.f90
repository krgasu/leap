!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Parallel module
  ! Make sure the parallel MPI environment
  ! is correctly setup
  use leapKinds
  use leapParallel
  implicit none
  type(parallel_obj):: parallel
  logical :: test_successful=.false.

  !< Test: parallel
  call parallel%initialize
  write(*,*) 'parallel%nproc = ', parallel%nproc
  write(*,*) 'parallel%rank  = ', parallel%rank%mine
  write(*,*) 'parallel%root  = ', parallel%rank%root
  call parallel%finalize

  test_successful=.true.
  write(*,'(a,l1)') 'Is test successful? ', test_successful
end program main
