!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test particles_point module
  ! Checks that Lagrangian point particles
  ! are correctly initialized, defined, read/written,
  ! and communicated in parallel.
  use leapKinds
  use leapCli
  use leapParallel
  use leapBlock
  use particles_point
  use iso_fortran_env, only: stderr => error_unit,&
                             stdout => output_unit
  implicit none
  type(block_obj)   :: block
  type(parallel_obj):: parallel
  type(particle_set):: partset1
  type(particle_set):: partset2
  real(wp)          :: xlo(3)
  real(wp)          :: xhi(3)
  integer           :: ilo(3)
  integer           :: ihi(3)
  integer           :: nx(3)
  integer           :: ngc=2
  real(wp)          :: L(3)
  integer           :: Nb(3)
  integer           :: n
  integer           :: rank
  integer           :: id
  real(wp)          :: time
  integer           :: iter
  real(wp)          :: error
  logical           :: test_successful=.false.
  integer,parameter :: npar_per_rank=10


  ! Launch parallel environment
  call parallel%initialize


  ! Domain size
  L(1)=1.0_wp
  L(2)=1.0_wp
  L(3)=1.0_wp

  ! Domain resolution
  nx = (/24,24,12/)

  ! Partition the domain
  select case (parallel%nproc)
  case (1)
    Nb(1)= 1;  Nb(2)=1;  Nb(3)=1
  case (2)
    Nb(1)= 2;  Nb(2)=1;  Nb(3)=1
  case (3)
    Nb(1)= 3;  Nb(2)=1;  Nb(3)=1
  case (4)
    Nb(1)= 2;  Nb(2)=2;  Nb(3)=1
  case default
    call parallel%stop("Run this test with 1 to 4 mpi ranks")
  end select

  ! Ghost cells
  ngc= 2

  ! Initialize block
  xlo=0.0_wp; ilo=1
  xhi=L     ; ihi=nx
  call block%initialize(xlo,xhi,ilo,ihi,ngc,parallel)
  call block%SetPeriodicity((/.true.,.true.,.true./))
  call block%partition(Nb)

  ! Manually define 2 paritlces
  call partset1%initialize('partset1', block,parallel,"default")


  ! Activate N particles on each rank
  partset1%count_=npar_per_rank
  call partset1%resize(partset1%count_)

  do n=1,partset1%count_
    ! Particle global ID
    partset1%p(n)%id  = (parallel%rank%mine-1)*partset1%count_+n
    ! Position
    partset1%p(n)%p(1)= (real(n,wp)-0.5_wp)*L(1)/real(partset1%count_,wp)
    partset1%p(n)%p(2)= (real(parallel%rank%mine,wp)-0.5_wp)*L(2)/real(parallel%nproc,wp)
    partset1%p(n)%p(3)= L(3)/2.0_wp
  end do

  ! Recycle any particle with negative ID
  call partset1%recycle
  call partset1%communicate

  partset1%write_file="partset1.h5"
  call partset1%write(0,0.0_wp)

  ! Initialize second particle
  call partset2%initialize('partset2', block,parallel,"default")

  ! Read previously written file
  partset2%read_file= "partset1.h5"
  call partset2%read(iter,time)

  error=0.0_wp
  do n=1,partset2%count_
    ! Get the original rank and local id
    rank= floor((partset2%p(n)%id-1)/real(npar_per_rank,wp)) + 1
    id  = int(partset2%p(n)%id-(rank-1)*npar_per_rank,kind=4)

    ! Compute error
    error = error + abs(partset2%p(n)%id  - ((rank-1)*npar_per_rank+id)                        )
    error = error + abs(partset2%p(n)%p(1)- (real(id,wp)-0.5_wp)*L(1)/real(npar_per_rank,wp)   )
    error = error + abs(partset2%p(n)%p(2)- (real(rank,wp)-0.5_wp)*L(2)/real(parallel%nproc,wp))
    error = error + abs(partset2%p(n)%p(3)- L(3)/2.0_wp                                        )
  end do


  if (error.lt.1e-12) test_successful=.true.

  partset2%write_file="partset2.h5"
  call partset2%write(0,0.0_wp)

  ! Print structure info
  if(parallel%rank%mine.eq.parallel%rank%root) call partset1%info
  if(parallel%rank%mine.eq.parallel%rank%root) call partset2%info

  ! Finalizations
  call partset1%finalize
  call partset2%finalize
  call block%finalize
  call parallel%finalize

  if (test_successful) then
    write(stdout, fmt='(a)') "Test passed."
  else
    write(stderr, fmt='(a)') "Test failed."
    error stop
  end if
end program main
