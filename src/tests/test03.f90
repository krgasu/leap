!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Monitor module
  ! Make sure monitors write to stdout
  ! and file as expected
  use leapKinds
  use leapParallel
  use leapMonitor
  implicit none
  type(monitor_set) :: monitors
  type(parallel_obj):: parallel
  logical           :: test_successful=.false.


  !< Test: monitor
  call parallel%initialize
  call monitors%initialize(parallel)

  write(*,*) "THIS TEST WILL GENERATE TWO MONITORS"
  write(*,*) "-- MONITOR_1 WRITES TO STDOUT"
  write(*,*) "-- MONITOR_2 WRITES TO FILE"
  call monitors%create("monitor_1",2,stdout=.true.,sep=" ")
  call monitors%set("monitor_1",1,label="AverylonglabelABCDEGHIJKLMNOPQRSTUVWXYZ")
  call monitors%set("monitor_1",2,label="Col2")
  call monitors%set("monitor_1",1,value=120   )
  call monitors%set("monitor_1",2,value=4.2_wp)

  call monitors%create("monitor_2",3,stdout=.false.,sep="|")
  call monitors%set("monitor_2",1,label="Col1" )
  call monitors%set("monitor_2",2,label="Col2" )
  call monitors%set("monitor_2",3,label="Col3" )
  call monitors%set("monitor_2",1,value=4.2_WP )
  call monitors%set("monitor_2",2,value=120    )
  call monitors%set("monitor_2",3,value=.true. )

  write(*,*) "WRITE MONITORS:"
  call monitors%print(print_labels=.true.)
  call monitors%print

  call monitors%set("monitor_1",1,value=240    )
  call monitors%set("monitor_1",2,value=8.4_wp )

  call monitors%set("monitor_2",1,value=8.4_wp )
  call monitors%set("monitor_2",2,value=240    )
  call monitors%set("monitor_2",3,value=.false.)

  call monitors%print

  write(*,*) "MONITOR INFO:"
  call monitors%info

  call monitors%finalize
  call parallel%finalize

  test_successful=.true.
  write(*,'(a,l1)') 'Is test successful? ', test_successful
end program main
