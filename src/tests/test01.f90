!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Parser module
  ! Make sure input files are correctly
  ! parsed
  use leapKinds
  use leapParser
  implicit none
  type(parser_obj)::parser
  logical  :: item1, item5(2)
  real(wp) :: item2,item6(3)
  integer  :: item3, item7(2)
  character(len=str64):: item4, item8(2)
  integer  :: fid,ierr
  logical  :: test_successful


  ! Generate an input file
  open(newunit=fid, file = 'test_input', action ='WRITE', iostat = ierr)
  if (ierr.ne.0) stop "Error writing input file"

  ! Write scalar (0D) entries
  write(fid,fmt='(a)') "Item 1 : .true."
  write(fid,fmt='(a)') "Item 2 : 3.14"
  write(fid,fmt='(a)') "Item 3 : 120"
  write(fid,fmt='(a)') "Item 4 : hello"

  ! Write scalar (1D) entries
  write(fid,fmt='(a)') "Item 5 : .true. .false."
  write(fid,fmt='(a)') "Item 6 : 1.2 2.4 3.6"
  write(fid,fmt='(a)') "Item 7 : 120 240"
  write(fid,fmt='(a)') "Item 8 : hello world"
  close(fid)
  !< Test: parser
  call parser%Initialize
  call parser%ParseFile('./test_input')
  call parser%Get('Item 1',item1)
  call parser%Get('Item 2',item2)
  call parser%Get('Item 3',item3)
  call parser%Get('Item 4',item4)
  call parser%Get('Item 5',item5)
  call parser%Get('Item 6',item6)
  call parser%Get('Item 7',item7)
  call parser%Get('Item 8',item8)


  test_successful=.true.
  if ( .not. (item1 .eqv..true.)) &
        test_successful=.false.
  if ( .not. (abs(item2-3.14_wp).lt.epsilon(1.0_wp))) &
        test_successful=.false.
  if ( .not. (item3 .eq. 120))   &
        test_successful=.false.
  if ( .not. (trim(adjustl(item4)) .eq. "hello")) &
        test_successful=.false.
  if ( .not. (item5(1) .eqv..true.) .or. &
       .not. (item5(2) .eqv..false.) ) &
        test_successful=.false.
  if ( .not. (abs(item6(1)-1.2_wp).lt.epsilon(1.0_wp)).or. &
       .not. (abs(item6(2)-2.4_wp).lt.epsilon(1.0_wp)).or. &
       .not. (abs(item6(3)-3.6_wp).lt.epsilon(1.0_wp))) &
        test_successful=.false.
  if ( .not. (item7(1).eq.120).or. &
       .not. (item7(2).eq.240)) &
        test_successful=.false.
  if ( .not. (trim(adjustl(item8(1))).eq."hello").or. &
       .not. (trim(adjustl(item8(2))).eq."world")) &
        test_successful=.false.

  ! Print content of parser
  if (.not.test_successful) call parser%print

  ! Delete file
  open(newunit=fid, file = 'test_input', status ='OLD', iostat = ierr)
  close(fid,status='DELETE')

  call parser%Finalize

  write(*,'(a,l1)') 'Is test successful? ', test_successful
end program main
