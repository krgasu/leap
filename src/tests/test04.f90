!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 module my_part
   use leapLagrangian
  implicit none
  type, extends(lagrangian_obj) :: part
      contains
      procedure :: assign => my_part_assign
  end type part
  contains
    subroutine my_part_assign(this,val)
      class(part), intent(inout) :: this                   !! A Lagrangian object
      class(lagrangian_obj), intent(in)    :: val          !! Value to be assigned
        select type (val)
        type is (part)
          this%p      = val%p
          this%c      = val%c
          this%id     = val%id
        end select
        return
    end subroutine my_part_assign
 end module my_part

program main
  !> Test IO module
  ! Chek files are correctly written/read
  !
  ! Supporteed Read/Write IO:
  !    - h5hut
  ! Supported Read only IO:
  !    - NGA raw files
  ! Supported Write only IO:
  !    - SILO
  use leapParallel
  implicit none
  type(parallel_obj):: parallel

  call parallel%initialize

  call check_IO_h5hut
  call check_IO_nga

  call parallel%finalize

  contains
    subroutine check_IO_h5hut
      use leapKinds
      use leapIO
      use my_part
      implicit none
      type(h5hut_obj)   :: h5
      type(part), allocatable :: particles(:)
      integer :: NPx, NPy,npoints
      integer :: i,j,n
      real(WP):: dx,dy,dt
      integer :: iter,iter_old
      real(WP):: time

      ! Initializations
      call h5%initialize("test.h5","W",parallel)

      ! Define data
      NPx=5; NPy=3
      dx=0.1_WP
      dy=0.1_WP
      dt=0.01_WP

      allocate(particles(NPx*NPy))
      ! Initial conditions
      iter=0; time=0.0_WP
      call h5%NewTimeStep(iter,time)
      do j=1,NPy
        do i=1,NPx
          n= (j-1)*5+i
          particles(n)%id=(parallel%rank%mine-1)*NPx*NPy+n
          particles(n)%c=(/i-1   ,(parallel%rank%mine-1)*NPy+j-1,0 /)
          particles(n)%p(1)=dx*particles(n)%c(1)
          particles(n)%p(2)=dx*particles(n)%c(2)
          particles(n)%p(3)=0.0_WP
        end do
      end do
      call h5%write("x", particles(:)%p(1)       )
      call h5%write("y", particles(:)%p(2)       )
      call h5%write("z", particles(:)%p(3)       )
      call h5%write("id",particles(:)%id         )
      call h5%flush

      ! Iterate
      do iter=1,10
        time=iter*dt
        call h5%NewTimeStep(iter,time)

        ! Shear particle array
        do j=1,NPy
          do i=1,NPx
            n=(j-1)*5+i
            particles(n)%p=particles(n)%p +                            &
                           dt*particles(n)%p(2)*(/1.0_WP,0.0_WP,0.0_WP/)
          end do
        end do

        ! Write data
        call h5%write("x", particles(:)%p(1)       )
        call h5%write("y", particles(:)%p(2)       )
        call h5%write("z", particles(:)%p(3)       )
        call h5%write("id",particles(:)%id         )
        call h5%flush
      end do
      ! Close hdf5 file
      deallocate(particles)
      call h5%finalize

      ! Now start over. Read the particles and further shear them
      ! Restart and read this time
      call h5%initialize("test.h5","RW",parallel)
      call h5%LastTimeStep(iter,time)
      call h5%getnpoints(npoints)
      write(*,*) 'iter = ', iter
      write(*,*) 'time = ', time
      write(*,*) 'npoints = ', npoints
      allocate(particles(npoints))
      call h5%read("x", particles(:)%p(1)       )
      call h5%read("y", particles(:)%p(2)       )
      call h5%read("z", particles(:)%p(3)       )
      call h5%read("id",particles(:)%id         )

      ! Iterate
      iter_old=iter
      do iter=iter_old+1,iter_old+10
        time=iter*dt
        call h5%NewTimeStep(iter,time)

        ! Shear particle array
        do n=1,npoints
          particles(n)%p=particles(n)%p +                            &
                         dt*particles(n)%p(2)*(/1.0_WP,0.0_WP,0.0_WP/)
        end do

        ! Write data
        call h5%write("x", particles(:)%p(1)       )
        call h5%write("y", particles(:)%p(2)       )
        call h5%write("z", particles(:)%p(3)       )
        call h5%write("id",particles(:)%id         )
        call h5%flush
      end do

      ! Finalizations
      deallocate(particles)
      call h5%finalize

      !>@todo
      ! Standardize this test and write to stdout whether
      ! it was successful or not.
      ! ------------------------------------------------------
      ! author: Mohamed Houssem Kasbaoui
      ! date: 2020-04-22 13:44
      ! @endtodo
      ! ------------------------------------------------------
      return
    end subroutine check_IO_h5hut

    subroutine check_IO_nga
      use leapKinds
      use leapBlock
      use leapEulerian
      implicit none
      type(block_obj)      :: block
      type(eulerian_set)   :: data
      type(eulerian_obj_r) :: U, V, W
      real(wp)             :: xlo(3)
      real(wp)             :: xhi(3)
      integer              :: ilo(3)
      integer              :: ihi(3)
      integer              :: nx(3)
      integer              :: Nb(3)
      integer              :: ngc
      real(wp)             :: L(3)
      real(wp)             :: time
      integer              :: iter

      ! Domain size
      L(1)=1.0_wp
      L(2)=1.0_wp
      L(3)=1.0_wp

      ! Domain resolution
      nx = (/512,256,256/)

      ! Ghost cells
      ngc= 2

      ! Partition the domain
      select case (parallel%nproc)
      case (1)
        Nb(1)=1; Nb(2)=1; Nb(3)=1
      case (2)
        Nb(1)=2; Nb(2)=1; Nb(3)=1
      case (4)
        Nb(1)=2; Nb(2)=2; Nb(3)=1
      case default
        call parallel%stop("Run this test with 1 to 4 mpi ranks")
      end select

      ! Initialize blocks and topo
      xlo=0.0_wp; ilo=1
      xhi=L     ; ihi=nx
      call block%initialize(xlo,xhi,ilo,ihi,ngc,parallel)
      call block%partition(Nb)

      ! Intialize the flow fields
      call data%initialize(block,parallel)
      call data%add('U',1,U)
      call data%add('V',2,V)
      call data%add('W',3,W)

      ! Read file
      data%read_file="data.init"
      call data%ReadNGA(iter,time)
      !>@todo
      ! Instead of relying on an external data.init
      ! manually write an NGA raw file with known values
      ! that can we can check whether they have been read
      ! correctly or not.
      ! ------------------------------------------------------
      ! author: Mohamed Houssem Kasbaoui
      ! date: 2020-04-22 13:43
      ! @endtodo
      ! ------------------------------------------------------

      ! Write file
      !data%overwrite=.false.
      data%write_file="data.h5"
      call data%write(0,0.0_wp)


      ! Finalize
      call data%finalize
      call block%finalize

      return
    end subroutine check_IO_nga
end program main
