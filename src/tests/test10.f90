!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Differential Operator module
  ! Create field and differentiate in
  ! x, y, and z directions using
  ! 2nd, 4th, or 6th order schemes.
  use leapKinds
  use leapCli
  use leapParallel
  use leapBlock
  use leapIO
  use leapEulerian
  use leapDiffOp
  use iso_fortran_env, only: stderr => error_unit,&
                             stdout => output_unit
  implicit none
  type(cli_obj)        :: cli
  type(parallel_obj)   :: parallel
  type(block_obj)      :: block
  type(op_obj)         :: op
  integer              :: scheme_order
  integer              :: Nb(3)
  real(wp)             :: L(3)
  integer              :: nx(3)
  logical              :: test_successful=.true.

  ! Command line options: scheme order
  call cli%get('scheme',scheme_order,default=2    )
  select case (scheme_order)
  case (2,4,6)
    ! Nothing to do
  case default
    error stop "Only 2nd, 4th, and 6th order schemes allowed"
  end select

  ! Initializations
  call parallel%initialize

  ! Domain size
  L = [1.0_wp, 1.0_wp, 1.0_wp]

  ! Domain resolution
  nx = [24,24,24]

  ! Partition the domain
  select case (parallel%nproc)
  case (1)
    Nb(1)= 1;  Nb(2)=1;  Nb(3)=1
  case (2)
    Nb(1)= 2;  Nb(2)=1;  Nb(3)=1
  case (3)
    Nb(1)= 3;  Nb(2)=1;  Nb(3)=1
  case (4)
    Nb(1)= 2;  Nb(2)=2;  Nb(3)=1
  case (16)
    Nb(1)= 2;  Nb(2)=2;  Nb(3)=4
  case (18)
    Nb(1)= 3;  Nb(2)=3;  Nb(3)=2
  case default
    call parallel%stop("Run this test with 1, 2, 3, 4, 16, or 18 mpi ranks")
  end select

  ! Initialize blocks and topo
  call block%initialize((/0.0_wp,0.0_wp,0.0_wp/),L, &
                        (/1,1,1/),nx,scheme_order-1,parallel)
  call block%partition(Nb)
  call block%SetPeriodicity([.true.,.true.,.true.])

  ! Initialize operators
  call op%initialize(block,parallel,order=scheme_order)

  ! Check interpolations
  call test_interpolations()

  ! Check gradients are computed correctly
  call test_gradients()

  ! Check convective terms are computed correctly
  call test_convective()

  ! Check Laplacian is computed correctly
  call test_laplacian()

  ! Finalize
  call op%finalize
  call block%finalize
  call parallel%finalize
  if (test_successful) then
    if (parallel%rank%mine.eq.parallel%rank%root) write(stdout, fmt='(a)') "Test passed."
  else
    if (parallel%rank%mine.eq.parallel%rank%root) write(stderr, fmt='(a)') "Test failed."
    error stop
  end if
  contains
    subroutine test_interpolations()
      implicit none
      type(eulerian_set)   :: data
      type(eulerian_obj_r) :: V, Vint(3)
      !real(wp)             :: time
      !integer              :: iter
      real(wp)             :: kx,ky,kz
      real(wp),parameter   :: twoPi=8.0_wp*atan(1.0_wp)
      integer              :: i,j,k
      real(wp)             :: bufr
      real(wp)             :: err(3)
      real(wp)             :: dVolx,dVoly,dVolz

      ! Intialize the flow fields
      call data%initialize(block,parallel)
      call data%add('V', 0,V   )
      call data%add('V1',1,Vint(1))
      call data%add('V2',2,Vint(2))
      call data%add('V3',3,Vint(3))
      data%write_file="data"

      associate (xm => block%xm, ym => block%ym, zm => block%zm, &
                 x  => block%x , y  => block%y , z  => block%z , &
                 lo => block%lo, hi => block%hi, ngc=> block%ngc)
        ! Initialize field
        kx= twoPi/L(1)
        ky= twoPi/L(2)
        kz= twoPi/L(3)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              V%cell(i,j,k) = cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k))
            end do
          end do
        end do
        call V%UpdateGhostCells

        Vint(1)= op%intrp1(V)
        Vint(2)= op%intrp2(V)
        Vint(3)= op%intrp3(V)

        err = 0.0_wp
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              dVolx = (xm(i) -xm(i-1))*(y(j+1)-y(j))   *(z(k+1)-z(k))
              dVoly = (x(i+1)-x(i))   *(ym(j) -ym(j-1))*(z(k+1)-z(k))
              dVolz = (x(i+1)-x(i))   *(y(j+1)-y(j))   *(zm(k) -zm(k-1))
              err(1) = err(1) + abs(Vint(1)%cell(i,j,k) - cos(kx*x (i))*cos(ky*ym(j))*cos(kz*zm(k)))*dVolx
              err(2) = err(2) + abs(Vint(2)%cell(i,j,k) - cos(kx*xm(i))*cos(ky*y (j))*cos(kz*zm(k)))*dVoly
              err(3) = err(3) + abs(Vint(3)%cell(i,j,k) - cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*z (k)))*dVolz
            end do
          end do
        end do

        call parallel%sum(err(1),bufr); bufr=err(1)/(L(1)*L(2)*L(3))
        call parallel%sum(err(2),bufr); bufr=err(2)/(L(1)*L(2)*L(3))
        call parallel%sum(err(3),bufr); bufr=err(3)/(L(1)*L(2)*L(3))
      end associate

      if (parallel%rank%mine.eq.parallel%rank%root) then
        write(stdout,fmt='(a12,es12.4)') 'Error x1m->x1: ', err(1)
        write(stdout,fmt='(a12,es12.4)') 'Error x3m->x2: ', err(2)
        write(stdout,fmt='(a12,es12.4)') 'Error x3m->x3: ', err(3)
      end if

      if (maxval(err).gt. 1.0e-1_wp) test_successful=.false.

      !iter=0; time=0.0_wp
      !call data%WriteSilo(iter,time)

      call data%finalize
      return
    end subroutine test_interpolations
    subroutine test_gradients()
      implicit none
      type(eulerian_set)   :: data
      type(eulerian_obj_r) :: U, dUdx, dUdy, dUdz
      !real(wp)             :: time
      !integer              :: iter
      real(wp)             :: kx,ky,kz
      real(wp),parameter   :: twoPi=8.0_wp*atan(1.0_wp)
      integer              :: i,j,k
      real(wp)             :: bufr
      real(wp)             :: err(3)
      real(wp)             :: dVolx,dVoly,dVolz

      ! Intialize the flow fields
      call data%initialize(block,parallel)
      call data%add('u',0,U)
      call data%add('dudx',1,dUdx)
      call data%add('dudy',2,dUdy)
      call data%add('dudz',3,dUdz)
      data%write_file="data"

      associate (xm => block%xm, ym => block%ym, zm => block%zm, &
                 x  => block%x , y  => block%y , z  => block%z , &
                 lo => block%lo, hi => block%hi, ngc=> block%ngc)
        ! Initialize field
        kx= twoPi/L(1)
        ky= twoPi/L(2)
        kz= twoPi/L(3)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              U%cell(i,j,k) = cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k))
            end do
          end do
        end do
        call U%UpdateGhostCells

        dUdx= op%d1dx1(U)
        dUdy= op%d1dx2(U)
        dUdz= op%d1dx3(U)

        err = 0.0_wp
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              dVolx = (xm(i)-xm(i-1))*(y(j+1)-y(j))*(z(k+1)-z(k))
              dVoly = (x(i+1)-x(i))*(ym(j)-ym(j-1))*(z(k+1)-z(k))
              dVolz = (x(i+1)-x(i))*(y(j+1)-y(j))*(zm(k)-zm(k-1))
              err(1) = err(1) + abs(-kx*sin(kx*x (i))*cos(ky*ym(j))*cos(kz*zm(k)) - dUdx%cell(i,j,k))*dVolx
              err(2) = err(2) + abs(-ky*cos(kx*xm(i))*sin(ky*y (j))*cos(kz*zm(k)) - dUdy%cell(i,j,k))*dVoly
              err(3) = err(3) + abs(-kz*cos(kx*xm(i))*cos(ky*ym(j))*sin(kz*z (k)) - dUdz%cell(i,j,k))*dVolz
            end do
          end do
        end do

        call parallel%sum(err(1),bufr); bufr=err(1)/(L(1)*L(2)*L(3))
        call parallel%sum(err(2),bufr); bufr=err(2)/(L(1)*L(2)*L(3))
        call parallel%sum(err(3),bufr); bufr=err(3)/(L(1)*L(2)*L(3))
      end associate

      if (parallel%rank%mine.eq.parallel%rank%root) then
        write(stdout,fmt='(a12,es12.4)') 'Error dUdx: ', err(1)
        write(stdout,fmt='(a12,es12.4)') 'Error dUdy: ', err(2)
        write(stdout,fmt='(a12,es12.4)') 'Error dUdz: ', err(3)
      end if

      if (maxval(err).gt. 1.0e-1_wp) test_successful=.false.

      !iter=0; time=0.0_wp
      !call data%WriteSilo(iter,time)

      call data%finalize
      return
    end subroutine test_gradients
    subroutine test_convective()
      implicit none
      type(eulerian_set)   :: data
      type(eulerian_obj_r) :: V(3),Vi(3,3),F(3,3)
      type(eulerian_obj_r) :: conv(3,3),sol(3,3)
      real(wp)             :: kx,ky,kz
      real(wp),parameter   :: twoPi=8.0_wp*atan(1.0_wp)
      real(wp)             :: err(3,3)
      real(wp)             :: bufr
      integer              :: i,j,k

      ! Intialize the flow fields
      call data%initialize(block,parallel)
      call data%add('V1', 1, V(1))
      call data%add('V2', 2, V(2))
      call data%add('V3', 3, V(3))

      call data%add('sol11', 1, sol(1,1))
      call data%add('sol21', 1, sol(2,1))
      call data%add('sol31', 1, sol(3,1))

      call data%add('sol12', 2, sol(1,2))
      call data%add('sol22', 2, sol(2,2))
      call data%add('sol32', 2, sol(3,2))

      call data%add('sol13', 3, sol(1,3))
      call data%add('sol23', 3, sol(2,3))
      call data%add('sol33', 3, sol(3,3))

      !call data%add('V1i', 0, Vi(1))
      !call data%add('V2i', 0, Vi(2))
      !call data%add('V3i', 0, Vi(3))
      data%write_file="data_conv"

      associate (xm => block%xm, ym => block%ym, zm => block%zm, &
                 x  => block%x , y  => block%y , z  => block%z , &
                 lo => block%lo, hi => block%hi, ngc=> block%ngc)

        ! Initialize field
        kx= twoPi/L(1)
        ky= twoPi/L(2)
        kz= twoPi/L(3)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              V(1)%cell(i,j,k) = cos(kx*x (i))*cos(ky*ym(j))*sin(kz*zm(k))
              V(2)%cell(i,j,k) = cos(kx*xm(i))*sin(ky*y (j))*cos(kz*zm(k))
              V(3)%cell(i,j,k) = sin(kx*xm(i))*cos(ky*ym(j))*cos(kz*z (k))
              ! Convective fluxes sol(i,j)=d(ViVj)/dxi
              sol(1,1)%cell(i,j,k) = -kx*sin(2.0_wp*kx*x (i))    * cos(ky*ym(j))*cos(ky*ym(j)) * sin(kz*zm(k))*sin(kz*zm(k))
              sol(2,1)%cell(i,j,k) = cos(kx*x (i))*cos(kx*x (i)) * ky*cos(2.0_wp*ky*ym(j))     * sin(kz*zm(k))*cos(kz*zm(k))
              sol(3,1)%cell(i,j,k) = cos(kx*x (i))*sin(kx*x (i)) * cos(ky*ym(j))*cos(ky*ym(j)) * kz*cos(2.0_wp*kz*zm(k))

              sol(1,2)%cell(i,j,k) = -kx*sin(2.0_wp*kx*xm(i))    * cos(ky*y (j))*sin(ky*y (j)) * sin(kz*zm(k))*cos(kz*zm(k))
              sol(2,2)%cell(i,j,k) = cos(kx*xm(i))*cos(kx*xm(i)) * ky*sin(2.0_wp*ky*y (j))     * cos(kz*zm(k))*cos(kz*zm(k))
              sol(3,2)%cell(i,j,k) = cos(kx*xm(i))*sin(kx*xm(i)) * sin(ky*y (j))*cos(ky*y (j)) * (-kz)*sin(2.0_wp*kz*zm(k))

              sol(1,3)%cell(i,j,k) = kx*cos(2.0_wp*kx*xm(i))     * cos(ky*ym(j))*cos(ky*ym(j)) * sin(kz*z (k))*cos(kz*z (k))
              sol(2,3)%cell(i,j,k) = cos(kx*xm(i))*sin(kx*xm(i)) * ky*cos(2.0_wp*ky*ym(j))     * cos(kz*z (k))*cos(kz*z (k))
              sol(3,3)%cell(i,j,k) = sin(kx*xm(i))*sin(kx*xm(i)) * cos(ky*ym(j))*cos(ky*ym(j)) * (-kz)*sin(2.0_wp*kz*z (k))
            end do
          end do
        end do
        call V(1)%UpdateGhostCells
        call V(2)%UpdateGhostCells
        call V(3)%UpdateGhostCells

        ! Compute convective terms
        Vi(1,1) = op%intrp1(V(1)); Vi(1,2) = op%intrp1(V(2)); Vi(1,3) = op%intrp1(V(3));
        Vi(2,1) = op%intrp2(V(1)); Vi(2,2) = op%intrp2(V(2)); Vi(2,3) = op%intrp2(V(3));
        Vi(3,1) = op%intrp3(V(1)); Vi(3,2) = op%intrp3(V(2)); Vi(3,3) = op%intrp3(V(3));


        err=0.0_wp
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              err(1,1) = err(1,1) + abs(Vi(1,1)%cell(i,j,k) - cos(kx*xm(i))*cos(ky*ym(j))*sin(kz*zm(k)))
              err(2,1) = err(2,1) + abs(Vi(2,1)%cell(i,j,k) - cos(kx*x (i))*cos(ky*y (j))*sin(kz*zm(k)))
              err(3,1) = err(3,1) + abs(Vi(3,1)%cell(i,j,k) - cos(kx*x (i))*cos(ky*ym(j))*sin(kz*z (k)))

              err(1,2) = err(1,2) + abs(Vi(1,2)%cell(i,j,k) - cos(kx*x (i))*sin(ky*y (j))*cos(kz*zm(k)))
              err(2,2) = err(2,2) + abs(Vi(2,2)%cell(i,j,k) - cos(kx*xm(i))*sin(ky*ym(j))*cos(kz*zm(k)))
              err(3,2) = err(3,2) + abs(Vi(3,2)%cell(i,j,k) - cos(kx*xm(i))*sin(ky*y (j))*cos(kz*z (k)))

              err(1,3) = err(1,3) + abs(Vi(1,3)%cell(i,j,k) - sin(kx*x (i))*cos(ky*ym(j))*cos(kz*z (k)))
              err(2,3) = err(2,3) + abs(Vi(2,3)%cell(i,j,k) - sin(kx*xm(i))*cos(ky*y (j))*cos(kz*z (k)))
              err(3,3) = err(3,3) + abs(Vi(3,3)%cell(i,j,k) - sin(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k)))
            end do
          end do
        end do

        call parallel%sum(err(1,1),bufr); err(1,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,1),bufr); err(2,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,1),bufr); err(3,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(1,2),bufr); err(1,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,2),bufr); err(2,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,2),bufr); err(3,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(1,3),bufr); err(1,3)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,3),bufr); err(2,3)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,3),bufr); err(3,3)=bufr/(nx(1)*nx(2)*nx(3))

        if (parallel%rank%mine.eq.parallel%rank%root) then
          write(stdout,fmt='(a12,es12.4)') 'Error(1,1): ', err(1,1)
          write(stdout,fmt='(a12,es12.4)') 'Error(2,1): ', err(2,1)
          write(stdout,fmt='(a12,es12.4)') 'Error(3,1): ', err(3,1)
          write(stdout,fmt='(a12,es12.4)') 'Error(1,2): ', err(1,2)
          write(stdout,fmt='(a12,es12.4)') 'Error(2,2): ', err(2,2)
          write(stdout,fmt='(a12,es12.4)') 'Error(3,2): ', err(3,2)
          write(stdout,fmt='(a12,es12.4)') 'Error(1,3): ', err(1,3)
          write(stdout,fmt='(a12,es12.4)') 'Error(2,3): ', err(2,3)
          write(stdout,fmt='(a12,es12.4)') 'Error(3,3): ', err(3,3)
        end if

        call data%add('F11', 0, F(1,1))
        call data%add('F21', 2, F(2,1))
        call data%add('F31', 3, F(3,1))

        call data%add('F12', 1, F(1,2))
        call data%add('F22', 0, F(2,2))
        call data%add('F32', 3, F(3,2))

        call data%add('F13', 1, F(1,3))
        call data%add('F23', 2, F(2,3))
        call data%add('F33', 0, F(3,3))

        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              F(1,1)%cell(i,j,k)= Vi(1,1)%cell(i,j,k)*Vi(1,1)%cell(i,j,k)
              F(2,1)%cell(i,j,k)= Vi(1,2)%cell(i,j,k)*Vi(2,1)%cell(i,j,k)
              F(3,1)%cell(i,j,k)= Vi(1,3)%cell(i,j,k)*Vi(3,1)%cell(i,j,k)

              F(1,2)%cell(i,j,k)= Vi(2,1)%cell(i,j,k)*Vi(1,2)%cell(i,j,k)
              F(2,2)%cell(i,j,k)= Vi(2,2)%cell(i,j,k)*Vi(2,2)%cell(i,j,k)
              F(3,2)%cell(i,j,k)= Vi(2,3)%cell(i,j,k)*Vi(3,2)%cell(i,j,k)

              F(1,3)%cell(i,j,k)= Vi(3,1)%cell(i,j,k)*Vi(1,3)%cell(i,j,k)
              F(2,3)%cell(i,j,k)= Vi(3,2)%cell(i,j,k)*Vi(2,3)%cell(i,j,k)
              F(3,3)%cell(i,j,k)= Vi(3,3)%cell(i,j,k)*Vi(3,3)%cell(i,j,k)
            end do
          end do
        end do
        call F(1,1)%UpdateGhostCells
        call F(2,1)%UpdateGhostCells
        call F(3,1)%UpdateGhostCells
        call F(1,2)%UpdateGhostCells
        call F(2,2)%UpdateGhostCells
        call F(3,2)%UpdateGhostCells
        call F(1,3)%UpdateGhostCells
        call F(2,3)%UpdateGhostCells
        call F(3,3)%UpdateGhostCells

        conv(1,1)= op%conv11(V(1),V(1))
        conv(2,1)= op%conv21(V(2),V(1))
        conv(3,1)= op%conv31(V(3),V(1))
        conv(1,2)= op%conv12(V(1),V(2))
        conv(2,2)= op%conv22(V(2),V(2))
        conv(3,2)= op%conv32(V(3),V(2))
        conv(1,3)= op%conv13(V(1),V(3))
        conv(2,3)= op%conv23(V(2),V(3))
        conv(3,3)= op%conv33(V(3),V(3))

        err = 0.0_wp
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              ! convective fluxes in 1-dir
              err(1,1) = err(1,1) + abs(conv(1,1)%cell(i,j,k) - sol(1,1)%cell(i,j,k))
              err(2,1) = err(2,1) + abs(conv(2,1)%cell(i,j,k) - sol(2,1)%cell(i,j,k))
              err(3,1) = err(3,1) + abs(conv(3,1)%cell(i,j,k) - sol(3,1)%cell(i,j,k))

              ! convective fluxes in 2-dir
              err(1,2) = err(1,2) + abs(conv(1,2)%cell(i,j,k) - sol(1,2)%cell(i,j,k))
              err(2,2) = err(2,2) + abs(conv(2,2)%cell(i,j,k) - sol(2,2)%cell(i,j,k))
              err(3,2) = err(3,2) + abs(conv(3,2)%cell(i,j,k) - sol(3,2)%cell(i,j,k))

              ! convective fluxes in 3-dir
              err(1,3) = err(1,3) + abs(conv(1,3)%cell(i,j,k) - sol(1,3)%cell(i,j,k))
              err(2,3) = err(2,3) + abs(conv(2,3)%cell(i,j,k) - sol(2,3)%cell(i,j,k))
              err(3,3) = err(3,3) + abs(conv(3,3)%cell(i,j,k) - sol(3,3)%cell(i,j,k))
            end do
          end do
        end do
        call parallel%sum(err(1,1),bufr); err(1,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,1),bufr); err(2,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,1),bufr); err(3,1)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(1,2),bufr); err(1,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,2),bufr); err(2,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,2),bufr); err(3,2)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(1,3),bufr); err(1,3)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(2,3),bufr); err(2,3)=bufr/(nx(1)*nx(2)*nx(3))
        call parallel%sum(err(3,3),bufr); err(3,3)=bufr/(nx(1)*nx(2)*nx(3))
      end associate

      if (parallel%rank%mine.eq.parallel%rank%root) then
          write(stdout,fmt='(a16,es12.4)') 'Error conv(1,1): ', err(1,1)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(2,1): ', err(2,1)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(3,1): ', err(3,1)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(1,2): ', err(1,2)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(2,2): ', err(2,2)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(3,2): ', err(3,2)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(1,3): ', err(1,3)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(2,3): ', err(2,3)
          write(stdout,fmt='(a16,es12.4)') 'Error conv(3,3): ', err(3,3)
      end if

      if (maxval(err).gt. 1.0e-1_wp) test_successful=.false.

      return
    end subroutine test_convective
    subroutine test_laplacian()
      implicit none
      real(wp), allocatable:: mat(:,:,:,:,:)
      real(wp), allocatable:: mat_ref(:,:,:,:,:)
      real(wp)             :: err(3,-1:1)
      real(wp)             :: bufr
      integer              :: stm
      integer              :: i,j,k,n
      real(wp):: dxi,dxmi(2)
      real(wp):: dyi,dymi(2)
      real(wp):: dzi,dzmi(2)

      if (scheme_order.ne.2) return

      ! Reference operator: 2nd order central differences
      associate (x=>block%x, y=>block%y, z=>block%z,   &
        xm=>block%xm, ym=>block%ym, zm=>block%zm,      &
        lo=>block%lo, hi=>block%hi)

        stm = 1
        allocate(mat_ref(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3),3,-stm:stm))

        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              dxi     = 1.0_wp/(x(i+1)-x(i))
              dxmi(1) = 1.0_wp/(xm(i) -xm(i-1))
              dxmi(2) = 1.0_wp/(xm(i+1) -xm(i))

              dyi     = 1.0_wp/(y(j+1)-y(j))
              dymi(1) = 1.0_wp/(ym(j) -ym(j-1))
              dymi(2) = 1.0_wp/(ym(j+1) -ym(j))

              dzi     = 1.0_wp/(z(k+1)-z(k))
              dzmi(1) = 1.0_wp/(zm(k) -zm(k-1))
              dzmi(2) = 1.0_wp/(zm(k+1) -zm(k))
              mat_ref(i,j,k,1,:) = [dxi*dxmi(1),dxi*(-dxmi(1))-dxi*dxmi(2),dxi*dxmi(2)]
              mat_ref(i,j,k,2,:) = [dyi*dymi(1),dyi*(-dymi(1))-dyi*dymi(2),dyi*dymi(2)]
              mat_ref(i,j,k,3,:) = [dzi*dzmi(1),dzi*(-dzmi(1))-dzi*dzmi(2),dzi*dzmi(2)]
            end do
          end do
        end do
      end associate

      ! Use leapDiffOp to build operator
      call op%BuildLaplacian(mat,stm)

      ! Compute norm 1 error
      err = 0.0_wp
      associate ( lo=>block%lo, hi =>block%hi  )
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              do n=-stm,stm
                err(1,n) = err(1,n) + abs(mat(i,j,k,1,n)-mat_ref(i,j,k,1,n))
                err(2,n) = err(2,n) + abs(mat(i,j,k,2,n)-mat_ref(i,j,k,2,n))
                err(3,n) = err(3,n) + abs(mat(i,j,k,3,n)-mat_ref(i,j,k,3,n))
              end do
            end do
          end do
        end do
      end associate

      do i=1,3
        do n=-stm,stm
          call parallel%sum(err(i,n),bufr); err(i,n)=bufr
        end do
      end do


      if (parallel%rank%mine.eq.parallel%rank%root) then
        write(stdout,fmt='(a12,es12.4)') 'Error(1,-1): ', err(1,-1)
        write(stdout,fmt='(a12,es12.4)') 'Error(2,-1): ', err(2,-1)
        write(stdout,fmt='(a12,es12.4)') 'Error(3,-1): ', err(3,-1)
        write(stdout,fmt='(a12,es12.4)') 'Error(1, 0): ', err(1, 0)
        write(stdout,fmt='(a12,es12.4)') 'Error(2, 0): ', err(2, 0)
        write(stdout,fmt='(a12,es12.4)') 'Error(3, 0): ', err(3, 0)
        write(stdout,fmt='(a12,es12.4)') 'Error(1,+1): ', err(1, 1)
        write(stdout,fmt='(a12,es12.4)') 'Error(2,+1): ', err(2, 1)
        write(stdout,fmt='(a12,es12.4)') 'Error(3,+1): ', err(3, 1)
      end if

      if (maxval(err).gt. 1.0e-1_wp) test_successful=.false.

      return
    end subroutine test_laplacian
end program main
