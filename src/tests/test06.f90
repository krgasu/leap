!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
program main
  !> Test Eulerian module
  ! Check that Eulerian fields are correctly
  ! initialized and that associated parallel
  ! operations, such as ghost cell update,
  ! work correctly.
  use leapKinds
  use leapParallel
  use leapBlock
  use leapEulerian
  implicit none
  type(parallel_obj)   :: parallel
  type(eulerian_set)   :: data
  type(block_obj)      :: block
  type(eulerian_obj_r) :: func, xdiff, ydiff
  type(eulerian_obj_i) :: cellID
  real(wp)             :: xlo(3)
  real(wp)             :: xhi(3)
  integer              :: ilo(3)
  integer              :: ihi(3)
  integer              :: nx(3)
  integer              :: ngc
  real(wp)             :: L(3)
  real(wp)             :: kx,ky,kz
  real(wp),parameter   :: twoPi=8.0_wp*atan(1.0_wp)
  integer              :: Nb(3)
  integer              :: i,j,k
  real(wp)             :: time
  integer              :: iter
  logical              :: test_successful=.false.
  real(wp)             :: err1,err2,err3,err4
  real(wp)             :: tmp1,tmp2,tmp3,tmp4

  ! Launch parallel environment
  call parallel%initialize

  ! Domain size
  L(1)=1.0_wp
  L(2)=1.0_wp
  L(3)=1.0_wp

  ! Domain resolution
  nx = (/24,24,12/)

  ! Partition the domain
  select case (parallel%nproc)
  case (1)
    Nb(1)= 1;  Nb(2)=1;  Nb(3)=1
  case (2)
    Nb(1)= 2;  Nb(2)=1;  Nb(3)=1
  case (3)
    Nb(1)= 3;  Nb(2)=1;  Nb(3)=1
  case (4)
    Nb(1)= 2;  Nb(2)=2;  Nb(3)=1
  case default
    call parallel%stop("Run this test with 1 to 4 mpi ranks")
  end select

  ! Ghost cells
  ngc= 2

  ! Initialize blocks and topo
  xlo=0.0_wp; ilo=1
  xhi=L     ; ihi=nx
  call block%initialize(xlo,xhi,ilo,ihi,ngc,parallel)
  call block%SetPeriodicity((/.true.,.true.,.true./))
  call block%partition(Nb)

  ! Intialize the flow fields
  call data%initialize(block,parallel)
  call data%add('func',  0,func)
  call data%add('xdiff', 0,xdiff)
  call data%add('ydiff', 0,ydiff)
  call data%add('cellID',0,cellID)
  data%write_file="data.h5"

  associate (xm => block%xm, ym => block%ym, zm => block%zm, &
             lo => block%lo, hi => block%hi, ngc=> block%ngc)
    ! Initialize field
    kx= twoPi/L(1)
    ky= twoPi/L(2)
    kz= twoPi/L(3)
    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          func%cell(i,j,k) = cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k))
        end do
      end do
    end do

    ! Update the ghost cells
    call func%UpdateGhostCells

    ! Differentiate the field in the x-direction
    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          xdiff%cell(i,j,k)=(func%cell(i+1,j,k)-func%cell(i-1,j,k))/block%dx(1)/2.0_wp
        end do
      end do
    end do

    ! Differentiate the field in the y-direction
    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          ydiff%cell(i,j,k)=(func%cell(i,j+1,k)-func%cell(i,j-1,k))/block%dx(2)/2.0_wp
        end do
      end do
    end do

    ! Lexicographic ordering
    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          cellID%cell(i,j,k) = (i-lo(1)+1) + (hi(1)-lo(1)+1)*(j-lo(2)) &
                             + (hi(1)-lo(1)+1)*(hi(2)-lo(2)+1)*(k-lo(3))
        end do
      end do
    end do
  end associate
  iter=0
  time=0.0_wp
  call data%write(iter,time,[character(len=str8):: 'func', 'xdiff', 'ydiff', 'cellID'])
  call data%finalize

  ! Re-initialize fields and read
  call data%initialize(block,parallel)
  call data%add('func',  0,func);   func  = 0.0_wp
  call data%add('xdiff', 0,xdiff);  xdiff = 0.0_wp
  call data%add('ydiff', 0,ydiff);  ydiff = 0.0_wp
  call data%add('cellID',0,cellID); cellID = 0
  data%read_file="data.h5"
  call data%read(iter,time,[character(len=str8):: 'func', 'xdiff', 'ydiff', 'cellID'])
  data%overwrite=.false.

  err1 =0.0_wp
  err2 =0.0_wp
  err3 =0.0_wp
  err4 =0.0_wp
  associate (xm => block%xm, ym => block%ym, zm => block%zm, &
             lo => block%lo, hi => block%hi, ngc=> block%ngc)

    do k=lo(3),hi(3)
      do j=lo(2),hi(2)
        do i=lo(1),hi(1)
          tmp1= cos(kx*xm(i))*cos(ky*ym(j))*cos(kz*zm(k))
          tmp2=(cos(kx*xm(i+1))*cos(ky*ym(j))*cos(kz*zm(k))- cos(kx*xm(i-1))*cos(ky*ym(j))*cos(kz*zm(k)))/block%dx(1)/2.0_wp
          tmp3=(cos(kx*xm(i))*cos(ky*ym(j+1))*cos(kz*zm(k))- cos(kx*xm(i))*cos(ky*ym(j-1))*cos(kz*zm(k)))/block%dx(2)/2.0_wp
          tmp4=(i-lo(1)+1) + (hi(1)-lo(1)+1)*(j-lo(2)) + (hi(1)-lo(1)+1)*(hi(2)-lo(2)+1)*(k-lo(3))
          err1 = err1 + abs(func%cell(i,j,k) - tmp1)
          err2 = err2 + abs(xdiff%cell(i,j,k) - tmp2)
          err3 = err3 + abs(ydiff%cell(i,j,k) - tmp3)
          err4 = err4 + abs(real(cellID%cell(i,j,k) - tmp4,WP))
        end do
      end do
    end do
  end associate

  if (err1+err2+err3+err4.lt.1e-12_wp) test_successful=.true.

  call data%finalize
  call block%finalize
  call parallel%finalize

  write(*,'(a,l1)') 'Is test successful? ', test_successful
end program main
