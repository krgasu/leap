!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapLagrangian
  !>--------------------------------------------------------------------------
  ! Module: leapLagrangian
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Module of abstract objets defining Lagrangian data structure
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapBlock
  use leapParser
  use leapIO
  use mpi_f08
  use leapUtils, only : sllist_obj
  implicit none

  private
  public :: lagrangian_obj, lagrangian_set

  ! Filter kernels for interpolation/extrapolations
  public :: kernel_1D
  public :: g1_box,g1_triangle,g1_parabolic,g1_cosine,g1_triweight,g1_roma,g1_cosine2
  public :: KERNEL_ROMA,KERNEL_BOX,KERNEL_TRIANGLE,KERNEL_PARABOLIC
  public :: KERNEL_COSINE,KERNEL_TRIWEIGHT,KERNEL_COSINE2

  type,abstract :: lagrangian_obj
    !> Base lagrangian object
    integer(leapI8)        :: id                           !! Identifying number (inactive if <0)
    real(WP)               :: p(3)                         !! position
    integer                :: c(3)                         !! nearest cell
    contains
      procedure :: Interpolate   => lagrangian_obj_Interpolate
      procedure :: Extrapolate   => lagrangian_obj_Extrapolate
      procedure :: Locate        => lagrangian_obj_Locate
      generic   :: assignment(=) => assign
      procedure(lagrangian_obj_assign), deferred :: assign
  end type lagrangian_obj

  type,abstract :: lagrangian_set
    !> Base structure for a collection of Lagrangian objects
    character(len=:),      allocatable :: name             !! Name of the Lagrangian set
    class(lagrangian_obj), allocatable :: p(:)             !! Array of Lagrangian_obj or any extended type
    class(lagrangian_obj), allocatable :: sample           !! Sample used in allocation of polymorphic data
    integer :: count_ = 0                                  !! Local count for this rank
    integer :: count  = 0                                  !! Total count across all MPI ranks

    ! Utilities for collisions and neighbor search
    type(block_obj) :: cblock                              !! Collision block
    type(sllist_obj), allocatable :: neighbors(:,:,:)      !! Singly linked list of neighbors
    integer,          allocatable :: objincell(:,:,:)      !! Number of objects in this list

    ! IO
    character(len=str64) :: read_file                      !! file to read
    character(len=str64) :: write_file                     !! file to write
    logical              :: overwrite=.true.               !! Switch to overwrite IO files

    ! Filters to go from Eulerian to Lagrangian and back
    real(wp)             :: l_filter                       !! Half filter width
    integer              :: stib=3                         !! Stencil size for filtering
    procedure(kernel_1D),nopass,pointer:: g1in=>g1_triangle!! 1D kernel used in interpolations
    procedure(kernel_1D),nopass,pointer:: g1ex=>int_g1_triangle
                                                           !! 1D kernel used in extrapolations
    ! Pointers to master block/parallel
    type(block_obj),    pointer :: block    => null()      !! Associated block structure
    type(parallel_obj), pointer :: parallel => null()      !! Associated parallel structure

    ! MPI related variables
    integer,allocatable :: count_proc(:)                   !! # of lagrangian objects per proc
    type(MPI_Datatype)  :: MPI_TYPE                        !! MPI variable type
    integer             :: MPI_SIZE=44                     !! MPI size
    contains
      generic   :: Initialize          => lagrangian_set_Init
      generic   :: Finalize            => lagrangian_set_Final
      procedure :: SetWriteFileName    => lagrangian_set_SetWriteFileName
      procedure :: SetReadFileName     => lagrangian_set_SetReadFileName
      procedure :: GetWriteFileName    => lagrangian_set_GetWriteFileName
      procedure :: GetReadFileName     => lagrangian_set_GetReadFileName
      procedure :: SetOverwrite        => lagrangian_set_SetOverwrite
      procedure :: CreateMPIType       => lagrangian_set_CreateMPIType
      procedure :: Recycle             => lagrangian_set_Recycle
      procedure :: Resize              => lagrangian_set_Resize
      procedure :: UpdateCount         => lagrangian_set_UpdateCount
      procedure :: SetFilterKernel     => lagrangian_set_SetFilterKernel
      procedure :: SetFilterSize       => lagrangian_set_SetFilterSize
      procedure :: Localize            => lagrangian_set_Localize
      procedure :: Communicate         => lagrangian_set_Communicate
      procedure :: GetOwnerRankByBlock => lagrangian_set_GetOwnerRankByBlock
      procedure :: ApplyPeriodicity    => lagrangian_set_ApplyPeriodicity
      procedure :: SetupCollisionBlock => lagrangian_set_SetupCollisionBlock
      procedure :: UpdateGhostObjects  => lagrangian_set_UpdateGhostObjects
      procedure :: FreeNeighborList    => lagrangian_set_FreeNeighborList
      procedure :: UpdateNeighborList  => lagrangian_set_UpdateNeighborList
      procedure :: Info                => lagrangian_set_Info
      procedure :: lagrangian_set_Init
      procedure :: lagrangian_set_Final
      procedure(lagrangian_SetObjectType),       deferred :: SetObjectType
      procedure(lagrangian_read),                deferred :: read
      procedure(lagrangian_write),               deferred :: write
      procedure(lagrangian_SetMPIDataTypeParams),deferred :: SetMPIDataTypeParams
  end type lagrangian_set

  real(WP), parameter :: RESIZE_INCREMENT   = 0.3_WP       !! Increment for resizing lagrangian arrays: 30% up or 30% smaller

  ! Indices defining kernels
  integer, parameter :: KERNEL_BOX       = 0
  integer, parameter :: KERNEL_TRIANGLE  = 1
  integer, parameter :: KERNEL_PARABOLIC = 2
  integer, parameter :: KERNEL_COSINE    = 3
  integer, parameter :: KERNEL_TRIWEIGHT = 4
  integer, parameter :: KERNEL_ROMA      = 5
  integer, parameter :: KERNEL_COSINE2   = 6

  abstract interface
    subroutine lagrangian_obj_assign(this,val)
      !> Deferred assignemnt ofan extended type value
      import lagrangian_obj
      class(lagrangian_obj), intent(inout) :: this         !! A Lagrangian object
      class(lagrangian_obj), intent(in)    :: val          !! Value to be assigned
    end subroutine lagrangian_obj_assign
    subroutine lagrangian_SetObjectType(this)
      !> Set the type of the polymorphic sample
      import lagrangian_set
      import WP
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
    end subroutine lagrangian_SetObjectType
    subroutine lagrangian_read(this,iter,time)
      !> Read lagrangian objects from file in parallel
      import lagrangian_set
      import WP
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      integer,               intent(out)   :: iter         !! Iteration at write
      real(wp),              intent(out)   :: time         !! Time at write
    end subroutine lagrangian_read
    subroutine lagrangian_write(this,iter,time)
      !> Write lagrangian objects to file in parallel
      import lagrangian_set
      import WP
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      integer,               intent(in)    :: iter         !! Iteration at write
      real(wp),              intent(in)    :: time         !! Time at write
    end subroutine lagrangian_write
    subroutine lagrangian_SetMPIDataTypeParams(this,types,lengths,displacement)
      !> Set up parameters used to create the MPI derived type
      import lagrangian_set
      import MPI_DATATYPE,MPI_ADDRESS_KIND
      class(lagrangian_set),intent(inout):: this           !! Set of Lagrangian objects
      type(MPI_DATATYPE), allocatable,    &
                            intent(out)  :: types(:)       !! Array of types
      integer, allocatable, intent(out)  :: lengths(:)     !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),     &
               allocatable, intent(out)  :: displacement(:)!! Array of displacements
    end subroutine lagrangian_SetMPIDataTypeParams
    function locator (this,lagobj) result(rank)
      !> Returns the MPI rank that owns the lagrangian object
      import lagrangian_obj, lagrangian_set
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      class(lagrangian_obj), intent (in) :: lagobj         !! Lagrangian obj to locate
      integer :: rank                                      !! rank that should own lagobj
    end function locator
    function kernel_1D(r) result(val)
      import WP
      real(wp), intent(in):: r                             !! Distance from center
      real(wp) :: val                                      !! Kernel value
    end function
  end interface
  contains
    ! Lagrangian object Methods
    ! ------------------------------------------------------
    function lagrangian_obj_Interpolate(this,l_filter,slo,shi,block,g1in,f) result(inter)
      !> Routine to interpolate a field f
      ! defined on an Eulerian stencil to the
      ! location of a lagrangian object
      implicit none
      class(lagrangian_obj),  intent(in):: this            !! A Lagrangian object
      real(wp),               intent(in):: l_filter        !! Filter size
      integer,                intent(in):: slo(3)          !! Stencil lower bound
      integer,                intent(in):: shi(3)          !! Stencil higher bound
      type(block_obj),pointer,intent(in):: block           !! A block object
      procedure(kernel_1D),   pointer, &
                              intent(in):: g1in            !! Filter kernel
      real(WP),               intent(in):: f(              &
                slo(1):shi(1),slo(2):shi(2),slo(3):shi(3)) !! Quantity to interpolate
      ! Work variables
      real(WP):: inter
      integer :: i,j,k
      real(wp):: bump,norm
      real(wp):: dVol

      inter=0.0_wp
      norm =0.0_wp
      do k=slo(3),shi(3)
        do j=slo(2),shi(2)
          do i=slo(1),shi(1)
            ! Cell volume
            dVol = (block%x(i+1)-block%x(i))*(block%y(j+1)-block%y(j))*(block%z(k+1)-block%z(k))

            bump = g1in((block%xm(i)-this%p(1))/l_filter)/l_filter &
                 * g1in((block%ym(j)-this%p(2))/l_filter)/l_filter &
                 * g1in((block%zm(k)-this%p(3))/l_filter)/l_filter

            inter = inter +f(i,j,k)*bump*dVol
            norm  = norm + bump*dVol
          end do
        end do
      end do

      inter=inter/norm

      return
    end function lagrangian_obj_Interpolate
    subroutine lagrangian_obj_Extrapolate(this,l_filter,slo,shi,block,int_g1ex,bump)
      !> Get a bump function centered on the lagrangian object
      implicit none
      class(lagrangian_obj),  intent(in):: this            !! A Lagrangian object
      real(wp),               intent(in):: l_filter        !! Filter size
      integer,                intent(in):: slo(3)          !! Stencil lower bound
      integer,                intent(in):: shi(3)          !! Stencil higher bound
      type(block_obj),pointer,intent(in):: block           !! A block object
      procedure(kernel_1D),   pointer, &
                              intent(in):: int_g1ex        !! Integrated filter kernel
      real(wp), allocatable             :: bump(:,:,:)     !! The bump function
      ! Work variables
      integer  :: i,j,k
      real(wp) :: x_r,x_l
      real(wp) :: y_r,y_l
      real(wp) :: z_r,z_l
      real(wp) :: dVol

      if (allocated(bump)) deallocate(bump)
      allocate( bump(slo(1):shi(1),slo(2):shi(2),slo(3):shi(3)))

      bump=0.0_WP
      do k=slo(3),shi(3)
        do j=slo(2),shi(2)
          do i=slo(1),shi(1)

            ! Cell volume
            dVol = (block%x(i+1)-block%x(i))*(block%y(j+1)-block%y(j))*(block%z(k+1)-block%z(k))

            ! Centered and non-dimensionalized cooridnates
            x_r = (block%x(i+1)-this%p(1))/l_filter
            x_l = (block%x(i  )-this%p(1))/l_filter
            y_r = (block%y(j+1)-this%p(2))/l_filter
            y_l = (block%y(j  )-this%p(2))/l_filter
            z_r = (block%z(k+1)-this%p(3))/l_filter
            z_l = (block%z(k  )-this%p(3))/l_filter

            bump(i,j,k) = (int_g1ex(x_r)-int_g1ex(x_l)) &
                         *(int_g1ex(y_r)-int_g1ex(y_l)) &
                         *(int_g1ex(z_r)-int_g1ex(z_l))/dVol
          end do
        end do
      end do

      return
    end subroutine lagrangian_obj_Extrapolate
    function lagrangian_obj_Locate(this,block) result (cell)
      !> Locate a Lagrangian object on an external
      ! grid. Returns the location of the cell containing
      ! the object.
      implicit none
      class(lagrangian_obj),  intent(inout):: this         !! A Lagrangian object
      class(block_obj),       intent(in)   :: block        !! External block
      integer :: cell(3)
      ! Work variables
      integer :: c_hi(3),c_lo(3)

      associate (x => block%x, y => block%y, z => block%z,  &
          lo => block%lo, hi => block%hi, ngc =>block%ngc)

        c_lo= lo-ngc; c_hi= hi+1+ngc

        ! X-Dir
        ! Sandwich xp between x(c) and x(c+1)
        do while (c_hi(1)-c_lo(1).ge.2)
          ! Compute a mid-point
          cell(1)= (c_hi(1)+c_lo(1))/2
          if (this%p(1).ge.x(cell(1))) then
            c_lo(1)=cell(1)
          else
            c_hi(1)=cell(1)
          end if
        end do
        ! xm(c) lies between x(c) and x(c+1)
        cell(1)= c_lo(1)

        ! Y-Dir
        ! Sandwich yp between y(c) and y(c+1)
        do while (c_hi(2)-c_lo(2).ge.2)
          ! Compute a mid-point
          cell(2)= (c_hi(2)+c_lo(2))/2
          if (this%p(2).ge.y(cell(2))) then
            c_lo(2)=cell(2)
          else
            c_hi(2)=cell(2)
          end if
        end do
        ! ym(c) lies between y(c) and y(c+1)
        cell(2)= c_lo(2)

        ! Z-Dir
        ! Sandwich zp between z(c) and z(c+1)
        do while (c_hi(3)-c_lo(3).ge.2)
          ! Compute a mid-point
          cell(3)= (c_hi(3)+c_lo(3))/2
          if (this%p(3).ge.z(cell(3))) then
            c_lo(3)=cell(3)
          else
            c_hi(3)=cell(3)
          end if
        end do
        ! zm(c) lies between z(c) and z(c+1)
        cell(3)= c_lo(3)
      end associate

      return
    end function lagrangian_obj_Locate

    ! Lagrangian Set Methods
    ! ------------------------------------------------------
    subroutine lagrangian_set_Init(this,name,block,parallel)
      !> Initialize lagrangian objects related IO
      implicit none
      class(lagrangian_set),     intent(inout) :: this     !! A set of Lagrangian objects
      character(len=*),            intent(in)  :: name     !! Name of variable
      type(block_obj),      target,intent(in)  :: block    !! A block object
      type(parallel_obj),   target,intent(in)  :: parallel !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Allocate arrays
      allocate(this%count_proc(this%parallel%nproc))
      this%count_proc(:)=0

      ! Set name of variable
      this%name=trim(adjustl(name))

      ! Set sample type
       call this%SetObjectType()

      ! Initialize array with length zero
      this%count_ = 0
      call this%Resize(this%count_)

      ! Create MPI type
      call this%CreateMPIType

      return
    end subroutine lagrangian_set_Init
    subroutine lagrangian_set_SetupCollisionBlock(this,ds,ngc)
      !> Initializes cblock to handle collisions. This is
      ! extra block is expected to be coarser than the 
      ! simulation block. It is used for cheap neighbor
      ! searches.
      implicit none
      class(lagrangian_set),     intent(inout) :: this     !! A set of Lagrangian objects
      real(wp),                  intent(in)    :: ds       !! Target grid spacing
      integer,                   intent(in)    :: ngc      !! Number of ghost cells for collision block
      ! work variables
      integer, allocatable :: npoints(:,:,:,:)
      integer, allocatable :: buffi(:,:,:)
      integer :: Nc(3)
      integer :: lo(3)
      integer :: hi(3)
      real(wp):: xhi(3)
      real(wp):: xlo(3)
      integer :: coor(3)
      integer :: n
      integer :: dir

      ! Map the extents of the collision block to those
      ! of the main block
      xlo = this%block%xlo
      xhi = this%block%xhi

      ! Number of grid points for the collision block
      ! owned by this rank.
      Nc = floor((xhi-xlo)/ds)
      Nc = [max(Nc(1),1), max(Nc(2),1), max(Nc(3),1)]

      ! Determine low and high cell bounds
      associate( mpi => this%parallel)
        allocate(npoints(mpi%npx,mpi%npy,mpi%npz,3))
        allocate(buffi  (mpi%npx,mpi%npy,mpi%npz))

        npoints = 0
        npoints(mpi%rank%dir(1),mpi%rank%dir(2),mpi%rank%dir(3),:) = Nc

        do dir=1,3
          call mpi%sum(npoints(:,:,:,dir),buffi)
          npoints(:,:,:,dir) = buffi
        end do

        do dir=1,3
          lo(dir) = 1; coor = mpi%rank%dir
          do n = 1,mpi%rank%dir(dir) - 1
            coor(dir) = n
            lo(dir)   = lo(dir) + npoints(coor(1),coor(2),coor(3),dir)
          end do
          coor    = mpi%rank%dir
          hi(dir) = lo(dir) - 1 + npoints(coor(1),coor(2),coor(3),dir)
        end do

        deallocate(npoints)
        deallocate(buffi)
      end associate

      ! Create a uniform grid within each block
      call this%cblock%Initialize(xlo,xhi,lo,hi,ngc,this%parallel)

      ! Synchronize some additional variables
      this%cblock%pmin    = this%block%pmin
      this%cblock%pmax    = this%block%pmax
      this%cblock%periods = this%block%periods

      ! Update grid ghost cells
      call this%cblock%UpdateGridGhostCells(this%cblock%x, idir=1)
      call this%cblock%UpdateGridGhostCells(this%cblock%y, idir=2)
      call this%cblock%UpdateGridGhostCells(this%cblock%z, idir=3)
      call this%cblock%UpdateGridGhostCells(this%cblock%xm,idir=1)
      call this%cblock%UpdateGridGhostCells(this%cblock%ym,idir=2)
      call this%cblock%UpdateGridGhostCells(this%cblock%zm,idir=3)

      ! Update spacing
      call this%cblock%UpdateSpacing

      ! Redefine MPI types on this grid
      call this%cblock%SetupUniformGrid(xlo,xhi,lo,hi)

      ! Allocate neighbor list
      allocate(this%neighbors(lo(1)-ngc:hi(1)+ngc,lo(2)-ngc:hi(2)+ngc,lo(3)-ngc:hi(3)+ngc))
      allocate(this%objincell(lo(1)-ngc:hi(1)+ngc,lo(2)-ngc:hi(2)+ngc,lo(3)-ngc:hi(3)+ngc))
      return
    end subroutine lagrangian_set_SetupCollisionBlock
    subroutine lagrangian_set_FreeNeighborList(this)
      ! Frees neighbors data/pointers
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      integer :: i,j,k
      if (.not.allocated(this%neighbors)) return
      associate (lo => this%cblock%lo, hi=>this%cblock%hi, ngc =>this%cblock%ngc)
        do k=lo(3)-ngc,hi(3)+ngc
          do j=lo(2)-ngc,hi(2)+ngc
            do i=lo(1)-ngc,hi(1)+ngc
              call this%neighbors(i,j,k)%free()
            end do
          end do
        end do
      end associate
      return
    end subroutine lagrangian_set_FreeNeighborList
    subroutine lagrangian_set_UpdateNeighborList(this)
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      integer :: coor(3)
      integer :: n
      if (.not.allocated(this%objincell) &
          .or. (.not.allocated(this%neighbors))) then
        call this%parallel%stop("Using collision block without proper initialization")
      end if

      this%objincell = 0
      do n=1,this%count_
        ! Get coordinate on collision block
        coor = this%p(n)%locate(this%cblock)
        ! Increment counter
        this%objincell(coor(1),coor(2),coor(3)) = this%objincell(coor(1),coor(2),coor(3)) + 1
        ! Add to neighbor list
        call this%neighbors(coor(1),coor(2),coor(3))%put(key=this%objincell(coor(1),coor(2),coor(3)),val=n)
      end do
      return
    end subroutine lagrangian_set_UpdateNeighborList

    subroutine lagrangian_set_Final(this)
      !> Finalize the structure
      implicit none
      class(lagrangian_set),     intent(inout) :: this     !! A set of Lagrangian objects

      this%count_= 0
      this%count = 0
      ! Deallocate arrays
      if(allocated(this%p))          deallocate(this%p)
      if(allocated(this%count_proc)) deallocate(this%count_proc)

      ! Collision/Neighbor search tools
      if(allocated(this%objincell)) deallocate(this%objincell)
      if(allocated(this%neighbors)) then
        call this%FreeNeighborList()
        deallocate(this%neighbors)
      end if

      ! Finalize collision block, if used
      call this%cblock%Finalize()

      ! Nullify pointers
      this%parallel => null()
      this%block    => null()

      return
    end subroutine lagrangian_set_Final
    subroutine lagrangian_set_SetWriteFileName(this,name)
      !> Set the base name of file to write
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A collection of Eulerian objects
      character(len=*),      intent(in)    :: name         !! Name of file
      this%write_file=name
      return
    end subroutine lagrangian_set_SetWriteFileName
    function lagrangian_set_GetWriteFileName(this) result(name)
      !> Return the base name of file to write
      implicit none
      class(lagrangian_set),  intent(inout) :: this        !! A collection of Eulerian objects
      character(len=str64)                  :: name        !! Name of file
      name=this%write_file
      return
    end function lagrangian_set_GetWriteFileName
    subroutine lagrangian_set_SetReadFileName(this,name)
      !> Set the base name of file to read
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A collection of Eulerian objects
      character(len=*),      intent(in)    :: name         !! Name of file
      this%read_file=name
      return
    end subroutine lagrangian_set_SetReadFileName
    function lagrangian_set_GetReadFileName(this) result(name)
      !> Return the base name of file to write
      implicit none
      class(lagrangian_set),  intent(inout) :: this        !! A collection of Eulerian objects
      character(len=str64)                  :: name        !! Name of file
      name=this%read_file
      return
    end function lagrangian_set_GetReadFileName
    subroutine lagrangian_set_SetOverwrite(this,overwrite)
      !> Set file overwritting
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A collection of Eulerian objects
      logical,               intent(in)    :: overwrite    !! Name of file
      this%overwrite=overwrite
      return
    end subroutine lagrangian_set_SetOverwrite
    subroutine lagrangian_set_CreateMPIType(this)
      !> Determines the size of the MPI derived type
      implicit none
      class(lagrangian_set), intent(inout) :: this
      ! Work variables
      integer,                        allocatable :: lengths(:)
      type(MPI_DATATYPE),             allocatable :: types(:)
      integer(kind=MPI_ADDRESS_KIND), allocatable :: displacement(:)
      integer(kind=MPI_ADDRESS_KIND) :: extent,lb
      type(MPI_DATATYPE) :: type_tmp
      integer :: ierr

      associate(mpi=>this%parallel)
        ! Compute types, lengths and displacements
        call this%SetMPIDataTypeParams(types,lengths,displacement)

        ! Create MPI type
        call MPI_TYPE_CREATE_STRUCT(size(lengths),lengths,displacement,types,type_tmp,ierr)
        if (ierr.ne.0) error stop "Problem with MPI_TYPE"

        ! Get actual lower bound and extent of this type
        call MPI_TYPE_GET_EXTENT(type_tmp,lb,extent,ierr)
        if (ierr.ne.0) error stop "Problem with MPI_TYPE"

        ! Adjust upper and lower bounds
        call MPI_TYPE_CREATE_RESIZED(type_tmp,lb,extent,this%MPI_TYPE,ierr)
        call MPI_TYPE_COMMIT(this%MPI_TYPE,ierr)
        if (ierr.ne.0) error stop "Problem with MPI_TYPE"

        ! Get the size of this type
        call MPI_TYPE_SIZE(this%MPI_TYPE,this%MPI_SIZE,ierr)
        if (ierr.ne.0) error stop "Problem with MPI_TYPE"
      end associate

      return
    end subroutine lagrangian_set_CreateMPIType
    subroutine lagrangian_set_Info(this)
      !> Prints diagnostics information about the derived type
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Lagrangian array to dump

      write(stdout,"(a20,a, a)"  ) 'name '               , repeat('-',19)//'>', this%name
      if (allocated(this%p)) then
        write(stdout,"(a20,a, i16)") 'size of l'  , repeat('-',19)//'>', size(this%p)
      else
        write(stdout,"(a20,a, a)"  ) 'size of l'  , repeat('-',19)//'>', "NOT ALLOCATED"
      end if
      write(stdout,"(a20,a, i16)") 'count_'              , repeat('-',19)//'>', this%count_
      write(stdout,"(a20,a, i16)") 'count'               , repeat('-',19)//'>', this%count
      write(stdout,"(a20,a, a)"  ) 'read_file '          , repeat('-',19)//'>', this%read_file
      write(stdout,"(a20,a, a)"  ) 'write_file'          , repeat('-',19)//'>', this%write_file
      write(stdout,"(a20,a, l16)") 'overwrite'           , repeat('-',19)//'>', this%overwrite
      write(stdout,"(a20,a, l16)") 'associated(parallel)', repeat('-',19)//'>', associated(this%parallel)
      write(stdout,"(a20,a, l16)") 'associated(block)'   , repeat('-',19)//'>', associated(this%block)
      write(stdout,"(a20,a, i16)") 'MPI_TYPE'            , repeat('-',19)//'>', this%MPI_TYPE
      write(stdout,"(a20,a, i16)") 'MPI_SIZE'            , repeat('-',19)//'>', this%MPI_SIZE
      return
    end subroutine lagrangian_set_Info
    subroutine lagrangian_set_Recycle(this)
      !> Sorting routine: stacks active lagrangian
      ! objects at the beginning of the array then
      ! resizes
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Lagrangian array to dump
      ! Work variables
      integer :: count_active                              !! number of active objects
      integer :: i                                         !! iterator

      count_active = 0
      if (allocated(this%p)) then
        do i=1,size(this%p)
          if (this%p(i)%id.gt.0) then
            count_active = count_active + 1
            if (i.ne.count_active) then
              ! Move back the object
              this%p(count_active)=this%p(i)

              ! Set current position inactive
              this%p(i)%id = -1
            end if
          end if
        end do
      end if

      ! Resize array to count_active
      call this%Resize(count_active)
      return
    end subroutine lagrangian_set_Recycle
    subroutine lagrangian_set_UpdateCount(this)
      !> Updates the total count of Lagrangian objects
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Lagrangian array to dump
      ! Work variables
      integer :: ierr

      associate(mpi=>this%parallel)
        call MPI_ALLGATHER(this%count_,1,mpi%INTEGER,this%count_proc,1,mpi%INTEGER,mpi%comm%g,ierr)
      end associate
      this%count=sum(this%count_proc)
      return
    end subroutine lagrangian_set_UpdateCount
    subroutine lagrangian_set_Resize(this,n)
      !> Changes the size of an array of Lagrangian objects
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Lagrangian array to dump
      integer,               intent(in)    :: n            !! New size
      ! Work variables
      class(lagrangian_obj), allocatable :: lagrangian_array(:)
      integer                            :: size_old
      integer                            :: size_new
      integer                            :: i


      ! Resize array to size n
      if (.not.allocated(this%p)) then
        ! the array is of size 0
        if (n.ne.0) then
          allocate(this%p(n),source=this%sample)
          this%p(1:n)%id=-huge(int(0,leapI8))
        end if
      else if (n.eq.0) then
        ! the array is allocated, but we want to empty it
        deallocate(this%p)
      else
        ! Update non zero size to another non zero size
        size_old = size(this%p)
        if (n.gt.size_old) then
          ! Increase from size_old to size_new
          size_new = max(n,int(size_old*(1+RESIZE_INCREMENT)))

          ! Allocate temporary array
          allocate(lagrangian_array(size_new),source=this%sample)

          ! Store values
          do i=1,size_old
            lagrangian_array(i)=this%p(i)
          end do

          ! Move the allocation from the
          ! temporary array to the final one
          call move_alloc(lagrangian_array,this%p)
          this%p(size_old+1:size_new)%id=-huge(int(0,leapI8))

        else if (n.lt.size_old*(1-RESIZE_INCREMENT)) then
          ! Decrease from size_old to size_new

          ! Allocate temporary array
          allocate(lagrangian_array(n),source=this%sample)

          ! Store values
          do i=1,n
            lagrangian_array(i)=this%p(i)
          end do

          ! Move the allocation from the
          ! temporary array to the final one
          call move_alloc(lagrangian_array,this%p)
        end if
      end if

      ! Update count info
      this%count_=n

      return
    end subroutine lagrangian_set_Resize
    subroutine lagrangian_set_Localize(this)
      !> Localize a Lagrangian object on the grid
      ! Returns the location of the closest
      ! collocated cell (staggering=0
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Lagrangian array to dump
      ! Work variables
      integer :: n,c_hi(3),c_lo(3)

      associate (x => this%block%x, y => this%block%y, z => this%block%z,  &
        lo => this%block%lo, hi => this%block%hi)


        do n=1,this%count_
          c_lo= lo; c_hi= hi+1
          ! X-Dir
          ! Sandwich xp between x(c) and x(c+1)
          do while (c_hi(1)-c_lo(1).ge.2)
            ! Compute a mid-point
            this%p(n)%c(1)= (c_hi(1)+c_lo(1))/2
            if (this%p(n)%p(1).ge.x(this%p(n)%c(1))) then
              c_lo(1)=this%p(n)%c(1)
            else
              c_hi(1)=this%p(n)%c(1)
            end if
          end do
          ! xm(c) lies between x(c) and x(c+1)
          this%p(n)%c(1)= c_lo(1)

          ! Y-Dir
          ! Sandwich yp between y(c) and y(c+1)
          do while (c_hi(2)-c_lo(2).ge.2)
            ! Compute a mid-point
            this%p(n)%c(2)= (c_hi(2)+c_lo(2))/2
            if (this%p(n)%p(2).ge.y(this%p(n)%c(2))) then
              c_lo(2)=this%p(n)%c(2)
            else
              c_hi(2)=this%p(n)%c(2)
            end if
          end do
          ! ym(c) lies between y(c) and y(c+1)
          this%p(n)%c(2)= c_lo(2)

          ! Z-Dir
          ! Sandwich zp between z(c) and z(c+1)
          do while (c_hi(3)-c_lo(3).ge.2)
            ! Compute a mid-point
            this%p(n)%c(3)= (c_hi(3)+c_lo(3))/2
            if (this%p(n)%p(3).ge.z(this%p(n)%c(3))) then
              c_lo(3)=this%p(n)%c(3)
            else
              c_hi(3)=this%p(n)%c(3)
            end if
          end do
          ! zm(c) lies between z(c) and z(c+1)
          this%p(n)%c(3)= c_lo(3)
        end do
      end associate

      return
    end subroutine lagrangian_set_Localize
    function lagrangian_set_GetOwnerRankByBlock(this,lagobj) result (rank)
      !> Returns the MPI rank that should own this lagrangian
      ! object based on which block it belongs to
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! A set of Lagrangian objects
      class (lagrangian_obj), intent(in)   :: lagobj       !! Lagrangian obj to locate
      integer :: rank                                      !! rank that should own lagobj
      rank=this%block%locate(lagobj%p)
      return
    end function lagrangian_set_GetOwnerRankByBlock

    subroutine lagrangian_set_Communicate(this,GetOwnerRankOpt)
      !> Communicate lagrangian objects across MPI_rank
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Set of Lagrangian objects
      procedure(locator),    optional      :: GetOwnerRankOpt
                                                           !! MPI Rank locator for communications
      procedure(locator),    pointer       :: GetOwnerRank => null ()
                                                           !! procedure points to the locator function to use
      class(lagrangian_obj), allocatable   :: buf_send(:,:)
      integer, dimension(this%parallel%nproc) :: counter
      integer, dimension(this%parallel%nproc) :: who_send
      integer, dimension(this%parallel%nproc) :: who_recv
      integer :: nb_recv,nb_send
      integer :: nobjects_old
      integer :: rank_send,rank_recv
      integer :: object_rank,ierr
      type(MPI_status) :: status
      integer :: i

      ! Select the appropriate rank locator
      if (present(GetOwnerRankOpt)) then
        ! User-defined locator
        GetOwnerRank => GetOwnerRankOpt
      else
        ! Default locator
        ! Returns the rank of the grid block that contains
        ! the position of the lagrangian_object
        GetOwnerRank => lagrangian_set_GetOwnerRankByBlock
      end if

      associate(mpi=>this%parallel)
        ! Recycle
        call this%Recycle

        ! Prepare information about who sends what to whom
        who_send=0

        ! Loop over objects owned by rank
        do i=1,this%count_
          ! Find MPI rank that owns the object
          object_rank=GetOwnerRank(this,this%p(i))

          ! Tell rank that this object will go to object_rank
          who_send(object_rank)=who_send(object_rank)+1
        end do

        ! Remove the diagonal (no send-recieve from one MPI-rank to itself)
        who_send(mpi%rank%mine)=0

        do rank_recv=1,mpi%nproc
          ! Gather the content of every who_send(rank_recv) to MPI rank rank_recv
          ! in the array who_recv
          call MPI_GATHER(who_send(rank_recv),1,mpi%INTEGER,          &
                          who_recv,1,mpi%INTEGER,rank_recv-1,mpi%comm%g,ierr)
        end do

        ! Prepare the buffers
        nb_send=maxval(who_send)! maximum number of objects to be sent to any rank
        nb_recv=sum(who_recv)   ! number of objects to be received from other ranks

        ! Allocate buffers to send objects
        allocate(buf_send(nb_send,mpi%nproc),source=this%sample)

        ! Find and pack the objects to be sent
        ! Prepare the counter
        counter(:)=0
        do i=1,this%count_
          ! Get the cpu
          object_rank=GetOwnerRank(this,this%p(i))

          if (object_rank .NE. mpi%rank%mine) then
            ! Prepare for sending
            counter(object_rank)=counter(object_rank)+1
            buf_send(counter(object_rank),object_rank)=this%p(i)

            ! Need to remove the object from this rank
            this%p(i)%id=-1
          end if
        end do

        ! Everybody resizes
        nobjects_old = this%count_
        call this%Resize(nobjects_old+nb_recv) ! Larger array to accomodate old and new objects
        this%count_=nobjects_old

        ! We just loop through the CPUs, pack objects in buf_send, send, unpack
        do rank_send=1,mpi%nproc
          ! Start transaction between rank_send and rank
          if (mpi%rank%mine.eq.rank_send) then
            ! I'm the sender
            ! Send out all my objects
            do rank_recv=1,mpi%nproc
              if(who_send(rank_recv) .GT. 0) then
                call MPI_SEND(buf_send(1,rank_recv)%id,who_send(rank_recv), &
                              this%MPI_TYPE,rank_recv-1,0,mpi%comm%g,ierr)
                if (ierr.ne.0) error stop "Problem with MPI_SEND"
              end if
            end do
          else
            ! I'm not the sender, I receive
            if (who_recv(rank_send) .GT. 0) then
              ! rank recieves new objects from rank_send
              call MPI_RECV(this%p(this%count_+1)%id,who_recv(rank_send), &
                                   this%MPI_TYPE, rank_send-1,0,mpi%comm%g,status,ierr)

              ! Update actual number of objects owned
              this%count_ = this%count_+who_recv(rank_send)
              if (ierr.ne.0) error stop "Problem with MPI_RECV"
            end if
          end if
        end do

        deallocate(buf_send)

        ! Remove inactive variables
        call this%Recycle

        ! Nullify pointer
        GetOwnerRank => null()

      end associate
      return
    end subroutine lagrangian_set_Communicate
    subroutine lagrangian_set_ApplyPeriodicity(this)
      !> Apply periodic boundary conditions
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Set of Lagrangian objects
      ! Work variable
      integer :: dir
      integer :: n

      do dir=1,3
        if (this%block%periods(dir)) then
          do n=1,this%count_
            this%p(n)%p(dir) = this%block%pmin(dir) + modulo(this%p(n)%p(dir)-this%block%pmin(dir),this%block%pmax(dir) - this%block%pmin(dir))
          end do
        end if
      end do
      return
    end subroutine lagrangian_set_ApplyPeriodicity
    subroutine lagrangian_set_UpdateGhostObjects(this,block)
      !> Updates ghost objects
      ! Copies objects that lie near or cross the
      ! block's boundaries
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Set of Lagrangian objects
      class(block_obj),optional,intent(in) :: block        !! Optional alternative block

      ! Remove previous ghost and deactivated objects
      call this%Recycle()

      ! Create and send new ghost objects in x,y,z directions
      if (present(block)) then
        call lagrangian_set_UpdateGhostObjectsDir(this,block,1)
        call lagrangian_set_UpdateGhostObjectsDir(this,block,2)
        call lagrangian_set_UpdateGhostObjectsDir(this,block,3)
      else
        call lagrangian_set_UpdateGhostObjectsDir(this,this%block,1)
        call lagrangian_set_UpdateGhostObjectsDir(this,this%block,2)
        call lagrangian_set_UpdateGhostObjectsDir(this,this%block,3)
      end if

      return
    end subroutine lagrangian_set_UpdateGhostObjects
    subroutine lagrangian_set_UpdateGhostObjectsDir(this,block,idir)
      !> Update ghost objects in the idir direction
      ! NOTE: Ghost objects must have negative IDs
      implicit none
      class(lagrangian_set),   intent(inout):: this        !! Set of Lagrangian objects
      class(block_obj),        intent(in)   :: block       !! Block object
      integer,                 intent(in)   :: idir        !! Direction of communication
      ! Work variables
      integer :: nsendL,nsendR                             !! Number of ghost objects to send to left/right rank
      integer :: nrecvL,nrecvR                             !! Number of ghost objects to receive from left/right rank
      class(lagrangian_obj),allocatable :: bufL(:),bufR(:) !! Buffers containing ghost objects to be sent
      type(MPI_STATUS)  :: statuses(4)                     !! For MPI non-blocking send/receive
      type(MPI_REQUEST) :: requests(4)                     !! For MPI non-blocking send/receive
      integer :: n                                         !! Iterator
      integer :: nobjects_old
      integer :: coor(3)
      integer :: ierr

      ! Figure out how many objects need to be sent
      nsendL=0
      nsendR=0
      do n=1,this%count_
        ! Determine wehther this object is close enough to the boundary
        ! to become a ghost object for a neighboring block
        coor = this%p(n)%locate(block)
        if ((coor(idir)-block%lo(idir).lt.block%ngc) .and. this%parallel%rank%L(idir)-1.ne.MPI_PROC_NULL) nsendL=nsendL+1
        if ((block%hi(idir)-coor(idir).lt.block%ngc) .and. this%parallel%rank%R(idir)-1.ne.MPI_PROC_NULL) nsendR=nsendR+1
      end do

      ! Allocate buffers to send objects
      if(nsendL.gt.0) allocate(bufL(nsendL),source=this%sample)
      if(nsendR.gt.0) allocate(bufR(nsendR),source=this%sample)

      ! Copy objects in buffers and make ID negative
      ! to signify that this is a ghost object
      nsendL=0
      nsendR=0
      associate(periodicity => block%periods, pmin => block%pmin, pmax => block%pmax)
        do n=1,this%count_
          ! Get coordinates of the object on the grid
          coor = this%p(n)%locate(block)
          ! Send object left, if it's close enough to the boundary
          if ((coor(idir)-block%lo(idir).lt.block%ngc) .and. this%parallel%rank%L(idir)-1.ne.MPI_PROC_NULL) then
            nsendL=nsendL+1
            bufL(nsendL)=this%p(n)
            bufL(nsendL)%id=-abs(bufL(nsendL)%id)
            if (periodicity(idir) .eqv. .true. .and.  &
                this%parallel%rank%dir(idir) .eq. 1)  then
                bufL(nsendL)%p(idir) = bufL(nsendL)%p(idir) + (pmax(idir)-pmin(idir))
            end if
          end if
          ! Send object right, if it's close enough to the boundary
          if ((block%hi(idir)-coor(idir).lt.block%ngc) .and. this%parallel%rank%R(idir)-1.ne.MPI_PROC_NULL) then
            nsendR=nsendR+1
            bufR(nsendR)=this%p(n)
            bufR(nsendR)%id=-abs(bufR(nsendR)%id)
            if (periodicity(idir) .eqv. .true. .and.  &
                this%parallel%rank%dir(idir) .eq. this%parallel%np(idir))  then
                bufR(nsendR)%p(idir) = bufR(nsendR)%p(idir) - (pmax(idir)-pmin(idir))
            end if
          end if
        end do
      end associate

      ! Communicate sizes with non-blocking MPI directives
      nrecvR = 0
      nrecvL = 0
      associate(mpi=>this%parallel)
        ! Post receives from Left and right ranks
        call MPI_IRECV( nrecvL, 1, mpi%INTEGER, &
                        mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(1), ierr)
        call MPI_IRECV( nrecvR, 1, mpi%INTEGER, &
                        mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(2), ierr)
        ! Send sizes to left and right ranks
        call MPI_ISEND( nsendR, 1, mpi%INTEGER, &
                        mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(3), ierr)
        call MPI_ISEND( nsendL, 1, mpi%INTEGER, &
                        mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(4), ierr)
        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )
      end associate

      ! Store old size and resize the set
      nobjects_old = this%count_
      call this%Resize(this%count_+nrecvL+nrecvR)

      ! Communicate sizes with non-blocking MPI directives
      associate(mpi=>this%parallel)
        ! Post receives from Left and right ranks
        if (nrecvL.gt.0) then
          call MPI_IRECV( this%p(nobjects_old+1)%id,       nrecvL, this%MPI_TYPE, &
                        mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(1), ierr)
        end if
        if (nrecvR.gt.0) then
          call MPI_IRECV( this%p(nobjects_old+nrecvL+1)%id,nrecvR, this%MPI_TYPE, &
                        mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(2), ierr)
        end if
        ! Send to left and right ranks
        if (nsendR.gt.0) then
          call MPI_ISEND( bufR(1)%id,                      nsendR, this%MPI_TYPE, &
                        mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(3), ierr)
        end if
        if (nsendL.gt.0) then
          call MPI_ISEND( bufL(1)%id,                      nsendL, this%MPI_TYPE, &
                        mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(4), ierr)
        end if
        ! Synchronize
        if (nrecvL.gt.0) call MPI_WAIT(requests(1),statuses(1),ierr)
        if (nrecvR.gt.0) call MPI_WAIT(requests(2),statuses(2),ierr)
        if (nsendR.gt.0) call MPI_WAIT(requests(3),statuses(3),ierr)
        if (nsendL.gt.0) call MPI_WAIT(requests(4),statuses(4),ierr)
      end associate

      return
    end subroutine lagrangian_set_UpdateGhostObjectsDir
    subroutine lagrangian_set_SetFilterKernel(this,kernel_interp,kernel_extrap)
      !> Reset the filter kerrnel
      ! Default is Triangle for interpolation
      ! and extrapolation
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Set of Lagrangian objects
      integer,               intent(in)  :: kernel_interp  !! Filter kernel for interpolations
      integer,               intent(in)  :: kernel_extrap  !! Filter kernel for extrapolations

      ! Set interpolation kernel
      select case (kernel_interp)
      case (KERNEL_BOX)
        this%g1in => g1_box
      case (KERNEL_TRIANGLE)
        this%g1in => g1_triangle
      case (KERNEL_PARABOLIC)
        this%g1in => g1_parabolic
      case (KERNEL_COSINE)
        this%g1in => g1_cosine
      case (KERNEL_TRIWEIGHT)
        this%g1in => g1_triweight
      case (KERNEL_ROMA)
        this%g1in => g1_roma
      case (KERNEL_COSINE2)
        this%g1in => g1_cosine2
      case default
        ! By default revert to Triangle filter
        this%g1in => g1_triangle
      end select

      ! Set extrapolation kernel
      select case (kernel_extrap)
      case (KERNEL_BOX)
        this%g1ex => int_g1_box
      case (KERNEL_TRIANGLE)
        this%g1ex => int_g1_triangle
      case (KERNEL_PARABOLIC)
        this%g1ex => int_g1_parabolic
      case (KERNEL_COSINE)
        this%g1ex => int_g1_cosine
      case (KERNEL_TRIWEIGHT)
        this%g1ex => int_g1_triweight
      case (KERNEL_ROMA)
        this%g1ex => int_g1_roma
      case (KERNEL_COSINE2)
        this%g1ex => int_g1_cosine2
      case default
        ! By default revert to Triangle filter
        this%g1ex => int_g1_triangle
      end select


      return
    end subroutine lagrangian_set_SetFilterKernel
    subroutine lagrangian_set_SetFilterSize(this,l_filter)
      !> Adjust the size of the filter
      implicit none
      class(lagrangian_set), intent(inout) :: this         !! Set of Lagrangian objects
      real(wp),              intent(in)    :: l_filter     !! Filter size
      ! Work variables
      real(wp) :: min_grid_spacing

      this%l_filter=l_filter

      ! Get minimum grid_spacing
      min_grid_spacing=minval(this%block%dx)

      ! Adjust stencil size
      this%stib = 1 + 2*ceiling(l_filter/min_grid_spacing)

      ! Check there are enough ghost cells
      if (this%block%ngc.lt.this%stib/2) call this%parallel%stop('Insufficient number of ghost cells for filtering.')

      return
    end subroutine lagrangian_set_SetFilterSize

    ! Filter kernels
    ! ------------------------------------------------------
    pure function g1_roma(r) result(val)
      !> Filtering kernel with support from -1 to 1
      ! Roma and Peskin's filter
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val

      if (abs(r).le.1.0_wp/3.0_wp) then
        val=0.5_wp*(1.0_wp+sqrt(-6.75_wp*r**2+1.0_wp))
      else if ((abs(r).ge.1.0_wp/3.0_wp).and.(abs(r).le.1.0_wp)) then
        val=0.25_wp*(5.0_wp-4.5_wp*abs(r)-sqrt(-3.0_wp*(1.0_wp-1.5_wp*abs(r))**2+1.0_wp))
      else
        val=0.0_WP
      end if

      return
    end function g1_roma
    pure function int_g1_roma(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Roma and Peskin filter
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp), parameter :: pi=4.0_wp*atan(1.0_wp)
      real(wp) :: part1,part2,part3
      real(wp) :: val

      if (abs(r).le.1.0_wp/3.0_wp) then
        part1 =  1.0_wp/8.0_wp*r*(sqrt(4.0_wp-27.0_wp*r**2)+4.0_wp)
        part2 =  sqrt(3.0_wp)/18.0_wp*asin(1.5_wp*sqrt(3.0_wp)*r)
        part3 =  0.0_wp
      else if ((abs(r).ge.1.0_wp/3.0_wp).and.(abs(r).le.1.0_wp)) then
        part1 =  1.25_wp*r-9.0_wp/16.0_wp*r*abs(r)-sqrt(3.0_wp)/36.0_wp*asin(sqrt(3.0_wp)*(1.5_wp*r-r/abs(r)))
        part2 = -1.0_wp/24.0_wp*sqrt(-27.0_wp*r**2+36.0_wp*abs(r)-8.0_wp)*(1.5_wp*r-r/abs(r))
        part3 =  r/abs(r)*(sqrt(3.0_wp)/108.0_wp*pi-1.0_wp/6.0_wp)
      else
        part1 = 0.5_wp*r/abs(r)
        part2 = 0.0_wp
        part3 = 0.0_wp
      end if
      val = part1 + part2 + part3

      return
    end function int_g1_roma
    pure function g1_box(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Box filter (step function)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val

      if ( (abs(r).le. 1.0_WP)) then
        val=0.5_wp
      else
        val=0.0_WP
      end if

      return
    end function g1_box
    pure function int_g1_box(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Box filter (step function)
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val=0.5_wp*new_r

      return
    end function int_g1_box
    pure function g1_triangle(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Triangular filter (linear interpolation)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val

      if ( (abs(r).le. 1.0_wp)) then
        val=1.0_wp-abs(r)
      else
        val=0.0_WP
      end if

      return
    end function g1_triangle
    pure function int_g1_triangle(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Triangular filter (linear interpolation)
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val = new_r - 0.5_wp*new_r*abs(new_r)

      return
    end function int_g1_triangle
    pure function g1_parabolic(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Parabolic filter
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val

      if ( (abs(r).le. 1.0_wp)) then
        val=3.0_wp/4.0_wp*(1.0_wp-abs(r)**2)
      else
        val=0.0_WP
      end if

      return
    end function g1_parabolic
    pure function int_g1_parabolic(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Parabolic filter
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val = 0.75_wp*(new_r-(1.0_wp/3.0_wp)*(new_r**3))

      return
    end function int_g1_parabolic
    pure function g1_cosine(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Parabolic filter
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val
      real(wp), parameter :: pi=4.0_wp*atan(1.0_wp)

      if ( (abs(r).le. 1.0_wp)) then
        val=pi/4.0_wp*cos(pi/2.0_wp*r)
      else
        val=0.0_WP
      end if

      return
    end function g1_cosine
    pure function int_g1_cosine(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Cosine filter
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val
      real(wp), parameter :: pi=4.0_wp*atan(1.0_wp)

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val = 0.5_wp*sin(0.5_wp*pi*new_r)

      return
    end function int_g1_cosine
    pure function g1_triweight(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Triweight filter
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val

      if ( (abs(r).le. 1.0_wp)) then
        val=(35.0_wp/32.0_wp)*((1-r**2)**3)
      else
        val=0.0_WP
      end if

      return
    end function g1_triweight
    pure function int_g1_triweight(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Triweight filter
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val = (35.0_wp/32.0_wp)*(new_r-(new_r**3)+3.0_wp/5.0_wp*(new_r**5)-(new_r**7)/7.0_wp)

      return
    end function int_g1_triweight
    pure function g1_cosine2(r) result(val)
      !> Filtering kernel with support from -1.0 to 1.0
      ! Parabolic filter
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: val
      real(wp), parameter :: pi=4.0_wp*atan(1.0_wp)

      if ( (abs(r).le. 1.0_wp)) then
        val=0.5_wp*(1.0_wp+ cos(pi*r))
      else
        val=0.0_WP
      end if

      return
    end function g1_cosine2
    pure function int_g1_cosine2(r) result(val)
      !> Integral of filtering kernel from 0 to r
      ! Cosine filter
      ! Here : r=x/l_f (non-dimensional position)
      implicit none
      real(wp),intent(in) :: r
      real(wp) :: new_r
      real(wp) :: val
      real(wp), parameter :: pi=4.0_wp*atan(1.0_wp)

      new_r = min(max(r,-1.0_wp),1.0_wp)
      val = 0.5_wp*(new_r+1.0_wp/pi*sin(pi*new_r))

      return
    end function int_g1_cosine2
end module leapLagrangian
