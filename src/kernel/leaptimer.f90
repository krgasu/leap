!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapTimer
  !>--------------------------------------------------------------------------
  ! Module: leapTimer
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Time management utility for LEAP
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  implicit none

  private
  public :: timer_obj

  type :: timer_obj
    !> Time management utility
    type(parser_obj), pointer :: parser                    !! Parser object
    real(wp) :: dt                                         !! Timestep
    real(wp) :: time         = 0.0_wp                      !! Time at the n-th timestep
    real(wp) :: time_max     = huge(1.0_wp)                !! Maximum simulation time
    real(wp) :: time_wall    = huge(1.0_wp)                !! Wall time
    real(wp) :: freq_write   = huge(1.0_wp)                !! Frequency of restart writes
    real(wp) :: freq_output  = huge(1.0_wp)                !! Frequency of visualization writes
    integer  :: iter         = 0                           !! Current iteration
    integer  :: iter_max     = huge(1)                     !! Maximum iterations
    logical  :: finish       = .false.                     !! Instructs simulation to end
    contains
      procedure :: Initialize              => timer_obj_Init
      procedure :: Finalize                => timer_obj_Final
      procedure :: StepForward             => timer_obj_StepForward
      procedure :: Done                    => timer_obj_Done
      procedure :: EndRun                  => timer_obj_EndRun
      procedure :: TimeToWriteRestartData  => timer_obj_TimeToWriteRestartData
      procedure :: TimeToWriteOutputData   => timer_obj_TimeToWriteOutputData
  end type timer_obj
  contains
    subroutine timer_obj_Init(this,parser)
      !> Initialize the timer
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      type(parser_obj),target,intent(in):: parser          !! Input file parsing tool
      ! Link to main structure
      this%parser => parser
      ! Read from input file important parameters
      call this%parser%Get("Timestep",          this%dt       )
      call this%parser%Get("Maximum time",      this%time_max )
      call this%parser%Get("Maximum iterations",this%iter_max )
      call this%parser%Get("Wall time",         this%time_wall)

      ! Read write/viz frequencies, if supplied
      call this%parser%Get("Frequency write restart data", this%freq_write  , huge(1.0_wp))
      call this%parser%Get("Frequency write output data",  this%freq_output , huge(1.0_wp))

      return
    end subroutine timer_obj_Init
    subroutine timer_obj_Final(this)
      !> Finalize
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      this%parser => null()
      return
    end subroutine timer_obj_Final
    subroutine timer_obj_StepForward(this)
      !> Move timer from n to n+1
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      ! Update time
      this%time = this%time + this%dt
      ! Update iteration count
      this%iter = this%iter + 1
      return
    end subroutine timer_obj_StepForward
    subroutine timer_obj_EndRun(this)
      !> Change run status to finished
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      this%finish = .true.
      return
    end subroutine timer_obj_EndRun
    function timer_obj_Done(this) result(res)
      !> Determines whether simulation is over
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      logical :: res
      res=.false.
      if (this%finish) then
        res=.true.
        return
      else
        ! Check simulation time
        if (this%time+this%dt.gt.this%time_max) then
          res=.true.
          return
        end if
        ! Check iteration count
        if (this%iter+1 .gt.this%iter_max) then
          res=.true.
          return
        end if
        ! Check walltime
      end if
      return
    end function timer_obj_Done
    function timer_obj_TimeToWriteRestartData(this) result(res)
      !> Determine whether it is time to write restart files
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      logical :: res
      res=.false.
      if (mod(this%time,this%freq_write) .lt. 0.5_wp*this%dt ) res=.true.
      if (mod(this%time,this%freq_write) .ge. this%freq_write -0.5_wp*this%dt ) res=.true.
      return
    end function timer_obj_TimeToWriteRestartData
    function timer_obj_TimeToWriteOutputData(this) result(res)
      !> Determine whether it is time to write visualization files
      implicit none
      class(timer_obj), intent(inout) :: this              !! Timer
      logical :: res
      res=.false.
      if (mod(this%time,this%freq_output) .lt. 0.5_wp*this%dt ) res=.true.
      if (mod(this%time,this%freq_output) .ge. this%freq_output -0.5_wp*this%dt ) res=.true.
      return
    end function timer_obj_TimeToWriteOutputData
end module leapTimer
