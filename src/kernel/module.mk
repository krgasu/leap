MODULE = kernel
FILES = leapkinds.f90 leapcli.f90 leaputils.f90 leapparser.f90        \
        leapparallel.f90 leapmonitor.f90 leapio_hdf5.f90 leapblock.f90\
        wrapperh5hut.f90 leapio_h5hut.f90 leapio_nga.f90              \
		leapio_silo.f90 leapio.f90 leaplagrangian.f90 leapeulerian.f90\
		leaptimer.f90 leapsolver.f90 leapcases.f90 leapbc.f90 leapdiffop.f90     \
		leapcuda.f90 leaphypre.f90

SRC += $(patsubst %, $(MODULE)/%, $(FILES))
