!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapEulerian
  !>--------------------------------------------------------------------------
  ! Module: leapEulerian
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Module of abstract objets defining Eulerian (field) data structure
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapBlock
  use leapIO
  use leapIO_mpi
  use mpi_f08
  use leapUtils, only : hashtbl_obj
  implicit none

  private
  public :: eulerian_obj_r, eulerian_obj_i, eulerian_set

  type,abstract :: eulerian_obj_base
    !> Base structure for eulerian data
    character(len=:),allocatable :: name                   !! Name of variable
    integer                      :: staggering=0           !! 0: Collocated; 1: X face centered
                                                           !! 2: Y-face centered; 3: Z-face centered
    type(parallel_obj), pointer  :: parallel=>null()       !! Associated parallel structure
    type(block_obj),    pointer  :: block   =>null()       !! Associated block structure
    contains
      procedure :: initialize          => eulerian_obj_Init
      procedure :: finalize            => eulerian_obj_Final
      procedure :: UpdateGhostCells    => eulerian_obj_UpdateGhostCells
      procedure :: AddUpGhostCells     => eulerian_obj_AddUpGhostCells
      procedure :: allocate            => eulerian_obj_allocate
      procedure :: deallocate          => eulerian_obj_deallocate
      procedure :: UpdateGhostCells_x  => eulerian_obj_UpdateGhostCells_x
      procedure :: UpdateGhostCells_y  => eulerian_obj_UpdateGhostCells_y
      procedure :: UpdateGhostCells_z  => eulerian_obj_UpdateGhostCells_z
      procedure :: AddUpGhostCells_x   => eulerian_obj_AddUpGhostCells_x
      procedure :: AddUpGhostCells_y   => eulerian_obj_AddUpGhostCells_y
      procedure :: AddUpGhostCells_z   => eulerian_obj_AddUpGhostCells_z
      procedure :: Info                => eulerian_obj_Info
      procedure :: mean                => eulerian_obj_mean
      procedure :: norm2               => eulerian_obj_norm2
      generic   :: assignment(=)       => eulerian_obj_AssignEulerianObj,   &
                                          eulerian_obj_AssignReal0D,        &
                                          eulerian_obj_AssignInt0D
      generic   :: operator(+) => eulerian_obj_AddEulerianRObj,    &
                                  eulerian_obj_AddEulerianIObj
      generic   :: operator(-) => eulerian_obj_SubEulerianRObj,    &
                                  eulerian_obj_SubEulerianIObj
      generic   :: operator(*) => eulerian_obj_MulReal0D,    &
                                  eulerian_obj_MulInt0D
      procedure :: eulerian_obj_AssignEulerianObj
      procedure :: eulerian_obj_AssignReal0D
      procedure :: eulerian_obj_AssignInt0D
      procedure :: eulerian_obj_AddEulerianRObj
      procedure :: eulerian_obj_AddEulerianIObj
      procedure :: eulerian_obj_SubEulerianRObj
      procedure :: eulerian_obj_SubEulerianIObj
      procedure :: eulerian_obj_MulReal0D
      procedure :: eulerian_obj_MulInt0D
  end type eulerian_obj_base

  type, extends (eulerian_obj_base) :: eulerian_obj_r
    !> Eulerian data of type real
    real(wp), allocatable :: cell(:,:,:)                   !! Cell data
  end type eulerian_obj_r

  type, extends (eulerian_obj_base) :: eulerian_obj_i
    !> Eulerian data of type integer
    integer, allocatable :: cell(:,:,:)                    !! Cell data
  end type eulerian_obj_i

  type :: eulerian_ptr
    !> Polymorphic pointer to either real or complex
    ! Eulerian data
    class(eulerian_obj_base), pointer :: p => null()
  end type eulerian_ptr

  type :: eulerian_set
    !> A collection of eulerian objects
    type(eulerian_ptr), allocatable :: field(:)            !! A collection of Eulerian objects
    type(hashtbl_obj),  private     :: tbl                 !! Hash table
    character(len=str64)            :: read_file           !! file to read
    character(len=str64)            :: write_file          !! file to write
    logical                         :: overwrite=.true.    !! Switch to overwrite IO files
    type(parallel_obj),pointer      :: parallel=>null()    !! Associated parallel structure
    type(block_obj),   pointer      :: block   =>null()    !! Associated block structure
    contains
      procedure :: Initialize          => eulerian_set_Init
      procedure :: Finalize            => eulerian_set_Final
      procedure :: SetWriteFileName    => eulerian_set_SetWriteFileName
      procedure :: SetReadFileName     => eulerian_set_SetReadFileName
      procedure :: GetWriteFileName    => eulerian_set_GetWriteFileName
      procedure :: GetReadFileName     => eulerian_set_GetReadFileName
      procedure :: SetOverwrite        => eulerian_set_SetOverwrite
      procedure :: Add                 => eulerian_set_Add
      procedure :: GetIndex            => eulerian_set_GetIndex
      procedure :: Read                => eulerian_set_Read
      procedure :: ReadSingle          => eulerian_set_ReadSingle
      procedure :: ReadNGA             => eulerian_set_ReadNGA
      procedure :: ReadAllNGA          => eulerian_set_ReadAllNGA
      procedure :: Write               => eulerian_set_Write
      procedure :: WriteSingle         => eulerian_set_WriteSingle
      procedure :: WriteSilo           => eulerian_set_WriteSilo
      procedure :: WriteSingleSilo     => eulerian_set_WriteSingleSilo
      procedure :: Info                => eulerian_set_Info
  end type eulerian_set

  ! Default hash table size
  integer, parameter :: EULERIAN_SET_HTBL_SIZE = 20
  contains

    ! Procedures for Eulerian Objects
    ! ------------------------------------------------------
    subroutine eulerian_obj_Init(this,name,block,parallel,stag)
      !> Initialize an Eulerian field
      implicit none
      class(eulerian_obj_base),   intent(inout) :: this    !! An Eulerian field
      character(len=*),           intent(in)    :: name    !! Name of variable
      type(block_obj),    target, intent(in)    :: block   !! A block object
      type(parallel_obj), target, intent(in)    :: parallel!! parallel structure from main program
      integer,                    intent(in)    :: stag    !! Staggering

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Set the staggering
      if (stag.lt.0.or.stag.gt.3) &
        call this%parallel%stop ("Unacceptable staggering value")
      this%staggering=stag

      ! Set name of variable
      this%name=trim(adjustl(name))

      ! Allocate data
      call this%allocate
      return
    end subroutine eulerian_obj_Init
    subroutine eulerian_obj_Final(this)
      !> Finalize the Eulerian object
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field

      ! Nullify pointers
      this%parallel => null()
      this%block    => null ()

      ! Deallocate data
      call this%deallocate
      return
    end subroutine eulerian_obj_Final
    subroutine eulerian_obj_allocate(this)
      !> Allocate an Eulerian object
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field

      associate (lo => this%block%lo, hi => this%block%hi, ngc=> this%block%ngc)
        select type (this)
        type is (eulerian_obj_r)
          allocate(real(WP)::this%cell(lo(1)-ngc:hi(1)+ngc,lo(2)-ngc:hi(2)+ngc,lo(3)-ngc:hi(3)+ngc))
          this%cell = 0.0_wp
        type is (eulerian_obj_i)
          allocate(integer::this%cell(lo(1)-ngc:hi(1)+ngc,lo(2)-ngc:hi(2)+ngc,lo(3)-ngc:hi(3)+ngc))
          this%cell = 0
        end select
      end associate
      return
    end subroutine eulerian_obj_allocate
    subroutine eulerian_obj_deallocate(this)
      !> Deallocate an Eulerian object
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      select type (this)
      type is (eulerian_obj_r)
        if(allocated (this%cell)) deallocate(this%cell)
      type is (eulerian_obj_i)
        if(allocated (this%cell)) deallocate(this%cell)
      end select
      return
    end subroutine eulerian_obj_deallocate
    subroutine eulerian_obj_UpdateGhostCells(this)
      !> Update the ghostcells
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field

      call this%UpdateGhostCells_x
      call this%UpdateGhostCells_y
      call this%UpdateGhostCells_z

      return
    end subroutine eulerian_obj_UpdateGhostCells
    subroutine eulerian_obj_AddUpGhostCells(this)
      !> Update and add-up the ghostcells
      class(eulerian_obj_base),intent(inout):: this        !! An Eulerian field

      call this%AddUpGhostCells_x
      call this%AddUpGhostCells_y
      call this%AddUpGhostCells_z
      call this%UpdateGhostCells

      return
    end subroutine eulerian_obj_AddUpGhostCells
    subroutine eulerian_obj_UpdateGhostCells_x(this)
      !> Update the ghostcells in the x direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: recvL(3),recvR(3)
      integer :: sendL(3),sendR(3)
      integer :: i
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D/1D and pseudo-2D/1D exceptions:
        ! more ghost cells than internal cells
        if (Ng(1)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(hi(1)+i,:,:) = this%cell(lo(1)+i-1,:,:)
              this%cell(lo(1)-i,:,:) = this%cell(hi(1)-i+1,:,:)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(hi(1)+i,:,:) = this%cell(lo(1)+i-1,:,:)
              this%cell(lo(1)-i,:,:) = this%cell(hi(1)-i+1,:,:)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        recvL(1)=lo(1)-ngc  ; recvL(2)=lo(2)-ngc  ; recvL(3)=lo(3)-ngc
        recvR(1)=hi(1)+1    ; recvR(2)=lo(2)-ngc  ; recvR(3)=lo(3)-ngc
        sendL(1)=lo(1)      ; sendL(2)=lo(2)-ngc  ; sendL(3)=lo(3)-ngc
        sendR(1)=hi(1)-ngc+1; sendR(2)=lo(2)-ngc  ; sendR(3)=lo(3)-ngc

        select type (this)
        type is (eulerian_obj_r)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_r(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_r(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_i(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_i(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

      end associate
      return
    end subroutine eulerian_obj_UpdateGhostCells_x
    subroutine eulerian_obj_UpdateGhostCells_y(this)
      !> Update the ghostcells in the y direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: recvL(3),recvR(3)
      integer :: sendL(3),sendR(3)
      integer :: i
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D/1D and pseudo-2D/1D exceptions:
        ! more ghost cells than internal cells
        if (Ng(2)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(:,hi(2)+i,:) = this%cell(:,lo(2)+i-1,:)
              this%cell(:,lo(2)-i,:) = this%cell(:,hi(2)-i+1,:)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(:,hi(2)+i,:) = this%cell(:,lo(2)+i-1,:)
              this%cell(:,lo(2)-i,:) = this%cell(:,hi(2)-i+1,:)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        recvL(1)=lo(1)-ngc  ; recvL(2)=lo(2)-ngc  ; recvL(3)=lo(3)-ngc
        recvR(1)=lo(1)-ngc  ; recvR(2)=hi(2)+1    ; recvR(3)=lo(3)-ngc
        sendL(1)=lo(1)-ngc  ; sendL(2)=lo(2)      ; sendL(3)=lo(3)-ngc
        sendR(1)=lo(1)-ngc  ; sendR(2)=hi(2)-ngc+1; sendR(3)=lo(3)-ngc

        select type (this)
        type is (eulerian_obj_r)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_r(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_r(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_i(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_i(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

      end associate
      return
    end subroutine eulerian_obj_UpdateGhostCells_y
    subroutine eulerian_obj_UpdateGhostCells_z(this)
      !> Update the ghostcells in the z direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: recvL(3),recvR(3)
      integer :: sendL(3),sendR(3)
      integer :: i
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D/1D and pseudo-2D/1D exceptions:
        ! more ghost cells than internal cells
        if (Ng(3)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(:,:,hi(3)+i) = this%cell(:,:,lo(3)+i-1)
              this%cell(:,:,lo(3)-i) = this%cell(:,:,hi(3)-i+1)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(:,:,hi(3)+i) = this%cell(:,:,lo(3)+i-1)
              this%cell(:,:,lo(3)-i) = this%cell(:,:,hi(3)-i+1)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        recvL(1)=lo(1)-ngc  ; recvL(2)=lo(2)-ngc  ; recvL(3)=lo(3)-ngc
        recvR(1)=lo(1)-ngc  ; recvR(2)=lo(2)-ngc  ; recvR(3)=hi(3)+1
        sendL(1)=lo(1)-ngc  ; sendL(2)=lo(2)-ngc  ; sendL(3)=lo(3)
        sendR(1)=lo(1)-ngc  ; sendR(2)=lo(2)-ngc  ; sendR(3)=hi(3)-ngc+1

        select type (this)
        type is (eulerian_obj_r)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_r(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_r(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Post receives from Left and right ranks
          call MPI_IRECV( this%cell(recvL(1),recvL(2),recvL(3)), 1, gc_slab_i(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( this%cell(recvR(1),recvR(2),recvR(3)), 1, gc_slab_i(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

      end associate
      return
    end subroutine eulerian_obj_UpdateGhostCells_z
    subroutine eulerian_obj_AddUpGhostCells_x(this)
      !> Add up ghostcells in the x direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: sendL(3),sendR(3)
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr
      real(wp),allocatable :: buffR_r(:,:,:),buffL_r(:,:,:)
      integer, allocatable :: buffR_i(:,:,:),buffL_i(:,:,:)
      integer :: i,j,k,ii,jj,kk

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D exceptions
        if (Ng(1)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(hi(1),:,:) = this%cell(hi(1),:,:) + this%cell(hi(1)+i,:,:)
              this%cell(lo(1),:,:) = this%cell(lo(1),:,:) + this%cell(lo(1)-i,:,:)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(hi(1),:,:) = this%cell(hi(1),:,:) + this%cell(hi(1)+i,:,:)
              this%cell(lo(1),:,:) = this%cell(lo(1),:,:) + this%cell(lo(1)-i,:,:)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        sendL(1)=lo(1)-ngc  ; sendL(2)=lo(2)-ngc  ; sendL(3)=lo(3)-ngc
        sendR(1)=hi(1)+1    ; sendR(2)=lo(2)-ngc  ; sendR(3)=lo(3)-ngc

        select type (this)
        type is (eulerian_obj_r)
          ! Allocate receive buffers
          allocate(buffR_r(ngc,Ng(2),Ng(3)))
          allocate(buffL_r(ngc,Ng(2),Ng(3)))

          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_r(1,1,1), ngc*Ng(2)*Ng(3), mpi%REAL_WP, &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_r(1,1,1), ngc*Ng(2)*Ng(3), mpi%REAL_WP, &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Allocate receive buffers
          allocate(buffR_i(ngc,Ng(2),Ng(3)))
          allocate(buffL_i(ngc,Ng(2),Ng(3)))
          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_i(1,1,1), ngc*Ng(2)*Ng(3), mpi%INTEGER, &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_i(1,1,1), ngc*Ng(2)*Ng(3), mpi%INTEGER, &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(1), &
            mpi%rank%R(1)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(1), &
            mpi%rank%L(1)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

        select type(this)
        type is (eulerian_obj_r)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(1)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1),lo(1)+ngc-1
                  ii=i-(lo(1)      )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(1)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2)-ngc,hi(2)+ngc
                do i=hi(1)-ngc+1,hi(1)
                  ii=i-(hi(1)-ngc+1)+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_r,buffR_r)
        type is (eulerian_obj_i)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(1)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1),lo(1)+ngc-1
                  ii=i-(lo(1)      )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(1)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2)-ngc,hi(2)+ngc
                do i=hi(1)-ngc+1,hi(1)
                  ii=i-(hi(1)-ngc+1)+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_i,buffR_i)
        end select

      end associate
      return
    end subroutine eulerian_obj_AddUpGhostCells_x
    subroutine eulerian_obj_AddUpGhostCells_y(this)
      !> Add up ghostcells in the y direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: sendL(3),sendR(3)
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr
      real(wp),allocatable :: buffR_r(:,:,:),buffL_r(:,:,:)
      integer, allocatable :: buffR_i(:,:,:),buffL_i(:,:,:)
      integer :: i,j,k,ii,jj,kk

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D exceptions
        if (Ng(2)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(:,hi(2),:) = this%cell(:,hi(2),:) + this%cell(:,hi(2)+i,:)
              this%cell(:,lo(2),:) = this%cell(:,lo(2),:) + this%cell(:,lo(2)-i,:)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(:,hi(2),:) = this%cell(:,hi(2),:) + this%cell(:,hi(2)+i,:)
              this%cell(:,lo(2),:) = this%cell(:,lo(2),:) + this%cell(:,lo(2)-i,:)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        sendL(1)=lo(1)-ngc  ; sendL(2)=lo(2)-ngc  ; sendL(3)=lo(3)-ngc
        sendR(1)=lo(1)-ngc  ; sendR(2)=hi(2)+1    ; sendR(3)=lo(3)-ngc

        select type (this)
        type is (eulerian_obj_r)
          ! Allocate receive buffers
          allocate(buffR_r(Ng(1),ngc,Ng(3)))
          allocate(buffL_r(Ng(1),ngc,Ng(3)))

          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_r(1,1,1), ngc*Ng(1)*Ng(3), mpi%REAL_WP, &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_r(1,1,1), ngc*Ng(1)*Ng(3), mpi%REAL_WP, &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Allocate receive buffers
          allocate(buffR_i(Ng(1),ngc,Ng(3)))
          allocate(buffL_i(Ng(1),ngc,Ng(3)))

          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_i(1,1,1), ngc*Ng(1)*Ng(3), mpi%INTEGER, &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_i(1,1,1), ngc*Ng(1)*Ng(3), mpi%INTEGER, &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(2), &
            mpi%rank%R(2)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(2), &
            mpi%rank%L(2)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

        select type(this)
        type is (eulerian_obj_r)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(2)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2),lo(2)+ngc-1
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)      )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(2)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=hi(2)-ngc+1,hi(2)
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(hi(2)-ngc+1)+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_r,buffR_r)
        type is (eulerian_obj_i)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(2)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=lo(2),lo(2)+ngc-1
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)      )+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(2)-1.ne.MPI_PROC_NULL) then
            do k=lo(3)-ngc,hi(3)+ngc
              do j=hi(2)-ngc+1,hi(2)
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(hi(2)-ngc+1)+1
                  kk=k-(lo(3)-ngc  )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_i,buffR_i)
        end select

      end associate
      return
    end subroutine eulerian_obj_AddUpGhostCells_y
    subroutine eulerian_obj_AddUpGhostCells_z(this)
      !> Add up ghostcells in the z direction
      ! with non-blocking mpi directives
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      ! Work variables
      integer :: Ng(3)
      integer :: sendL(3),sendR(3)
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: ierr
      real(wp),allocatable :: buffR_r(:,:,:),buffL_r(:,:,:)
      integer, allocatable :: buffR_i(:,:,:),buffL_i(:,:,:)
      integer :: i,j,k,ii,jj,kk

      associate (lo => this%block%lo, hi => this%block%hi,   &
                 ngc=> this%block%ngc, mpi => this%parallel, &
                 gc_slab_r => this%block%gc_slab_r,          &
                 gc_slab_i => this%block%gc_slab_i)

        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        ! 2D exceptions
        if (Ng(3)-2*ngc.le.ngc) then
          select type (this)
          type is (eulerian_obj_r)
            do i=1,ngc
              this%cell(:,:,hi(3)) = this%cell(:,:,hi(3)) + this%cell(:,:,hi(3)+i)
              this%cell(:,:,lo(3)) = this%cell(:,:,lo(3)) + this%cell(:,:,lo(3)-i)
            end do
          type is (eulerian_obj_i)
            do i=1,ngc
              this%cell(:,:,hi(3)) = this%cell(:,:,hi(3)) + this%cell(:,:,hi(3)+i)
              this%cell(:,:,lo(3)) = this%cell(:,:,lo(3)) + this%cell(:,:,lo(3)-i)
            end do
          end select
          return
        end if

        ! Address of first element in buffer
        sendL(1)=lo(1)-ngc  ; sendL(2)=lo(2)-ngc  ; sendL(3)=lo(3)-ngc
        sendR(1)=lo(1)-ngc  ; sendR(2)=lo(2)-ngc  ; sendR(3)=hi(3)+1

        select type (this)
        type is (eulerian_obj_r)
          ! Allocate receive buffers
          allocate(buffR_r(Ng(1),Ng(2),ngc))
          allocate(buffL_r(Ng(1),Ng(2),ngc))

          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_r(1,1,1), ngc*Ng(2)*Ng(1), mpi%REAL_WP, &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_r(1,1,1), ngc*Ng(2)*Ng(1), mpi%REAL_WP, &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_r(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_r(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(3), ierr)
        type is (eulerian_obj_i)
          ! Allocate receive buffers
          allocate(buffR_i(Ng(1),Ng(2),ngc))
          allocate(buffL_i(Ng(1),Ng(2),ngc))
          ! Post receives from Left and right ranks
          call MPI_IRECV( buffL_i(1,1,1), ngc*Ng(2)*Ng(1), mpi%INTEGER, &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( buffR_i(1,1,1), ngc*Ng(2)*Ng(1), mpi%INTEGER, &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( this%cell(sendR(1),sendR(2),sendR(3)), 1, gc_slab_i(3), &
            mpi%rank%R(3)-1, 0, mpi%comm%g, requests(4), ierr)
          call MPI_ISEND( this%cell(sendL(1),sendL(2),sendL(3)), 1, gc_slab_i(3), &
            mpi%rank%L(3)-1, 0, mpi%comm%g, requests(3), ierr)
        end select

        ! Synchronize
        call MPI_WAITALL( 4, requests, statuses, ierr )

        select type(this)
        type is (eulerian_obj_r)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(3)-1.ne.MPI_PROC_NULL) then
            do k=lo(3),lo(3)+ngc-1
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)      )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(3)-1.ne.MPI_PROC_NULL) then
            do k=hi(3)-ngc+1,hi(3)
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(hi(3)-ngc+1)+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_r(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_r,buffR_r)
        type is (eulerian_obj_i)
          ! Add left buffer to left ghostcells
          if (mpi%rank%L(3)-1.ne.MPI_PROC_NULL) then
            do k=lo(3),lo(3)+ngc-1
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(lo(3)      )+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffL_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          ! Add right buffer to right ghostcells
          if (mpi%rank%R(3)-1.ne.MPI_PROC_NULL) then
            do k=hi(3)-ngc+1,hi(3)
              do j=lo(2)-ngc,hi(2)+ngc
                do i=lo(1)-ngc,hi(1)+ngc
                  ii=i-(lo(1)-ngc  )+1
                  jj=j-(lo(2)-ngc  )+1
                  kk=k-(hi(3)-ngc+1)+1
                  this%cell(i,j,k)=this%cell(i,j,k) + buffR_i(ii,jj,kk)
                end do
              end do
            end do
          end if
          deallocate(buffL_i,buffR_i)
        end select

      end associate
      return
    end subroutine eulerian_obj_AddUpGhostCells_z
    subroutine eulerian_obj_Info(this)
      !> Print info about this structure
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field

      write(stdout,"(a20,a, a)"  ) 'name'                , repeat('-',19)//'>', this%name
      write(stdout,"(a20,a, i16)") 'staggering'          , repeat('-',19)//'>', this%staggering
      write(stdout,"(a20,a, l16)") 'associated(parallel)', repeat('-',19)//'>', associated(this%parallel)
      write(stdout,"(a20,a, l16)") 'associated(block)'   , repeat('-',19)//'>', associated(this%block)
      return
    end subroutine eulerian_obj_Info
    function eulerian_obj_mean(this) result (val)
      !> Compute the mean of an Eulerian_obj
      implicit none
      class(eulerian_obj_base), intent(in)    :: this      !! An Eulerian field
      real(wp) :: val
      ! Work variable
      real(wp) :: vol
      integer  :: i,j,k
      real(wp) :: buff_r

      associate (lo => this%block%lo,  hi => this%block%hi,         &
        dxm=>this%block%dxm,dym=>this%block%dym,dzm=>this%block%dzm)

        val = 0.0_wp; vol = 0.0_wp
        select type(this)
          type is (eulerian_obj_r)
            do k=lo(3),hi(3)
              do j=lo(2),hi(2)
                do i=lo(1),hi(1)
                  vol  = vol + dxm(i)*dym(j)*dzm(k)
                  val  = val + this%cell(i,j,k)*dxm(i)*dym(j)*dzm(k)
                end do
              end do
            end do
          type is (eulerian_obj_i)
            do k=lo(3),hi(3)
              do j=lo(2),hi(2)
                do i=lo(1),hi(1)
                  vol  = vol + dxm(i)*dym(j)*dzm(k)
                  val  = val + real(this%cell(i,j,k),wp)*dxm(i)*dym(j)*dzm(k)
                end do
              end do
            end do
          class default
            call this%parallel%stop("Error encountered with Eulerian object")
        end select
        call this%parallel%sum(vol,buff_r); vol=buff_r
        call this%parallel%sum(val,buff_r); val=buff_r/vol
      end associate
      return
    end function eulerian_obj_mean
    function eulerian_obj_norm2(this) result (val)
      !> Compute norm2 of an Eulerian_obj
      implicit none
      class(eulerian_obj_base), intent(in)    :: this      !! An Eulerian field
      real(wp) :: val
      ! Work variable
      real(wp) :: vol
      integer  :: i,j,k
      real(wp) :: buff_r

      associate (lo => this%block%lo,  hi => this%block%hi,         &
        dxm=>this%block%dxm,dym=>this%block%dym,dzm=>this%block%dzm)

        val = 0.0_wp; vol = 0.0_wp
        select type(this)
          type is (eulerian_obj_r)
            do k=lo(3),hi(3)
              do j=lo(2),hi(2)
                do i=lo(1),hi(1)
                  vol  = vol + dxm(i)*dym(j)*dzm(k)
                  val  = val + (this%cell(i,j,k)**2)*dxm(i)*dym(j)*dzm(k)
                end do
              end do
            end do
          type is (eulerian_obj_i)
            do k=lo(3),hi(3)
              do j=lo(2),hi(2)
                do i=lo(1),hi(1)
                  vol  = vol + dxm(i)*dym(j)*dzm(k)
                  val  = val + (real(this%cell(i,j,k),wp)**2)*dxm(i)*dym(j)*dzm(k)
                end do
              end do
            end do
          class default
            call this%parallel%stop("Error encountered with Eulerian object")
        end select
        call this%parallel%sum(vol,buff_r); vol=buff_r
        call this%parallel%sum(val,buff_r); val=sqrt(buff_r/vol)
      end associate
      return
    end function eulerian_obj_norm2
    subroutine eulerian_obj_AssignEulerianObj(this,in)
      !> Assignment for Eulerian_obj
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      class(eulerian_obj_base), intent(in)    :: in        !! Object to assign

      select type (this)
      type is (eulerian_obj_r)
        select type(in)
        type is (eulerian_obj_r)
          this%cell = in%cell
        type is (eulerian_obj_i)
          this%cell = real(in%cell,wp)
        class default
          call this%parallel%stop("Error assigning Eulerian object")
        end select

      type is (eulerian_obj_i)
        select type(in)
        type is (eulerian_obj_i)
          this%cell = in%cell
        class default
          call this%parallel%stop("Error assigning Eulerian object")
        end select

      class default
        call this%parallel%stop("Error assigning Eulerian object")
      end select

      return
    end subroutine eulerian_obj_AssignEulerianObj
    subroutine eulerian_obj_AssignReal0D(this,in)
      !> Assignment for Eulerian_obj
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      real(wp),                 intent(in)    :: in        !! Object to assign

      select type (this)
      type is (eulerian_obj_r)
          this%cell = in

      class default
        call this%parallel%stop("Error assigning Eulerian object")
      end select

      return
    end subroutine eulerian_obj_AssignReal0D
    subroutine eulerian_obj_AssignInt0D(this,in)
      !> Assignment for Eulerian_obj
      implicit none
      class(eulerian_obj_base), intent(inout) :: this      !! An Eulerian field
      integer,                  intent(in)    :: in        !! Object to assign

      select type (this)
      type is (eulerian_obj_r)
          this%cell = real(in,wp)

      type is (eulerian_obj_i)
          this%cell = in

      class default
        call this%parallel%stop("Error assigning Eulerian object")
      end select

      return
    end subroutine eulerian_obj_AssignInt0D
    function eulerian_obj_AddEulerianRObj(this,in) result (res)
      !> Addition
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      type(eulerian_obj_r),     intent(in) :: in           !! An Eulerian field
      type(eulerian_obj_r)                 :: res

      call res%initialize(this%name,this%block,this%parallel,this%staggering)
      select type (this)
      type is (eulerian_obj_r)
          res%cell = this%cell+in%cell

      type is (eulerian_obj_i)
          res%cell = real(this%cell,wp)+in%cell

      class default
        call this%parallel%stop("Error performing addition of Eulerian object")
      end select

      return
    end function eulerian_obj_AddEulerianRObj
    function eulerian_obj_AddEulerianIObj(this,in) result (res)
      !> Addition
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      type(eulerian_obj_i),     intent(in) :: in           !! An Eulerian field
      class(eulerian_obj_base), allocatable:: res

      select type (this)
      type is (eulerian_obj_r)
        allocate(eulerian_obj_r:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_r)
          res%cell = this%cell+real(in%cell,wp)
        end select

      type is (eulerian_obj_i)
        allocate(eulerian_obj_i:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_i)
          res%cell = this%cell+in%cell
        end select

      class default
        call this%parallel%stop("Error performing addition of Eulerian object")
      end select

      return
    end function eulerian_obj_AddEulerianIObj
    function eulerian_obj_SubEulerianRObj(this,in) result (res)
      !> Addition
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      type(eulerian_obj_r),     intent(in) :: in           !! An Eulerian field
      type(eulerian_obj_r)                 :: res

      call res%initialize(this%name,this%block,this%parallel,this%staggering)
      select type (this)
      type is (eulerian_obj_r)
          res%cell = this%cell-in%cell

      type is (eulerian_obj_i)
          res%cell = real(this%cell,wp)-in%cell

      class default
        call this%parallel%stop("Error performing addition of Eulerian object")
      end select

      return
    end function eulerian_obj_SubEulerianRObj
    function eulerian_obj_SubEulerianIObj(this,in) result (res)
      !> Addition
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      type(eulerian_obj_i),     intent(in) :: in           !! An Eulerian field
      class(eulerian_obj_base), allocatable:: res

      select type (this)
      type is (eulerian_obj_r)
        allocate(eulerian_obj_r:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_r)
          res%cell = this%cell-real(in%cell,wp)
        end select

      type is (eulerian_obj_i)
        allocate(eulerian_obj_i:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_i)
          res%cell = this%cell-in%cell
        end select

      class default
        call this%parallel%stop("Error performing addition of Eulerian object")
      end select

      return
    end function eulerian_obj_SubEulerianIObj
    function eulerian_obj_MulReal0D(this,in) result (res)
      !> Multiplication
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      real(wp),                 intent(in) :: in           !! An Eulerian field
      type(eulerian_obj_r)                 :: res

      call res%initialize(this%name,this%block,this%parallel,this%staggering)
      select type (this)
      type is (eulerian_obj_r)
          res%cell = this%cell*in

      class default
        call this%parallel%stop("Error performing multiplication of Eulerian object")
      end select

      return
    end function eulerian_obj_MulReal0D
    function eulerian_obj_MulInt0D(this,in) result (res)
      !> Multiplication
      implicit none
      class(eulerian_obj_base), intent(in) :: this         !! An Eulerian field
      integer,                  intent(in) :: in           !! An Eulerian field
      class(eulerian_obj_base),allocatable :: res

      select type (this)
      type is (eulerian_obj_r)
        allocate(eulerian_obj_r:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_r)
          res%cell = this%cell*real(in,wp)
        end select

      type is (eulerian_obj_i)
        allocate(eulerian_obj_i:: res)
        call res%initialize(this%name,this%block,this%parallel,this%staggering)

        select type(res)
        type is (eulerian_obj_i)
          res%cell = this%cell*in
        end select

      class default
        call this%parallel%stop("Error performing multiplication of Eulerian object")
      end select

      return
    end function eulerian_obj_MulInt0D

    ! Procedure for Eulerian Sets
    ! ------------------------------------------------------
    subroutine eulerian_set_Init(this,block,parallel)
      !> Initialize a collection of Eulerian objects
      implicit none
      class(eulerian_set),        intent(inout) :: this    !! A collection of Eulerian objects
      type(block_obj),    target, intent(in)    :: block   !! A block object
      type(parallel_obj), target, intent(in)    :: parallel!! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Initialize hash table, with default size of 20
      call this%tbl%initialize(EULERIAN_SET_HTBL_SIZE)

      return
    end subroutine eulerian_set_Init
    subroutine eulerian_set_Final(this)
      !> Finalize structure
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      ! Work variables
      integer :: n

      ! Nullify pointers
      this%parallel => null()
      this%block    => null()

      ! Finalize each Eulerian object
      if (allocated(this%field)) then
        do n=1,size(this%field)
          call this%field(n)%p%finalize
        end do
        deallocate(this%field)
      end if

      ! Clear hash table
      call this%tbl%finalize

      return
    end subroutine eulerian_set_Final
    subroutine eulerian_set_Add(this,name,stag,obj)
      !> Add a new element to a collection of
      ! Eulerian objects
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      character(len=*),    intent(in)    :: name           !! Name of variable
      integer,             intent(in)    :: stag           !! Staggering
      class(eulerian_obj_base), target,  &
                           intent(inout) :: obj            !! Eulerian obj to link and initialize
      ! Work bariables
      type(eulerian_ptr),  allocatable   :: tmp_array(:)

      ! Initialize Eulerian object
      call obj%initialize(name, this%block,this%parallel,stag)

      ! First resize array to accomodate a new element
      if (.not.allocated(this%field)) then
        allocate(this%field(1))
      else
        ! Allocate temporary array
        allocate(tmp_array(size(this%field)+1))

        ! Store old values
        tmp_array(1:size(this%field)) = this%field(:)

        ! Move the allocation from the
        ! temporary array to the final one
        call move_alloc(tmp_array,this%field)
      end if

      ! Link step
      this%field(size(this%field))%p => obj

      ! Add to hash table
      call this%tbl%put(key=this%tbl%HashString(name),val=size(this%field))

      return
    end subroutine eulerian_set_Add
    subroutine eulerian_set_SetWriteFileName(this,name)
      !> Set the base name of file to write
      implicit none
      class(eulerian_set),        intent(inout) :: this    !! A collection of Eulerian objects
      character(len=*),           intent(in)    :: name    !! Name of file
      this%write_file=name
      return
    end subroutine eulerian_set_SetWriteFileName
    function eulerian_set_GetWriteFileName(this) result(name)
      !> Return the base name of file to write
      implicit none
      class(eulerian_set),        intent(inout) :: this    !! A collection of Eulerian objects
      character(len=str64)                      :: name    !! Name of file
      name=this%write_file
      return
    end function eulerian_set_GetWriteFileName
    subroutine eulerian_set_SetReadFileName(this,name)
      !> Set the base name of file to read
      implicit none
      class(eulerian_set),        intent(inout) :: this    !! A collection of Eulerian objects
      character(len=*),           intent(in)    :: name    !! Name of file
      this%read_file=name
      return
    end subroutine eulerian_set_SetReadFileName
    function eulerian_set_GetReadFileName(this) result(name)
      !> Return the base name of file to write
      implicit none
      class(eulerian_set),        intent(inout) :: this    !! A collection of Eulerian objects
      character(len=str64)                      :: name    !! Name of file
      name=this%read_file
      return
    end function eulerian_set_GetReadFileName
    subroutine eulerian_set_SetOverwrite(this,overwrite)
      !> Set file overwritting
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      logical,             intent(in)    :: overwrite      !! Name of file
      this%overwrite=overwrite
      return
    end subroutine eulerian_set_SetOverwrite
    subroutine eulerian_set_Read(this,iter,time,list)
      !> Read Eulerian data
      implicit none
      class(eulerian_set),     intent(inout):: this        !! A collection of Eulerian objects
      integer,                 intent(out)  :: iter        !! Iteration at write
      real(wp),                intent(out)  :: time        !! Time at write
      character(str8),optional,intent(in)   :: list(:)     !! Names of fields to write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer         :: n,ind

      ! Nothing to read, if empty
      if (.not.allocated(this%field)) return

      ! Open file
      call h5%initialize(trim(adjustl(this%read_file)),"R",this%parallel)

      ! Jump to last step
      call h5%LastTimeStep(iter,time)

      if (.not.present(list)) then
        ! Read all fields
        do n=1,size(this%field)
          call this%ReadSingle(h5,n)
        end do
      else
        do  n=1,size(list)
          ! Get index of this Eulerian_obj
          ind = this%GetIndex(list(n))
          if (ind.lt.1) call this%parallel%stop('Unable to find '//list(n)//' in Eulerian_set')

          call this%ReadSingle(h5,ind)
        end do
      end if

      ! Close file
      call h5%finalize
      return
    end subroutine eulerian_set_Read
    subroutine eulerian_set_ReadNGA(this,iter,time)
      !> Read Eulerian data using MPI binary file tools
      implicit none
      class(eulerian_set),     intent(inout) :: this       !! A collection of Eulerian objects
      integer,                 intent(out)   :: iter       !! Iteration at write
      real(wp),                intent(out)   :: time       !! Time at write
      ! Work variables
      type(ngadata_obj) :: ngadata                         !! NGAdata structure

      ! Open file
      call ngadata%initialize(trim(adjustl(this%read_file)),"R",this%block,this%parallel)

      ! Return time of this snapshot
      time=ngadata%time
      iter=0 ! Iteration count is not stored in NGA-style raw files

      ! Read all fields
      call this%ReadAllNGA(ngadata)

      ! Close file
      call ngadata%finalize

      return
    end subroutine eulerian_set_ReadNGA
    subroutine eulerian_set_Write(this,iter,time,list)
      !> Write Eulerian data
      implicit none
      class(eulerian_set),     intent(inout) :: this       !! A collection of Eulerian objects
      integer,                 intent(in)    :: iter       !! Iteration at write
      real(wp),                intent(in)    :: time       !! Time at write
      character(str8),optional,intent(in)    :: list(:)    !! Names of fields to write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer         :: n, ind

      ! Nothing to write, if empty
      if (.not.allocated(this%field)) return

      ! Open the file
      if (this%overwrite) then
        call h5%initialize(trim(adjustl(this%write_file)),"W",this%parallel)
      else
        call h5%initialize(trim(adjustl(this%write_file)),"RW",this%parallel)
      end if

      ! Create a new time step
      call h5%NewTimeStep(iter,time)

      if (.not.present(list)) then
        ! Write all fields
        do n=1,size(this%field)
          call this%WriteSingle(h5,n)
        end do
      else
        do  n=1,size(list)
          ! Get index of this Eulerian_obj
          ind = this%GetIndex(list(n))
          if (ind.lt.1) call this%parallel%stop('Unable to find '//list(n)//' in Eulerian_set')

          call this%WriteSingle(h5,ind)
        end do
      end if
      call h5%finalize

      return
    end subroutine eulerian_set_Write
    subroutine eulerian_set_WriteSilo(this,iter,time,list)
      !> Write Eulerian data using SILO
      implicit none
      class(eulerian_set),     intent(inout):: this        !! A collection of Eulerian objects
      integer,                 intent(in)   :: iter        !! Iteration at write
      real(wp),                intent(in)   :: time        !! Time at write
      character(str8),optional,intent(in)   :: list(:)     !! Names of fields to write
      ! Work variables
      type(silo_obj) :: silo
      integer        :: n, ind

      ! Nothing to write, if empty
      if (.not.allocated(this%field)) return

      call silo%initialize(trim(adjustl(this%write_file)),"W",this%parallel)

      ! Add new timestep
      call silo%NewTimeStep(time)

      ! Write grid
      call silo%WriteGrid(this%block,iter,time)

      ! Write grid attributes
      if (.not.present(list)) then
        ! Write all fields
        do n=1,size(this%field)
          call this%WriteSingleSilo(silo,n)
        end do
      else
        do  n=1,size(list)
          ! Get index of this Eulerian_obj
          ind = this%GetIndex(list(n))
          if (ind.lt.1) call this%parallel%stop('Unable to find '//list(n)//' in Eulerian_set')

          call this%WriteSingleSilo(silo,ind)
        end do
      end if

      call silo%finalize
      return
    end subroutine eulerian_set_WriteSilo
    function eulerian_set_GetIndex(this,name) result (val)
      !> Returns the index of an Eulerian_obj contained
      ! in this%fields given its name.
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      character(len=*),    intent(in)    :: name           !! Name of the field
      integer :: val
      call this%tbl%get(key=this%tbl%HashString(name),val=val)
      return
    end function eulerian_set_GetIndex
    subroutine eulerian_set_ReadSingle(this,h5,ind)
      !> Read one Eulerian object based on name
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      type(h5hut_obj),     intent(inout) :: h5             !! H5hut structure
      integer,             intent(in)    :: ind            !! Index of Eulerian object
      ! Work variables
      real(wp),allocatable :: buff_r(:,:,:)                !! Buffer to write
      integer, allocatable :: buff_i(:,:,:)                !! Buffer to write

      ! Read field
      associate (lo => this%block%lo, hi => this%block%hi, ngc=> this%block%ngc)
        select type (my_field => this%field(ind)%p)
        type is (eulerian_obj_r)
          ! Allocate buffer
          allocate(buff_r(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Read data
          call h5%read(my_field%name,buff_r,lo,hi)
          my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))=buff_r
          ! Clean
          deallocate(buff_r)
        type is (eulerian_obj_i)
          ! Allocate buffer
          allocate(buff_i(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Read data
          call h5%read(my_field%name,buff_i,lo,hi)
          my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))=buff_i
          ! Clean
          deallocate(buff_i)
        end select

        call this%field(ind)%p%UpdateGhostCells
      end associate
      return
    end subroutine eulerian_set_ReadSingle
    subroutine eulerian_set_ReadAllNGA(this,ngadata)
      !> Read all Eulerian objects in file
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      type(ngadata_obj),   intent(inout) :: ngadata        !! NGAdata structure
      ! Work variables
      real(wp),allocatable :: buff_r(:,:,:)                !! Buffer to write
      !integer, allocatable :: buff_i(:,:,:)                !! Buffer to write
      integer         :: n
      integer         :: nFields_

      ! Count number of fields in the data structure
      nFields_=0
      if (allocated(this%field)) nFields_=nFields_+size(this%field)

      if (nFields_.gt.size(ngadata%names)) then
        call this%parallel%stop("The number of Eulerian objects to read does not"// &
                                " match the number of Eulerian objects found in file")
      end if

      ! Loop over all  Eulerian objects
      if (allocated(this%field)) then
        do n=1,size(this%field)
          ! Read field
          associate (lo => this%block%lo, hi => this%block%hi, ngc=> this%block%ngc)
            select type (my_field => this%field(n)%p)
            type is (eulerian_obj_r)
              ! Allocate buffer
              allocate(buff_r(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
              ! Read data
              call ngadata%read(my_field%name,buff_r,lo,hi)
              my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))=buff_r
              ! Clean
              deallocate(buff_r)
            end select
          end associate
        end do
      end if

      return
    end subroutine eulerian_set_ReadAllNGA
    subroutine eulerian_set_WriteSingle(this,h5,ind)
      !> Write a single Eulerian object to file
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      type(h5hut_obj),     intent(inout) :: h5             !! H5hut structure
      integer,             intent(in)    :: ind            !! Index of Eulerian object
      ! Work variables
      real(wp),allocatable :: buff_r(:,:,:)                !! Buffer to write
      integer, allocatable :: buff_i(:,:,:)                !! Buffer to write
      real(wp)        :: shift(3)                          !! Shift to account for staggering
      real(wp)        :: origin(3)                         !! Absolute origin


      ! Get grid origin and displacement
      origin=this%block%pmin

      ! Write grid attributes
      shift=0.5_wp*this%block%dx
      select case(this%field(ind)%p%staggering)
      case (0)
        ! Collocated - cell centered
        ! Do nothing
      case (1)
        ! Staggered - X-face centered
        shift(1)=0.0_wp
      case (2)
        ! Staggered - X-face centered
        shift(2)=0.0_wp
      case (3)
        ! Staggered - X-face centered
        shift(3)=0.0_wp
      end select

      ! Write grid attributes
      call h5%WriteGrid(this%field(ind)%p%name,origin+shift,this%block%dx)

      associate (lo => this%block%lo, hi => this%block%hi, ngc=> this%block%ngc)
        select type (my_field=>this%field(ind)%p)
        type is (eulerian_obj_r)
          ! Allocate buffer
          allocate(buff_r(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Write data
          buff_r = my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))
          call h5%write(my_field%name,buff_r,lo,hi)
          deallocate(buff_r)
        type is (eulerian_obj_i)
          ! Allocate buffer
          allocate(buff_i(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Write data
          buff_i = my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))
          call h5%write(my_field%name,buff_i,lo,hi)
          deallocate(buff_i)
        end select
      end associate

      return
    end subroutine eulerian_set_WriteSingle
    subroutine eulerian_set_WriteSingleSilo(this,silo,ind)
      !> Write a single Eulerian objects to file
      ! using SILO
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      type(silo_obj),      intent(inout) :: silo           !! Silo structure
      integer,             intent(in)    :: ind            !! Index of Eulerian object
      ! Work variables
      real(wp),allocatable :: buff_r(:,:,:)                !! Buffer to write
      integer, allocatable :: buff_i(:,:,:)                !! Buffer to write
      character(len=:),allocatable ::var_name              !! Local copy of variable name

      ! Store variable name
      var_name=this%field(ind)%p%name

      associate (lo => this%block%lo, hi => this%block%hi, ngc=> this%block%ngc)
        select type (my_field=>this%field(ind)%p)
        type is (eulerian_obj_r)

          ! Allocate buffer
          allocate(buff_r(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Write data
          buff_r = real(my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)),leapSP)
          call silo%write(var_name,buff_r,lo,hi,my_field%staggering)
          deallocate(buff_r)

        type is (eulerian_obj_i)

          ! Allocate buffer
          allocate(buff_i(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3)))
          ! Write data
          buff_i = my_field%cell(lo(1):hi(1), lo(2):hi(2), lo(3):hi(3))
          call silo%write(var_name,buff_i,lo,hi,my_field%staggering)
          deallocate(buff_i)

        end select
      end associate

      return
    end subroutine eulerian_set_WriteSingleSilo
    subroutine eulerian_set_Info(this)
      !> Print info about this collection
      ! of eulerian objects
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(eulerian_set), intent(inout) :: this           !! A collection of Eulerian objects
      ! Work variables
      integer :: n

      if (allocated(this%field)) then
        write(stdout,"(a20,a, i16)") 'fields'   , repeat('-',19)//'>', size(this%field)
        do n=1,size(this%field)
          write(stdout,"(a,i2,a)"  ) repeat('-',26)//" ",n," "//repeat('-',26)
          call this%field(n)%p%Info
        end do
        write(stdout,"(a56)"  ) repeat('-',56)
        write(stdout,"(a56)"  ) repeat('-',56)
      else
        write(stdout,"(a20,a, a)"  ) 'fields'  , repeat('-',19)//'>', "NOT ALLOCATED"
      end if
      write(stdout,"(a20,a, a)"  ) 'File to read'        , repeat('-',19)//'>', this%read_file
      write(stdout,"(a20,a, a)"  ) 'File to write'       , repeat('-',19)//'>', this%write_file
      write(stdout,"(a20,a, l16)") 'IO overwrite'        , repeat('-',19)//'>', this%overwrite
      write(stdout,"(a20,a, l16)") 'associated(parallel)', repeat('-',19)//'>', associated(this%parallel)
      write(stdout,"(a20,a, l16)") 'associated(block)'   , repeat('-',19)//'>', associated(this%block)
      return
    end subroutine eulerian_set_Info
end module leapEulerian
