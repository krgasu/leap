!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapHypre
  !>--------------------------------------------------------------------------
  ! Module: leaphypre
  ! Summary: Mohamed Houssem Kasbaoui
  !
  ! Module giving access to the HYPRE solvers for sparse linear systems.
  !
  ! Note 1.
  ! Solver naming convention: X_Y_Z, where
  ! X: HYPRE interface  (S: Struct, SS: SStruct, IJ: IJ),
  ! Y: Solver (e.g. PCG),
  ! Z: Preconditioner (e.g. DS).
  ! Available solvers: S_SMG_NONE, S_PFMG_NONE, S_PCG_NONE, IJ_AMG_NONE,
  ! IJ_PCG_DS, IJ_PCG_AMG
  !
  ! Note 2.
  ! GPU support is enabled with IJ_AMG_NONE.
  ! To be able to use GPU acceleration, LEAP must be compiled against
  ! GPU-enabled HYPRE.
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapEulerian
  use leapBlock
  use leapParallel
  use mpi_f08, only : MPI_Comm
  use, intrinsic :: iso_c_binding,   only: c_ptr
  implicit none
  include 'HYPREf.h'

  private
  public  :: hypre_obj

  type :: hypre_obj
    !> A utility to call/use HYPRE scallable solvers
    type(parallel_obj), pointer  :: parallel=>null()       !! Associated parallel structure
    type(block_obj),    pointer  :: block   =>null()       !! Associated block structure

    ! Parameters of linear system to solve (mat*sol=rhs)
    real(wp), allocatable:: mat(:,:,:,:,:)                 !! Matrix
    integer              :: stm=1                          !! Stencil extent for matrix
    real(wp)             :: h2                             !! Scaling factor

    ! Common variables to all HYPRE solvers
    character(len=:),allocatable :: solver_name            !! Name of solver (see list below)
    integer(kind=leapI8) :: solver                         !! Hypre solver pointer
    integer(kind=leapI8) :: precond                        !! Hypre preconditioner pointer
    integer(kind=leapI8) :: matrix                         !! Hypre matrix pointer
    integer(kind=leapI8) :: rhs                            !! Hypre vector pointer for right-hand side
    integer(kind=leapI8) :: sol                            !! Hypre vector pointer for solution
    integer              :: interface=-1                   !! Hypre interface
    type(MPI_Comm)       :: comm                           !! MPI communicator used by Hyper

    ! Solver parameters
    real(wp)             :: MaxTol    = 1.0e-8_wp          !! Maximum relative tolerance
    integer              :: MaxIt     = 20                 !! Maximum number of subiterations
    integer              :: RelaxType = 1                  !! Relaxation Type for multigrid solvers
    integer              :: it        = 0                  !! Number of iteration at end of solve
    real(wp)             :: rel                            !! Relative error at end of solve

    ! Specific to Struct interface
    integer              :: dim=3                          !! Dimensionality of the problem
    integer              :: st=1                           !! Stencil bounds (-st to +st); st=1 gives a 7-point stencil in 3D
    integer              :: st_size                        !! Stencil size
    integer(kind=leapI8) :: grid                           !! Hypre grid pointer
    integer(kind=leapI8) :: stencil                        !! Hypre stencil pointer

    ! Specific to IJ interface
    integer(kind=leapI8) :: par_matrix                     !! Pointer to ParCSR storage
    integer(kind=leapI8) :: par_rhs                        !! Pointer to ParCSR storage
    integer(kind=leapI8) :: par_sol                        !! Pointer to ParCSR storage
    type(eulerian_obj_i) :: irow                           !! Row index (one row = one grid point)
    integer              :: irow_lo, irow_hi               !! low and high row indices
    real(wp), pointer    :: rhs_values(:)                  !! Storage arrays, rhs
    real(wp), pointer    :: sol_values(:)                  !! Storage arrays, sol
    real(wp), pointer    :: values(:)                      !! Storage arrays, matrix coefficients
    integer,  pointer    :: rows(:)                        !! Storage arrays, row indices
    integer,  pointer    :: cols(:)                        !! Storage array, column indices
    integer,  pointer    :: tmpi(:)                        !! Storage arrays, tmp integer values
#ifdef USE_GPU
    type(c_ptr)          :: p_rhs_values
    type(c_ptr)          :: p_sol_values
    type(c_ptr)          :: p_values
    type(c_ptr)          :: p_rows
    type(c_ptr)          :: p_cols
    type(c_ptr)          :: p_tmpi
#endif
    contains
      procedure :: Initialize      => hypre_obj_Init
      procedure :: Finalize        => hypre_obj_Final
      procedure :: SelectSolver    => hypre_obj_SelectSolver
      procedure :: Setup           => hypre_obj_Setup
      procedure :: SetRHS          => hypre_obj_SetRHS
      procedure :: Solve           => hypre_obj_Solve
      ! Subroutines for IJ interface
      procedure :: hypre_obj_SetupPointersIJ
      procedure :: hypre_obj_SetupRowsIJ
      procedure :: hypre_obj_SetupRHSIJ
      procedure :: hypre_obj_SetupSolIJ
      procedure :: hypre_obj_BuildMatrixIJ
      procedure :: hypre_obj_SetupMatrixIJ
      procedure :: hypre_obj_SetRHSIJ
      procedure :: hypre_obj_SetupSolIJver
      procedure :: hypre_obj_SolveIJ
      ! Subroutines for Struct interface
      procedure :: hypre_obj_SetupGridS
      procedure :: hypre_obj_SetupStencilS
      procedure :: hypre_obj_SetupSolverS
      procedure :: hypre_obj_SetupMatrixS
      procedure :: hypre_obj_SetupRHSS
      procedure :: hypre_obj_SetupSolS
      procedure :: hypre_obj_BuildMatrixS
      procedure :: hypre_obj_SetRHSS
      procedure :: hypre_obj_SolveS
      procedure :: hypre_obj_PrintMatrixS
  end type hypre_obj

  integer, parameter :: HYPRE_INT_IJ      = 1
  integer, parameter :: HYPRE_INT_Struct  = 2
  integer, parameter :: HYPRE_INT_SStruct = 3

  character(len=*),parameter :: HYPRE_SOL_IJ_PCG_AMG ='IJ-PCG-AMG'
  character(len=*),parameter :: HYPRE_SOL_IJ_PCG_DS  ='IJ-PCG-DS'
  character(len=*),parameter :: HYPRE_SOL_IJ_AMG_NONE='IJ-AMG-NONE'

  character(len=*),parameter :: HYPRE_SOL_S_SMG_NONE='SMG'
  !> HYPRE DOC: SMG is a parallel semicoarsening multigrid solver for solving
  !     \nabla \cdot \left( D\nabla u\right) + \sigma u = f
  ! on logically rectangular grids. The code solves both 2D and 3D problems with
  ! discretization stencils of up to 9-points in 2D and up to 27-points in 3D.
  ! Note: SMG is a particularly robust method. The algorithm semicoarsens in the
  ! z-direction and uses plane smoothing. The xy plane-solves are effected by one
  ! V-cycle of the 2D SMG algorithm, which semicoarsens in the y-direction and uses
  ! line smoothing.
  character(len=*),parameter :: HYPRE_SOL_S_PFMG_NONE='PFMG'
  !> HYPRE DOC: PFMG is a parallel semicoarsening multigrid solver that uses pointwise relaxation.
  ! For periodic problems, users should try to set the grid size in periodic dimensions
  ! to be as close to a power-of-two as possible. That is, if the grid size in a periodic
  ! dimension is given by N=2^m * M  where  M is not a power-of-two, then M should be
  ! as small as possible. Large values of  will generally result in slower convergence rates.
  ! Note: PFMG is not as robust as SMG, but is much more efficient per V-cycle.
  character(len=*),parameter :: HYPRE_SOL_S_PCG_NONE ='PCG'
  !> HYPRE DOC: Preconditioned Conjugate Gradient, belongs to family of Krylov Solvers
  contains
    subroutine hypre_obj_Init(this,block,parallel)
      !> Initialize the hypre object
      implicit none
      class(hypre_obj),           intent(inout):: this     !! Hypre machinery
      type(block_obj),    target, intent(in)   :: block    !! A block object
      type(parallel_obj), target, intent(in)   :: parallel !! parallel structure from main program
      ! Work variables
      integer :: ierr

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Scaling factor
      this%h2 = minval(this%block%dx)**2

      ! Set internal MPI communicator
      this%comm = this%parallel%comm%w

      ! Initialize Hypre internals
      call HYPRE_Init(ierr)
      return
    end subroutine hypre_obj_Init
    subroutine hypre_obj_Final(this)
      !> Destroy objects/pointers and clear data
      use leapCuda
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr
#ifdef USE_GPU
      integer :: stat
#endif

      ! Nullify pointers
      this%parallel => null()
      this%block    => null()

      if (allocated(this%solver_name)) then
        select case (this%interface)
        case (HYPRE_INT_Struct)
          call HYPRE_StructVectorDestroy (this%rhs,    ierr)
          call HYPRE_StructVectorDestroy (this%sol,    ierr)
          call HYPRE_StructMatrixDestroy (this%matrix, ierr)
          call HYPRE_StructStencilDestroy(this%stencil,ierr)
          call HYPRE_StructGridDestroy   (this%grid,   ierr)

          select case (this%solver_name)
          case (HYPRE_SOL_S_PCG_NONE)
            call HYPRE_StructPCGDestroy  (this%solver, ierr)
          case (HYPRE_SOL_S_SMG_NONE)
            call HYPRE_StructSMGDestroy  (this%solver, ierr)
          case (HYPRE_SOL_S_PFMG_NONE)
            call HYPRE_StructPFMGDestroy (this%solver, ierr)
          case default
            call this%parallel%stop("HYPRE: Unknown solver")
          end select

        case (HYPRE_INT_SStruct)
          ! Not yet implemented
        case (HYPRE_INT_IJ)

          call this%irow%Finalize
          call HYPRE_IJVectorDestroy     (this%rhs,    ierr)
          call HYPRE_IJVectorDestroy     (this%sol,    ierr)
          call HYPRE_IJMatrixDestroy     (this%matrix, ierr)
          select case (this%solver_name)
          case (HYPRE_SOL_IJ_AMG_NONE)
            call HYPRE_BoomerAMGDestroy  (this%solver, ierr)
          case (HYPRE_SOL_IJ_PCG_DS)
            call HYPRE_ParCSRPCGDestroy  (this%solver, ierr)
          case (HYPRE_SOL_IJ_PCG_AMG)
            call HYPRE_BoomerAMGDestroy  (this%precond,ierr)
            call HYPRE_ParCSRPCGDestroy  (this%solver, ierr)
          case default
            call this%parallel%stop("HYPRE: Unknown solver")
          end select
#ifdef USE_GPU
          ! Clean up -- GPU variables
          stat = device_free(this%p_rhs_values)
          stat = device_free(this%p_sol_values)
          stat = device_free(this%p_rows)
          stat = device_free(this%p_cols)
          stat = device_free(this%p_tmpi)
          stat = device_free(this%p_values)
#else
          deallocate(this%rhs_values)
          deallocate(this%sol_values)
          deallocate(this%values)
          deallocate(this%rows)
          deallocate(this%cols)
          deallocate(this%tmpi)
#endif

        case default
          call this%parallel%stop("Unknown or uninitialized HYPRE interface")
        end select


        deallocate(this%solver_name)
      end if

      return
    end subroutine hypre_obj_Final
    subroutine hypre_obj_SelectSolver(this,name,MaxTol,MaxIt,RelaxType)
      !> Select one of the preconfigured solvers and
      ! get solver-specific parameters
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      character(len=*),  intent(in)   :: name              !! Name of solver to be used
      real(wp), optional,intent(in)   :: MaxTol            !! Maximum relative tolerance
      integer,  optional,intent(in)   :: MaxIt             !! Maximum number of subiterations
      integer,  optional,intent(in)   :: RelaxType         !! Relaxation type

      ! Assign name
      this%solver_name = trim(adjustl(name))

      ! Read remaining parameters
      if (present(MaxTol)) this%MaxTol = MaxTol
      if (present(MaxIt))  this%MaxIt  = MaxIt

      ! Check against known solvers and get solver specific parameters
      select case (this%solver_name)
      case (HYPRE_SOL_IJ_AMG_NONE)
        this%interface = HYPRE_INT_IJ
      case (HYPRE_SOL_IJ_PCG_AMG)
        this%interface = HYPRE_INT_IJ
      case (HYPRE_SOL_IJ_PCG_DS)
        this%interface = HYPRE_INT_IJ
      case (HYPRE_SOL_S_PCG_NONE)
        this%interface = HYPRE_INT_Struct
      case (HYPRE_SOL_S_SMG_NONE)
        this%interface = HYPRE_INT_Struct
      case (HYPRE_SOL_S_PFMG_NONE)
        this%interface = HYPRE_INT_Struct
        if (present(RelaxType)) then
          ! 0: Jacobi
          ! 1: Weighted Jacobi (default)
          ! 2: Red/Black Gauss-Seidel (symmetric: RB pre-relaxation, BR post-relaxation)
          ! 3: Red/Black Gauss-Seidel (nonsymmetric: RB pre-relaxation and post-relaxation)
          this%RelaxType = RelaxType
        end if
      case default
        call this%parallel%stop("HYPRE: Unknown solver")
      end select

      return
    end subroutine hypre_obj_SelectSolver
    subroutine hypre_obj_Setup(this)
      !> Setup the hypre objects in preparation for solves
      ! Note: Setting up HYPRE is an expensive operation
      ! so it's best to do this only once during a simulation
      ! as opposed to setting-up and destorying each time-step.
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      integer :: i,j,k
      real(wp):: dxi,dxmi(2)
      real(wp):: dyi,dymi(2)
      real(wp):: dzi,dzmi(2)

      if (.not.allocated(this%mat)) then
        ! Use default operator: 2nd order Laplacian
        ! Set stencil extent
        this%stm = 1

        ! Set matrix coefficients
        associate (x=>this%block%x, y=>this%block%y, z=>this%block%z,   &
          xm=>this%block%xm, ym=>this%block%ym, zm=>this%block%zm,      &
          lo=>this%block%lo, hi =>this%block%hi)
          allocate(this%mat(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3),3,-this%stm:this%stm))

          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                dxi     = 1.0_wp/(x(i+1)-x(i))
                dxmi(1) = 1.0_wp/(xm(i) -xm(i-1))
                dxmi(2) = 1.0_wp/(xm(i+1) -xm(i))

                dyi     = 1.0_wp/(y(j+1)-y(j))
                dymi(1) = 1.0_wp/(ym(j) -ym(j-1))
                dymi(2) = 1.0_wp/(ym(j+1) -ym(j))

                dzi     = 1.0_wp/(z(k+1)-z(k))
                dzmi(1) = 1.0_wp/(zm(k) -zm(k-1))
                dzmi(2) = 1.0_wp/(zm(k+1) -zm(k))
                ! Second derivative with respect to x1 (d2dx1): lower/diagonal/upper
                this%mat(i,j,k,1,:) = [dxi*dxmi(1),dxi*(-dxmi(1))-dxi*dxmi(2),dxi*dxmi(2)]
                ! Second derivative with respect to x2 (d2dx2): lower/diagonal/upper
                this%mat(i,j,k,2,:) = [dyi*dymi(1),dyi*(-dymi(1))-dyi*dymi(2),dyi*dymi(2)]
                ! Second derivative with respect to x3 (d2dx3): lower/diagonal/upper
                this%mat(i,j,k,3,:) = [dzi*dzmi(1),dzi*(-dzmi(1))-dzi*dzmi(2),dzi*dzmi(2)]
              end do
            end do
          end do
        end associate
      end if

      select case (this%interface)
      case (HYPRE_INT_Struct)
        ! Initialize the hypre components
        call hypre_obj_SetupGridS(this)
        call hypre_obj_SetupStencilS(this)
        call hypre_obj_SetupRHSS(this)
        call hypre_obj_SetupSolS(this)
        call hypre_obj_SetupMatrixS(this)

        ! Now setup the sovler
        call hypre_obj_SetupSolverS(this)
      case (HYPRE_INT_SStruct)
        ! Not yet implemented
      case (HYPRE_INT_IJ)
        call hypre_obj_SetupPointersIJ(this)
        call hypre_obj_SetupRowsIJ(this)
        call hypre_obj_SetupMatrixIJ(this)
        call hypre_obj_SetupRHSIJ(this)
        call hypre_obj_SetupSolIJ(this)

        ! Now setup the sovler
        call hypre_obj_SetupSolIJver(this)
      case default
        call this%parallel%stop("Unknown or uninitialized HYPRE interface")
      end select

      ! Clear internal matrix
      deallocate(this%mat)

      return
    end subroutine hypre_obj_Setup
    subroutine hypre_obj_SetupPointersIJ(this)
      use leapCuda
      use, intrinsic :: iso_c_binding, only: c_f_pointer
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      integer :: stp
      integer :: nval
      integer :: Ng(3)
#ifdef USE_GPU
      integer :: ierr
      integer :: stat
#endif

      ! Determine the dimensionality of the problem
      associate (lo=>this%block%lo, hi =>this%block%hi)

        ! Get number of grid points in each direction
        call this%parallel%max(hi(1),Ng(1))
        call this%parallel%max(Ng(1)-lo(1)+1,Ng(1))
        call this%parallel%max(hi(2),Ng(2))
        call this%parallel%max(Ng(2)-lo(2)+1,Ng(2))
        call this%parallel%max(hi(3),Ng(3))
        call this%parallel%max(Ng(3)-lo(3)+1,Ng(3))
      end associate

      ! Determine stencil size
      stp = 1                                ! Diangonal entry
      if (Ng(1).gt.1) stp = stp + 2*this%stm ! upper/lower diagonal entries in x1-dir
      if (Ng(2).gt.1) stp = stp + 2*this%stm ! upper/lower diagonal entries in x2-dir
      if (Ng(3).gt.1) stp = stp + 2*this%stm ! upper/lower diagonal entries in x3-dir

      nval = (this%block%hi(3)-this%block%lo(3)+1) &
           * (this%block%hi(2)-this%block%lo(2)+1) &
           * (this%block%hi(1)-this%block%lo(1)+1)

#ifdef USE_GPU
      ! Allocate data on GPU's Unified Memory
      stat = device_malloc_managed(int(nval  * wp, leapI8), this%p_rhs_values)
      stat = device_malloc_managed(int(nval  * wp, leapI8), this%p_sol_values)
      stat = device_malloc_managed(int(nval  * 4,  leapI8), this%p_rows)
      stat = device_malloc_managed(int(stp   * wp, leapI8), this%p_values)
      stat = device_malloc_managed(int(stp   * 4,  leapI8), this%p_cols)
      stat = device_malloc_managed(int(2     * 4,  leapI8), this%p_tmpi)

      ! Assigns the target of the C pointes to the Fortran pointers
      ! and specifies the shape.
      call c_f_pointer(this%p_rhs_values, this%rhs_values, [nval])
      call c_f_pointer(this%p_sol_values, this%sol_values, [nval])
      call c_f_pointer(this%p_rows,       this%rows,       [nval])
      call c_f_pointer(this%p_values,     this%values,     [stp])
      call c_f_pointer(this%p_cols,       this%cols,       [stp])
      call c_f_pointer(this%p_tmpi,       this%tmpi,       [2])

#else
      allocate(this%rhs_values(nval))
      allocate(this%sol_values(nval))
      allocate(this%values(stp))
      allocate(this%rows(nval))
      allocate(this%cols(stp))
      allocate(this%tmpi(2))
#endif

#ifdef USE_GPU
      ! Instruct HYPRE to run on GPU
      call HYPRE_SetMemoryLocation(HYPRE_MEMORY_DEVICE, ierr)
      call HYPRE_SetExecutionPolicy(HYPRE_EXEC_DEVICE, ierr)

      ! Use HYPRE's SpGEMM instead of vendor implementation
      call HYPRE_SetSpGemmUseVendor(0, ierr)
#endif
    end subroutine hypre_obj_SetupPointersIJ
    subroutine hypre_obj_SetupRowsIJ(this)
      !> Setup row indexing used with IJ interface
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer,allocatable :: ncell(:)
      integer,allocatable :: buffi(:)
      integer :: i,j,k
      integer :: nn

      associate(nproc=>this%parallel%nproc, rank=>this%parallel%rank, &
        parallel=>this%parallel, block=>this%block )

        allocate(buffi(nproc))
        allocate(ncell(nproc))

        ncell = 0
        ncell(rank%mine) = (block%hi(3)-block%lo(3)+1) &
                         * (block%hi(2)-block%lo(2)+1) &
                         * (block%hi(1)-block%lo(1)+1)

        call parallel%sum(ncell,buffi); ncell=buffi

        this%irow_lo = 0
        do i=1,rank%mine - 1
          this%irow_lo = this%irow_lo+ ncell(i)
        end do

        ! Initialize row indicies: wall = -1
        call this%irow%Initialize('irow',block,parallel,0)
        this%irow%cell=-1

        ! Update indices and update ghostcells
        nn = this%irow_lo
        do k=block%lo(3),block%hi(3)
          do j=block%lo(2),block%hi(2)
            do i=block%lo(1),block%hi(1)
              this%irow%cell(i,j,k) = nn
              nn = nn + 1
            end do
          end do
        end do
        this%irow_hi=  nn - 1
        call this%irow%UpdateGhostCells

        deallocate(ncell,buffi)
      end associate
      return
    end subroutine hypre_obj_SetupRowsIJ
    subroutine hypre_obj_SetupMatrixIJ(this)
      !> Setup matrix with IJ interface
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr

      ! Create an empty matrix object
      call HYPRE_IJMatrixCreate(this%comm, this%irow_lo,this%irow_hi, this%irow_lo,this%irow_hi,this%matrix,ierr)

      ! Set storage type: only HYPRE_PARCSR supported per HYPRE doc
      call HYPRE_IJMatrixSetObjectType(this%matrix,HYPRE_PARCSR,ierr)

      ! Initialize before setting coefficients
      call HYPRE_IJMatrixInitialize(this%matrix,ierr)

      ! Set matrix coefficients
      call hypre_obj_BuildMatrixIJ(this)

      ! Assemble matrix
      call HYPRE_IJMatrixAssemble(this%matrix, ierr)

      ! Get parcsr matrix object
      call HYPRE_IJMatrixGetObject(this%matrix,this%par_matrix,ierr)
      return
    end subroutine hypre_obj_SetupMatrixIJ
    subroutine hypre_obj_BuildMatrixIJ(this)
      !> Set the coefficients of the matrix
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer  :: ierr
      integer  :: n,i,j,k
      real(wp) :: coef
      integer  :: Ng(3)
      integer  :: st

      associate (x=>this%block%x, y=>this%block%y, z=>this%block%z,   &
        xm=>this%block%xm, ym=>this%block%ym, zm=>this%block%zm,      &
        dxm=>this%block%dxm, dym=>this%block%dym, dzm=>this%block%dzm,&
        lo=>this%block%lo, hi =>this%block%hi, irow=>this%irow)

        ! Get number of grid points in each direction
        call this%parallel%max(hi(1),Ng(1))
        call this%parallel%max(Ng(1)-lo(1)+1,Ng(1))
        call this%parallel%max(hi(2),Ng(2))
        call this%parallel%max(Ng(2)-lo(2)+1,Ng(2))
        call this%parallel%max(hi(3),Ng(3))
        call this%parallel%max(Ng(3)-lo(3)+1,Ng(3))

        ! Setup the values of the matrix
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)

              ! Set column ID and operator value
              n = 0

              ! Set off-diagonal coefficients in the x3-dir
              if (Ng(3).gt.1) then
                do st=-this%stm,this%stm
                  ! Treat diagonals seperately
                  if (st.eq.0) cycle
                  if (irow%cell(i,j,k+st).ne.-1) then
                    n = n+1
                    coef = this%mat(i,j,k,3,st)
                    this%cols(n) = irow%cell(i,j,k+st); this%values(n) = coef*this%h2
                  end if
                end do
              end if
              ! Set off-diagonal coefficients in the x2-dir
              if (Ng(2).gt.1) then
                do st=-this%stm,this%stm
                  ! Treat diagonals seperately
                  if (st.eq.0) cycle
                  if (irow%cell(i,j+st,k).ne.-1) then
                    n = n+1
                    coef = this%mat(i,j,k,2,st)
                    this%cols(n) = irow%cell(i,j+st,k); this%values(n) = coef*this%h2
                  end if
                end do
              end if
              ! Set off-diagonal coefficients in the x1-dir
              if (Ng(1).gt.1) then
                do st=-this%stm,this%stm
                  ! Treat diagonals seperately
                  if (st.eq.0) cycle
                  if (irow%cell(i+st,j,k).ne.-1) then
                    n = n+1
                    coef = this%mat(i,j,k,1,st)
                    this%cols(n) = irow%cell(i+st,j,k); this%values(n) = coef*this%h2
                  end if
                end do
              end if

              ! - Diagonal
              n = n+1
              coef = 0.0_wp
              if (Ng(1).gt.1) coef = coef + this%mat(i,j,k,1,0)
              if (Ng(2).gt.1) coef = coef + this%mat(i,j,k,2,0)
              if (Ng(3).gt.1) coef = coef + this%mat(i,j,k,3,0)
              this%cols(n) = irow%cell(i,j,k)  ; this%values(n) = coef*this%h2

              ! number of columns (stencil size) and ID of row to set
              this%tmpi(1) = n
              this%tmpi(2) = irow%cell(i,j,k)

              ! Set values one row at a time
              call HYPRE_IJMatrixAddToValues(this%matrix,1,this%tmpi(1),this%tmpi(2),this%cols(1:n),this%values(1:n),ierr)
            end do
          end do
        end do
      end associate

      return
    end subroutine hypre_obj_BuildMatrixIJ
    subroutine hypre_obj_SetupRHSIJ(this)
      !> Setup the rhs vector
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr
      integer :: n,i,j,k

      call HYPRE_IJVectorCreate(this%comm,this%irow_lo,this%irow_hi, this%rhs , ierr)
      call HYPRE_IJVectorSetObjectType(this%rhs, HYPRE_PARCSR, ierr)
      call HYPRE_IJVectorInitialize(this%rhs,ierr)

      associate(lo=>this%block%lo, hi=>this%block%hi, irow=>this%irow, &
          irow_hi=>this%irow_hi,irow_lo =>this%irow_lo)
        n = 0
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              n = n + 1
              this%rhs_values(n) = 0.0_wp
              this%rows(n)       = irow%cell(i,j,k)
            end do
          end do
        end do
        call HYPRE_IJVectorSetValues(this%rhs, irow_hi-irow_lo+1,this%rows,this%rhs_values,  ierr)
      end associate

      call HYPRE_IJVectorAssemble(this%rhs, ierr)
      call HYPRE_IJVectorGetObject(this%rhs, this%par_rhs, ierr)

      return
    end subroutine hypre_obj_SetupRHSIJ
    subroutine hypre_obj_SetupSolIJ(this)
      !> Setup the solution vector, and initialize it to zero
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr
      integer :: n,i,j,k

      call HYPRE_IJVectorCreate(this%comm,this%irow_lo,this%irow_hi, this%sol , ierr)
      call HYPRE_IJVectorSetObjectType(this%sol, HYPRE_PARCSR, ierr)
      call HYPRE_IJVectorInitialize(this%sol,ierr)

      associate(lo=>this%block%lo, hi=>this%block%hi, irow=>this%irow, &
          irow_hi=>this%irow_hi,irow_lo =>this%irow_lo)
        n = 0
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              n = n + 1
              this%sol_values(n) = 0.0_wp
              this%rows(n)       = irow%cell(i,j,k)
            end do
          end do
        end do
        call HYPRE_IJVectorSetValues(this%sol, irow_hi-irow_lo+1,this%rows,this%sol_values,  ierr)
      end associate

      call HYPRE_IJVectorAssemble(this%sol, ierr)
      call HYPRE_IJVectorGetObject(this%sol, this%par_sol, ierr)

      return
    end subroutine hypre_obj_SetupSolIJ
    subroutine hypre_obj_SetRHSIJ(this,rhs)
      !> Set the entries of the rhs vector, one element at a time
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(in)   :: rhs             !! Right hand side
      ! Work variables
      integer :: n,i,j,k
      integer :: ierr

      associate(lo=>this%block%lo, hi=>this%block%hi, irow=>this%irow, &
          irow_hi=>this%irow_hi,irow_lo =>this%irow_lo)
        n = 0
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              n = n + 1
              this%rhs_values(n) = rhs%cell(i,j,k)*this%h2
              this%rows(n)       = irow%cell(i,j,k)
            end do
          end do
        end do
        call HYPRE_IJVectorSetValues(this%rhs, irow_hi-irow_lo+1,this%rows,this%rhs_values,  ierr)
      end associate

      call HYPRE_IJVectorAssemble(this%rhs, ierr)

      return
    end subroutine hypre_obj_SetRHSIJ
    subroutine hypre_obj_SetupSolIJver(this)
      !> Setup solver with IJ interface
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery

      ! Work variables
      integer :: ierr
      integer :: precond_id

      select case (this%solver_name)
      case (HYPRE_SOL_IJ_AMG_NONE)
        ! Using Boomer AMG (no preconditioning)
        call HYPRE_BoomerAMGCreate           (this%solver,             ierr)
        call HYPRE_BoomerAMGSetPrintLevel    (this%solver, 0,          ierr)

#ifdef USE_GPU
        ! Parameters optimized for GPU
        call HYPRE_BoomerAMGSetRelaxType    (this%solver, 11,          ierr)
        call HYPRE_BoomerAMGSetRelaxOrder   (this%solver, 0,           ierr)
        call HYPRE_BoomerAMGSetCoarsenType  (this%solver, 8,           ierr)
        call HYPRE_BoomerAMGSetInterpType   (this%solver, 18,          ierr)
        call HYPRE_BoomerAMGSetAggNumLevels (this%solver, 0,           ierr)
        call HYPRE_BoomerAMGSetAggInterpType(this%solver, 5,           ierr)
        call HYPRE_BoomerAMGSetRAP2         (this%solver, 0,           ierr)
#else
        call HYPRE_BoomerAMGSetOldDefault   (this%solver,              ierr)
        call HYPRE_BoomerAMGSetRelaxType    (this%solver, 3,           ierr)
        call HYPRE_BoomerAMGSetRelaxOrder   (this%solver, 1,           ierr)
        call HYPRE_BoomerAMGSetCoarsenType  (this%solver, 6,           ierr)
        call HYPRE_BoomerAMGSetInterpType   (this%solver, 0,           ierr)
#endif
        call HYPRE_BoomerAMGSetNumSweeps    (this%solver, 1,           ierr)
        call HYPRE_BoomerAMGSetMaxLevels    (this%solver, 25,          ierr)
        call HYPRE_BoomerAMGSetMaxIter      (this%solver, this%MaxIt,  ierr)
        call HYPRE_BoomerAMGSetTol          (this%solver, this%MaxTol, ierr)
        call HYPRE_BoomerAMGSetKeepTransp   (this%solver, 1,           ierr)

        call HYPRE_BoomerAMGSetup(this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
      case (HYPRE_SOL_IJ_PCG_AMG)
        ! Using PCG with AMG preconditioning
        call HYPRE_ParCSRPCGCreate          (this%comm,   this%solver, ierr)
        call HYPRE_ParCSRPCGSetTol          (this%solver, this%MaxTol, ierr)
        call HYPRE_ParCSRPCGSetMaxIter      (this%solver, this%MaxIt,  ierr)
        call HYPRE_ParCSRPCGSetTwoNorm      (this%solver, 1,           ierr)
        call HYPRE_ParCSRPCGSetLogging      (this%solver, 1,           ierr)

        call HYPRE_BoomerAMGCreate          (this%precond,             ierr)
        call HYPRE_BoomerAMGSetPrintLevel   (this%precond, 1,          ierr)
        call HYPRE_BoomerAMGSetCoarsenType  (this%precond, 8,          ierr)
        call HYPRE_BoomerAMGSetInterpType   (this%precond, 3,          ierr)
        call HYPRE_BoomerAMGSetRelaxType    (this%precond, 6,          ierr)
        call HYPRE_BoomerAMGSetNumSweeps    (this%precond, 1,          ierr)
        call HYPRE_BoomerAMGSetMaxIter      (this%precond, 1,          ierr)
        call HYPRE_BoomerAMGSetTol          (this%precond, 0.0_wp,     ierr)

        precond_id=2
        call HYPRE_ParCSRPCGSetPrecond(this%solver, precond_id, this%precond, ierr)

        call HYPRE_ParCSRPCGSetup(this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
      case (HYPRE_SOL_IJ_PCG_DS)
        ! Using PCG with DS preconditioning
        call HYPRE_ParCSRPCGCreate           (this%comm,   this%solver,ierr)
        call HYPRE_ParCSRPCGSetTol           (this%solver, this%MaxTol,ierr)
        call HYPRE_ParCSRPCGSetMaxIter       (this%solver, this%MaxIt, ierr)
        call HYPRE_ParCSRPCGSetTwoNorm       (this%solver, 1,          ierr)
        !call HYPRE_ParCSRPCGSetPrintLevel(this%solver, 2, ierr)
        call HYPRE_ParCSRPCGSetLogging       (this%solver, 1,          ierr)

        precond_id=1
        call HYPRE_ParCSRPCGSetPrecond(this%solver, precond_id, this%precond, ierr)

        call HYPRE_ParCSRPCGSetup(this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
      case default
        call this%parallel%stop("Unknown HYPRE solver")
      end select

      return
    end subroutine hypre_obj_SetupSolIJver
    subroutine hypre_obj_SetupStencilS(this)
      !> Setup the discretization stencil
      ! Each entry represents the relative offset (in index space)
      ! E.g.: a 2D 5-pt stencil would have the following geometry
      !  -- Offset { {0,0}, {-1,0}, {1,0}, {0,-1}, {0,1} }
      ! E.g.: a 3D 7-pt stencil would have the following geometry
      !  -- Offset { {0,0,0}, {-1,0,0}, {1,0,0}, {0,-1,0}, {0,1,0}, {0,0,-1}, {0,0,1} }
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer, allocatable :: offset(:,:)
      integer, allocatable :: offset_(:)
      integer :: n
      integer :: ierr

      ! Determine stencil size
      this%st_size= 1+2*this%dim*this%st

      ! Create an empty stencil object
      call HYPRE_StructStencilCreate(this%dim,this%st_size,this%stencil,ierr)

      ! Set the stencil entries
      allocate(offset(this%st_size,this%dim))
      allocate(offset_(this%dim))

      ! Offsets of a 7-point stencil
      select case (this%dim)
      case (2)
        offset(1,:)=[ 0, 0]
        offset(2,:)=[-1, 0]
        offset(3,:)=[ 1, 0]
        offset(4,:)=[ 0,-1]
        offset(5,:)=[ 0, 1]
      case (3)
        offset(1,:)=[ 0, 0, 0]
        offset(2,:)=[-1, 0, 0]
        offset(3,:)=[ 1, 0, 0]
        offset(4,:)=[ 0,-1, 0]
        offset(5,:)=[ 0, 1, 0]
        offset(6,:)=[ 0, 0,-1]
        offset(7,:)=[ 0, 0, 1]
      end select

      ! Assign stencil entries
      do n=1,this%st_size
        offset_=offset(n,1:this%dim)
        call HYPRE_StructStencilSetElement(this%stencil,n-1,offset_,ierr)
      end do
      deallocate(offset,offset_)
      return
    end subroutine hypre_obj_SetupStencilS
    subroutine hypre_obj_SetupGridS(this)
      !> Setup the hypre grid
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: periodicity(3)
      integer :: ierr

      ! Create a 3D grid object
      call HYPRE_StructGridCreate(this%comm,this%dim,this%grid, ierr)

      ! Set Hypre grid extents (only one block per mpi rank)
      call HYPRE_StructGridSetExtents(this%grid,this%block%lo(1:this%dim),this%block%hi(1:this%dim),ierr)

      ! Assume full periodicity (periodicity=[nx,ny,nz])
      periodicity = 0
      if (this%block%periods(1).eqv. .true.) call this%parallel%max(this%block%hi(1),periodicity(1))
      if (this%block%periods(2).eqv. .true.) call this%parallel%max(this%block%hi(2),periodicity(2))
      if (this%block%periods(3).eqv. .true.) call this%parallel%max(this%block%hi(3),periodicity(3))
      call HYPRE_StructGridSetPeriodic(this%grid,periodicity(1:this%dim),ierr)

      ! Assemble the grid
      call HYPRE_StructGridAssemble(this%grid,ierr)
      return
    end subroutine hypre_obj_SetupGridS
    subroutine hypre_obj_SetupRHSS(this)
      !> Setup the rhs vector
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr
      ! Create empty vector
      call HYPRE_StructVectorCreate(this%comm,this%grid,this%rhs,ierr)
      ! Indicate vector coefficients are ready to set
      call HYPRE_StructVectorInitialize(this%rhs,ierr)
      ! Assemble for future use
      call HYPRE_StructVectorAssemble(this%rhs,ierr)
      !call HYPRE_StructVectorPrint('vec',this%rhs,ierr)
      return
    end subroutine hypre_obj_SetupRHSS
    subroutine hypre_obj_SetupSolS(this)
      !> Setup the solution vector, and initialize it to zero
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      real(wp), allocatable :: values(:)
      integer :: nval
      integer :: ierr
      ! Create empty vector
      call HYPRE_StructVectorCreate(this%comm,this%grid,this%sol,ierr)
      ! Indicate vector coefficients are ready to set
      call HYPRE_StructVectorInitialize(this%sol,ierr)

      ! Initial guess is the zero vector (maybe changed later)
      nval=(this%block%hi(3)-this%block%lo(3)+1) &
          *(this%block%hi(2)-this%block%lo(2)+1) &
          *(this%block%hi(1)-this%block%lo(1)+1)

      allocate(values(nval))
      values=0.0_wp

      call HYPRE_StructVectorSetBoxValues(this%sol,this%block%lo(1:this%dim), &
           this%block%hi(1:this%dim),values(1:nval),ierr)

      deallocate(values)

      ! Assemble for future use
      call HYPRE_StructVectorAssemble(this%sol,ierr)
      return
    end subroutine hypre_obj_SetupSolS
    subroutine hypre_obj_SetupMatrixS(this)
      !> Setup and build the matrix
      !
      ! Currently, assuming that we are solving
      !     laplacian( sol ) = rhs
      ! with periodic boundary conditions.
      ! This will be generalized at a later time to include
      ! non-periodic BC, and other differential equations.
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr
      ! Create an empty matrix object
      call HYPRE_StructMatrixCreate(this%comm,this%grid,this%stencil,this%matrix,ierr)
      ! initialize matrix (indicates the matrix coefficients are ready to be set)
      call HYPRE_StructMatrixInitialize(this%matrix,ierr)
      ! Now build the matrix of coefficients
      call hypre_obj_BuildMatrixS(this)
      !! Assemble matrix
      call HYPRE_StructMatrixAssemble(this%matrix,ierr)
      !call HYPRE_StructMatrixPrint("mat",this%matrix,ierr)
      !call hypre_obj_PrintMatrixS(this)
      return
    end subroutine hypre_obj_SetupMatrixS
    subroutine hypre_obj_BuildMatrixS(this)
      !> Define the entries of the matrix Ax=b one row at a time
      !  Finite difference/Finite volume 2nd order Laplacian:
      ! ddu/dxdx = -2 u(i,j,k)/dxdx + u(i-1,j,k)/dxdx + u(i+1,j,k)/dxdx
      ! ddu/dydy = -2 u(i,j,k)/dydy + u(i,j-1,k)/dydy + u(i,j+1,k)/dydy
      ! ddu/dzdz = -2 u(i,j,k)/dzdz + u(i,j,k-1)/dzdz + u(i,j,k+1)/dzdz
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      real(wp):: values(this%st_size)
      integer :: sindices(this%st_size)
      integer :: i,j,k
      integer :: ierr

      if (this%st.ne.1) call this%parallel%stop("Hypre: the build matrix routine requires st=1")

      ! Stencil indicies
      ! must match the indices used setting the HYPRE stencil elements
      do i=1,this%st_size
        sindices(i) = i-1
      end do

      ! Set values for all interior grid points
      associate (x=>this%block%x, y=>this%block%y, z=>this%block%z)
      ! Loop over cell following Hypre convention (from bottom left, to top right)
      do k=this%block%lo(3),this%block%hi(3)
        do j=this%block%lo(2),this%block%hi(2)
          do i=this%block%lo(1),this%block%hi(1)
            ! Center point (entry=0)
            values(1) = -2.0_wp/(x(i+1)-x(i))**2 &
                        -2.0_wp/(y(j+1)-y(j))**2 &
                        -2.0_wp/(z(k+1)-z(k))**2
            ! X-dir left point (entry = 1)
            values(2) = 1.0_wp/(x(i+1)-x(i))**2
            ! X-dir right point (entry = 2)
            values(3) = 1.0_wp/(x(i+1)-x(i))**2
            ! Y-dir left point (entry = 3)
            values(4) = 1.0_wp/(y(j+1)-y(j))**2
            ! Y-dir right point (entry = 4)
            values(5) = 1.0_wp/(y(j+1)-y(j))**2
            ! Z-dir left point (entry = 5)
            values(6) = 1.0_wp/(z(k+1)-z(k))**2
            ! Z-dir right point (entry = 6)
            values(7) = 1.0_wp/(z(k+1)-z(k))**2

            call HYPRE_StructMatrixSetValues(this%matrix,[i,j,k],this%st_size,&
              sindices(1:this%st_size),values(1:this%st_size),ierr)
          end do
        end do
      end do
      end associate
      return
    end subroutine hypre_obj_BuildMatrixS
!    subroutine hypre_obj_BuildMatrixS_box(this)
!      !> Define the entries of the matrix Ax=b by blocks
!      !  Finite difference/Finite volume 2nd order Laplacian:
!      ! ddu/dxdx = -2 u(i,j,k)/dxdx + u(i-1,j,k)/dxdx + u(i+1,j,k)/dxdx
!      ! ddu/dydy = -2 u(i,j,k)/dydy + u(i,j-1,k)/dydy + u(i,j+1,k)/dydy
!      ! ddu/dzdz = -2 u(i,j,k)/dzdz + u(i,j,k-1)/dzdz + u(i,j,k+1)/dzdz
!      implicit none
!      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
!      ! Work variables
!      real(wp), allocatable :: values(:)
!      integer :: sindices(this%st_size)
!      integer :: nval
!      integer :: i,j,k,m
!      integer :: ierr
!
!      if (this%st.ne.1) call this%parallel%stop("Hypre: the build matrix routine requires st=1")
!
!      ! Number of grid points
!      nval=(this%block%hi(3)-this%block%lo(3)+1) &
!          *(this%block%hi(2)-this%block%lo(2)+1) &
!          *(this%block%hi(1)-this%block%lo(1)+1)*this%st_size
!
!      ! Number of values
!      allocate(values(nval))
!
!      ! Stencil indicies
!      ! must match the indices used setting the HYPRE stencil elements
!      do i=1,this%st_size
!        sindices(i) = i-1
!      end do
!
!      ! Set values for all interior grid points
!      ! following Hypre v2.21.0 documentation
!      associate (x=>this%block%x, y=>this%block%y, z=>this%block%z)
!      m=1
!      ! Loop over cell following Hypre convention (from bottom left, to top right)
!      do k=this%block%lo(3),this%block%hi(3)
!        do j=this%block%lo(2),this%block%hi(2)
!          do i=this%block%lo(1),this%block%hi(1)
!            ! Center point (entry=0)
!            values(m) = -1.0_wp/((x(i)-x(i-1))*(x(i+1)-x(i)))-1.0_wp/((x(i+2)-x(i+1))*(x(i+1)-x(i))) &
!                        -1.0_wp/((y(j)-y(j-1))*(y(j+1)-y(j)))-1.0_wp/((y(j+2)-y(j+1))*(y(j+1)-y(j))) &
!                        -1.0_wp/((z(k)-z(k-1))*(z(k+1)-z(k)))-1.0_wp/((z(k+2)-z(k+1))*(z(k+1)-z(k)))
!            m=m+1
!            ! X-dir left point (entry = 1)
!            values(m) = 1.0_wp/((x(i  )-x(i-1))*(x(i+1)-x(i)))
!            m=m+1
!            ! X-dir right point (entry = 2)
!            values(m) = 1.0_wp/((x(i+2)-x(i+1))*(x(i+1)-x(i)))
!            m=m+1
!            ! Y-dir left point (entry = 3)
!            values(m) = 1.0_wp/((y(j  )-y(j-1))*(y(j+1)-y(j)))
!            m=m+1
!            ! Y-dir right point (entry = 4)
!            values(m) = 1.0_wp/((y(j+2)-y(j+1))*(y(j+1)-y(j)))
!            m=m+1
!            ! Z-dir left point (entry = 5)
!            values(m) = 1.0_wp/((z(k  )-z(k-1))*(z(k+1)-z(k)))
!            m=m+1
!            ! Z-dir right point (entry = 6)
!            values(m) = 1.0_wp/((z(k+2)-z(k+1))*(z(k+1)-z(k)))
!            m=m+1
!          end do
!        end do
!      end do
!      end associate
!
!      call HYPRE_StructMatrixSetBoxValues(this%matrix,this%block%lo(1:this%dim),&
!           this%block%hi(1:this%dim),this%st_size,sindices(1:this%st_size),values(1:nval),ierr)
!
!      deallocate(values)
!      return
!    end subroutine hypre_obj_BuildMatrixS_box
    subroutine hypre_obj_SetupSolverS(this)
      !> Setup the hypre solver
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      integer :: ierr

      select case (this%solver_name)
      case (HYPRE_SOL_S_PCG_NONE)
        ! Create
        call HYPRE_StructPCGCreate(this%comm,this%solver, ierr)
        ! Parameterize
        call HYPRE_StructPCGSetMaxIter(     this%solver, this%MaxIt,   ierr)
        call HYPRE_StructPCGSetTol(         this%solver, this%MaxTol,  ierr)
        call HYPRE_StructPCGSetPrintLevel(  this%solver, 1,            ierr)
        call HYPRE_StructPCGSetLogging(     this%solver, 1,            ierr)
        ! Setup
        call HYPRE_StructPCGSetup(this%solver,this%matrix,this%rhs,this%sol,ierr)

      case(HYPRE_SOL_S_SMG_NONE)
        ! Create
        call HYPRE_StructSMGCreate(this%comm,this%solver,   ierr)
        ! Parameterize
        call HYPRE_StructSMGSetMaxIter(     this%solver, this%MaxIt,   ierr)
        call HYPRE_StructSMGSetTol(         this%solver, this%MaxTol,  ierr)
        call HYPRE_StructSMGSetNumPreRelax( this%solver, 1,            ierr)
        call HYPRE_StructSMGSetNumPostRelax(this%solver, 1,            ierr)
        call HYPRE_StructSMGSetPrintLevel(  this%solver, 1,            ierr)
        call HYPRE_StructSMGSetLogging(     this%solver, 1,            ierr)
        ! Setup
        call HYPRE_StructSMGSetup(this%solver,this%matrix,this%rhs,this%sol,ierr)

      case(HYPRE_SOL_S_PFMG_NONE)
        ! Create
        call HYPRE_StructPFMGCreate(this%comm,this%solver , ierr)
        ! Parameterize
        call HYPRE_StructPFMGSetMaxIter(    this%solver, this%MaxIt,    ierr)
        call HYPRE_StructPFMGSetTol(        this%solver, this%MaxTol,   ierr)
        call HYPRE_StructPFMGSetRelaxType(  this%solver, this%RelaxType,ierr)
        call HYPRE_StructPFMGSetRAPType(    this%solver, 0,             ierr)
        call HYPRE_StructPFMGSetLogging(    this%solver, 1,             ierr)
        call HYPRE_StructPFMGSetPrintLevel( this%solver, 1,             ierr)
        ! Setup
        call HYPRE_StructPFMGSetup(this%solver,this%matrix,this%rhs,this%sol,ierr)
      case default
        call this%parallel%stop("Unknown HYPRE solver")
      end select

      return
    end subroutine hypre_obj_SetupSolverS
    subroutine hypre_obj_SetRHS(this,rhs)
      !> Set the entries of the rhs vector, one element at a time
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(in)   :: rhs             !! Right hand side
      select case (this%interface)
      case (HYPRE_INT_Struct)
        call hypre_obj_SetRHSS(this,rhs)
      case (HYPRE_INT_SStruct)
      case (HYPRE_INT_IJ)
        call hypre_obj_SetRHSIJ(this,rhs)
      case default
        call this%parallel%stop("Unknown or uninitialized HYPRE interface")
      end select
      return
    end subroutine
    subroutine hypre_obj_SetRHSS(this,rhs)
      !> Set the entries of the rhs vector, one element at a time
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(in)   :: rhs             !! Right hand side
      ! Work variables
      integer :: i,j,k
      integer :: ierr

      ! Loop over cell following Hypre convention (from bottom left, to top right)
      do k=this%block%lo(3),this%block%hi(3)
        do j=this%block%lo(2),this%block%hi(2)
          do i=this%block%lo(1),this%block%hi(1)
            call HYPRE_StructVectorSetValues(this%rhs,[i,j,k],rhs%cell(i,j,k),ierr)
          end do
        end do
      end do

      call HYPRE_StructVectorAssemble(this%rhs,ierr)
      return
    end subroutine hypre_obj_SetRHSS
!    subroutine hypre_obj_SetRHS_box(this,rhs)
!      !> Set the entries of the rhs vector, by blocks
!      implicit none
!      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
!      type(eulerian_obj_r),intent(in)   :: rhs             !! Right hand side
!      ! Work variables
!      integer :: i,j,k,m
!      integer :: nval
!      real(wp), allocatable :: values(:)
!      integer :: ierr
!
!      ! Number of grid points
!      nval=(this%block%hi(3)-this%block%lo(3)+1) &
!          *(this%block%hi(2)-this%block%lo(2)+1) &
!          *(this%block%hi(1)-this%block%lo(1)+1)
!
!      allocate (values(nval))
!
!      ! Loop over cell following Hypre convention (from bottom left, to top right)
!      m=1
!      do k=this%block%lo(3),this%block%hi(3)
!        do j=this%block%lo(2),this%block%hi(2)
!          do i=this%block%lo(1),this%block%hi(1)
!            values(m)=rhs%cell(i,j,k)
!            m=m+1
!          end do
!        end do
!      end do
!
!      call HYPRE_StructVectorSetBoxValues(this%rhs,this%block%lo(1:this%dim),&
!           this%block%hi(1:this%dim),values(1:nval),ierr)
!      call HYPRE_StructVectorAssemble(this%rhs,ierr)
!      return
!    end subroutine hypre_obj_SetRHS_box
    subroutine hypre_obj_Solve(this,sol)
      !> Solve the system Ax=b and return the solution
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(inout):: sol             !! Solution vector

      select case (this%interface)
      case (HYPRE_INT_Struct)
        call hypre_obj_SolveS(this,sol)
      case (HYPRE_INT_SStruct)
      case (HYPRE_INT_IJ)
        call hypre_obj_SolveIJ(this,sol)
      case default
        call this%parallel%stop("Unknown or uninitialized HYPRE interface")
      end select
      return
    end subroutine hypre_obj_Solve
    subroutine hypre_obj_SolveS(this,sol)
      !> Solve the system Ax=b and return the solution, Struct interface
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(inout):: sol             !! Solution vector
      ! Work variables
      integer :: ierr
      integer :: nval
      integer :: i,j,k,m
      real(wp), allocatable :: values(:)

      ! Number of grid points
      nval=(this%block%hi(3)-this%block%lo(3)+1) &
          *(this%block%hi(2)-this%block%lo(2)+1) &
          *(this%block%hi(1)-this%block%lo(1)+1)

      select case (this%solver_name)
      case (HYPRE_SOL_S_PCG_NONE)
        call HYPRE_StructPCGSolve(this%solver,this%matrix,this%rhs,this%sol, ierr)
        call HYPRE_StructPCGGetNumIterations(this%solver,this%it,ierr)
        call HYPRE_StructPCGGetFinalRelative(this%solver,this%rel,ierr)
      case (HYPRE_SOL_S_SMG_NONE)
        call HYPRE_StructSMGSolve(this%solver,this%matrix,this%rhs,this%sol, ierr)
        call HYPRE_StructSMGGetNumIterations(this%solver,this%it,ierr)
        call HYPRE_StructSMGGetFinalRelative(this%solver,this%rel,ierr)
      case (HYPRE_SOL_S_PFMG_NONE)
        call HYPRE_StructPFMGSolve(this%solver,this%matrix,this%rhs,this%sol, ierr)
        call HYPRE_StructPFMGGetNumIteration(this%solver,this%it,ierr)
        call HYPRE_StructPFMGGetFinalRelativ(this%solver,this%rel,ierr)
      case default
        call this%parallel%stop("HYPRE: Unknown solver")
      end select

      allocate(values(nval))

      ! Get the values
      call HYPRE_StructVectorGetBoxValues(this%sol,this%block%lo(1:this%dim),&
           this%block%hi(1:this%dim),values(1:nval),ierr)
      ! Loop over cell following Hypre convention (from bottom left, to top right)
      m=1
      do k=this%block%lo(3),this%block%hi(3)
        do j=this%block%lo(2),this%block%hi(2)
          do i=this%block%lo(1),this%block%hi(1)
            sol%cell(i,j,k)=values(m)
            m=m+1
          end do
        end do
      end do

      deallocate(values)
      return
    end subroutine hypre_obj_SolveS
    subroutine hypre_obj_SolveIJ(this,sol)
      !> Solve the system Ax=b and return the solution, IJ interface
      implicit none
      class(hypre_obj),    intent(inout):: this            !! Hypre machinery
      type(eulerian_obj_r),intent(inout):: sol             !! Solution vector
      ! Work variables
      integer :: ierr
      integer :: i,j,k,n

      select case (this%solver_name)
      case (HYPRE_SOL_IJ_AMG_NONE)
        call HYPRE_BoomerAMGSolve           (this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
        call HYPRE_BoomerAMGGetNumIterations(this%solver, this%it,  ierr)
        call HYPRE_BoomerAMGGetFinalReltvRes(this%solver, this%rel, ierr)
      case (HYPRE_SOL_IJ_PCG_AMG)
        call HYPRE_ParCSRPCGSolve           (this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
        call HYPRE_ParCSRPCGGetNumIterations(this%solver, this%it,  ierr)
        call HYPRE_ParCSRPCGGetFinalRelative(this%solver, this%rel, ierr)
      case (HYPRE_SOL_IJ_PCG_DS)
        call HYPRE_ParCSRPCGSolve           (this%solver, this%par_matrix, this%par_rhs, this%par_sol, ierr)
        call HYPRE_ParCSRPCGGetNumIterations(this%solver, this%it,  ierr)
        call HYPRE_ParCSRPCGGetFinalRelative(this%solver, this%rel, ierr)
      case default
        call this%parallel%stop("Unknown HYPRE solver")
      end select

      ! - Transfer solution
      associate(lo=>this%block%lo, hi=>this%block%hi, irow=>this%irow, &
        irow_hi=>this%irow_hi,irow_lo =>this%irow_lo)

        call HYPRE_IJVectorGetValues(this%sol, irow_hi-irow_lo+1,this%rows,this%sol_values, ierr)

        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              n = irow%cell(i,j,k) - irow_lo  + 1
              sol%cell(i,j,k) = this%sol_values(n)
            end do
          end do
        end do

        call sol%UpdateGhostCells
      end associate
      return
    end subroutine hypre_obj_SolveIJ
    subroutine hypre_obj_PrintMatrixS(this)
      !> Setup the hypre grid
      implicit none
      class(hypre_obj),  intent(inout):: this              !! Hypre machinery
      ! Work variables
      real(wp), allocatable :: values(:)
      integer :: sindices(this%st_size)
      integer :: i,j,k,nval,m
      integer :: ierr

      ! Number of grid points
      nval=(this%block%hi(3)-this%block%lo(3)+1) &
          *(this%block%hi(2)-this%block%lo(2)+1) &
          *(this%block%hi(1)-this%block%lo(1)+1)*this%st_size

      ! Number of values
      allocate(values(nval))

      ! Stencil indicies
      ! must match the indices used setting the HYPRE stencil elements
      do i=1,this%st_size
        sindices(i) = i-1
      end do

      call HYPRE_StructMatrixGetBoxValues(this%matrix,this%block%lo,this%block%hi,this%st_size,sindices,values(1:nval),ierr)
      ! Loop over cell following Hypre convention (from bottom left, to top right)
      m=1
      do k=this%block%lo(3),this%block%hi(3)
        do j=this%block%lo(2),this%block%hi(2)
          do i=this%block%lo(1),this%block%hi(1)
            write(*,fmt='( 3(a,i3),a,7f12.4)') '[',i,',',j,',',k,']', values(m:m+6)
            m=m+7
          end do
        end do
      end do
      deallocate(values)
      return
    end subroutine hypre_obj_PrintMatrixS
end module leaphypre
