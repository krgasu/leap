!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapParallel
  !>--------------------------------------------------------------------------
  ! Module: leapParallel
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Contains all MPI decomposition info and directives used in LEAP.
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use mpi_f08
  implicit none

  private
  public :: parallel_obj


  type :: patch
    !> Contains MPI ranks of current process
    ! and neighboring ones
    integer :: root=1                                      !! Rank of root
    integer :: mine=1                                      !! Rank of this process
    integer :: R(3)                                        !! Rank of right neighbor
    integer :: L(3)                                        !! Rank of left  neighbor
    integer :: dir(3)                                      !! Process cooridnate in each direction
  end type patch

  type :: communicators
    !> MPI communicators
    type(MPI_Comm) :: w                                    !! Default World communicator
    type(MPI_Comm) :: g                                    !! Grid communicator
  end type communicators

  type :: parallel_obj
    !> Utility to handle MPI communications
    integer :: nproc                                       !! Total number of ranks
    integer :: np(3)                                       !! Number of ranks in each direction
    integer :: npx                                         !! Number of ranks in the x direction
    integer :: npy                                         !! Number of ranks in the y direction
    integer :: npz                                         !! Number of ranks in the z direction
    type(patch)         :: rank                            !! Rank and grid decompostion info
    type(communicators) :: comm                            !! Communicators
    ! Elementary data types
    type(MPI_Datatype) :: REAL_SP
    type(MPI_Datatype) :: REAL_DP
    type(MPI_Datatype) :: REAL_WP
    type(MPI_Datatype) :: COMPLEX_SP
    type(MPI_Datatype) :: COMPLEX_DP
    type(MPI_Datatype) :: COMPLEX_WP
    type(MPI_Datatype) :: INTEGER
    type(MPI_Datatype) :: INT8
    type(MPI_Datatype) :: LOGICAL
    ! MPI Info for panfs
    character(len=str8) :: mpiiofs
    type(MPI_Info)      :: mpi_info
    contains
      procedure         :: Initialize      => parallel_obj_Init
      procedure, nopass :: Finalize        => parallel_obj_Final
      procedure         :: SetMPIFSHints   => parallel_obj_SetMPIFSHints
      procedure         :: Topology        => parallel_obj_Topology
      procedure, nopass :: Time            => parallel_obj_Time
      procedure         :: Stop            => parallel_obj_Stop
      !< Compute the maximum across all ranks
      generic           :: max => parallel_obj_max_int_0d, parallel_obj_max_int_1d, &
                                  parallel_obj_max_real_0d, parallel_obj_max_real_1d
      !< Compute the minimum across all ranks
      generic           :: min => parallel_obj_min_int_0d, parallel_obj_min_int_1d, &
                                  parallel_obj_min_real_0d, parallel_obj_min_real_1d
      !< Sum across all ranks
      generic           :: sum => parallel_obj_sum_int_0d, parallel_obj_sum_int_1d, &
                                  parallel_obj_sum_int_2d, parallel_obj_sum_int_3d, &
                                  parallel_obj_sum_real_0d,parallel_obj_sum_real_1d,&
                                  parallel_obj_sum_real_2d,parallel_obj_sum_real_3d
      !< Broadcast to all ranks
      generic           :: bcast=>parallel_obj_bcast_int_0d, parallel_obj_bcast_int_1d, &
                                  parallel_obj_bcast_int_2d, parallel_obj_bcast_int_3d, &
                                  parallel_obj_bcast_real_0d,parallel_obj_bcast_real_1d,&
                                  parallel_obj_bcast_real_2d,parallel_obj_bcast_real_3d,&
                                  parallel_obj_bcast_char
      procedure, private :: parallel_obj_max_int_0d
      procedure, private :: parallel_obj_max_real_0d
      procedure, private :: parallel_obj_max_int_1d
      procedure, private :: parallel_obj_max_real_1d
      procedure, private :: parallel_obj_min_int_0d
      procedure, private :: parallel_obj_min_real_0d
      procedure, private :: parallel_obj_min_int_1d
      procedure, private :: parallel_obj_min_real_1d
      procedure, private :: parallel_obj_sum_int_0d
      procedure, private :: parallel_obj_sum_int_1d
      procedure, private :: parallel_obj_sum_int_2d
      procedure, private :: parallel_obj_sum_int_3d
      procedure, private :: parallel_obj_sum_real_0d
      procedure, private :: parallel_obj_sum_real_1d
      procedure, private :: parallel_obj_sum_real_2d
      procedure, private :: parallel_obj_sum_real_3d
      procedure, private :: parallel_obj_bcast_int_0d
      procedure, private :: parallel_obj_bcast_int_1d
      procedure, private :: parallel_obj_bcast_int_2d
      procedure, private :: parallel_obj_bcast_int_3d
      procedure, private :: parallel_obj_bcast_real_0d
      procedure, private :: parallel_obj_bcast_real_1d
      procedure, private :: parallel_obj_bcast_real_2d
      procedure, private :: parallel_obj_bcast_real_3d
      procedure, private :: parallel_obj_bcast_char
  end type parallel_obj

contains

  subroutine parallel_obj_init(this)
    !> Initialize the parallel environement
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    ! Work variables
    logical :: ok
    integer :: ierr
    integer :: size_sp,size_dp

    ! Initialize a first basic MPI environment
    call MPI_INITIALIZED(ok,ierr)
    if (.not.ok) call MPI_INIT(ierr)
    ! Total number of ranks
    call MPI_COMM_SIZE(MPI_COMM_WORLD,this%nproc,ierr)
    ! Store the MPI_COMM_WORLD
    this%comm%w = MPI_COMM_WORLD

    ! MPI types
    this%REAL_SP   =MPI_REAL
    this%REAL_DP   =MPI_DOUBLE_PRECISION
    this%COMPLEX_SP=MPI_COMPLEX
    this%COMPLEX_DP=MPI_DOUBLE_COMPLEX
    this%INTEGER   =MPI_INTEGER
    this%INT8      =MPI_INTEGER8
    this%LOGICAL   =MPI_LOGICAL

    ! Working precision
    call MPI_TYPE_SIZE(MPI_REAL,            size_sp,ierr)
    call MPI_TYPE_SIZE(MPI_DOUBLE_PRECISION,size_dp,ierr)
    if (WP .eq. size_sp) then
      this%REAL_WP = MPI_REAL
    else if (WP .eq. size_dp) then
      this%REAL_WP = MPI_DOUBLE_PRECISION
    else
      call this%stop('Error in parallel_obj_init: no WP equivalent in MPI')
    end if
    call MPI_TYPE_SIZE(MPI_COMPLEX,size_sp,ierr)
    call MPI_TYPE_SIZE(MPI_DOUBLE_COMPLEX,size_dp,ierr)
    if (2*WP .eq. size_sp) then
      this%COMPLEX_WP = MPI_COMPLEX
    else if (2*WP .eq. size_dp) then
      this%COMPLEX_WP = MPI_DOUBLE_COMPLEX
    else
      call this%stop('Error in parallel_obj_init: no complex WP equivalent in MPI')
    end if

    ! Topology
    call MPI_COMM_RANK(this%comm%w,this%rank%mine,ierr)
    this%rank%mine = this%rank%mine+1
    this%rank%root = 1

    if (this%nproc.eq.1) this%comm%g=this%comm%w

    return
  end subroutine parallel_obj_init

  subroutine parallel_obj_SetMPIFSHints(this)
    !> Set the MPI file system hints
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    type(parser_obj) :: parser                             !! An input parser
    ! Work variables
    integer :: ierr

    ! Initialize parser
    call parser%initialize
    call parser%parsefile

    ! Create info
    call parser%get('MPIIO fs',this%mpiiofs,'ufs')
    select case(trim(this%mpiiofs))
    case('panfs')
      ! PanFS parallel file system
      this%mpiiofs = "panfs:"
      call MPI_INFO_CREATE(this%mpi_info,ierr)
      call MPI_INFO_SET(this%mpi_info,"panfs_concurrent_write","1",ierr)
    case('ufs')
      ! Unix file system
      this%mpiiofs = "ufs:"
      this%mpi_info = MPI_INFO_NULL
    case('lustre')
      ! Lustre file system
      this%mpiiofs = "lustre:"
      call MPI_INFO_CREATE(this%mpi_info,ierr)
      call MPI_INFO_SET(this%mpi_info,"romio_ds_write","disable",ierr)
    case('win')
      this%mpiiofs = ""
      this%mpi_info = MPI_INFO_NULL
    case('agave')
      ! Parameters for AGAVE cluster
      ! - set data sieving on read/write to automatic (disabled if handled by the filesystem)
      ! - Enable collective buffering on read/write
      this%mpiiofs = ""
      call MPI_INFO_CREATE(this%mpi_info,ierr)
      call MPI_INFO_SET(this%mpi_info,"romio_ds_write","automatic",ierr)
      call MPI_INFO_SET(this%mpi_info,"romio_ds_read", "automatic",ierr)
      call MPI_INFO_SET(this%mpi_info,"romio_cb_write","enable",   ierr)
      call MPI_INFO_SET(this%mpi_info,"romio_cb_read", "enable",   ierr)
    case default
      call this%stop('parallel_obj_topology: unknown mpiio fs')
    end select

     call parser%finalize
    return
  end subroutine parallel_obj_SetMPIFSHints

  subroutine parallel_obj_final
    !> Finalize MPI and the parallel environment
    implicit none
    ! Work variables
    integer :: ierr

    ! Finalize MPI
    call MPI_FINALIZE(ierr)

    return
  end subroutine parallel_obj_final

  subroutine parallel_obj_topology(this,is_periodic,Ng,Nb)
    !> Builds a Cartesian topolgy with MPI
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    logical,             intent(in)    :: is_periodic(3)   !! Periodicity
    integer,             intent(in)    :: Ng(3)            !! # Grid points
    integer,optional,    intent(in)    :: Nb(3)            !! Explicit block decomposition
    ! Work variables
    integer, dimension(3) :: dims
    integer, dimension(3) :: coords
    logical :: reorder
    integer :: ndims
    integer :: ierr

    ! Initialize the dims var
    dims  = 0
    ndims = 3
    if (present(Nb)) then
      this%np = Nb
      dims    = Nb
    else
      ! Enforce 1 directional node for 1D & 2D simulations
      if (Ng(1).eq.1) dims(1)=1
      if (Ng(2).eq.1) dims(2)=1
      if (Ng(3).eq.1) dims(3)=1
      call MPI_DIMS_CREATE(this%nproc,ndims,dims,ierr)
      this%np  = dims
    end if
    this%npx = this%np(1)
    this%npy = this%np(2)
    this%npz = this%np(3)

    ! Test if nproc is correct
    if (this%nproc .ne. this%npx*this%npy*this%npz) &
      call this%stop('Error setting up the parallel topology: mismatch in parallel decomposition')

    ! Set MPI topology
    reorder = .true.
    call MPI_CART_CREATE(this%comm%w,ndims,dims,is_periodic,reorder,this%comm%g,ierr)

    ! Return the cartesian coordinate of this rank
    call MPI_COMM_RANK(this%comm%g,this%rank%mine,ierr)
    call MPI_CART_COORDS(this%comm%g,this%rank%mine,ndims,coords,ierr)
    this%rank%mine   = this%rank%mine + 1
    this%rank%dir(1) = coords(1) + 1
    this%rank%dir(2) = coords(2) + 1
    this%rank%dir(3) = coords(3) + 1

    ! Define a root processor at coordinates (1,1,1)
    coords = 0
    call MPI_CART_RANK(this%comm%g,coords,this%rank%root,ierr)
    this%rank%root  = this%rank%root + 1

    ! Look up the ranks for the neighbors
    call MPI_CART_SHIFT(this%comm%g,0,1,this%rank%L(1),this%rank%R(1),ierr)
    this%rank%L(1)=this%rank%L(1)+1
    this%rank%R(1)=this%rank%R(1)+1

    call MPI_CART_SHIFT(this%comm%g,1, 1,this%rank%L(2),this%rank%R(2),ierr)
    this%rank%L(2)=this%rank%L(2)+1
    this%rank%R(2)=this%rank%R(2)+1

    call MPI_CART_SHIFT(this%comm%g,2, 1,this%rank%L(3),this%rank%R(3),ierr)
    this%rank%L(3)=this%rank%L(3)+1
    this%rank%R(3)=this%rank%R(3)+1
    return
  end subroutine parallel_obj_topology

  function parallel_obj_time() result(wtime)
    !> Returns the elapsed time since an arbitrary
    ! origin. Note that different ranks return
    ! different WTIMEs
    implicit none
    real(WP) :: wtime
    wtime = MPI_WTIME()
    return
  end function parallel_obj_time


  subroutine parallel_obj_stop(this,msg)
    !> Subroutine to gracefully stop the execution
    ! with an optional error message.
    use iso_fortran_env, only : stderr => error_unit
    implicit none
    class(parallel_obj),        intent(inout) :: this      !! Parallel object
    character(len=*), optional, intent(in)    :: msg       !! Error message
    ! Work variable
    integer :: ierr

    ! Specify who sends the abort signal and what it means
    if (present(msg)) then
       write(stderr,'(a,i0,2a)') 'LEAP: termination signal received on rank: ',this%rank%mine,'. Reason: ',trim(msg)
    else
       write(stderr,'(a,i0)'   ) 'LEAP: termination signal received on rank: ',this%rank%mine
    end if

    ! Call general abort
    call MPI_ABORT(this%comm%w,0,ierr)

    return
  end subroutine parallel_obj_stop

  subroutine parallel_obj_max_real_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,this%REAL_WP,MPI_MAX,this%comm%g,ierr)
    return
  end subroutine parallel_obj_max_real_0d

  subroutine parallel_obj_max_int_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, intent(in)  :: A
    integer, intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,MPI_INTEGER,MPI_MAX,this%comm%g,ierr)
    return
  end subroutine parallel_obj_max_int_0d

  subroutine parallel_obj_max_real_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(wp), dimension(:), intent(in)  :: A
    real(wp), dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),this%REAL_WP,MPI_MAX,this%comm%g,ierr)
    return
  end subroutine parallel_obj_max_real_1d

  subroutine parallel_obj_max_int_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:), intent(in)  :: A
    integer, dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),MPI_INTEGER,MPI_MAX,this%comm%g,ierr)
    return
  end subroutine parallel_obj_max_int_1d

  subroutine parallel_obj_min_real_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,this%REAL_WP,MPI_MIN,this%comm%g,ierr)
    return
  end subroutine parallel_obj_min_real_0d

  subroutine parallel_obj_min_int_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, intent(in)  :: A
    integer, intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,MPI_INTEGER,MPI_MIN,this%comm%g,ierr)
    return
  end subroutine parallel_obj_min_int_0d

  subroutine parallel_obj_min_real_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(wp), dimension(:), intent(in)  :: A
    real(wp), dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),this%REAL_WP,MPI_MIN,this%comm%g,ierr)
    return
  end subroutine parallel_obj_min_real_1d

  subroutine parallel_obj_min_int_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:), intent(in)  :: A
    integer, dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),MPI_INTEGER,MPI_MIN,this%comm%g,ierr)
    return
  end subroutine parallel_obj_min_int_1d

  subroutine parallel_obj_sum_real_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), intent(in)  :: A
    real(WP), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,this%REAL_WP,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_real_0d

  subroutine parallel_obj_sum_int_0d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, intent(in)  :: A
    integer, intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,1,MPI_INTEGER,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_int_0d

  subroutine parallel_obj_sum_int_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:), intent(in)  :: A
    integer, dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),MPI_INTEGER,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_int_1d

  subroutine parallel_obj_sum_int_2d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:,:), intent(in)  :: A
    integer, dimension(:,:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),MPI_INTEGER,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_int_2d

  subroutine parallel_obj_sum_int_3d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:,:,:), intent(in)  :: A
    integer, dimension(:,:,:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),MPI_INTEGER,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_int_3d

  subroutine parallel_obj_sum_real_1d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:), intent(in)  :: A
    real(WP), dimension(:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),this%REAL_WP,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_real_1d

  subroutine parallel_obj_sum_real_2d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:,:), intent(in)  :: A
    real(WP), dimension(:,:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),this%REAL_WP,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_real_2d

  subroutine parallel_obj_sum_real_3d(this,A,B)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:,:,:), intent(in)  :: A
    real(WP), dimension(:,:,:), intent(out) :: B
    ! Work variables
    integer :: ierr
    call MPI_ALLREDUCE(A,B,size(A),this%REAL_WP,MPI_SUM,this%comm%g,ierr)
    return
  end subroutine parallel_obj_sum_real_3d

  ! MPI_BCAST
  subroutine parallel_obj_bcast_char(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    character(len=*),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,len(A),MPI_CHARACTER,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_char

  subroutine parallel_obj_bcast_int_0d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer,intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,1,MPI_INTEGER,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_int_0d

  subroutine parallel_obj_bcast_int_1d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),MPI_INTEGER,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_int_1d

  subroutine parallel_obj_bcast_int_2d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:,:),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),MPI_INTEGER,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_int_2d

  subroutine parallel_obj_bcast_int_3d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    integer, dimension(:,:,:),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),MPI_INTEGER,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_int_3d

  subroutine parallel_obj_bcast_real_0d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,1,this%REAL_WP,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_real_0d

  subroutine parallel_obj_bcast_real_1d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),this%REAL_WP,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_real_1d

  subroutine parallel_obj_bcast_real_2d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:,:) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),this%REAL_WP,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_real_2d

  subroutine parallel_obj_bcast_real_3d(this,A)
    implicit none
    class(parallel_obj), intent(inout) :: this             !! Parallel object
    real(WP), dimension(:,:,:),intent(in) :: A
    ! Work variables
    integer :: ierr
    call MPI_BCAST(A,size(A),this%REAL_WP,this%rank%root-1,this%comm%g,ierr)
    return
  end subroutine parallel_obj_bcast_real_3d
end module leapParallel
