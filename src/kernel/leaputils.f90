!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapUtils
  !>--------------------------------------------------------------------------
  ! Module: leapUtils
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Utilities for LEAP.
  ! Currently implemented:
  !   - Subroutine that creates a directory
  !   - Singly-linked lists
  !   - Hash table with chaining
  ! --------------------------------------------------------------------------
  use leapKinds
  use mpi_f08
  implicit none

  private
  public :: leap_create_directory,sllist_obj,hashtbl_obj

  type :: sllist_obj
    !> Singly-linked list
    type(sllist_obj), pointer :: child => null()
    integer                   :: key   =  -1
    integer                   :: val   =  -1
    contains
      procedure :: Put  => sllist_obj_Put
      procedure :: Get  => sllist_obj_Get
      procedure :: Free => sllist_obj_Free
  end type sllist_obj

  type :: hashtbl_obj
    !> Hash table
    type(sllist_obj), allocatable :: vec(:)
    integer                       :: vec_len = 0
    contains
      procedure :: Initialize          => hashtbl_obj_Init
      procedure :: Finalize            => hashtbl_obj_Final
      procedure :: Put                 => hashtbl_obj_Put
      procedure :: Get                 => hashtbl_obj_Get
      procedure, nopass :: HashString  => hashtbl_obj_HashString
  end type hashtbl_obj
  contains
    subroutine leap_create_directory(dirname)
      use iso_fortran_env, only : stderr => error_unit
      character(len=*), intent(in) :: dirname
      ! Work variables
      integer :: fid
      integer :: ierr
      ! Attempt to create a dummy file
      open(newunit=fid,file=trim(adjustl(dirname))//'/test',action='write',status='replace',iostat=ierr)
      if (ierr.eq. 0) then
        ! Folder exists and is writable
        ! Delete test file
        close(fid, status='delete')
      else
        ! Create folder
        call system('mkdir -p '//trim(adjustl(dirname)))

        ! Test folder is writable
        open(newunit=fid,file=trim(adjustl(dirname))//'/test',action='write',status='replace',iostat=ierr)
        if (ierr.eq. 0) then
          ! Delete test file
          close(fid, status='delete')
        else
          write(stderr,*) 'Unable to create/write in directory: '//trim(adjustl(dirname))
          call MPI_ABORT(MPI_COMM_WORLD,0,ierr)
        end if
      end if
      return
    end subroutine leap_create_directory

    ! Singly-linked lists
    ! -------------------------- !
    pure recursive subroutine sllist_obj_Put(this,key,val)
      implicit none
      class(sllist_obj), intent(inout) :: this
      integer,           intent(in)    :: key, val
      if (this%key .ge. 0) then
        if (this%key .ne. key) then
          if ( .not. associated(this%child) ) allocate(this%child)
          call sllist_obj_Put(this%child,key,val)
        end if
      else
        this%key = key
        this%val = val
      end if
      return
    end subroutine sllist_obj_Put
    pure recursive subroutine sllist_obj_Get(this,key,val)
      implicit none
      class(sllist_obj), intent(in)    :: this
      integer,           intent(in)    :: key
      integer,           intent(out)   :: val
      if (this%key == key) then
        val = this%val
      else if(associated(this%child)) then ! keep going
        call sllist_obj_Get(this%child,key,val)
      else ! at the end of the list, no key found
        ! exit indication
        val = -1
        return
      end if
      return
    end subroutine sllist_obj_Get
    pure recursive subroutine sllist_obj_Free(this)
      implicit none
      class(sllist_obj), intent(inout) :: this
      if (associated(this%child)) then
        call sllist_obj_Free(this%child)
        deallocate(this%child)
      end if
      this%child => null()
      this%key = -1
      this%val = -1
      return
    end subroutine sllist_obj_Free
    ! Hash table with chaining
    ! -------------------------- !
    pure subroutine hashtbl_obj_init(this,tbl_len)
      implicit none
      class(hashtbl_obj),    intent(inout) :: this
      integer,               intent(in)    :: tbl_len
      if (allocated(this%vec)) deallocate(this%vec)
      allocate(this%vec(tbl_len))
      this%vec_len = tbl_len
      return
    end subroutine hashtbl_obj_init
    pure subroutine hashtbl_obj_final(this)
      implicit none
      class(hashtbl_obj), intent(inout) :: this
      integer     :: i
      if (allocated(this%vec)) then
        do concurrent (i=1:this%vec_len)
          call this%vec(i)%free()
        end do
        deallocate(this%vec)
      end if
      return
    end subroutine hashtbl_obj_final
    pure subroutine hashtbl_obj_Put(this,key,val)
      implicit none
      class(hashtbl_obj),  intent(inout) :: this
      integer,             intent(in)    :: key, val
      integer                            :: hash
      hash = mod(key,this%vec_len) + 1
      call this%vec(hash)%put(key=key,val=val)
      return
    end subroutine hashtbl_obj_Put
    pure subroutine hashtbl_obj_Get(this,key,val)
      implicit none
      class(hashtbl_obj),  intent(in)    :: this
      integer,             intent(in)    :: key
      integer,             intent(out)   :: val
      integer                            :: hash
      hash = mod(key,this%vec_len) + 1
      call this%vec(hash)%get(key=key,val=val)
      return
    end subroutine hashtbl_obj_Get
    pure function hashtbl_obj_HashString(str) result(val)
      character(len=*),    intent(in)    :: str
      integer                            :: val
      ! Work variable
      !integer :: tmp(len_trim(str))
      integer :: i

      val = 0
      do i=1,len_trim(str)
        val = mod(val*31 + ichar(str(i:i)),35386740)
      end do

      !do concurrent (i=1:len_trim(str))
      !  tmp(i) = ichar(str(i:i))*(11**i)
      !end do
      !val = sum(tmp)
      return
    end function hashtbl_obj_HashString
end module leapUtils
