!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapDiffOp
  !>--------------------------------------------------------------------------
  ! Module: leapDiffOp
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Module of differentional operators that apply on Eulerian objects
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapEulerian
  use leapBlock
  use leapParallel
  use leapBC
  implicit none

  private
  public :: op_obj

  type :: op_obj
    !> Utitlity with differential operators
    type(parallel_obj),pointer    :: parallel => null()    !! Parent parallel structure
    type(block_obj),   pointer    :: block    => null()    !! Parent block structure
    !> Boundary conditions
    type(eulerian_obj_i)          :: mask

    ! Morinishi basic interpolations and differentiation schemes
    ! E.g.:
    !  morinishi_int(:,k) : Interpolation on a stencil of size k*dx
    !  morinishi_ddx(:,k) : Differentiation on a stencil of size k*dx
    real(wp),allocatable         :: morinishi_int(:,:)     !! Morinishi's interpolations
    real(wp),allocatable         :: morinishi_ddx(:,:)     !! Morinishi's differntiations
    real(wp),allocatable         :: alpha(:)               !! Morinishi's coeff for high-order schemes
    ! Interpolation coefficients
    real(wp),allocatable         :: c_intrp1m(:,:)         !! Interpolate from x1m to x1
    real(wp),allocatable         :: c_intrp2m(:,:)         !! Interpolate from x2m to x2
    real(wp),allocatable         :: c_intrp3m(:,:)         !! Interpolate from x3m to x3
    real(wp),allocatable         :: c_intrp1 (:,:)         !! Interpolate from x1  to x1m
    real(wp),allocatable         :: c_intrp2 (:,:)         !! Interpolate from x2  to x2m
    real(wp),allocatable         :: c_intrp3 (:,:)         !! Interpolate from x3  to x3m
    ! First order derivatives
    real(wp),allocatable         :: c_d1dx1m(:,:)          !! Differentiate x1m-centered data in x1-dir, result is at x1
    real(wp),allocatable         :: c_d1dx2m(:,:)          !! Differentiate x2m-centered data in x2-dir, result is at x2
    real(wp),allocatable         :: c_d1dx3m(:,:)          !! Differentiate x3m-centered data in x3-dir, result is at x3
    real(wp),allocatable         :: c_d1dx1 (:,:)          !! Differentiate x1-centered  data in x1-dir, result is at x1m
    real(wp),allocatable         :: c_d1dx2 (:,:)          !! Differentiate x2-centered  data in x2-dir, result is at x2m
    real(wp),allocatable         :: c_d1dx3 (:,:)          !! Differentiate x3-centered  data in x3-dir, result is at x3m
    ! Stencil sizes
    integer, private             :: scheme_order = 2
    integer                      :: st = 1
    contains
    procedure :: Initialize => op_obj_Init
    procedure :: Finalize   => op_obj_Final
    procedure :: gradx      => op_obj_gradx
    procedure :: grady      => op_obj_grady
    procedure :: gradz      => op_obj_gradz
    procedure :: div        => op_obj_div
    procedure :: p_div      => op_obj_p_div
    ! Interpolations:
    procedure :: intrp1     => op_obj_intrp1
    procedure :: intrp2     => op_obj_intrp2
    procedure :: intrp3     => op_obj_intrp3
    ! Differentiation: d(phi)/dx_i
    procedure :: d1dx1      => op_obj_d1dx1
    procedure :: d1dx2      => op_obj_d1dx2
    procedure :: d1dx3      => op_obj_d1dx3
    ! Convective operator: div(Ui Uj)
    procedure :: conv11     => op_obj_conv11
    procedure :: conv21     => op_obj_conv21
    procedure :: conv31     => op_obj_conv31
    procedure :: conv12     => op_obj_conv12
    procedure :: conv22     => op_obj_conv22
    procedure :: conv32     => op_obj_conv32
    procedure :: conv13     => op_obj_conv13
    procedure :: conv23     => op_obj_conv23
    procedure :: conv33     => op_obj_conv33
    ! Build a laplacian operator with Morinishi's schemes
    procedure :: BuildLaplacian &
                            => op_obj_BuildLaplacian
    procedure :: ApplyLaplacianDC &
                            => op_obj_ApplyLaplacianDC
    procedure :: StrainRate => op_obj_StrainRate
    ! Other stuff
    procedure :: grad_x     => op_obj_grad_x
    procedure :: grad_y     => op_obj_grad_y
    procedure :: grad_z     => op_obj_grad_z
  end type op_obj

  contains
  subroutine op_obj_Init(this,block,parallel,order)
    implicit none
    class(op_obj),             intent(inout) :: this       !! Differential operators utility
    type(block_obj),      target, intent(in) :: block      !! A block object
    type(parallel_obj),   target, intent(in) :: parallel   !! parallel structure from main program
    integer,             optional,intent(in) :: Order      !! Order of interpolation/differentiation schemes
    ! Work variables
    integer :: i,j,k,n

    ! Point to the master objects
    this%parallel => parallel
    this%block    => block

    ! Initialize mask
    call this%mask%initialize('mask',this%block,this%parallel,0)
    this%mask%cell = 0

    associate (                                 &
      lo => this%block%lo, hi => this%block%hi, &
      xm => this%block%xm, x  => this%block%x,  &
      ym => this%block%ym, y  => this%block%y,  &
      zm => this%block%zm, z  => this%block%z,  &
      st=>this%st, scheme_order=>this%scheme_order)

      ! Set scheme order, if explicitely stated,
      ! or use default 2nd order
      if (present(order)) scheme_order = order

      ! Set stencil size
      st = scheme_order/2
      if (this%block%ngc.lt.2*st-1) call this%parallel%stop("Insufficient number of ghost cells for the requested scheme")

      ! Basic interpolation and differentiation
      allocate(this%morinishi_int(2*st, st)); this%morinishi_int = 0.0_wp
      allocate(this%morinishi_ddx(2*st, st)); this%morinishi_ddx = 0.0_wp
      allocate(this%alpha(st))

      ! Eq. 139 and 140
      ! Fourth order: Eq. 32 and 35 in paper
      ! Second order: Eq. 87
      do n=1,st
        this%morinishi_int(st - (n-1),n) =  0.5_wp
        this%morinishi_int(st +  n   ,n) =  0.5_wp
        this%morinishi_ddx(st - (n-1),n) = -1.0_wp/real(2*n-1,wp)
        this%morinishi_ddx(st +  n   ,n) =  1.0_wp/real(2*n-1,wp)
      end do

      ! Solutions to Eq. 12 in Desjardins et al, 2008.
      select case (scheme_order)
      case (2)
        this%alpha=[1.0_wp]
      case (4)
        this%alpha=[9.0_wp/8.0_wp, -1.0_wp/8.0_wp]
      case (6)
        this%alpha=[75.0_wp/64.0_wp, -25.0_wp/128.0_wp, 3.0_wp/128.0_wp]
      case default
        error stop "Only second, fourth, and sixth order schemes for now"
      end select

      ! Coefficients for differentiation and interpolation
      ! -----------------------------------------
      allocate(this%c_d1dx1  (-st+1:st,lo(1)-st:hi(1)+st-1)); this%c_d1dx1   = 0.0_wp
      allocate(this%c_d1dx2  (-st+1:st,lo(2)-st:hi(2)+st-1)); this%c_d1dx2   = 0.0_wp
      allocate(this%c_d1dx3  (-st+1:st,lo(3)-st:hi(3)+st-1)); this%c_d1dx3   = 0.0_wp
      allocate(this%c_intrp1 (-st+1:st,lo(1)-st:hi(1)+st-1)); this%c_intrp1  = 0.0_wp
      allocate(this%c_intrp2 (-st+1:st,lo(2)-st:hi(2)+st-1)); this%c_intrp2  = 0.0_wp
      allocate(this%c_intrp3 (-st+1:st,lo(3)-st:hi(3)+st-1)); this%c_intrp3  = 0.0_wp

      allocate(this%c_d1dx1m (-st:st-1,lo(1)-st+1:hi(1)+st)); this%c_d1dx1m  = 0.0_wp
      allocate(this%c_d1dx2m (-st:st-1,lo(2)-st+1:hi(2)+st)); this%c_d1dx2m  = 0.0_wp
      allocate(this%c_d1dx3m (-st:st-1,lo(3)-st+1:hi(3)+st)); this%c_d1dx3m  = 0.0_wp
      allocate(this%c_intrp1m(-st:st-1,lo(1)-st+1:hi(1)+st)); this%c_intrp1m = 0.0_wp
      allocate(this%c_intrp2m(-st:st-1,lo(2)-st+1:hi(2)+st)); this%c_intrp2m = 0.0_wp
      allocate(this%c_intrp3m(-st:st-1,lo(3)-st+1:hi(3)+st)); this%c_intrp3m = 0.0_wp

      ! 1-dir
      do i=lo(1)-st,hi(1)+st-1
        do n=1,st
          this%c_d1dx1  (-st+1:st,i) = this%c_d1dx1 (-st+1:st,i)  + this%alpha(n)*this%morinishi_ddx(:,n)/(x (i+1)-x (i  ))
          this%c_intrp1 (-st+1:st,i) = this%c_intrp1 (-st+1:st,i) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
      do i=lo(1)-st+1,hi(1)+st
        do n=1,st
          this%c_d1dx1m (-st:st-1,i) = this%c_d1dx1m(-st:st-1,i)  + this%alpha(n)*this%morinishi_ddx(:,n)/(xm(i  )-xm(i-1))
          this%c_intrp1m(-st:st-1,i) = this%c_intrp1m(-st:st-1,i) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
      ! 2-dir
      do j=lo(2)-st,hi(2)+st-1
        do n=1,st
          this%c_d1dx2  (-st+1:st,j) = this%c_d1dx2 (-st+1:st,j)  + this%alpha(n)*this%morinishi_ddx(:,n)/(y (j+1)-y (j  ))
          this%c_intrp2 (-st+1:st,j) = this%c_intrp2 (-st+1:st,j) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
      do j=lo(2)-st+1,hi(2)+st
        do n=1,st
          this%c_d1dx2m (-st:st-1,j) = this%c_d1dx2m(-st:st-1,j)  + this%alpha(n)*this%morinishi_ddx(:,n)/(ym(j  )-ym(j-1))
          this%c_intrp2m(-st:st-1,j) = this%c_intrp2m(-st:st-1,j) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
      ! 3-dir
      do k=lo(3)-st,hi(3)+st-1
        do n=1,st
          this%c_d1dx3  (-st+1:st,k) = this%c_d1dx3 (-st+1:st,k)  + this%alpha(n)*this%morinishi_ddx(:,n)/(z (k+1)-z (k  ))
          this%c_intrp3 (-st+1:st,k) = this%c_intrp3 (-st+1:st,k) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
      do k=lo(3)-st+1,hi(3)+st
        do n=1,st
          this%c_d1dx3m (-st:st-1,k) = this%c_d1dx3m(-st:st-1,k)  + this%alpha(n)*this%morinishi_ddx(:,n)/(zm(k  )-zm(k-1))
          this%c_intrp3m(-st:st-1,k) = this%c_intrp3m(-st:st-1,k) + this%alpha(n)*this%morinishi_int(:,n)
        end do
      end do
    end associate

    return
  end subroutine op_obj_Init
  subroutine op_obj_Final(this)
    !> Clear data
    implicit none
    class(op_obj),           intent(inout) :: this         !! Differential operators utility
    ! Nullify pointers
    this%parallel => null()
    this%block    => null()
    return
  end subroutine op_obj_Final
  function op_obj_gradx(this,name,in) result(out)
    !> Compute the derivative in the x-direction (dir=1)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    character(len=*),     intent(in)    :: name            !! Name to give this variable
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    real(wp),pointer :: xp(:)
    integer :: i,j,k

    call out%Initialize(name,this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (1)
      xp => this%block%xm
    case (0,2,3)
      xp => this%block%x
    end select

    associate (lo => this%block%lo,  hi => this%block%hi)
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)
            out%cell(i,j,k)=0.5_wp*(in%cell(i+1,j,k)-in%cell(i-1,j,k))/(xp(i+1)-xp(i))
          end do
        end do
      end do
    end associate
    return
  end function op_obj_gradx
  function op_obj_grady(this,name,in) result(out)
    !> Compute the derivative in the z-direction (dir=2)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    character(len=*),     intent(in)    :: name            !! Name to give this variable
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    real(wp),pointer :: yp(:)
    integer :: i,j,k

    call out%Initialize(name,this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (2)
      yp => this%block%ym
    case (0,1,3)
      yp => this%block%y
    end select

    associate (lo => this%block%lo,  hi => this%block%hi)
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)
            out%cell(i,j,k)=0.5_wp*(in%cell(i,j+1,k)-in%cell(i,j-1,k))/(yp(j+1)-yp(j))
          end do
        end do
      end do
    end associate
    return
  end function op_obj_grady
  function op_obj_gradz(this,name,in) result(out)
    !> Compute the derivative in the z-direction (dir=3)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    character(len=*),     intent(in)    :: name            !! Name to give this variable
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    real(wp),pointer :: zp(:)
    integer :: i,j,k

    call out%Initialize(name,this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (3)
      zp => this%block%zm
    case (0,1,2)
      zp => this%block%z
    end select

    associate (lo => this%block%lo,  hi => this%block%hi)
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)
            out%cell(i,j,k)=0.5_wp*(in%cell(i,j,k+1)-in%cell(i,j,k-1))/(zp(k+1)-zp(k))
          end do
        end do
      end do
    end associate
    return
  end function op_obj_gradz
  function op_obj_div(this,name,in1,in2,in3) result(out)
    !> Compute the divergence of a vector (in1,in2,in3)
    ! This function takes in1,in2,in3 cell-centered (stag=0)
    ! and returns the divergence on cell centers (stag=0)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    character(len=*),     intent(in)    :: name            !! Name to give this variable
    type(eulerian_obj_r), intent(in)    :: in1             !! Component in 1-dir
    type(eulerian_obj_r), intent(in)    :: in2             !! Component in 2-dir
    type(eulerian_obj_r), intent(in)    :: in3             !! Component in 3-dir
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n,m

    call out%Initialize(name,this%block,this%parallel, 0 )

    associate (lo => this%block%lo, hi => this%block%hi )
      do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
        do n=-this%st+1,this%st
          do m=-this%st,this%st-1
            out%cell(i,j,k) = out%cell(i,j,k)                               &
              + this%c_d1dx1(n,i)*this%c_intrp1m(m,i+n)*in1%cell(i+n+m,j,k) &
              + this%c_d1dx2(n,j)*this%c_intrp2m(m,j+n)*in2%cell(i,j+n+m,k) &
              + this%c_d1dx3(n,k)*this%c_intrp3m(m,k+n)*in3%cell(i,j,k+n+m)
          end do
        end do
      end do
    end associate

    return
  end function op_obj_div
  function op_obj_p_div(this,in1,in2,in3) result(out)
    !> Compute the divergence of a vector (in1,in2,in3)
    ! This function takes in1,in2,in3 face-centered (stag=1/2/3)
    ! and returns the divergence on cell centers (stag=0)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Component in 1-dir
    type(eulerian_obj_r), intent(in)    :: in2             !! Component in 2-dir
    type(eulerian_obj_r), intent(in)    :: in3             !! Component in 3-dir
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k

    call out%Initialize('div',this%block,this%parallel, 0 )

    associate (lo => this%block%lo, hi => this%block%hi )
      do concurrent (k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
        out%cell(i,j,k) =                                                    &
           dot_product(this%c_d1dx1(:,i),in1%cell(i-this%st+1:i+this%st,j,k)) &
         + dot_product(this%c_d1dx2(:,j),in2%cell(i,j-this%st+1:j+this%st,k)) &
         + dot_product(this%c_d1dx3(:,k),in3%cell(i,j,k-this%st+1:k+this%st))
      end do
    end associate

    return
  end function op_obj_p_div
  function op_obj_intrp1(this,in) result(out)
    !> Interpolate in the x1-direction.
    ! Note: If input is face-centered (on x1), result is
    ! cell-centered (on x1m). If input is cell-centered (on x1m),
    ! result is face-centered (on x1).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to interpolate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('interp-x1',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,2,3)
        ! From xm to x
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                slo = i - this%st
                shi = i + this%st - 1

                out%cell(i,j,k)=dot_product(this%c_intrp1m(:,i),in%cell(slo:shi,j,k))
              end do
            end do
          end do
      case (1)
        ! From x to xm
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                slo = i - this%st + 1
                shi = i + this%st

                out%cell(i,j,k)=dot_product(this%c_intrp1 (:,i),in%cell(slo:shi,j,k))
              end do
            end do
          end do
      end select
    end associate
    return
  end function op_obj_intrp1
  function op_obj_intrp2(this,in) result(out)
    !> Interpolate in the x2-direction.
    ! Note: If input is face-centered (on x2), result is
    ! cell-centered (on x2m). If input is cell-centered (on x2m),
    ! result is face-centered (on x2).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to interpolate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('interp-x2',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,1,3)
        ! From xm to x
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = j - this%st
              shi = j + this%st - 1

              out%cell(i,j,k)=dot_product(this%c_intrp2m(:,j),in%cell(i,slo:shi,k))
            end do
          end do
        end do
      case (2)
        ! From x to xm
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = j - this%st + 1
              shi = j + this%st

              out%cell(i,j,k)=dot_product(this%c_intrp2 (:,j),in%cell(i,slo:shi,k))
            end do
          end do
        end do
      end select
    end associate
    return
  end function op_obj_intrp2
  function op_obj_intrp3(this,in) result(out)
    !> Interpolate in the x3-direction.
    ! Note: If input is face-centered (on x3), result is
    ! cell-centered (on x3m). If input is cell-centered (on x3m),
    ! result is face-centered (on x3).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to interpolate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('interp-x3',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,1,2)
        ! From xm to x
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = k - this%st
              shi = k + this%st - 1

              out%cell(i,j,k)=dot_product(this%c_intrp3m(:,k),in%cell(i,j,slo:shi))
            end do
          end do
        end do
      case (3)
        ! From x to xm
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = k - this%st + 1
              shi = k + this%st

              out%cell(i,j,k)=dot_product(this%c_intrp3 (:,k),in%cell(i,j,slo:shi))
            end do
          end do
        end do
      end select
    end associate
    return
  end function op_obj_intrp3
  function op_obj_d1dx1(this,in) result(out)
    !> Compute the derivative in the x1-direction.
    ! Note: If input is face-centered (on x1), result is
    ! cell-centered (on x1m). If input is cell-centered (on x1m),
    ! result is face-centered (on x1).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('d1dx1',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,2,3)
        ! From xm to x
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                slo = i - this%st
                shi = i + this%st - 1

                out%cell(i,j,k)=dot_product(this%c_d1dx1m(:,i),in%cell(slo:shi,j,k))
              end do
            end do
          end do
      case (1)
        ! From x to xm
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)
                slo = i - this%st + 1
                shi = i + this%st

                out%cell(i,j,k)=dot_product(this%c_d1dx1 (:,i),in%cell(slo:shi,j,k))
              end do
            end do
          end do
      end select
    end associate
    return
  end function op_obj_d1dx1
  function op_obj_d1dx2(this,in) result(out)
    !> Compute the derivative in the x2-direction.
    ! Note: If input is face-centered (on x2), result is
    ! cell-centered (on x2m). If input is cell-centered (on x2m),
    ! result is face-centered (on x2).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('d1dx2',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,1,3)
        ! From xm to x
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = j - this%st
              shi = j + this%st - 1

              out%cell(i,j,k)=dot_product(this%c_d1dx2m(:,j),in%cell(i,slo:shi,k))
            end do
          end do
        end do
      case (2)
        ! From x to xm
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = j - this%st + 1
              shi = j + this%st

              out%cell(i,j,k)=dot_product(this%c_d1dx2 (:,j),in%cell(i,slo:shi,k))
            end do
          end do
        end do
      end select
    end associate
    return
  end function op_obj_d1dx2
  function op_obj_d1dx3(this,in) result(out)
    !> Compute the derivative in the x3-direction.
    ! Note: If input is face-centered (on x3), result is
    ! cell-centered (on x3m). If input is cell-centered (on x3m),
    ! result is face-centered (on x3).
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k
    integer :: slo, shi

    call out%Initialize('d1dx3',this%block,this%parallel,in%staggering)

    associate (lo => this%block%lo,  hi => this%block%hi)
      select case (in%staggering)
      case (0,1,2)
        ! From xm to x
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = k - this%st
              shi = k + this%st - 1

              out%cell(i,j,k)=dot_product(this%c_d1dx3m(:,k),in%cell(i,j,slo:shi))
            end do
          end do
        end do
      case (3)
        ! From x to xm
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              slo = k - this%st + 1
              shi = k + this%st

              out%cell(i,j,k)=dot_product(this%c_d1dx3 (:,k),in%cell(i,j,slo:shi))
            end do
          end do
        end do
      end select
    end associate
    return
  end function op_obj_d1dx3
  function op_obj_conv11(this,in1,in2) result(out)
    !> Compute d(U1 U1)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,0             )
    call out%Initialize('ddx1(U1U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp1(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st, this%st-1
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx1m(n,i)*in1c%cell(i+n,j,k)* &
                                               0.5_wp*(in2%cell(i+2*n+1,j,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv11
  function op_obj_conv21(this,in1,in2) result(out)
    !> Compute d(U2 U1)/dx2
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,2             )
    call out%Initialize('ddx2(U2U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp1(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx2 (n,j)*in1c%cell(i,j+n,k)* &
                                               0.5_wp*(in2%cell(i,j+2*n-1,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv21
  function op_obj_conv31(this,in1,in2) result(out)
    !> Compute d(U3 U1)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,3             )
    call out%Initialize('ddx3(U3U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp1(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx3 (n,k)*in1c%cell(i,j,k+n)* &
                                               0.5_wp*(in2%cell(i,j,k+2*n-1)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv31
  function op_obj_conv12(this,in1,in2) result(out)
    !> Compute d(U1 U2)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,0             )
    call out%Initialize('ddx1(U1U2)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp


    ! Interpolate to cell center
    in1c = this%intrp2(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx1 (n,i)*in1c%cell(i+n,j,k)* &
                                               0.5_wp*(in2%cell(i+2*n-1,j,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv12
  function op_obj_conv22(this,in1,in2) result(out)
    !> Compute d(U2 U2)/dx2
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,0             )
    call out%Initialize('ddx2(U2U2)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp


    ! Interpolate to cell center
    in1c = this%intrp2(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st, this%st-1
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx2m(n,j)*in1c%cell(i,j+n,k)* &
                                               0.5_wp*(in2%cell(i,j+2*n+1,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv22
  function op_obj_conv32(this,in1,in2) result(out)
    !> Compute d(U3 U1)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,0             )
    call out%Initialize('ddx3(U3U2)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    ! Interpolate U3 from (xm,ym,z) to (x,ym,z)
    in1c = this%intrp2(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx3 (n,k)*in1c%cell(i,j,k+n)* &
                                               0.5_wp*(in2%cell(i,j,k+2*n-1)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv32
  function op_obj_conv13(this,in1,in2) result(out)
    !> Compute d(U1 U1)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,0             )
    call out%Initialize('ddx1(U1U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp3(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx1 (n,i)*in1c%cell(i+n,j,k)* &
                                               0.5_wp*(in2%cell(i+2*n-1,j,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv13
  function op_obj_conv23(this,in1,in2) result(out)
    !> Compute d(U2 U1)/dx2
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,2             )
    call out%Initialize('ddx2(U2U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp3(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st+1, this%st
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx2 (n,j)*in1c%cell(i,j+n,k)* &
                                               0.5_wp*(in2%cell(i,j+2*n-1,k)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv23
  function op_obj_conv33(this,in1,in2) result(out)
    !> Compute d(U3 U1)/dx1
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in1             !! Convecting velocity: Ui
    type(eulerian_obj_r), intent(in)    :: in2             !! Velocity Uj
    type(eulerian_obj_r)                :: in1c            !! Interpolated Ui from face to cell center
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k,n

    call in1c%Initialize('in1c',     this%block,this%parallel,3             )
    call out%Initialize('ddx3(U3U1)',this%block,this%parallel,in2%staggering)
    out = 0.0_wp

    in1c = this%intrp3(in1)
    call in1c%UpdateGhostCells

    associate (lo => this%block%lo,  hi => this%block%hi)
     do k=lo(3),hi(3)
       do j=lo(2),hi(2)
         do i=lo(1),hi(1)
           do n = -this%st, this%st-1
             out%cell(i,j,k) = out%cell(i,j,k) + this%c_d1dx3m(n,k)*in1c%cell(i,j,k+n)* &
                                               0.5_wp*(in2%cell(i,j,k+2*n+1)+in2%cell(i,j,k))
           end do
         end do
       end do
     end do
    end associate
    return
  end function op_obj_conv33

  subroutine op_obj_BuildLaplacian(this,mat,stm)
    !> Build Laplacian operator using Morinishi's schemes
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    real(wp), allocatable,intent(out)   :: mat(:,:,:,:,:)  !! Matrix that stores the laplacian operator
    integer,              intent(out)   :: stm             !! Stencil extent
    ! Work variables
    integer :: i,j,k,n,m

    if (allocated(mat)) deallocate(mat)

    ! Set stencil extent based on current scheme order
    stm = 2*this%st-1

    associate ( lo=>this%block%lo, hi =>this%block%hi  )

      allocate(mat(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3),3,-stm:stm))

      mat = 0.0_wp
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)

            do n=-this%st+1,this%st
              do m=-this%st,this%st-1
                mat(i,j,k,1,n+m)= mat(i,j,k,1,n+m) + this%c_d1dx1 (n,i)*this%c_d1dx1m(m,i+n)
                mat(i,j,k,2,n+m)= mat(i,j,k,2,n+m) + this%c_d1dx2 (n,j)*this%c_d1dx2m(m,j+n)
                mat(i,j,k,3,n+m)= mat(i,j,k,3,n+m) + this%c_d1dx3 (n,k)*this%c_d1dx3m(m,k+n)
              end do
            end do

          end do
        end do
      end do

    end associate

    return
  end subroutine op_obj_BuildLaplacian
  subroutine op_obj_ApplyLaplacianDC(this,rhs,bcs,varname)
    use leapBC
    !> Apply Dirichlet boundary conditions to the
    ! RHS of a Laplacian equation
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(inout) :: rhs
    type(bc_set),         intent(inout) :: bcs
    character(len=*),     intent(in)    :: varname
    ! Work variables
    type(extent_obj)  :: extents
    real(wp), pointer :: val(:,:,:)
    integer           :: n,m,id
    integer           :: i,j,k
    logical           :: found
    integer           :: dir
    integer           :: side


    ! Leave if no regions
    if (.not.allocated(bcs%region)) return

    ! Loop over regions
    do id=1,bcs%count

      ! Check Whether we have a BC for Volume Fraction
      found = bcs%CheckBCExists(bcs%region(id)%name,varname)
      if (.not.found) cycle
      if (bcs%GetBCType(bcs%region(id)%name,varname).ne.BC_DIRICHLET) cycle

      ! Get extents
      extents = bcs%GetExtents(bcs%region(id)%name)

      ! Get direction and side of BC
      call bcs%GetSideDirByRegion(bcs%region(id)%name,side,dir)

      ! Get a pointer to the BC values
      call bcs%GetBCPointer(bcs%region(id)%name,varname,val)

      ! Assuming zero gradient
      associate(lo => extents%lo, hi=>extents%hi)
        if (dir.eq.1) then
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)

                do n=-this%st+1,this%st
                  do m=-this%st,this%st-1
                    if (side.eq.1 .and. (n+m.gt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx1 (n,i)*this%c_d1dx1m(m,i+n)*val(i,j,k)
                    end if
                    if (side.eq.0 .and. (n+m.lt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx1 (n,i)*this%c_d1dx1m(m,i+n)*val(i,j,k)
                    end if
                  end do
                end do

              end do
            end do
          end do
        end if
        if (dir.eq.2) then
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)

                do n=-this%st+1,this%st
                  do m=-this%st,this%st-1
                    if (side.eq.1 .and. (n+m.gt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx2 (n,j)*this%c_d1dx2m(m,j+n)*val(i,j,k)
                    end if
                    if (side.eq.0 .and. (n+m.lt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx2 (n,j)*this%c_d1dx2m(m,j+n)*val(i,j,k)
                    end if
                  end do
                end do

              end do
            end do
          end do
        end if
        if (dir.eq.3) then
          do k=lo(3),hi(3)
            do j=lo(2),hi(2)
              do i=lo(1),hi(1)

                do n=-this%st+1,this%st
                  do m=-this%st,this%st-1
                    if (side.eq.1 .and. (n+m.gt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx3 (n,k)*this%c_d1dx3m(m,k+n)*val(i,j,k)
                    end if
                    if (side.eq.0 .and. (n+m.lt.0)) then
                      rhs%cell(i,j,k) = rhs%cell(i,j,k)  - this%c_d1dx3 (n,k)*this%c_d1dx3m(m,k+n)*val(i,j,k)
                    end if
                  end do
                end do

              end do
            end do
          end do
        end if
      end associate

      val => null()
    end do

    return
  end subroutine op_obj_ApplyLaplacianDC

  function op_obj_StrainRate(this,U,V,W) result(S)
    !> Compute the strain rate tensor from the velocity field.
    ! Result is on mid points (staggering=0).
    ! Tensor is stored as follows:
    ! S = 0.5*( grad(u) + grad(u)^T )
    !     ( S(1) S(4) S(6) )
    !   = ( S(4) S(2) S(5) )
    !     ( S(6) S(5) S(3) )
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(Eulerian_obj_r), intent(in)    :: U               !! Fluid velocity field in 1-dir
    type(Eulerian_obj_r), intent(in)    :: V               !! Fluid velocity field in 2-dir
    type(Eulerian_obj_r), intent(in)    :: W               !! Fluid velocity field in 3-dir
    type(eulerian_obj_r)                :: S(6)            !! Result
    ! Work variables
    character(str8) :: name
    integer :: i,j,k,n,m,l

    do n=1,6
      call S(n)%initialize(name,this%block,this%parallel,0)
      S(n) = 0.0_wp
    end do

    associate (lo => this%block%lo,  hi => this%block%hi)
      ! Diagonal elements
      !------------------------------
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)
            S(1)%cell(i,j,k) = dot_product(this%c_d1dx1(:,i),U%cell(i-this%st+1:i+this%st,j,k))
            S(2)%cell(i,j,k) = dot_product(this%c_d1dx2(:,j),V%cell(i,j-this%st+1:j+this%st,k))
            S(3)%cell(i,j,k) = dot_product(this%c_d1dx3(:,k),W%cell(i,j,k-this%st+1:k+this%st))
          end do
        end do
      end do

      ! Off-diagonal elements
      !------------------------------
      do k=lo(3),hi(3)
        do j=lo(2),hi(2)
          do i=lo(1),hi(1)

           ! Interpolate and differentiate
           ! 0.5*(du/dy+dv/dx)
            do l = -this%st+1,this%st
              do n = -this%st, this%st-1
                do m = -this%st+1,this%st
                  S(4)%cell(i,j,k) = S(4)%cell(i,j,k)                                                       &
                    + 0.5_wp*this%c_d1dx1(l,i)*this%c_intrp1m(n,i+l)*this%c_intrp2(m,j)*V%cell(i+l+n,j+m,k) &
                    + 0.5_wp*this%c_d1dx2(l,j)*this%c_intrp2m(n,j+l)*this%c_intrp1(m,i)*U%cell(i+m,j+l+n,k)
                end do
              end do
            end do

            ! 0.5*(dw/dy+dv/dz)
            do l = -this%st+1,this%st
              do n = -this%st, this%st-1
                do m = -this%st+1,this%st
                  S(5)%cell(i,j,k) = S(5)%cell(i,j,k)                                                       &
                    + 0.5_wp*this%c_d1dx2(l,j)*this%c_intrp2m(n,j+l)*this%c_intrp3(m,k)*W%cell(i,j+l+n,k+m) &
                    + 0.5_wp*this%c_d1dx3(l,k)*this%c_intrp3m(n,k+l)*this%c_intrp2(m,j)*V%cell(i,j+m,k+l+n)
                end do
              end do
            end do

            ! 0.5*(du/dz+dw/dx)
            do l = -this%st+1,this%st
              do n = -this%st, this%st-1
                do m = -this%st+1,this%st
                  S(6)%cell(i,j,k) = S(6)%cell(i,j,k)                                                       &
                    + 0.5_wp*this%c_d1dx3(l,k)*this%c_intrp3m(n,k+l)*this%c_intrp1(m,i)*U%cell(i+m,j,k+l+n) &
                    + 0.5_wp*this%c_d1dx1(l,i)*this%c_intrp1m(n,i+l)*this%c_intrp3(m,k)*W%cell(i+l+n,j,k+m)
                end do
              end do
            end do

          end do
        end do
      end do
    end associate

    do n=1,6
      call S(n)%UpdateGhostCells
    end do
    return
  end function op_obj_StrainRate



  function op_obj_grad_x(this,in) result(out)
    !> Compute the derivative in the x-direction (dir=1)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k

    call out%Initialize('grad_x',this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (0,2,3)
      ! From xm to x
      associate (lo => this%block%lo,  hi => this%block%hi, xm => this%block%xm)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i,j,k)-in%cell(i-1,j,k))/(xm(i)-xm(i-1))
            end do
          end do
        end do
      end associate
    case (1)
      ! From x to xm
      associate (lo => this%block%lo,  hi => this%block%hi, x => this%block%x)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i+1,j,k)-in%cell(i,j,k))/(x(i+1)-x(i))
            end do
          end do
        end do
      end associate
    end select
    return
  end function op_obj_grad_x
  function op_obj_grad_y(this,in) result(out)
    !> Compute the derivative in the y-direction (dir=2)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k

    call out%Initialize('grad_y',this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (0,1,3)
      ! From ym to y
      associate (lo => this%block%lo,  hi => this%block%hi, ym => this%block%ym)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i,j,k)-in%cell(i,j-1,k))/(ym(j)-ym(j-1))
            end do
          end do
        end do
      end associate
    case (2)
      ! From y to ym
      associate (lo => this%block%lo,  hi => this%block%hi, y => this%block%y)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i,j+1,k)-in%cell(i,j,k))/(y(j+1)-y(j))
            end do
          end do
        end do
      end associate
    end select
    return
  end function op_obj_grad_y
  function op_obj_grad_z(this,in) result(out)
    !> Compute the derivative in the z-direction (dir=3)
    class(op_obj),        intent(inout) :: this            !! Differential operators utility
    type(eulerian_obj_r), intent(in)    :: in              !! Field to differentiate
    type(eulerian_obj_r)                :: out             !! Result
    ! Work variables
    integer :: i,j,k

    call out%Initialize('grad_z',this%block,this%parallel,in%staggering)

    select case (in%staggering)
    case (0,1,2)
      ! From zm to z
      associate (lo => this%block%lo,  hi => this%block%hi, zm => this%block%zm)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i,j,k)-in%cell(i,j,k-1))/(zm(k)-zm(k-1))
            end do
          end do
        end do
      end associate
    case (3)
      ! From z to zm
      associate (lo => this%block%lo,  hi => this%block%hi, z => this%block%z)
        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              out%cell(i,j,k)=(in%cell(i,j,k+1)-in%cell(i,j,k))/(z(k+1)-z(k))
            end do
          end do
        end do
      end associate
    end select
    return
  end function op_obj_grad_z
end module leapDiffOp
