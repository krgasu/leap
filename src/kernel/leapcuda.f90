!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapCuda
  !>--------------------------------------------------------------------------
  ! Module: leapCuda
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Provides interfaces to CUDA Runtime API
  ! --------------------------------------------------------------------------
#ifdef USE_GPU
   use, intrinsic :: iso_c_binding, only: c_int
   implicit none

   integer(c_int), parameter :: cudaMemAttachGlobal = 1
   integer(c_int), parameter :: cudaMemAttachHost = 2
   integer(c_int), parameter :: cudaMemAttachSingle = 4

   interface
      integer(c_int) function cudaMallocManaged(dPtr, size, flags) bind(c, name="cudaMallocManaged")
         use, intrinsic :: iso_c_binding
         type(c_ptr), intent(out) :: dPtr
         integer(c_size_t), value :: size
         integer(c_int), value :: flags
      end function cudaMallocManaged
      integer(c_int) function cudaFree(dPtr) bind(c, name="cudaFree")
         use, intrinsic :: iso_c_binding
         type(c_ptr), value :: dPtr
      end function cudaFree
      integer(c_int) function cudaDeviceGetAttribute(val,attr,device) bind(c, name="cudaDeviceGetAttribute")
         use, intrinsic :: iso_c_binding
         integer(c_int), intent(out) :: val
         integer(c_int), value :: attr
         integer(c_int), value :: device
      end function cudaDeviceGetAttribute
      integer(c_int) function cudaGetDeviceCount(val) bind(c, name="cudaGetDeviceCount")
         use, intrinsic :: iso_c_binding
         integer(c_int), intent(out) :: val
      end function cudaGetDeviceCount
   end interface

   contains

   ! Wrapper functions
   ! ------------------------------------------------------
   integer function device_malloc_managed(nbytes, dPtr) result(stat)
     use, intrinsic :: iso_c_binding, only: c_size_t, c_ptr
     use, intrinsic :: iso_fortran_env, only: int64
     integer(int64), intent(in) :: nbytes
     type(c_ptr), intent(inout) :: dPtr
     stat = cudaMallocManaged(dPtr, int(nbytes,c_size_t), cudaMemAttachGlobal)
   end function device_malloc_managed
   integer function device_free(dPtr) result(stat)
     use, intrinsic :: iso_c_binding, only: c_ptr
     type(c_ptr), intent(inout) :: dPtr
     stat = cudaFree(dPtr)
   end function device_free
   integer function device_Get_Attribute(val,attr,device) result(stat)
     use, intrinsic :: iso_c_binding, only: c_ptr, c_int
     use, intrinsic :: iso_fortran_env, only: int32
     integer(int32), intent(out) :: val
     integer(int32), intent(in) :: attr
     integer(int32), intent(in) :: device
     integer(c_int) :: c_val

     stat = cudaDeviceGetAttribute(c_val,int(attr,c_int),int(device,c_int))
     val=int(c_val,int32)
   end function device_Get_Attribute
   integer function device_Get_Count(val) result(stat)
     use, intrinsic :: iso_c_binding, only: c_ptr, c_int
     use, intrinsic :: iso_fortran_env, only: int32
     integer(int32), intent(out) :: val
     integer(c_int) :: c_val

     stat = cudaGetDeviceCount(c_val)
     val=int(c_val,int32)
   end function device_Get_Count

   ! Custom subroutines
   ! ------------------------------------------------------
   subroutine device_info(devID)
     !> Get information about GPU
     implicit none
     integer, intent(in) :: devID
     integer :: stat, val
     stat = device_Get_Count(val); write (*,*) "Number of GPU capable devices:", val

     if (val.ne.0) then
       stat = device_Get_Attribute(val,  13, devId); write (*,*) "Clock rate in kHz:", val
       stat = device_Get_Attribute(val,  16, devId); write (*,*) "SM count:", val
       stat = device_Get_Attribute(val,  81, devId); write (*,*) "Shared memory per SM in bytes:", val
       stat = device_Get_Attribute(val,  82, devId); write (*,*) "Number of 32-bit Registers per SM:", val
       stat = device_Get_Attribute(val,  39, devId); write (*,*) "Max threads per SM:", val
       stat = device_Get_Attribute(val,  38, devId); write (*,*) "L2 Cache Size in bytes: ", val
       stat = device_Get_Attribute(val,  36, devId); write (*,*) "Memory Clock Rate in kHz: ", val
       stat = device_Get_Attribute(val,  75, devId); write (*,*) "Compute Capability, major:", val
       stat = device_Get_Attribute(val,  76, devId); write (*,*) "Compute Capability, minor:", val
     end if

     return
   end subroutine device_info
#endif
end module leapCuda

