!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapIO_hdf5
  !>--------------------------------------------------------------------------
  ! Module: leapIO_hdf5
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Read/write data in the HDF5 format
  ! --------------------------------------------------------------------------
  use hdf5
  use leapKinds
  use leapParallel
  use leapUtils, only : hashtbl_obj
  implicit none

  private
  public :: hdf5_obj

  type :: hdf5_obj
    !> A utility to read/write files in HDF5
    type(parallel_obj), pointer   :: parallel=>null()      !! Associated parallel structure
    integer(HID_T)                :: fid                   !! File identifier
    integer(HID_T),   allocatable :: gid(:)                !! Group identifier
    character(len=:), allocatable :: filename              !! file to read/write
    integer,            private   :: group_count=0         !! Number of groups
    type(hashtbl_obj),  private   :: tbl                   !! Hash table
    contains
      procedure :: Initialize            => hdf5_obj_Init
      procedure :: Finalize              => hdf5_obj_Final
      procedure :: Open                  => hdf5_obj_Open
      procedure :: Close                 => hdf5_obj_Close
      procedure :: CreateGroup           => hdf5_obj_CreateGroup
      procedure :: OpenGroup             => hdf5_obj_OpenGroup
      procedure :: CloseGroup            => hdf5_obj_CloseGroup
      procedure :: ReadGroupNames        => hdf5_obj_ReadGroupNames
      generic   :: ReadAttributes        => hdf5_obj_ReadAttributes0D, &
                                            hdf5_obj_ReadAttributes1D
      generic   :: WriteAttributes       => hdf5_obj_WriteAttributes0D,&
                                            hdf5_obj_WriteAttributes1D
      generic   :: Read                  => hdf5_obj_Read3D,hdf5_obj_Read1D
      generic   :: Write                 => hdf5_obj_Write3D,hdf5_obj_Write1D
      procedure :: WriteCoord            => hdf5_obj_WriteCoord
      procedure :: ReadCoord             => hdf5_obj_ReadCoord
      ! Private procedures
      procedure, private ::              &
                           GetGroupIndex => hdf5_obj_GetGroupIndex
      procedure, private ::              &
                          GetGroupObject => hdf5_obj_GetGroupObject
      procedure, private, nopass ::      &
                            FixGroupName => hdf5_obj_FixGroupName
      procedure, private :: hdf5_obj_Read3D
      procedure, private :: hdf5_obj_Read1D
      procedure, private :: hdf5_obj_Write3D
      procedure, private :: hdf5_obj_Write1D
      procedure, private :: hdf5_obj_WriteAttributes1D
      procedure, private :: hdf5_obj_WriteAttributes0D
      procedure, private :: hdf5_obj_ReadAttributes1D
      procedure, private :: hdf5_obj_ReadAttributes0D
  end type

  ! Default hash table size
  integer, parameter :: HDF5_SET_HTBL_SIZE = 20
  contains
    impure subroutine hdf5_obj_Init(this,filename,access_flag,parallel)
      !> Initialize the hdf5 object
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: filename          !! File to read/write
      character(len=*), intent(in)    :: access_flag       !! File access mode
      type(parallel_obj), target, &
                        intent(in)    :: parallel          !! parallel structure from main program
      ! Work variables
      integer :: ierr

      ! Point to the master objects
      this%parallel => parallel

      ! Set the file name
      this%filename = trim(adjustl(filename))

      ! Initialize the hash table
      call this%tbl%initialize(HDF5_SET_HTBL_SIZE)

      ! Initialize HDF5's Fortran interface
      call H5open_f(ierr)

      ! Open file
      call this%Open(access_flag)

      return
    end subroutine hdf5_obj_Init
    impure subroutine hdf5_obj_Final(this)
      !> Finalize the hdf5 object
      implicit none
      class(hdf5_obj), intent(inout) :: this              !! A HDF5 object
      ! Work variables
      integer :: ierr

      ! Close file
      call this%Close

      ! Finalize hash table
      call this%tbl%finalize

      if (allocated(this%gid)) deallocate(this%gid)
      this%group_count = 0

      ! Nullify pointers
      this%parallel => null()

      ! Finalize HDF5's Fortran interface
      call H5close_f(ierr)

      return
    end subroutine hdf5_obj_Final
    impure subroutine hdf5_obj_Open(this,flag)
      !> Open a hdf5 file
      use mpi_f08
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: flag              !! Access mode
      ! Work variables
      integer       :: ierr
      integer(HID_T):: plistid

      ! MPI stuff
      call H5Pcreate_f(H5P_FILE_ACCESS_F, plistid, ierr)

      ! Add MPI IO communicator to the file access property list
      call H5Pset_fapl_mpio_f(plistid, this%parallel%comm%g%MPI_VAL, MPI_INFO_NULL%MPI_VAL, ierr)

      ! Instruct HDF5 to do metadata operations collectively
      ! This will cause MPI rank=0 to read and broadcast to
      ! others, avoiding a read rush in large MPI jobs.
      call H5Pset_all_coll_metadata_ops_f(plistid,.true.,ierr)

      select case(trim(adjustl(flag)))
      case ('W','w' )
        call H5Fcreate_f(this%filename, H5F_ACC_TRUNC_F, this%fid, ierr, access_prp=plistid)
        if (ierr.ne.0) &
          call this%parallel%stop("Unable to create new HDF5 file: "//this%filename)
      case ('R','r' )
        call H5Fopen_f(this%filename, H5F_ACC_RDONLY_F, this%fid, ierr, access_prp=plistid)
        if (ierr.ne.0) &
          call this%parallel%stop("Unable to open existing HDF5 file in Read mode: "//this%filename)
      case ('RW','rw')
        call H5Fopen_f(this%filename, H5F_ACC_RDWR_F, this%fid, ierr, access_prp=plistid)
        if (ierr.ne.0) &
          call this%parallel%stop("Unable to open existing HDF5 file in Read/Write mode: "//this%filename)
      case default
        call this%parallel%stop("Invalide acces mode for HDF5 file: "//this%filename)
      end select

      ! Close property list
      call h5pclose_f(plistid,ierr)

      return
    end subroutine hdf5_obj_Open
    impure subroutine hdf5_obj_Close(this)
      !> Close hdf5 file
      implicit none
      class(hdf5_obj), intent(in) :: this                  !! A HDF5 object
      ! Work variables
      integer :: ierr

      ! Close the file
      call H5Fclose_f(this%fid, ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Failed closing HDF5 file: "//this%filename)

      return
    end subroutine hdf5_obj_Close
    impure subroutine hdf5_obj_CreateGroup(this,groupname)
      !> Create a group (analogous to directory) in an HDF5
      ! file and update hash table
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! File to read/write
      ! Work variables
      integer             :: ierr
      character(len=:), &
              allocatable :: groupname_
      integer(HID_T),   &
              allocatable :: tmp_array(:)

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      ! Resize
      if (.not.allocated(this%gid)) then
        allocate(this%gid(1))
        this%group_count = 1
      else
        this%group_count = size(this%gid) + 1
        allocate(tmp_array(this%group_count))
        tmp_array(1:this%group_count-1) = this%gid(:)
        call move_alloc(tmp_array,this%gid)
      end if

      ! Add to hash table
      call this%tbl%put(key=this%tbl%HashString(groupname_),val=this%group_count)

      ! Create a group in the HDF5 file
      call H5Gcreate_f(this%fid,groupname_,this%gid(this%group_count),ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to create a group in HDF5 file "//this%filename)

      return
    end subroutine hdf5_obj_CreateGroup
    impure subroutine hdf5_obj_OpenGroup(this,groupname)
      !> Open a group (analogous to directory) in an HDF5
      ! file and updates hash table
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! File to read/write
      ! Work variables
      integer(HID_T)      :: obj
      integer             :: ierr
      character(len=:), &
              allocatable :: groupname_
      integer(HID_T),   &
              allocatable :: tmp_array(:)

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      ! Open group
      call H5Oopen_f(this%fid,groupname,obj,ierr)

      if (ierr.ne.0) &
        call this%parallel%stop("Unable to open group "//groupname_//" in HDF5 file "//this%filename)

      ! Resize
      if (.not.allocated(this%gid)) then
        allocate(this%gid(1))
        this%group_count = 1
        this%gid(1) = obj
      else
        this%group_count = size(this%gid) + 1
        allocate(tmp_array(this%group_count))
        tmp_array(1:this%group_count-1) = this%gid(:)
        call move_alloc(tmp_array,this%gid)
        this%gid(this%group_count) = obj
      end if

      ! Add to hash table
      call this%tbl%put(key=this%tbl%HashString(groupname_),val=this%group_count)

      return
    end subroutine hdf5_obj_OpenGroup
    impure subroutine hdf5_obj_CloseGroup(this,groupname)
      !> Close an HDF5 group
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: groupname            !! Group to close
      ! Work variables
      integer             :: ierr
      integer             :: n
      character(len=:), &
              allocatable :: groupname_

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      ! get index of this group
      n = this%GetGroupIndex(groupname_)
      if (n.le.0) &
        call this%parallel%stop("Unable to find group "//groupname_//"in HDF5 file "//this%filename)

      ! Close a group in the HDF5 file
      call H5Gclose_f(this%gid(n), ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to close group "//groupname_//"in HDF5 file "//this%filename)

      return
    end subroutine hdf5_obj_CloseGroup
    pure function hdf5_obj_GetGroupIndex(this,name) result(val)
      !> Returns the index of a group
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: name                 !! Name of region
      integer :: val
      call this%tbl%get(key=this%tbl%HashString(trim(adjustl(name))),val=val)
      return
    end function hdf5_obj_GetGroupIndex
    impure function hdf5_obj_GetGroupObject(this,name) result(val)
      !> Returns the HDF5 object id of the group
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: name                 !! Name of region
      integer(HID_T) :: val
      ! Work variable
      integer :: n
      if (name.eq.'/') then
        val = this%fid
      else
        ! get index of this group
        n = this%GetGroupIndex(name)
        if (n.le.0) &
          call this%parallel%stop("Unable to find group "//name//"in HDF5 file "//this%filename)

        val = this%gid(n)
      end if
      return
    end function hdf5_obj_GetGroupObject
    impure subroutine hdf5_obj_ReadGroupNames(this,basegroup,names)
      !> Read the groups (i.e., directories) under a given base group
      ! in an HDF5 file.
      use iso_c_binding
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: basegroup         !! Base group to explore
      character(len=str64), &
           allocatable, intent(out)   :: names(:)          !! Names of groups under the base group
      ! Work variables
      integer(HID_T)      :: obj
      type(H5O_info_t)    :: info
      character(len=str64):: obj_name
      integer(SIZE_T)     :: name_size
      integer             :: idx
      integer             :: ierr
      character(len=:), &
              allocatable :: groupname
      character(len=:), &
              allocatable :: name_

      ! Initilize the array of names as an empty array
      names = [character(len=str64)::]

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname=this%FixGroupName(basegroup)

      ! Iterate over objects under the basegroup
      idx = 0; ierr = 0
      do while (ierr .eq. 0)
        ! Open object
        call H5Oopen_by_idx_f(this%fid,groupname,H5_INDEX_NAME_F,H5_ITER_NATIVE_F, int(idx,HSIZE_T),obj,ierr)
        if (ierr.eq.0) then
          ! Get name and info of the object
          call H5Iget_name_f(obj,obj_name,int(str64,SIZE_T),name_size,ierr)
          call H5Oget_info_f(obj,info,ierr)

          ! Consider only objects that are groups
          if ( info%type .eq. H5O_TYPE_GROUP_F) then
            ! Add object name to list
            name_ = obj_name(len(groupname)+1:name_size)
            names = [character(len=str64):: names(:), name_]

          end if
          idx = idx + 1
        end if
        ! Close object
        call H5Oclose_f(obj,ierr)
      end do

      return
    end subroutine hdf5_obj_ReadGroupNames
    impure subroutine hdf5_obj_ReadAttributes0D(this,groupname,label,val)
      !> Read a scalar attribute under a given group
      use iso_c_binding
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: groupname            !! Groupname
      character(len=*), intent(in) :: label                !! Attribute label
      class(*),intent(out)         :: val                  !! Attribute value
      ! Work variables
      integer         :: ierr
      integer         :: n
      integer, target :: int2log
      type(c_ptr)     :: fptr
      integer(HID_T)  :: dtype
      integer(HID_T)  :: sid
      integer(HID_T)  :: attrid
      integer(HSIZE_T):: dims(1)
      integer(HSIZE_T):: maxdims(1)
      character(len=:),&
          allocatable :: groupname_

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      ! Open the attribute
      if (groupname_.eq.'/') then
        call H5Aopen_f(this%fid, trim(adjustl(label)), attrid, ierr)
      else
        ! get index of this group
        n = this%GetGroupIndex(groupname_)
        if (n.le.0) &
          call this%parallel%stop("Unable to find group "//groupname_//" in hdf5 file "//this%filename)

        call H5Aopen_f(this%gid(n), trim(adjustl(label)), attrid, ierr)
      end if

      ! Get datatype and spaceid
      call H5Aget_type_f(attrid,dtype,ierr)
      call H5Aget_space_f(attrid,sid,ierr)

      ! Check dimensions
      call H5Sget_simple_extent_dims_f(sid, dims, maxdims, ierr)
      if (dims(1).ne.1) &
          call this%parallel%stop("Size mismatch when reading attribute"//trim(adjustl(label))//" under "//groupname_//" in hdf5 file "//this%filename)

      ! Read attribute
      select type(val)
      type is (real(leapDP))
        fptr = c_loc(val)
      type is (real(leapSP))
        fptr = c_loc(val)
      type is (integer)
        fptr = c_loc(val)
      type is (integer(leapI8))
        fptr = c_loc(val)
      type is (logical)
        fptr = c_loc(int2log)
      end select
      call H5Aread_f(attrid, dtype,fptr, ierr)

      select type(val)
      type is (logical)
        if (int2log.eq.1) then
          val = .true.
        else
          val = .false.
        end if
      end select

      ! Remove attribute ID
      call H5Aclose_f(attrid, ierr)

      ! Close data space
      call H5Sclose_f(sid, ierr)
      return
    end subroutine hdf5_obj_ReadAttributes0D
    impure subroutine hdf5_obj_ReadAttributes1D(this,groupname,label,val)
      !> Read a 1-D array of attributes under a given group
      use iso_c_binding
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: groupname            !! Groupname
      character(len=*), intent(in) :: label                !! Attribute label
      class(*),intent(out)         :: val(:)               !! Attribute values
      ! Work variables
      integer         :: ierr
      integer         :: n
      integer, target, &
          allocatable :: int2log(:)
      type(c_ptr)     :: fptr
      integer(HID_T)  :: sid
      integer(HID_T)  :: dtype
      integer(HID_T)  :: attrid
      integer(HSIZE_T):: dims(1)
      integer(HSIZE_T):: maxdims(1)
      character(len=:),&
          allocatable :: groupname_

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      ! Create Attribute ID
      if (groupname_.eq.'/') then
        call H5Aopen_f(this%fid, trim(adjustl(label)), attrid, ierr)
      else
        ! get index of this group
        n = this%GetGroupIndex(groupname_)
        if (n.le.0) &
          call this%parallel%stop("Unable to find group "//groupname_//" in hdf5 file "//this%filename)

        call H5Aopen_f(this%gid(n), trim(adjustl(label)), attrid, ierr)
      end if

      ! Get datatype and spaceid
      call H5Aget_type_f(attrid,dtype,ierr)
      call H5Aget_space_f(attrid,sid,ierr)

      ! Check dimensions
      call H5Sget_simple_extent_dims_f(sid, dims, maxdims, ierr)
      if (size(val).ne.dims(1)) &
          call this%parallel%stop("Size mismatch when reading attribute"//trim(adjustl(label))//" under "//groupname_//" in hdf5 file "//this%filename)

      select type(val)
      type is (real(leapDP))
        fptr = c_loc(val(1))
      type is (real(leapSP))
        fptr = c_loc(val(1))
      type is (integer)
        fptr = c_loc(val(1))
      type is (integer(leapI8))
        fptr = c_loc(val(1))
      type is (logical)
        allocate(int2log(size(val)))
        fptr = c_loc(int2log(1))
      end select
      call H5Aread_f(attrid, dtype,fptr, ierr)

      ! Special treatment of FORTRAN logicals
      select type(val)
      type is (logical)
        val = .false.
        where (int2log.eq.1) val =.true.
      end select

      ! Remove attribute ID
      call H5Aclose_f(attrid, ierr)

      ! Close data space
      call H5Sclose_f(sid, ierr)

      if (allocated(int2log)) deallocate(int2log)
      return
    end subroutine hdf5_obj_ReadAttributes1D
    impure subroutine hdf5_obj_WriteAttributes0D(this,groupname,label,val)
      use iso_c_binding
      !> Write a scalar attribute
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(in)    :: val
      ! Work variables
      integer         :: ierr
      integer         :: n
      integer, target :: log2int
      type(c_ptr)     :: fptr
      integer(HID_T)  :: sid
      integer(HID_T)  :: dtype
      integer(HID_T)  :: attrid
      integer(HSIZE_T):: dims(1)
      integer         :: ndim
      character(len=:),&
          allocatable :: groupname_

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      select type(val)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
      type is (logical)
        dtype = H5T_NATIVE_INTEGER
      end select

      ! Create data space
      dims(1) = 1
      ndim    = 1
      call H5Screate_simple_f(ndim, dims, sid, ierr)

      ! Create Attribute ID
      if (groupname_.eq.'/') then
        call H5Acreate_f(this%fid, trim(adjustl(label)), dtype, sid, attrid, ierr)
      else
        ! get index of this group
        n = this%GetGroupIndex(groupname_)
        if (n.le.0) &
          call this%parallel%stop("Unable to find group "//groupname_//"in hdf5 file "//this%filename)

        call H5Acreate_f(this%gid(n), trim(adjustl(label)), dtype, sid, attrid, ierr)
      end if

      select type(val)
      type is (real(leapDP))
        fptr = c_loc(val)
      type is (real(leapSP))
        fptr = c_loc(val)
      type is (integer)
        fptr = c_loc(val)
      type is (integer(leapI8))
        fptr = c_loc(val)
      type is (logical)
        ! Special treatment of FORTRAN logicals
        log2int = 0
        if (val.eqv..true.) log2int = 1
        fptr = c_loc(log2int)
      end select

      call H5Awrite_f(attrid,dtype,fptr,ierr)

      ! Remove attribute ID
      call H5Aclose_f(attrid, ierr)

      ! Close data space
      call H5Sclose_f(sid, ierr)

      return
    end subroutine hdf5_obj_WriteAttributes0D
    impure subroutine hdf5_obj_WriteAttributes1D(this,groupname,label,val)
      !> Write an array of attributes
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(in)    :: val(:)
      ! Work variables
      integer         :: ierr
      integer         :: n
      integer, target, &
          allocatable :: log2int(:)
      type(c_ptr)     :: fptr
      integer(HID_T)  :: sid
      integer(HID_T)  :: dtype
      integer(HID_T)  :: attrid
      integer(HSIZE_T):: dims(1)
      integer         :: ndim
      character(len=:),&
          allocatable :: groupname_

      ! Format name of this group
      groupname_=this%FixGroupName(groupname)

      select type(val)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
      type is (logical)
        dtype = H5T_NATIVE_INTEGER
      end select

      ! Create data space
      dims(1) = size(val)
      ndim    = 1
      call H5Screate_simple_f(ndim, dims, sid, ierr)

      ! Create Attribute ID
      if (groupname_.eq.'/') then
        call H5Acreate_f(this%fid, trim(adjustl(label)), dtype, sid, attrid, ierr)
      else
        ! get index of this group
        n = this%GetGroupIndex(groupname_)
        if (n.le.0) &
          call this%parallel%stop("Unable to find group "//groupname_//"in hdf5 file "//this%filename)

        call H5Acreate_f(this%gid(n), trim(adjustl(label)), dtype, sid, attrid, ierr)
      end if
      select type(val)
      type is (real(leapDP))
        fptr = c_loc(val(1))
      type is (real(leapSP))
        fptr = c_loc(val(1))
      type is (integer)
        fptr = c_loc(val(1))
      type is (integer(leapI8))
        fptr = c_loc(val(1))
      type is (logical)
        ! Special treatment of FORTRAN logicals
        allocate(log2int(size(val))); log2int = 0
        where (val .eqv. .true.) log2int = 1

        fptr = c_loc(log2int(1))
      end select

      call H5Awrite_f(attrid,dtype,fptr,ierr)

      ! Remove attribute ID
      call H5Aclose_f(attrid, ierr)

      ! Close data space
      call H5Sclose_f(sid, ierr)

      if (allocated(log2int)) deallocate(log2int)
      return
    end subroutine hdf5_obj_WriteAttributes1D
    impure subroutine hdf5_obj_Read3D(this,basegroup,name,array,lo,hi)
      !> Read a 3D dataset located under basegroup and given by name
      use iso_c_binding
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: basegroup            !! Parent group containing the dataset
      character(len=*), intent(in) :: name                 !! Dataset name
      class(*),         intent(out):: array(:,:,:)         !! Data array
      integer,          intent(in) :: lo(3)                !! Low bounds
      integer,          intent(in) :: hi(3)                !! High bounds
      ! Work variables
      integer(HID_T)   :: did
      integer(HID_T)   :: sidc
      integer(HID_T)   :: sidg
      integer(HID_T)   :: plist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(3)
      integer(HSIZE_T) :: offset(3)
      integer          :: ndim
      real(wp)         :: npoint
      integer          :: maxhi(3)
      integer          :: minlo(3)
      integer(HID_T)   :: obj
      integer          :: ierr
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(basegroup)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)

      ! Reading 3D data
      ndim = 3

      ! Get number of grid points in each direction
      call this%parallel%max(hi,maxhi)
      call this%parallel%min(lo,minlo)

      ! Check for invalid arrays
      npoint = sum(real(hi,wp)-real(lo,wp)+real(1,wp))
      if (npoint.lt.0) then
        dims = int(0,kind=HSIZE_T)
      else
        dims = int(hi-lo+1,kind=HSIZE_T)
      end if

      ! Open dataset
      call H5Dopen_f(obj, trim(adjustl(name)), did, ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to open dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Get the global data space from file
      call H5Dget_space_f(did,sidg,ierr)

      ! Create the local data space
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset = int(lo - minlo,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Select all elements from the local data space
      offset = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_COLLECTIVE_F,ierr )

      ! Read dataset
      select type(array)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      end select
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to read dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Close dataset
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property lsit
      call H5Pclose_f(plist,ierr)

      return
    end subroutine hdf5_obj_Read3D
    impure subroutine hdf5_obj_Read1D(this,basegroup,name,array)
      !> Read a 1D dataset located under basegroup and given by name
      use iso_c_binding
      implicit none
      class(hdf5_obj),  intent(in) :: this                 !! A HDF5 object
      character(len=*), intent(in) :: basegroup            !! Parent group containing the dataset
      character(len=*), intent(in) :: name                 !! Dataset name
      class(*),         intent(out):: array(:,:,:)         !! Data array
      ! Work variables
      integer(HID_T)   :: did
      integer(HID_T)   :: sidc
      integer(HID_T)   :: sidg
      integer(HID_T)   :: plist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(1)
      integer(HSIZE_T) :: offset(1)
      integer          :: ndim
      integer,          &
           allocatable :: npoint_proc(:),buff(:)
      integer(HID_T)   :: obj
      integer          :: ierr
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(basegroup)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)


      ! Open dataset
      call H5Dopen_f(obj, trim(adjustl(name)), did, ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to open dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Get the global data space from file
      call H5Dget_space_f(did,sidg,ierr)

      ! Check data is 1D
      call H5Sget_simple_extent_ndims_f(sidg,ndim,ierr)
      if (ndim.ne.1) &
        call this%parallel%stop("Dimension mismatch for dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Get number of grid points in each direction
      allocate(npoint_proc(this%parallel%nproc))
      allocate(buff(this%parallel%nproc))
      npoint_proc = 0
      npoint_proc(this%parallel%rank%mine) = size(array)
      call this%parallel%sum(npoint_proc, buff); npoint_proc=buff

      ! Create the local data space
      dims(1) = int(size(array),kind=HSIZE_T)
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset(1) = int(sum(npoint_proc(1:this%parallel%rank%mine-1)),kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Select all elements from the local data space
      offset = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_COLLECTIVE_F,ierr )

      ! Read dataset
      select type(array)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
        call H5Dread_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      end select
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to read dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Close dataset
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property lsit
      call H5Pclose_f(plist,ierr)

      return
    end subroutine hdf5_obj_Read1D
    impure subroutine hdf5_obj_Write3D(this,groupname,name,array,lo,hi)
      !> Write Eulerian/3D data to a HDF5 file
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: name              !! Variable name
      class(*),         intent(in)    :: array(:,:,:)      !! 3-D data array
      integer,          intent(in)    :: lo(3)             !! Low bounds
      integer,          intent(in)    :: hi(3)             !! High bounds
      ! Work variables
      integer          :: ierr
      integer(HID_T)   :: sidc,sidg
      integer(HID_T)   :: did
      integer(HID_T)   :: plist
      integer(HID_T)   :: dplist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(3)
      integer(HSIZE_T) :: offset(3)
      integer          :: ndim
      integer          :: maxhi(3)
      integer          :: minlo(3)
      integer(HID_T)   :: obj
      real(wp)         :: npoint
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(groupname)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)

      ! writing 3d arrays
      ndim = 3

      ! Get number of grid points in each direction
      call this%parallel%max(hi,maxhi)
      call this%parallel%min(lo,minlo)

      ! Check for invalid arrays
      npoint = sum(real(hi,wp)-real(lo,wp)+real(1,wp))
      if (npoint.lt.0) then
        dims = int(0,kind=HSIZE_T)
      else
        dims = int(hi-lo+1,kind=HSIZE_T)
      end if

      ! Create the global data space
      call H5Screate_simple_f(ndim,int(maxhi-minlo+1,kind=HSIZE_T),sidg,ierr)

      ! Create the local data space
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset = int(lo - minlo,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Select all elements from the local data space
      offset = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Create property list for data set creation
      call H5Pcreate_f(H5P_DATASET_CREATE_F,dplist,ierr)
      call H5Pset_fill_time_f(dplist,H5D_FILL_TIME_NEVER_F,ierr)

      ! Create data space
      select type(array)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
      end select
      call H5Dcreate_f(obj, trim(adjustl(name)), dtype, sidg, did, ierr)

      ! Close property list
      call H5Pclose_f(dplist,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_COLLECTIVE_F,ierr )

      ! Write data set
      select type(array)
      type is (real(leapDP))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (real(leapSP))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer)
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer(leapI8))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      end select

      ! Close data set
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property list
      call H5Pclose_f(plist,ierr)
      return
    end subroutine hdf5_obj_Write3D
    impure subroutine hdf5_obj_Write1D(this,groupname,name,array)
      !> Write an array/1D data to a HDF5 file.
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: name              !! Variable name
      class(*),         intent(in)    :: array(:)          !! 1-D data array
      ! Work variables
      integer          :: ierr
      integer(HID_T)   :: sidc,sidg
      integer(HID_T)   :: did
      integer(HID_T)   :: plist
      integer(HID_T)   :: dplist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(1)
      integer(HSIZE_T) :: offset(1)
      integer          :: ndim
      integer(HID_T)   :: obj
      integer,          &
           allocatable :: npoint_proc(:),buff(:)
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(groupname)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)

      ! writing 1d array
      ndim = 1

      ! Get number of grid points in each direction
      allocate(npoint_proc(this%parallel%nproc))
      allocate(buff(this%parallel%nproc))
      npoint_proc = 0
      npoint_proc(this%parallel%rank%mine) = size(array)
      call this%parallel%sum(npoint_proc, buff); npoint_proc=buff

      ! Check for invalid arrays
      dims = int(size(array),kind=HSIZE_T)

      ! Create the global data space
      call H5Screate_simple_f(ndim,[integer(kind=HSIZE_T):: int(size(array),kind=HSIZE_T)],sidg,ierr)

      ! Create the local data space
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset(1) = int(sum(npoint_proc(1:this%parallel%rank%mine-1)),kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Select all elements from the local data space
      offset(1) = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Create property list for data set creation
      call H5Pcreate_f(H5P_DATASET_CREATE_F,dplist,ierr)
      call H5Pset_fill_time_f(dplist,H5D_FILL_TIME_NEVER_F,ierr)

      ! Create data space
      select type(array)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
      end select
      call H5Dcreate_f(obj, trim(adjustl(name)), dtype, sidg, did, ierr)

      ! Close property list
      call H5Pclose_f(dplist,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_COLLECTIVE_F,ierr )

      ! Write data set
      select type(array)
      type is (real(leapDP))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (real(leapSP))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer)
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      type is (integer(leapI8))
        call H5Dwrite_f(did,dtype,array,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
      end select

      ! Close data set
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property list
      call H5Pclose_f(plist,ierr)
      return
    end subroutine hdf5_obj_Write1D
    impure subroutine hdf5_obj_ReadCoord(this,groupname,name,Coord)
      !> Read coordinates from HDF5 file. Only the root MPI rank
      ! does the reading, and then broadcasts to other MPI ranks.
      implicit none
      class(hdf5_obj),  intent(in)    :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: name              !! Variable name
      class(*),         intent(out)   :: Coord(:)          !! 1-D Coordinates
      ! Work variables
      integer          :: ierr
      integer(HID_T)   :: sidc,sidg
      integer(HID_T)   :: did
      integer(HID_T)   :: plist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(1)
      integer(HSIZE_T) :: offset(1)
      integer          :: ndim
      integer(HID_T)   :: obj
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(groupname)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)

      ! writing 1d array
      ndim = 1

      ! Dimension
      dims = int(size(Coord),kind=HSIZE_T)

      ! Open dataset
      call H5Dopen_f(obj, trim(adjustl(name)), did, ierr)
      if (ierr.ne.0) &
        call this%parallel%stop("Unable to open dataset "//trim(adjustl(name))//" under "//groupname_//" in HDF5 file "//this%filename)

      ! Get the global data space from file
      call H5Dget_space_f(did,sidg,ierr)

      ! Create the local data space
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Select all elements from the local data space
      offset = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_INDEPENDENT_F,ierr )

      if (this%parallel%rank%mine.eq.1) then
        ! Read data set
        select type(Coord)
        type is (real(leapDP))
          dtype = H5T_NATIVE_DOUBLE
          call H5Dread_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        type is (real(leapSP))
          dtype = H5T_NATIVE_REAL
          call H5Dread_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        type is (integer)
          dtype = H5T_NATIVE_INTEGER
          call H5Dread_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        end select
      end if

      ! Broad cast
      select type (Coord)
      type is (real(WP))
        call this%parallel%bcast(Coord)
      type is (integer)
        call this%parallel%bcast(Coord)
      end select

      ! Close dataset
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property lsit
      call H5Pclose_f(plist,ierr)
      return
    end subroutine hdf5_obj_ReadCoord
    impure subroutine hdf5_obj_WriteCoord(this,groupname,name,Coord)
      !> Write coordinates to HDF5 file. Only the root MPI rank
      ! does the writing.
      implicit none
      class(hdf5_obj),  intent(inout) :: this              !! A HDF5 object
      character(len=*), intent(in)    :: groupname         !! Groupname
      character(len=*), intent(in)    :: name              !! Variable name
      class(*),         intent(in)    :: Coord(:)          !! 1-D Coordinates
      ! Work variables
      integer          :: ierr
      integer(HID_T)   :: sidc,sidg
      integer(HID_T)   :: did
      integer(HID_T)   :: plist
      integer(HID_T)   :: dplist
      integer(HID_T)   :: dtype
      integer(HSIZE_T) :: dims(1)
      integer(HSIZE_T) :: offset(1)
      integer          :: ndim
      integer(HID_T)   :: obj
      character(len=:), &
           allocatable :: groupname_

      ! Make sure basegroup is properly formatted,
      ! with '/' at the begining and end
      groupname_=this%FixGroupName(groupname)

      ! Find parent object
      obj = this%GetGroupObject(groupname_)

      ! writing 1d array
      ndim = 1

      ! Dimension
      dims = int(size(Coord),kind=HSIZE_T)

      ! Create the global data space
      call H5Screate_simple_f(ndim,[integer(kind=HSIZE_T):: int(size(Coord),kind=HSIZE_T)],sidg,ierr)

      ! Create the local data space
      call H5Screate_simple_f(ndim,dims,sidc,ierr)

      ! Select a subset of the global data space
      offset(1) = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidg,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Select all elements from the local data space
      offset(1) = int(0,kind=HSIZE_T)
      call H5Sselect_hyperslab_f(sidc,H5S_SELECT_SET_F,offset,dims,ierr )

      ! Create property list for data set creation
      call H5Pcreate_f(H5P_DATASET_CREATE_F,dplist,ierr)
      call H5Pset_fill_time_f(dplist,H5D_FILL_TIME_NEVER_F,ierr)

      ! Create data space
      select type(Coord)
      type is (real(leapDP))
        dtype = H5T_NATIVE_DOUBLE
      type is (real(leapSP))
        dtype = H5T_NATIVE_REAL
      type is (integer)
        dtype = H5T_NATIVE_INTEGER
      type is (integer(leapI8))
        dtype = H5T_STD_I64LE
      end select
      call H5Dcreate_f(obj, trim(adjustl(name)), dtype, sidg, did, ierr)

      ! Close property list
      call H5Pclose_f(dplist,ierr)

      ! Create property list
      call H5Pcreate_f(H5P_DATASET_XFER_F, plist, ierr )
      call H5Pset_dxpl_mpio_f(plist,H5FD_MPIO_INDEPENDENT_F,ierr )

      if (this%parallel%rank%mine.eq.1) then
        ! Write data set
        select type(Coord)
        type is (real(leapDP))
          call H5Dwrite_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        type is (real(leapSP))
          call H5Dwrite_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        type is (integer)
          call H5Dwrite_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        type is (integer(leapI8))
          call H5Dwrite_f(did,dtype,Coord,dims,ierr,mem_space_id=sidc,file_space_id=sidg,xfer_prp=plist)
        end select
      end if

      ! Close data set
      call H5Dclose_f(did,ierr)

      ! Close data spaces
      call H5Sclose_f(sidc,ierr)
      call H5Sclose_f(sidg,ierr)

      ! Close property list
      call H5Pclose_f(plist,ierr)
      return
    end subroutine hdf5_obj_WriteCoord
    pure function hdf5_obj_FixGroupName(groupname) result(val)
      !> Function that will append and prepend '/' if missing
      implicit none
      character(len=*), intent(in) :: groupname
      character(len=:), allocatable :: val
      ! Work variables
      integer :: n

      val=trim(adjustl(groupname))
      if (val(1:1).ne.'/') val = '/'//val

      n=len(val)
      if (val(n:n).ne.'/') val = val//'/'
      return
    end function
end module leapIO_hdf5
