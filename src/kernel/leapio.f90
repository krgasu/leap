!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapIO
  !>--------------------------------------------------------------------------
  ! Module: leapIO
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Collection of IO modules
  ! --------------------------------------------------------------------------
  use leapIO_mpi,   only: ngadata_obj
  use leapIO_hdf5,  only: hdf5_obj
  use leapIO_h5hut, only: h5hut_obj
  use leapIO_silo,  only: silo_obj
end module leapIO
