!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapCli
  !>--------------------------------------------------------------------------
  ! Module: leapCli
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! A simple command line interface for LEAP
  ! --------------------------------------------------------------------------
  use leapKinds
  implicit none
  private
  public :: cli_obj
  type :: cli_obj
    !> Procedures to parse the command line switches
    contains
      procedure,nopass :: get     => cli_obj_get
  end type cli_obj

  contains
  subroutine cli_obj_get(switch,val,found,default)
    !> Get command line options
    implicit none
    character(len=*), intent(in)  :: switch                !! Cli switch
    class(*),         intent(out) :: val                   !! Value of the switch
    logical, optional,intent(out) :: found                 !! true if switch found
    class(*),optional,intent(in)  :: default               !! Default value
    ! Work variables
    character(len=str8):: carg
    integer :: narg
    integer :: n,ios

    ! Read program inputs
    narg=command_argument_count()
    if (narg.eq.0.and. .not.present(default)) &
      error stop "CLI: no command line switches found."

    if(present(found)) found=.false.

    n=0
    ! Read inputs
    do while (n<narg)
      n=n+1
      call get_command_argument(n,carg)

      if (trim(adjustl(carg)).eq.'-'//switch)  then
        n=n+1
        call get_command_argument(n,carg,status=ios)
        if (ios.ne.0) error stop "CLI: switch requires a valid value"

        select type (val)
        type is (real(kind=8)   )
          read(carg,*) val
        type is (real(kind=4)   )
          read(carg,*) val
        type is (integer(kind=8))
          read(carg,*) val
        type is (integer(kind=4))
          read(carg,*) val
        type is (logical)
          read(carg,*) val
        type is (character(len=*))
          val=trim(adjustl(carg))
        end select
        if(present(found)) found=.true.
        ! leave
        return
      end if
    end do

    ! Finished looping and didn't find flag
    if (present(default)) then
      select type (val)
      type is (real(kind=8))
        select type(default)
        type is (real(kind=8))
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      type is (real(kind=4))
        select type(default)
        type is (real(kind=4))
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      type is (integer(kind=8))
        select type(default)
        type is (integer(kind=8))
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      type is (integer(kind=4))
        select type(default)
        type is (integer(kind=4))
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      type is (logical)
        select type(default)
        type is (logical)
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      type is (character(len=*))
        select type(default)
        type is (character(len=*))
          val=default
        class default
          error stop "CLI: input and default value have different types"
        end select
      end select
    end if


    return
  end subroutine cli_obj_get
end module leapCli
