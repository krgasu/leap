!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapKinds
  !>--------------------------------------------------------------------------
  ! Module: leapKinds
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Defines working precision, integer and real kinds, and string sizes used
  ! in LEAP.
  ! --------------------------------------------------------------------------
  use iso_fortran_env
  implicit none
  integer, parameter :: leapSP = real32                       !! 4-Byte reals
  integer, parameter :: leapDP = real64                       !! 8-Byte reals
  integer, parameter :: leapI4 = int32                        !! 4-Byte integers
  integer, parameter :: leapI8 = int64                        !! 8-Byte integers
#ifdef USE_SP
  integer, parameter :: WP     = leapSP                       !! Working precision
#else
  integer, parameter :: WP     = leapDP                       !! Working precision
#endif
  integer, parameter :: str8   = 8
  integer, parameter :: str64  = 64
  integer, parameter :: str256 = 256
  ! Keeping the values below for backward compatibility
  integer, parameter :: str_short  = 8
  integer, parameter :: str_medium = 64
  integer, parameter :: str_long   = 8192
end module leapKinds
