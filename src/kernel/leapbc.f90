!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapBC
  !>--------------------------------------------------------------------------
  ! Module: leapBC
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Defines and manages boundary conditions.
  ! Current limitations:
  ! - Supports only second order schemes.
  ! - Neumann BC assume gradient=0.
  ! - Symmetry BC are not implemented yet.
  !
  ! These limitations will be lifted as the need arises.
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapBlock
  use leapEulerian
  use leapParallel
  use mpi_f08
  use leapUtils, only : hashtbl_obj
  implicit none

  private
  public :: bc_set
  public :: BC_WALL, BC_INFLOW, BC_OUTFLOW, BC_DIRICHLET, BC_NEUMANN, BC_SYMMETRY
  public :: extent_obj

  ! Parameters
  integer, parameter :: BC_UNDEFINED = 0
  integer, parameter :: BC_WALL      = 1
  integer, parameter :: BC_INFLOW    = 2
  integer, parameter :: BC_OUTFLOW   = 3
  integer, parameter :: BC_DIRICHLET = 4
  integer, parameter :: BC_NEUMANN   = 5
  integer, parameter :: BC_SYMMETRY  = 7

  integer, parameter :: BC_LEFT   = 0
  integer, parameter :: BC_RIGHT  = 1

  type :: bc_obj
    !> Specified a boundary condition for a variable
    ! The BC type can be among the predefined types
    ! (BC_WALL, BC_INFLOW, etc). Val(:,:,:) stores
    ! the imposed values for BC_DIRICHLET, or the
    ! gradient in the normal for direction  for
    ! BC_NEUMANN. Be mindful of staggering:
    ! E.g. if the variable is cell-centered (e.g. P)
    ! and the BC is on BC_LEFT in the 1-DIR, then
    ! val(i,j,k) is on x(i),ym(j),zm(k). For a
    ! face-centered variable like V (on xm(i),y(j),
    ! zm(k)) if the BC is on BC_LEFT in the 1-DIR,
    ! then val(i,j,k) is on x(i),y(j),zm(k).
    character(str8)   :: name                              !! Name of variable
    integer           :: type   = BC_UNDEFINED             !! Type of BC
    real(wp), pointer :: val(:,:,:)                        !! Values on the parent region
  end type bc_obj

  type :: extent_obj
    !> Extents of a region
    integer        :: lo(3) = -1                           !! Low bound
    integer        :: hi(3) = -1                           !! High bound
  end type extent_obj

  type :: region_obj
    !> Defines regions where boundary conditions
    ! are applied. Regions must be 2D planes in 3D simulations,
    ! defined by their extent xlo, xhi, and the normal to
    ! the plane (which gives side and dir)
    ! E.g.: a region with normal '+x1' (dir=1,side=BC_LEFT)
    ! looks like this:
    !
    !        side   side
    !      BC_LEFT BC_RIGHT
    !          ______
    ! xhi(2)  |      |
    !         |      | ilo(2)=3
    !         |______|
    !         |      |
    !         |      | ilo(2)=2
    !         |______|
    !         |      |
    !         |      | ilo(1)=1
    ! xlo(2)  |______|_____________> dir=1
    !         ^
    !      Region on
    !      this side
    character(len=:), allocatable :: name                  !! Name of this region
    type(block_obj)               :: region                !! Block/grid for this region
    real(wp)                      :: xlo(3)                !! Position of lower left corner
    real(wp)                      :: xhi(3)                !! Position of upper right corner
    integer                       :: lo(3)=-100            !! Grid low bound
    integer                       :: hi(3)=-100            !! Grid hi bound
    integer                       :: dir                   !! Normal direction (=1,2,3)
    integer                       :: side                  !! Side (BC_LEFT or BC_RIGHT)
    integer                       :: count=0               !! Count of variables defined on this region
    type(bc_obj),     allocatable :: BC(:)                 !! Variables on this region
    type(hashtbl_obj),  private   :: tbl                   !! Hash table
    contains
       procedure :: Initialize      => region_obj_Init
       procedure :: Finalize        => region_obj_Final
       procedure :: Add             => region_obj_Add
       procedure :: Expand          => region_obj_Expand
       procedure :: GetBCIndex      => region_obj_GetBCIndex
  end type region_obj

  type :: bc_set
    !> Collection of regions
    type(region_obj), allocatable:: region(:)              !! Array of regions
    type(parallel_obj), pointer  :: parallel => null()     !! Associated parallel structure
    type(block_obj),    pointer  :: block    => null()     !! Associated block structure
    type(hashtbl_obj),  private  :: tbl                    !! Hash table
    integer                      :: count  = 0             !! Total number of regions across all MPI ranks
    contains
      procedure :: Initialize          => bc_set_Init
      procedure :: Finalize            => bc_set_Final
      procedure :: Add                 => bc_set_Add
      procedure :: Expand              => bc_set_Expand
      procedure :: UpdateExtents       => bc_set_UpdateExtents
      procedure :: BuildMask           => bc_set_BuildMask
      procedure :: GetRegionIndex      => bc_set_GetRegionIndex
      procedure :: GetExtents          => bc_set_GetExtents
      procedure :: SetBC               => bc_set_SetBC
      procedure :: GetSideDirByNormal  => bc_set_GetSideDirByNormal
      procedure :: GetSideDirByRegion  => bc_set_GetSideDirByRegion
      procedure :: GetBCType           => bc_set_GetBCType
      procedure :: GetBCPointer        => bc_set_GetBCPointer
      procedure :: UpdateBoundary      => bc_set_UpdateBoundary
      procedure :: UpdateBoundaryDirichlet &
                                       => bc_set_UpdateBoundaryDirichlet
      procedure :: UpdateBoundaryNeumann &
                                       => bc_set_UpdateBoundaryNeumann
      procedure :: Info                => bc_set_Info
      procedure :: Write               => bc_set_Write
      procedure :: Read                => bc_set_Read
      procedure, nopass :: CheckBounds => bc_set_CheckBounds
      procedure :: CheckBCExists       => bc_set_CheckBCExists
  end type bc_set

  ! Default hash table size
  integer, parameter :: BC_SET_HTBL_SIZE = 20
  contains
    pure subroutine region_obj_Init(this,name,xlo,xhi,dir,side)
      !> Initializes a region
      class(region_obj), intent(inout) :: this             !! A Region
      character(len=*),  intent(in)    :: name             !! Name of region
      real(wp),          intent(in)    :: xlo(3)           !! Position of low left corner
      real(wp),          intent(in)    :: xhi(3)           !! Position of high right corner
      integer,           intent(in)    :: dir              !! Direction of normal
      integer,           intent(in)    :: side             !! Side (left or right) of the cell

      this%name   = name
      this%xlo    = xlo
      this%xhi    = xhi
      this%dir    = dir
      this%side   = side

      call this%tbl%initialize(BC_SET_HTBL_SIZE)
      return
    end subroutine region_obj_Init
    pure subroutine region_obj_Final(this)
      ! Finalizes a region
      class(region_obj), intent(inout) :: this             !! A Region
      ! Work variable
      integer :: n

      if (allocated(this%name)) deallocate(this%name)
      do n=1,this%count
        if (associated(this%BC(n)%val)) deallocate(this%BC(n)%val)
        this%BC(n)%val => null()
      end do
      if (allocated(this%BC))   deallocate(this%BC)

      call this%tbl%finalize
      return
    end subroutine region_obj_Final
    pure subroutine region_obj_Expand(this)
      !> Resize array to accomodate a new element
      class(region_obj), intent(inout) :: this             !! A Region
      ! Work variables
      type(bc_obj),  allocatable :: tmp_array(:)

      ! First resize arrays to accomodate a new element
      if (.not.allocated(this%BC)) then
        allocate(this%BC(1))
        this%count = 1
      else
        ! New size
        this%count = size(this%BC) + 1

        ! Allocate temporary array
        allocate(tmp_array(this%count))

        ! Store old values
        tmp_array(1:this%count-1) = this%BC(:)

        ! Move the allocation from the
        ! temporary array to the final one
        call move_alloc(tmp_array,this%BC)
      end if

      return
    end subroutine region_obj_Expand
    pure subroutine region_obj_Add(this,name,type,extents)
      !> Adds a new variable to region
      class(region_obj), intent(inout) :: this             !! A Region
      character(len=*),  intent(in)    :: name             !! Region name
      integer,           intent(in)    :: type             !! BC type
      type(extent_obj),  intent(in)    :: extents          !! Region extents

      ! First resize arrays to accomodate a new element
      call this%Expand()

      ! Add to hash table
      call this%tbl%put(key=this%tbl%HashString(name),val=this%count)

      ! Store the new BC
      this%BC(this%count)%name = name
      this%BC(this%count)%type = type

      select case(type)
        case (BC_DIRICHLET, BC_NEUMANN)
          ! Allocate val
          associate(lo => extents%lo, hi => extents%hi)
            allocate(this%BC(this%count)%val(lo(1):hi(1),lo(2):hi(2),lo(3):hi(3)))
            this%BC(this%count)%val = 0.0_wp
          end associate
        case default
          ! Do not allocate
      end select

      return
    end subroutine region_obj_Add
    pure function region_obj_GetBCIndex(this,name) result(val)
      !> Returns index of a variable in this region, or -1
      ! if not found.
      class(region_obj), intent(in) :: this                !! A Region
      character(len=*),  intent(in) :: name                !! Name of region
      integer :: val
      call this%tbl%get(key=this%tbl%HashString(name),val=val)
      return
    end function region_obj_GetBCIndex

    impure subroutine bc_set_Init(this,block,parallel)
      !> Initializes bc_set
      class(bc_set),              intent(inout) :: this    !! Boundary conditions
      type(block_obj),    target, intent(in)    :: block   !! A block object
      type(parallel_obj), target, intent(in)    :: parallel!! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      call this%tbl%initialize(BC_SET_HTBL_SIZE)
      return
    end subroutine bc_set_Init
    pure subroutine bc_set_Final(this)
      !> Finalizes bc_set
      class(bc_set), intent(inout) :: this                 !! Boundary conditions
      ! Work variables
      integer :: n

      ! Nullify pointers
      this%parallel => null()
      this%block    => null()

      ! Clear regions
      if (allocated(this%region)) then
        do n=1,this%count
        call this%region(n)%Finalize
        end do

        deallocate(this%region)
        this%count = 0
      end if

      ! Clear hash table
      call this%tbl%finalize

      return
    end subroutine bc_set_Final
    pure subroutine bc_set_Expand(this)
      !> Resize array to accomodate a new element
      class(bc_set), intent(inout) :: this                 !! Boundary conditions
      ! Work bariables
      type(region_obj),  allocatable :: tmp_array(:)

      if (.not.allocated(this%region)) then
        allocate(this%region(1))
        this%count = 1
      else
        ! New size
        this%count = size(this%region)+1

        ! Allocate temporary array
        allocate(tmp_array(this%count))

        ! Store old values
        tmp_array(1:this%count-1) = this%region(:)

        ! Move the allocation from the
        ! temporary array to the final one
        call move_alloc(tmp_array,this%region)
      end if

      return
    end subroutine bc_set_Expand
    impure subroutine bc_set_Add(this,name,xlo,xhi,normal)
      !> Add a new region to bc_set
      class(bc_set),    intent(inout) :: this              !! Boundary conditions
      character(len=*), intent(in)    :: name              !! Name of region
      real(wp),         intent(in)    :: xlo(3)            !! Position of lower left corner
      real(wp),         intent(in)    :: xhi(3)            !! Position of upper right corner
      character(len=*), intent(in)    :: normal            !! Oriented normal
      ! Work variables
      integer :: side
      integer :: dir
      logical :: is_valid

      ! Check that region bounds are valid
      is_valid = this%CheckBounds(xlo,xhi)
      if (.not. is_valid) call this%parallel%stop("Invalid boundary region: "//trim(adjustl(name)))

      ! First resize array to accomodate a new element
      call this%Expand()

      ! Get side and direction from normal
      call this%GetSideDirByNormal(normal,side,dir)

      ! Initialize new region
      call this%region(this%count)%initialize(name, xlo, xhi, dir, side)

      ! Add to hash table
      call this%tbl%put(key=this%tbl%HashString(name),val=this%count)

      ! Update the extents
      call this%UpdateExtents(name)
      return
    end subroutine bc_set_Add

    pure subroutine bc_set_UpdateExtents(this,name)
      !> Finds the intersection between block owned
      ! by this MPI rank, and the plane defining the region
      class(bc_set),    intent(inout) :: this              !! Boundary conditions
      character(len=*), intent(in)    :: name              !! Region name
      ! Work variable
      integer :: ind
      logical :: overlap
      real(wp):: xlo(3),xhi(3)
      integer :: i,j,k
      real(wp):: normal(3)
      real(wp):: ds

      ! Get index of region
      ind = this%GetRegionIndex(name)

      ! Initialize indices
      this%region(ind)%lo =  huge(1)/100
      this%region(ind)%hi = -huge(1)/100

      associate (lo => this%block%lo, hi => this%block%hi,    &
        x => this%block%x, y => this%block%y, z=>this%block%z )

        xlo  = this%region(ind)%xlo
        xhi  = this%region(ind)%xhi

        ds   = 0.5_wp*minval(this%block%dx)

        normal = 0.0_wp
        select case (this%region(ind)%side)
        case (BC_LEFT)
          normal(this%region(ind)%dir) =  1.0_wp
          xhi = xhi + ds*normal
        case (BC_RIGHT)
          normal(this%region(ind)%dir) = -1.0_wp
          xlo = xlo + ds*normal
        end select


        do k=lo(3),hi(3)
          do j=lo(2),hi(2)
            do i=lo(1),hi(1)
              overlap = .true.
              if ( x(i) .ge. xhi(1) .or. xlo(1) .ge. x(i+1) ) overlap=.false.
              if ( y(j) .ge. xhi(2) .or. xlo(2) .ge. y(j+1) ) overlap=.false.
              if ( z(k) .ge. xhi(3) .or. xlo(3) .ge. z(k+1) ) overlap=.false.
              if (overlap) then
                this%region(ind)%lo(1) = min(this%region(ind)%lo(1),i)
                this%region(ind)%hi(1) = max(this%region(ind)%hi(1),i)

                this%region(ind)%lo(2) = min(this%region(ind)%lo(2),j)
                this%region(ind)%hi(2) = max(this%region(ind)%hi(2),j)

                this%region(ind)%lo(3) = min(this%region(ind)%lo(3),k)
                this%region(ind)%hi(3) = max(this%region(ind)%hi(3),k)
              end if
          end do
          end do
        end do
      end associate

      return
    end subroutine bc_set_UpdateExtents
    impure subroutine bc_set_BuildMask(this,name,mask)
      !> Build an integer field, where cells=0 denotes interior
      ! cells, and cells=1 denotes boundary cells for input variable
      use leapeulerian
      class(bc_set),        intent(in)    :: this          !! Boundary conditions
      character(len=*),     intent(in)    :: name          !! Name of the variable
      type(eulerian_obj_i), intent(inout) :: mask          !! Mask for this variable
      ! Work variables
      type(extent_obj) :: extents
      integer          :: i,j,k
      integer          :: n,ind
      logical          :: found

      ! Initialize all cell to be interior cells
      mask%cell = 1

      ! Leave, if not regions found
      if (.not.allocated(this%region)) return

      ! Loop over regions
      do n=1,this%count

        ! Get index of variable within this region
        ! Check Whether we have a BC for this variable on this region
        found = this%CheckBCExists(this%region(n)%name,name)
        if (.not.found) cycle

        ! If variable found, get its index
        ind = this%region(n)%GetBCIndex(name)

        ! Get extents of this region
        extents = this%GetExtents(this%region(n)%name)

        ! Mark cells as boundary cells
        if (this%region(n)%dir.eq.mask%staggering.and.this%region(n)%side.eq.BC_LEFT) then
          do concurrent (k=extents%lo(3):extents%hi(3),j=extents%lo(2):extents%hi(2),i=extents%lo(1):extents%hi(1))
            mask%cell(i,j,k) = 0
          end do
        end if
      end do

      call mask%UpdateGhostCells()
      return
    end subroutine bc_set_BuildMask
    pure function bc_set_CheckBounds(xlo,xhi) result(val)
      !> Make sure bounds represent a plane
      real(wp), intent(in) :: xlo(3)                       !! Lower left corner
      real(wp), intent(in) :: xhi(3)                       !! Upper right corner
      logical :: val                                       !! True, if bounds represent a plane
      ! Work variables
      real(wp):: vol

      ! Compute volume of this region
      vol = (xhi(1)-xlo(1))*(xhi(2)-xlo(2))*(xhi(3)-xlo(3))

      if (vol.eq.0)  then
        val = .true.
      else
        val = .false.
      end if
    end function bc_set_CheckBounds
    pure function bc_set_CheckBCExists(this,region,var) result(val)
      !> Check whether there is BC for a given variable on
      ! a given region
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: region               !! Region name
      character(len=*), intent(in) :: var                  !! Variable name
      logical :: val
      ! Work variable
      integer :: ind_reg, ind_var

      ind_reg = this%GetRegionIndex(region)
      ind_var = this%region(ind_reg)%GetBCIndex(var)

      if (ind_var.le.0) then
        val=.false.
      else
        val = .true.
      end if
      return
    end function bc_set_CheckBCExists
    pure function bc_set_GetExtents(this,name) result (extents)
      !> Returns the extents (lo and hi bounds) of a region.
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: name                 !! Region name
      type(extent_obj):: extents
      ! Work variable
      integer :: ind

      ! Get index of region
      ind = this%GetRegionIndex(name)
      extents%lo = this%region(ind)%lo
      extents%hi = this%region(ind)%hi
      return
    end function bc_set_GetExtents
    pure function bc_set_GetRegionIndex(this,name) result(val)
      !> Returns the index of a region, or -1 if not found.
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: name                 !! Region name
      integer :: val
      call this%tbl%get(key=this%tbl%HashString(name),val=val)
      return
    end function bc_set_GetRegionIndex
    impure subroutine bc_set_GetSideDirByNormal(this,normal,side,dir)
      ! Finds the direction and side from a string containing
      ! the normal direction
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: normal               !! String denoting the normal direction
      integer,          intent(out):: dir                  !! Direction (=1,2,3,)
      integer,          intent(out):: side                 !! Side (=BC_LEFT,BC_RIGHT)

      select case (trim(adjustl(normal)))
      case ('+x1','+X1','+x','+X')
        dir = 1; side = BC_LEFT
      case ('-x1','-X1','-x','-X')
        dir = 1; side = BC_RIGHT
      case ('+x2','+X2','+y','+Y')
        dir = 2; side = BC_LEFT
      case ('-x2','-X2','-y','-Y')
        dir = 2; side = BC_RIGHT
      case ('+x3','+X3','+z','+Z')
        dir = 3; side = BC_LEFT
      case ('-x3','-X3','-z','-Z')
        dir = 3; side = BC_RIGHT
      case default
        call this%parallel%stop("Invalid value for BC normal")
      end select
      return
    end subroutine bc_set_GetSideDirByNormal
    pure subroutine bc_set_GetSideDirByRegion(this,region,side,dir)
      ! Finds the direction and side from a string containing
      ! the normal direction
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: region               !! Region name
      integer,          intent(out):: dir                  !! Direction (=1,2,3,)
      integer,          intent(out):: side                 !! Side (=BC_LEFT,BC_RIGHT)
      ! Work variable
      integer :: ind

      ind  = this%GetRegionIndex(region)
      side = this%region(ind)%side
      dir  = this%region(ind)%dir
      return
    end subroutine bc_set_GetSideDirByRegion
    pure function bc_set_GetBCType(this,region,var) result(val)
      ! Finds the direction and side from a string containing
      ! the normal direction
      class(bc_set),    intent(in) :: this                 !! Boundary conditions
      character(len=*), intent(in) :: region               !! Region name
      character(len=*), intent(in) :: var                  !! Variable name
      integer :: val
      ! Work variable
      integer :: ind_reg
      integer :: ind_var

      ind_reg =  this%GetRegionIndex(region)
      ind_var =  this%region(ind_reg)%GetBCIndex(var)

      val = this%region(ind_reg)%BC(ind_var)%type
      return
    end function bc_set_GetBCType
    impure subroutine bc_set_GetBCPointer(this,region,var,val)
      !> Fetches a pointer to the val array describing the
      ! Dirichlet or Neumann BC of a given variable on a given region.
      class(bc_set),     intent(in)   :: this              !! Boundary conditions
      character(len=*),  intent(in)   :: region            !! Region name
      character(len=*),  intent(in)   :: var               !! Variable name
      real(wp), pointer, intent(inout):: val(:,:,:)        !! Pointer
      ! Work variable
      integer :: ind_reg
      integer :: ind_var

      if (associated(val)) val => null()

      ind_reg =  this%GetRegionIndex(region)
      ind_var =  this%region(ind_reg)%GetBCIndex(var)
      val     => this%region(ind_reg)%BC(ind_var)%val
      return
    end subroutine bc_set_GetBCPointer
    impure subroutine bc_set_SetBC(this,region,type,var)
      !> Set boundary condition of a given type, for a given
      ! variable on a given region.
      class(bc_set),              intent(inout) :: this    !! Boundary conditions
      character(len=*),           intent(in)    :: region  !! Region name
      integer,                    intent(in)    :: type    !! BC type
      character(len=*), optional, intent(in)    :: var     !! Variable name
      ! Work variable
      type(extent_obj) :: extents
      integer          :: ind_reg

      ! Get index of region and its extents
      ind_reg = this%GetRegionIndex(region)
      extents = this%GetExtents(region)

      select case (type)
        case (BC_WALL     )
          call this%region(ind_reg)%Add('U',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('V',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('W',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('P',BC_NEUMANN,  extents)
        case (BC_INFLOW   )
          call this%region(ind_reg)%Add('U',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('V',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('W',BC_DIRICHLET,extents)
          call this%region(ind_reg)%Add('P',BC_NEUMANN,  extents)
        case (BC_OUTFLOW  )
          call this%region(ind_reg)%Add('U',BC_NEUMANN,  extents)
          call this%region(ind_reg)%Add('V',BC_NEUMANN,  extents)
          call this%region(ind_reg)%Add('W',BC_NEUMANN,  extents)
          call this%region(ind_reg)%Add('P',BC_NEUMANN,  extents)
        case (BC_DIRICHLET)
          if (.not.present(var)) &
            call this%parallel%stop("Unable to apply Dirichlet BC without providing the name of the variable")
            call this%region(ind_reg)%Add(var,BC_DIRICHLET,extents)
        case (BC_NEUMANN  )
          if (.not.present(var)) &
            call this%parallel%stop("Unable to apply Neumann BC without providing the name of the variable")
            call this%region(ind_reg)%Add(var,BC_NEUMANN,extents)
        case (BC_SYMMETRY )
          ! TO-DO
      case default
        call this%parallel%stop("Uknown BC type on region:"//region)
      end select

      return
    end subroutine bc_set_SetBC
    impure subroutine bc_set_UpdateBoundary(this,var)
      !> Imposes boundary conditions for a given variable.
      class(bc_set),         intent(in)    :: this         !! Boundary conditions
      type(eulerian_obj_r),  intent(inout) :: var          !! Eulerian variable
      ! Work variables
      logical :: found
      integer :: n

      if (.not.allocated(this%region)) return

      do n=1,this%count

        ! Check Whether we have a BC for this variable on this region
        found = this%CheckBCExists(this%region(n)%name,var%name)
        if (.not.found) cycle

        select case (this%GetBCType(this%region(n)%name,var%name))
        case (BC_DIRICHLET)
          call this%UpdateBoundaryDirichlet(this%region(n)%name,var)
        case (BC_NEUMANN  )
          call this%UpdateBoundaryNeumann  (this%region(n)%name,var)
        case (BC_SYMMETRY )
          ! TO-DO
        end select
      end do

      if (this%count.ne.0.and. (.not.found)) &
        call this%parallel%stop('Unable to find any boundary conditions for variable '//trim(adjustl(var%name)))
      return
    end subroutine bc_set_UpdateBoundary
    impure subroutine bc_set_UpdateBoundaryDirichlet(this,region,var)
      !> Update ghostboundaries to enforce Dirichlet BC
      class(bc_set),         intent(in) :: this         !! Boundary conditions
      character(len=*),      intent(in)    :: region       !! Region name
      type(eulerian_obj_r),  intent(inout) :: var          !! Eulerian variable
      ! Work variables
      type(extent_obj) :: extents
      integer          :: i,j,k
      integer          :: shift(3)
      integer          :: dir
      integer          :: side
      real(wp), pointer:: val(:,:,:)

      ! Get extents
      extents = this%GetExtents(region)

      ! Get direction and side of BC
      call this%GetSideDirByRegion(region,side,dir)

      ! Get a pointer to the BC values
      call this%GetBCPointer(region,var%name,val)
      shift = 0

      if (dir.eq.var%staggering) then
        ! Treatment for face-centered fields in dir-direction
        select case (side)
        case (BC_RIGHT)
          shift(dir) = 1
        case (BC_LEFT)
          shift(dir) = 0
        end select

        associate(lo => extents%lo, hi=>extents%hi)
          do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
            var%cell(i+shift(1),j+shift(2),k+shift(3)) = val(i,j,k)
          end do
        end associate

      else

        ! Treatment for cell-centered fields in dir-direction
        select case (side)
        case (BC_RIGHT)
          shift(dir) =  1
        case (BC_LEFT)
          shift(dir) = -1
        end select

        associate(lo => extents%lo, hi=>extents%hi)
          do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
            var%cell(i+shift(1),j+shift(2),k+shift(3)) = 2.0_wp*val(i,j,k) - var%cell(i,j,k)
          end do
        end associate

      end if
      return
    end subroutine bc_set_UpdateBoundaryDirichlet
    impure subroutine bc_set_UpdateBoundaryNeumann(this,region,var)
      !> Update ghostboundaries to enforce Neumann BC
      class(bc_set),         intent(in) :: this         !! Boundary conditions
      character(len=*),      intent(in)    :: region       !! Region name
      type(eulerian_obj_r),  intent(inout) :: var          !! Eulerian variable
      ! Work variables
      type(extent_obj) :: extents
      integer          :: i,j,k
      integer          :: shift(3)
      integer          :: dir
      integer          :: side

      ! Get extents
      extents = this%GetExtents(region)

      ! Get direction and side of BC
      call this%GetSideDirByRegion(region,side,dir)

      shift = 0

      if (dir.eq.var%staggering) then
        ! Treatment for face-centered fields in dir-direction
        select case (side)
        case (BC_RIGHT)
          shift(dir) = 1
        case (BC_LEFT)
          shift(dir) = 0
        end select

        ! Assuming zero gradient
        associate(lo => extents%lo, hi=>extents%hi)
          do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
            var%cell(i+shift(1),j+shift(2),k+shift(3)) = var%cell(i,j,k)
          end do
        end associate

      else

        ! Treatment for cell-centered fields in dir-direction
        select case (side)
        case (BC_RIGHT)
          shift(dir) =  1
        case (BC_LEFT)
          shift(dir) = -1
        end select

        ! Assuming zero gradient
        associate(lo => extents%lo, hi=>extents%hi)
          do concurrent ( k=lo(3):hi(3), j=lo(2):hi(2), i=lo(1):hi(1))
            var%cell(i+shift(1),j+shift(2),k+shift(3)) = var%cell(i,j,k)
          end do
        end associate

      end if
      return
    end subroutine bc_set_UpdateBoundaryNeumann
    impure subroutine bc_set_Write(this,iter,time,name)
      !> Write bc_set to disk using HDF5.
      ! The file structure follows this convention:
      ! / (root)
      !  !-- Time
      !  !-- Iter
      !  !-- Region 1
      !     !-- xlo
      !     !-- xhi
      !     !-- dir
      !     !-- side
      !     |-- Var 1
      !        |-- Type
      !        |-- Values(:,:,:)
      !     !-- Var 2
      !         .
      !         .
      !  !-- Region 2
      !      .
      !      .
      use leapIO
      implicit none
      class(bc_set), intent(inout) :: this                 !! Boundary conditions
      integer,       intent(in)    :: iter                 !! Iteration at write
      real(wp),      intent(in)    :: time                 !! Time at write
      character(len=*),optional,intent(in):: name          !! Name of file to write
      ! Work variables
      type(hdf5_obj)       :: hdf5
      type(extent_obj)     :: extents
      character(len=:), &
               allocatable :: groupname
      character(len=:), &
               allocatable :: varname
      integer              :: n,m
      real(wp)             :: xlo(3)
      real(wp)             :: xhi(3)

      ! Initialize HDF5 utility
      if (present(name)) then
        call hdf5%Initialize(trim(adjustl(name)),'W',this%parallel)
      else
        call hdf5%Initialize('bcs.hdf5','W',this%parallel)
      end if

      ! Write number of regions
      call hdf5%WriteAttributes('/','Iteration',    iter      )
      call hdf5%WriteAttributes('/','Time',         time      )

      do n=1,this%count
        ! Create new group for this region
        groupname = this%region(n)%name
        call hdf5%CreateGroup(groupname)

        ! Write extents
        extents = this%GetExtents(groupname)
        xlo     = this%region(n)%xlo
        xhi     = this%region(n)%xhi
        call hdf5%WriteAttributes(groupname,'xlo',xlo)
        call hdf5%WriteAttributes(groupname,'xhi',xhi)
        call hdf5%WriteAttributes(groupname,'dir', this%region(n)%dir )
        call hdf5%WriteAttributes(groupname,'side',this%region(n)%side)

        ! Write boundary conditions
        do m=1,size(this%region(n)%BC)
          ! Store variable name
          varname = trim(adjustl(this%region(n)%BC(m)%name))

          ! Create group for this variable
          call hdf5%CreateGroup(trim(adjustl(groupname))//'/'//varname)

          ! Write BC type
          call hdf5%WriteAttributes(trim(adjustl(groupname))//'/'//varname,'type',this%region(n)%BC(m)%type)

          select case (this%region(n)%BC(m)%type)
            case (BC_WALL     )
              ! Nothing to do
            case (BC_INFLOW   )
              ! Nothing to do
            case (BC_OUTFLOW  )
              ! Nothing to do
            case (BC_DIRICHLET)
              call hdf5%Write((trim(adjustl(groupname))//'/'//varname),'values',this%region(n)%BC(m)%val,extents%lo,extents%hi)
            case (BC_NEUMANN  )
              call hdf5%Write((trim(adjustl(groupname))//'/'//varname),'values',this%region(n)%BC(m)%val,extents%lo,extents%hi)
            case (BC_SYMMETRY )
              ! Nothing to do
          end select
          ! Close group
          call hdf5%CloseGroup(trim(adjustl(groupname))//'/'//varname)
        end do

        ! Close the group
        call hdf5%CloseGroup(groupname)
      end do

      call hdf5%Finalize()
      return
    end subroutine bc_set_Write
    impure subroutine bc_set_Read(this,iter,time,name)
      !> Read boundary conditions from file
      use leapIO
      implicit none
      class(bc_set), intent(inout) :: this                 !! Boundary conditions
      integer,       intent(out)   :: iter                 !! Iteration read from file
      real(wp),      intent(out)   :: time                 !! Time read from file
      character(len=*),optional,intent(in):: name          !! Name of file to read
      ! Work variables
      type(hdf5_obj)       :: hdf5
      type(extent_obj)     :: extents
      character(len=str64),&
                allocatable:: gnames(:)
      character(len=str64),&
                allocatable:: vnames(:)
      character(len=:), &
                allocatable:: group_
      integer              :: vtype
      integer              :: n,m

      real(wp)             :: xlo(3)
      real(wp)             :: xhi(3)
      integer              :: side
      integer              :: dir
      character(len=3)     :: normal
      real(wp), pointer    :: dataptr(:,:,:)

      ! Initialize HDF5 utility
      if (present(name)) then
        call hdf5%Initialize(trim(adjustl(name)),'R',this%parallel)
      else
        call hdf5%Initialize('bcs.hdf5','R',this%parallel)
      end if

      ! Write number of regions
      call hdf5%ReadAttributes('/','Iteration',    iter      )
      call hdf5%ReadAttributes('/','Time',         time      )

      call hdf5%ReadGroupNames('/',gnames)

      if (allocated(gnames)) then
        do n=1,size(gnames)

          ! Open group
          call hdf5%OpenGroup(trim(adjustl(gnames(n))))

          ! Read extents of region
          call hdf5%ReadAttributes(trim(adjustl(gnames(n))),'xlo',  xlo )
          call hdf5%ReadAttributes(trim(adjustl(gnames(n))),'xhi',  xhi )
          call hdf5%ReadAttributes(trim(adjustl(gnames(n))),'dir',  dir )
          call hdf5%ReadAttributes(trim(adjustl(gnames(n))),'side', side)

          ! Rebuild normal
          if (side.eq.BC_RIGHT) normal(1:1) = '-'
          if (side.eq.BC_LEFT ) normal(1:1) = '+'
          write(normal(2:3),fmt='(a1,i1)')'x',dir

          ! Add region
          call this%Add(trim(adjustl(gnames(n))),xlo,xhi,normal)

          ! Get region extents
          extents = this%GetExtents(trim(adjustl(gnames(n))))

          ! Read groups under this group
          call hdf5%ReadGroupNames('/'//trim(adjustl(gnames(n)))//'/',vnames)

          do m=1,size(vnames)
            ! Path to this variable
            group_ = trim(adjustl(gnames(n)))//'/'//trim(adjustl(vnames(m)))

            ! Open subgroup
            call hdf5%OpenGroup(group_)

            ! Read type
            call hdf5%ReadAttributes(group_,'type',vtype)

            ! Read values
            select case (vtype)
              case (BC_WALL     )
                ! Nothing to do
              case (BC_INFLOW   )
                ! Nothing to do
              case (BC_OUTFLOW  )
                ! Nothing to do
              case (BC_DIRICHLET)
                ! Create boundary condition
                call this%region(n)%Add(trim(adjustl(vnames(m))), BC_DIRICHLET, extents)
                ! Get a pointer to data values
                call this%GetBCPointer(trim(adjustl(gnames(n))),trim(adjustl(vnames(m))),dataptr)
                ! Read values
                call hdf5%Read(group_,'values',dataptr,extents%lo,extents%hi)
              case (BC_NEUMANN  )
                ! Create boundary condition
                call this%region(n)%Add(trim(adjustl(vnames(m))), BC_NEUMANN,   extents)
                ! Get a pointer to data values
                call this%GetBCPointer(trim(adjustl(gnames(n))),trim(adjustl(vnames(m))),dataptr)
                ! Read values
                call hdf5%Read(group_,'values',dataptr,extents%lo,extents%hi)
              case (BC_SYMMETRY )
                ! Nothing to do
            end select

            ! Close subgroup
            call hdf5%CloseGroup(group_)
          end do

          ! Close group
          call hdf5%CloseGroup(trim(adjustl(gnames(n))))
        end do
      end if

      call hdf5%Finalize()
      return
    end subroutine bc_set_Read
    impure subroutine bc_set_Info(this)
      !> Print to stdout information on bc_set, for debugging
      use iso_fortran_env, only : stdout => output_unit
      class(bc_set), intent(inout) :: this                 !! Boundary conditions
      ! work variable
      integer :: i,m

      if (allocated(this%region)) then
        write(stdout,"(a20,a, i16)") 'Nbr Regions'         , repeat('-',19)//'>', this%count
        write(stdout,"(a)")repeat('-',40)

        do i=1,this%count
          write(stdout,"(a20,a, a)"  )    'Region name',      repeat('-',19)//'>', this%region(i)%name
          write(stdout,"(a20,a, 3f16.4)") 'Region xlo',       repeat('-',19)//'>', this%region(i)%xlo
          write(stdout,"(a20,a, 3f16.4)") 'Region xhi',       repeat('-',19)//'>', this%region(i)%xhi
          write(stdout,"(a20,a, 3i16)"  ) 'Region lo',        repeat('-',19)//'>', this%region(i)%lo
          write(stdout,"(a20,a, 3i16)"  ) 'Region hi',        repeat('-',19)//'>', this%region(i)%hi
          write(stdout,"(a25,a, i16)")    'Region Dir',       repeat('-',14)//'>', this%region(i)%dir
          write(stdout,"(a25,a, i16)")    'Region Side',      repeat('-',14)//'>', this%region(i)%side
          write(stdout,"(a20,a, i16)")    'Region var count', repeat('-',19)//'>', this%region(i)%count
          do m=1,this%region(i)%count
            write(stdout,"(a25,a, a)"  )   'var'            , repeat('-',14)//'>', this%region(i)%BC(m)%name
            write(stdout,"(a25,a, i16)")   'Type'           , repeat('-',14)//'>', this%region(i)%BC(m)%type
            write(stdout,"(a25,a, l16)")   'associated(val)', repeat('-',14)//'>', associated(this%region(i)%BC(m)%val)
          end do
          write(stdout,"(a)")repeat('-',40)
        end do
      else
        write(stdout,"(a)") 'No regions defined'
      end if
      return
    end subroutine bc_set_Info
end module leapBC
