!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2021 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapSolver
  !>--------------------------------------------------------------------------
  ! Module: leapSolver
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Module of abstract objets defining Lagrangian data structure
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapTimer
  implicit none

  private
  public :: solver_obj

  type,abstract :: solver_obj
    !> Base solver object
    character(len=:), allocatable :: name                  !! Name of this solver
    type(parser_obj),   pointer   :: parser  =>null()      !! Associated parser
    type(parallel_obj), pointer   :: parallel=>null()      !! Associated parallel structure
    type(timer_obj),    pointer   :: timer   =>null()      !! Associated timer
    contains
      procedure(solver_initialize), deferred :: Initialize
      procedure(solver_finalize),   deferred :: Finalize
      procedure(solver_inicond),    deferred :: SetInitialConditions
      procedure(solver_prepare),    deferred :: PrepareSolver
      procedure(solver_advance),    deferred :: AdvanceSolution
      procedure(solver_monitor),    deferred :: Monitor
      procedure(solver_restart),    deferred :: WriteRestartData
      procedure(solver_output),     deferred :: WriteOutputData
  end type solver_obj

  abstract interface
    subroutine solver_initialize(this,timer,parallel,parser)
      !> Initialize solver
      import solver_obj,parallel_obj,parser_obj,timer_obj
      class(solver_obj), intent(inout)     :: this         !! A LEAP solver
      type(timer_obj),   target,intent(in) :: timer        !! Timer utility
      type(parallel_obj),target,intent(in) :: parallel     !! Parallel machinery
      type(parser_obj),  target,intent(in) :: parser       !! Parser for input file
    end subroutine solver_initialize
    subroutine solver_finalize(this)
      !> Finalize solver
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_finalize
    subroutine solver_inicond(this)
      !> Set initial conditions at n=0
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_inicond
    subroutine solver_prepare(this)
      !> Prepare data before run
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_prepare
    subroutine solver_advance(this)
      !> Advance solution from t to t+dt
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_advance
    subroutine solver_monitor(this)
      !> Compute and write monitored values
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_monitor
    subroutine solver_restart(this)
      !> Write simulation data to disk required for restarts
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_restart
    subroutine solver_output(this)
      !> Write single precision data needed for visualizations
      import solver_obj
      class(solver_obj), intent(inout) :: this             !! A LEAP solver
    end subroutine solver_output
  end interface
end module leapSolver
