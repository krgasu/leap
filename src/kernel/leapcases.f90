!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapCases
  !>--------------------------------------------------------------------------
  ! Module: leapcases
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Abstract simulation cases for use with LEAP solvers
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParser
  use leapParallel
  use leapBlock
  implicit none

  private
  public :: case_obj

  type,abstract :: case_obj
    !> An abstract simulation case
    character(len=str64)        :: name                    !! Name of simulation case
    type(block_obj),    pointer :: block   =>null()        !! Block information
    type(parallel_obj), pointer :: parallel=>null()        !! Associated parallel obj
    type(parser_obj),   pointer :: parser  =>null()        !! Associated parser
    contains
      procedure :: Initialize  => case_obj_init
      procedure :: Finalize    => case_obj_final
  end type case_obj
  contains
    subroutine case_obj_init(this,block,parallel,parser)
      !> Initialize the solver
      implicit none
      class (case_obj),         intent(inout) :: this      !! Simulation case
      type(block_obj),   target,intent(in)    :: block     !! Associated block
      type(parallel_obj),target,intent(in)    :: parallel  !! Associated parallel obj
      type(parser_obj),  target,intent(in)    :: parser    !! Parser
      this%block   => block
      this%parallel=> parallel
      this%parser  => parser
      return
    end subroutine case_obj_init
    subroutine case_obj_final(this)
      !> Finalize the solver
      implicit none
      class (case_obj), intent(inout) :: this
      this%block   => null()
      this%parallel=> null()
      this%parser  => null()
      return
    end subroutine case_obj_final
end module leapCases

