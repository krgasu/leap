!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapIO_h5hut
  !>--------------------------------------------------------------------------
  ! Module: leapIO_h5hut
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Read/write data in the HDF5 format using H5hut
  ! --------------------------------------------------------------------------
  use h5hut
  use leapKinds
  use leapParallel
  implicit none

  private
  public :: h5hut_obj

  type :: h5hut_obj
    !> A utility to read/write files in HDF5 using H5hut
    type(parallel_obj), pointer :: parallel=>null()        !! Associated parallel structure
    integer(leapI8)             :: fid                     !! File identifier
    character(len=str64)        :: filename                !! file to read/write
    contains
      procedure :: Initialize          => h5hut_Init
      procedure :: Finalize            => h5hut_Final
      procedure :: Open                => h5hut_Open
      procedure :: Close               => h5hut_Close
      procedure :: NewTimeStep         => h5hut_NewTimeStep
      procedure :: LastTimeStep        => h5hut_LastTimeStep
      procedure :: JumpToStep          => h5hut_JumpToStep
      procedure :: StepCount           => h5hut_StepCount
      procedure :: GetNPoints          => h5hut_GetNPoints
      procedure :: GetNFields          => h5hut_GetNFields
      procedure :: WriteGrid           => h5hut_WriteGrid
      generic   :: Write               => h5hut_Write1D,h5hut_WriteScalar3D
      generic   :: Read                => h5hut_Read1D,h5hut_ReadScalar3D
      generic   :: WriteAttributes     => h5hut_WriteAttributes0D,h5hut_WriteAttributes1D
      generic   :: ReadAttributes      => h5hut_ReadAttributes0D,h5hut_ReadAttributes1D
      procedure :: Flush               => h5hut_Flush
      procedure, private :: h5hut_Write1D
      procedure, private :: h5hut_WriteScalar3D
      procedure, private :: h5hut_Read1D
      procedure, private :: h5hut_ReadScalar3D
      procedure, private :: h5hut_WriteAttributes1D
      procedure, private :: h5hut_WriteAttributes0D
      procedure, private :: h5hut_ReadAttributes1D
      procedure, private :: h5hut_ReadAttributes0D
  end type

  contains
    subroutine h5hut_Init(this,filename,access_flag,parallel)
      !> Initialize structure
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in)    :: filename          !! File to read/write
      character(len=*), intent(in)    :: access_flag       !! File access mode
      type(parallel_obj), target,intent(in):: parallel     !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel

      ! Set the file name
      this%filename=filename

      ! Open file
      call this%Open(access_flag)

      return
    end subroutine h5hut_Init
    subroutine h5hut_Final(this)
      !> Finalize structure
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object

      ! Close file
      call this%Close

      ! Nullify pointers
      this%parallel => null()

      return
    end subroutine h5hut_Final
    subroutine h5hut_Open(this,flag)
      !> Open a hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in) :: flag
      ! Work variables
      integer(leapI8) :: ACCESS_FLAG

      select case(trim(adjustl(flag)))
      case ("RW")
        ACCESS_FLAG = H5_O_RDWR
      case ("W" )
        ACCESS_FLAG = H5_O_WRONLY
      case ("R" )
        ACCESS_FLAG = H5_O_RDONLY
      case default
        ACCESS_FLAG = H5_O_RDONLY
      end select

      this%fid  = h5_Openfile (this%filename, ACCESS_FLAG, H5_PROP_DEFAULT)

      return
    end subroutine h5hut_Open
    subroutine h5hut_Close(this)
      !> Close hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      ! Work variables
      integer(leapI8) :: ierr

      ierr = h5_Closefile (this%fid)
      return
    end subroutine h5hut_Close
    subroutine h5hut_NewTimeStep(this,iter,time,label_iter,label_time)
      !> Create a new time step and update attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer,          intent(in)    :: iter              !! iteration counter
      real(WP),         intent(in)    :: time              !! iteration counter
      character(len=*), optional,     &
                        intent(in)    :: label_iter        !! Optional iteration label
      character(len=*), optional,     &
                        intent(in)    :: label_time        !! Optional time label
      ! Work variables
      integer(leapI8) :: step
      integer(leapI8) :: ierr

      ! Increment by a new time step
      step = h5_getnsteps(this%fid) + 1
      ierr = h5_setstep(this%fid,step)

      ! Set time step attributes
      if (present(label_time)) then
        ierr = h5_writestepattrib_r8(this%fid,label_time,(/real(time,leapDP)/), int(1,leapI8))
      else
        ierr = h5_writestepattrib_r8(this%fid, 'Time',   (/real(time,leapDP)/), int(1,leapI8))
      end if
      if (present(label_iter)) then
        ierr = h5_writestepattrib_i4(this%fid, label_iter,(/iter/),             int(1,leapI8))
      else
        ierr = h5_writestepattrib_i4(this%fid, 'Iter',    (/iter/),             int(1,leapI8))
      end if
      return
    end subroutine h5hut_NewTimeStep
    subroutine h5hut_WriteAttributes0D(this,label,val)
      !> Write scalar attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(in)    :: val               !! Optional time label
      ! Work variables
      integer(leapI8) :: ierr

      ! Move to latest time step
      ierr = h5_setstep(this%fid,h5_getnsteps(this%fid))

      select type(val)
      type is (real(leapDP))
        ierr = h5_writestepattrib_r8(this%fid,trim(adjustl(label)),(/val/), int(1,leapI8))
      type is (real(leapSP))
        ierr = h5_writestepattrib_r4(this%fid,trim(adjustl(label)),(/val/), int(1,leapI8))
      type is (integer)
        ierr = h5_writestepattrib_i4(this%fid,trim(adjustl(label)),(/val/), int(1,leapI8))
      type is (integer(leapI8))
        ierr = h5_writestepattrib_i8(this%fid,trim(adjustl(label)),(/val/), int(1,leapI8))
      end select

      return
    end subroutine h5hut_WriteAttributes0D
    subroutine h5hut_WriteAttributes1D(this,label,val)
      !> Write an array of attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(in)    :: val(:)            !! Optional time label
      ! Work variables
      integer(leapI8) :: ierr

      ! Move to latest time step
      ierr = h5_setstep(this%fid,h5_getnsteps(this%fid))

      select type(val)
      type is (real(leapDP))
        ierr = h5_writestepattrib_r8(this%fid,trim(adjustl(label)),val, int(size(val),leapI8))
      type is (real(leapSP))
        ierr = h5_writestepattrib_r4(this%fid,trim(adjustl(label)),val, int(size(val),leapI8))
      type is (integer)
        ierr = h5_writestepattrib_i4(this%fid,trim(adjustl(label)),val, int(size(val),leapI8))
      type is (integer(leapI8))
        ierr = h5_writestepattrib_i8(this%fid,trim(adjustl(label)),val, int(size(val),leapI8))
      end select

      return
    end subroutine h5hut_WriteAttributes1D
    subroutine h5hut_ReadAttributes0D(this,label,val)
      !> Read scalar attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(out)   :: val               !! Optional time label
      ! Work variables
      integer(leapI8) :: ierr
      real(leapDP)    :: bufr8(1)
      real(leapSP)    :: bufr4(1)
      integer         :: bufi4(1)
      integer(leapI8) :: bufi8(1)

      ! Move to latest time step
      ierr = h5_setstep(this%fid,h5_getnsteps(this%fid))

      select type(val)
      type is (real(leapDP))
        ierr = h5_readstepattrib_r8(this%fid,trim(adjustl(label)),bufr8)
        val=bufr8(1)
      type is (real(leapSP))
        ierr = h5_readstepattrib_r4(this%fid,trim(adjustl(label)),bufr4)
        val=bufr4(1)
      type is (integer)
        ierr = h5_readstepattrib_i4(this%fid,trim(adjustl(label)),bufi4)
        val=bufi4(1)
      type is (integer(leapI8))
        ierr = h5_readstepattrib_i8(this%fid,trim(adjustl(label)),bufi8)
        val=bufi8(1)
      end select

      return
    end subroutine h5hut_ReadAttributes0D
    subroutine h5hut_ReadAttributes1D(this,label,val)
      !> Read an array of attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in)    :: label             !! attribute label
      class(*),         intent(out)   :: val(:)            !! Optional time label
      ! Work variables
      integer(leapI8) :: ierr

      ! Move to latest time step
      ierr = h5_setstep(this%fid,h5_getnsteps(this%fid))

      select type(val)
      type is (real(leapDP))
        ierr = h5_readstepattrib_r8(this%fid,trim(adjustl(label)),val)
      type is (real(leapSP))
        ierr = h5_readstepattrib_r4(this%fid,trim(adjustl(label)),val)
      type is (integer)
        ierr = h5_readstepattrib_i4(this%fid,trim(adjustl(label)),val)
      type is (integer(leapI8))
        ierr = h5_readstepattrib_i8(this%fid,trim(adjustl(label)),val)
      end select

      return
    end subroutine h5hut_ReadAttributes1D
    subroutine h5hut_LastTimeStep(this,iter,time,label_iter,label_time)
      !> Get information about the last time step
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer,          intent(out)   :: iter              !! last step
      real(WP),         intent(out)   :: time              !! Time of last step
      character(len=*), optional,     &
                        intent(in)    :: label_iter        !! Optional iteration label
      character(len=*), optional,     &
                        intent(in)    :: label_time        !! Optional time label
      ! Work variables
      integer       :: atti(1)
      real(WP)      :: attr(1)
      integer(leapI8)  :: step
      integer(leapI8)  :: ierr

      ! Jump to last step
      step = h5_getnsteps(this%fid)
      ierr = h5_setstep(this%fid,step)

      ! Read time step attributes
      if (present(label_time)) then
        ierr = h5_readstepattrib_r8(this%fid, label_time,attr )
      else
        ierr = h5_readstepattrib_r8(this%fid, 'Time',    attr )
      end if
      if (present(label_iter)) then
        ierr = h5_readstepattrib_i4(this%fid, label_iter,atti )
      else
        ierr = h5_readstepattrib_i4(this%fid, 'Iter',    atti )
      end if

      time=attr(1)
      iter=atti(1)
      return
    end subroutine h5hut_LastTimeStep
    function h5hut_StepCount(this) result (steps4)
      !> Return number of time steps
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer                         :: steps4            !! Number of steps
      ! Work variable
      integer(leapI8)  :: steps8

      ! Return number of steps
      steps8 = h5_getnsteps(this%fid)

      steps4=int(steps8,leapI4)

      return
    end function h5hut_StepCount
    subroutine h5hut_JumpToStep(this,step,iter,time,label_iter,label_time)
      !> Jump to a specific time step
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer,          intent(in)    :: step              !! Time step
      integer,          intent(out)   :: iter              !! Iteration number at this time step
      real(WP),         intent(out)   :: time              !! Time at this time step
      character(len=*), optional,     &
                        intent(in)    :: label_iter        !! Optional iteration label
      character(len=*), optional,     &
                        intent(in)    :: label_time        !! Optional time label
      ! Work variables
      integer          :: atti(1)
      real(WP)         :: attr(1)
      integer(leapI8)  :: ierr

      ! Jump to specific time step
      ierr = h5_setstep(this%fid,int(step,leapI8))

      ! Read time step attributes
      if (present(label_time)) then
        ierr = h5_readstepattrib_r8(this%fid, label_time,attr )
      else
        ierr = h5_readstepattrib_r8(this%fid, 'Time',    attr )
      end if
      if (present(label_iter)) then
        ierr = h5_readstepattrib_i4(this%fid, label_iter,atti )
      else
        ierr = h5_readstepattrib_i4(this%fid, 'Iter',    atti )
      end if

      time=attr(1)
      iter=atti(1)

      return
    end subroutine h5hut_JumpToStep
    subroutine h5hut_GetNPoints(this,npoints)
      !> Get number of data points in step
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer,          intent(out)   :: npoints           !! Number of data points

      npoints = int(h5pt_GetNPoints(this%fid))

      return
    end subroutine h5hut_GetNPoints
    subroutine h5hut_GetNFields(this,nfields)
      !> Get number of fields in step
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      integer,          intent(out)   :: nfields           !! Number of fields

      nfields = int(h5bl_getnumfields(this%fid))

      return
    end subroutine h5hut_GetNFields
    subroutine h5hut_Flush(this)
      !> Flush step data to disk
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      ! Work variables
      integer(leapI8) :: ierr

      ierr = h5_Flushstep (this%fid)
      return
    end subroutine h5hut_Flush
    subroutine h5hut_WriteGrid(this,name,xlo,dx)
      !> Write the grid attributes
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      character(len=*), intent(in) :: name                 !! Variable name
      real(wp),         intent(in) :: xlo(3)               !! Coordinate of the low bound
      real(wp),         intent(in) :: dx(3)                !! Grid spacing
      ! Work variables
      integer(leapI8) :: ierr
      real(leapDP)     :: xlo_(3),dx_(3)

      xlo_=real(xlo, leapDP)
      dx_ =real(dx,  leapDP)

      ! Set block attributes
      ierr = h5bl_writefieldattrib_r8(this%fid,trim(adjustl(name)),"__Origin__",xlo_,int(3,leapI8))
      ierr = h5bl_writefieldattrib_r8(this%fid,trim(adjustl(name)),"__Spacing__",dx_,int(3,leapI8))

      return
    end subroutine h5hut_WriteGrid
    subroutine h5hut_ReadScalar3D(this,name,array,lo,hi)
      !> Read Eulerian/3D data from a hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      class(*),         intent(out):: array(:,:,:)         !! 3-D data array
      character(len=*), intent(in) :: name                 !! Variable name
      integer,          intent(in) :: lo(3)                !! Low bounds
      integer,          intent(in) :: hi(3)                !! High bounds
      ! Work variables
      integer(leapI8) :: ierr
      integer(leapI8) :: lo_(3),hi_(3)

      lo_ =int( lo, leapI8)
      hi_ =int( hi, leapI8)

      ! Set view
      ierr = h5bl_3d_setview(this%fid,lo_(1),hi_(1),lo_(2),hi_(2),lo_(3),hi_(3))

      ! Read data
      select type(array)
      type is (real(leapDP))
        ierr = h5bl_3d_read_scalar_field_r8(this%fid,trim(adjustl(name)), array)
      type is (real(leapSP))
        ierr = h5bl_3d_read_scalar_field_r4(this%fid,trim(adjustl(name)), array)
      type is (integer)
        ierr = h5bl_3d_read_scalar_field_i4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI8))
        ierr = h5bl_3d_read_scalar_field_i8(this%fid,trim(adjustl(name)), array)
      end select

      return
    end subroutine h5hut_ReadScalar3D
    subroutine h5hut_WriteScalar3D(this,name,array,lo,hi)
      !> Write Eulerian/3D data to a hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      class(*),         intent(in)    :: array(:,:,:)      !! 3-D data array
      character(len=*), intent(in)    :: name              !! Variable name
      integer,          intent(in)    :: lo(3)             !! Low bounds
      integer,          intent(in)    :: hi(3)             !! High bounds
      ! Work variables
      integer(leapI8) :: ierr
      integer(leapI8) :: lo_(3),hi_(3)

      lo_ =int( lo, leapI8)
      hi_ =int( hi, leapI8)

      ! Set view
      ierr = h5bl_3d_setview(this%fid,lo_(1),hi_(1),lo_(2),hi_(2),lo_(3),hi_(3))

      ! Write data
      select type(array)
      type is (real(leapDP))
        ierr = h5bl_3d_write_scalar_field_r8(this%fid,trim(adjustl(name)), array)
      type is (real(leapSP))
        ierr = h5bl_3d_write_scalar_field_r4(this%fid,trim(adjustl(name)), array)
      type is (integer)
        ierr = h5bl_3d_write_scalar_field_i4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI8))
        ierr = h5bl_3d_write_scalar_field_i8(this%fid,trim(adjustl(name)), array)
      end select

      return
    end subroutine h5hut_WriteScalar3D
    subroutine h5hut_Read1D(this,name,array)
      !> Read Lagrangian/1D data fom a hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      class(*),         intent(out)   :: array(:)          !! 1-D data array
      character(len=*), intent(in)    :: name              !! Variable name
      ! Work variables
      integer(leapI8) :: ierr

      ! Set number of particles this mpi rank will read
      ierr = h5pt_setnpoints(this%fid, int(size(array),leapI8))

      ! Read data
      select type(array)
      type is (real(leapDP))
        ierr = h5pt_readdata_r8(this%fid,trim(adjustl(name)), array)
      type is (real(leapSP))
        ierr = h5pt_readdata_r4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI4))
        ierr = h5pt_readdata_i4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI8))
        ierr = h5pt_readdata_i8(this%fid,trim(adjustl(name)), array)
      end select
      return
    end subroutine h5hut_Read1D
    subroutine h5hut_Write1D(this,name,array)
      !> Write Lagrangian/1D data to a hdf5 file with h5hut
      implicit none
      class(h5hut_obj), intent(inout) :: this              !! A H5hut object
      class(*),         intent(in)    :: array(:)          !! 1-D data array
      character(len=*), intent(in)    :: name              !! Variable name
      ! Work variables
      integer(leapI8) :: ierr

      ! Set number of particles this mpi rank will write
      ierr = h5pt_setnpoints(this%fid, int(size(array),leapI8))

      ! Write data
      select type(array)
      type is (real(leapDP))
        ierr = h5pt_writedata_r8(this%fid,trim(adjustl(name)), array)
      type is (real(leapSP))
        ierr = h5pt_writedata_r4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI4))
        ierr = h5pt_writedata_i4(this%fid,trim(adjustl(name)), array)
      type is (integer(leapI8))
        ierr = h5pt_writedata_i8(this%fid,trim(adjustl(name)), array)
      end select
      return
    end subroutine h5hut_Write1D
end module leapIO_h5hut
