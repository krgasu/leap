!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapParser
  !>--------------------------------------------------------------------------
  ! Module: leapPaser
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Simple parser to read inputs from ASCII files
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapCli
  implicit none
  private
  public :: parser_obj

  integer, parameter         :: MAX_LINE_SIZE    = 4096    !! Maximum number of characters in a line
  integer, parameter         :: MAX_LABEL_SIZE   = 128     !! Maximum number of characters in a label
  integer, parameter         :: TAB_ICHAR        = 9       !! ichar of a tab character
  character(len=3),parameter :: COMMENT_SYMBOLS  ='!#%'    !! Allowed comment symbols
  character(len=1),parameter :: SEPARATOR        =':'      !! Character used to seperate label form value

  type :: entry_obj
    !> Object representing an entry in the input file
    character(len=:),allocatable :: label                  !! Column label
    character(len=:),allocatable :: value                  !! Value
  end type entry_obj

  type :: parser_obj
    !> Type represents a parser that can be used
    ! to get information from an input file
    type(entry_obj), allocatable :: entries(:)             !! Entries in an input file
    contains
      procedure :: Initialize               => parser_obj_Init
      procedure :: Finalize                 => parser_obj_Final
      procedure :: ParseFile                => parser_obj_ParseFile
      generic   :: Get                      => parser_obj_read0D,parser_obj_read1D
      procedure :: IsDefined                => parser_obj_IsDefined
      procedure :: Print                    => parser_obj_Print
      procedure, private :: ParseLine       => parser_obj_ParseLine
      procedure, private :: AddEntry        => parser_obj_AddEntry
      procedure, private :: FetchLabelID    => parser_obj_FetchLabelID
      generic  , private :: AssignDefault   => parser_obj_AssignDefault0D,parser_obj_AssignDefault1D
      procedure, private,nopass :: parser_obj_ReformatLine
      procedure, private        :: parser_obj_read0D
      procedure, private        :: parser_obj_read1D
      procedure, private,nopass :: parser_obj_AssignDefault0D
      procedure, private,nopass :: parser_obj_AssignDefault1D
  end type parser_obj

contains
  subroutine parser_obj_init(this)
    !> Initialization the praser
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object

    ! Set number of fields to zero
    if (allocated(this%entries)) deallocate(this%entries)

    return
  end subroutine parser_obj_init

  subroutine parser_obj_final(this)
    !> Finalization routine
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object

    if (allocated(this%entries)) deallocate(this%entries)
    return
  end subroutine parser_obj_final

  subroutine parser_obj_ParseFile(this,optInput)
    !> Read & parse the input file
    implicit none
    class(parser_obj),          intent(inout) :: this      !! Parser object
    character(len=*), optional, intent(in)    :: optInput  !! Optional input file name
    character(len=str64) :: input_file                     !! Input file name to use
    integer       :: fid                                   !! File ID
    type(cli_obj) :: cli                                   !! A CLI tool
    ! Work variables
    character(len=MAX_LINE_SIZE) :: line                   !! A line from the file
    integer :: ierr

    ! Define file name
    if (present(optInput)) then
      input_file=trim(adjustl(optInput))
    else
      call cli%get('i',input_file,default="input")
    end if

    ! Open the file
    open (newunit=fid,file=input_file,form='formatted',status='old',iostat=ierr)
    if (ierr .ne. 0) stop 'Parser : unable to open the input file.'

    ! Read file line by line
    ierr = 0
    do while (ierr .eq. 0)
      ! Read new line
      read(fid,'(a)',iostat=ierr) line
      if (ierr.ne.0) cycle

      call this%ParseLine(line)
    end do

    ! Close file
    close(fid)

    return
  end subroutine parser_obj_ParseFile

  subroutine parser_obj_ReformatLine(line)
    !> Parse a line
    implicit none
    character(MAX_LINE_SIZE),intent(inout):: line          !! A line from the input file
    ! Work variables
    integer :: comment_index                               !! Left most position of a comment symbol
    integer :: n

    ! Remove the tabs
    do n=1,MAX_LINE_SIZE
      if (ichar(line(n:n)).EQ.TAB_ICHAR) line(n:n)=' '
    end do

    ! Remove comments
    comment_index = scan(line,COMMENT_SYMBOLS)
    if (comment_index.ne.0) line(comment_index:) = ''

    ! Trim
    line = adjustl(line)

    return
  end subroutine parser_obj_ReformatLine

  subroutine parser_obj_ParseLine(this,line)
    !> Parse a line
    implicit none
    class(parser_obj), intent(inout)      :: this          !! Parser object
    character(MAX_LINE_SIZE),intent(inout):: line          !! A line from the input file
    ! Work variables
    character(MAX_LABEL_SIZE) :: label                     !! A label
    character(MAX_LINE_SIZE)  :: value                     !! A value
    integer :: separator_index                             !! Left most position of the separator
    integer :: entry_id
    type(entry_obj):: new_entry

    ! Remove tabs and comments
    call parser_obj_ReformatLine(line)

    ! Get position of separator (normally ":")
    separator_index = index(line,SEPARATOR)

    ! If found a separator, create a new entry
    if (separator_index.ne.0) then

      read(line(1:separator_index-1),'(a)') label
      read(line(separator_index+1:), '(a)') value

      if (len_trim(value).ne.0) then

        entry_id=this%FetchLabelID(label)

        if (entry_id.ne.0) then
          this%entries(entry_id)%value=trim(adjustl(value))
        else
          ! Add a new entry
          new_entry%label=trim(adjustl(label))
          new_entry%value=trim(adjustl(value))
          call this%AddEntry(new_entry)
        end if
      end if
    end if

    return
  end subroutine parser_obj_ParseLine

  subroutine parser_obj_AddEntry(this,entry)
    !> Resize entries array to add a new entry
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object
    type(entry_obj),   intent(in)    :: entry              !! New entrysize
    ! Work variables
    type(entry_obj), allocatable :: my_entries(:)
    integer                      :: size_old

    ! Resize array and add a new entry
    if (.not.allocated(this%entries)) then
      allocate(this%entries(1))
      this%entries(1)=entry
    else
      ! Get old size
      size_old = size(this%entries)

      ! Allocate temporary array
      allocate(my_entries(size_old+1))

      ! Copy values to temporary variable
      my_entries(1:size_old)=this%entries(1:size_old)

      ! Add new entry
      my_entries(size_old+1)=entry

      ! Move the allocation from the
      ! temporary array to the final one
      call move_alloc(my_entries,this%entries)
    end if

    return
  end subroutine parser_obj_AddEntry

  function parser_obj_FetchLabelID(this,label) result(id)
    !> Return ID of label in the array of entries
    ! Returns 0 if label not found.
    implicit none
    class(parser_obj), intent(in)  :: this                 !! Parser object
    character(*),      intent(in)  :: label                !! label to look for
    ! Work variables
    integer :: id
    integer :: n

    ! Initialize ID at zero (not found)
    id=0

    if (allocated(this%entries)) then
      do n=1,size(this%entries)
        if (trim(adjustl(this%entries(n)%label)).eq.trim(adjustl(label))) then
          id=n
          return
        end if
      end do

    end if

    return
  end function parser_obj_FetchLabelID

  function parser_obj_IsDefined(this,label) result(found)
    !> Check whether the field is defined
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object
    character(*),      intent(in)    :: label              !! label to look for
    logical :: found

    found=.false.
    if( this%FetchLabelID(label).ne.0) found=.true.

    return
  end function parser_obj_IsDefined

  subroutine parser_obj_print(this)
    !> Prints all variables found in the parsed file
    use iso_fortran_env, only : stdout => output_unit, &
                                stderr => error_unit
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object
    integer :: n                                           !! Iterator
    integer :: l1,l2                                       !! String lengths
    character(len=80) :: str

    if (allocated(this%entries)) then
      do n=1,size(this%entries)
        l1=len_trim(this%entries(n)%label)
        l2=len_trim(this%entries(n)%value)
        str=this%entries(n)%label(1:l1)//repeat('-',80-l1-l2)//this%entries(n)%value(1:l2)
        write(stdout,"(a80)") str
      end do
    else
      write(stderr,"(a)") "Parser: Entries not allocated"
    end if

    return
  end subroutine parser_obj_print

  subroutine parser_obj_read0D(this,label,value,default)
    !> Read value: 0D version
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object
    character(*),      intent(in)    :: label              !! Label
    class(*),          intent(out)   :: value              !! Value to return
    class(*),optional, intent(in)    :: default            !! Default value to return in case label is not found
    ! Work variables
    integer :: entry_ID
    integer :: ierr=0

    ! Get ID of label (or 0 if not found)
    entry_ID = this%FetchLabelID(label)

    if (entry_ID.eq.0.and..not.present(default)) then
      print*,'Error in Parser: '// trim(adjustl(label)) //' not found'
      stop

    else if (entry_ID.eq.0.and.present(default)) then
      ! Check default and value are of the same type
      call this%AssignDefault(value,default)

    else
      select type (value)
      type is (logical)
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (real(leapDP))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (real(leapSP))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (integer(leapI4))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (integer(leapI8))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (character(len=*))
        value=trim(adjustl(this%entries(entry_ID)%value))
      class default
          stop "Error in Parser: unknown variable type"
      end select
    end if

    if (ierr.ne.0) then
      print*,'Error in Parser: An error occured while reading the value of: '// trim(adjustl(label))
      stop
    end if

    return
  end subroutine parser_obj_read0D

  subroutine parser_obj_read1D(this,label,value,default)
    !> Read value: 1D version
    implicit none
    class(parser_obj), intent(inout) :: this               !! Parser object
    character(*),      intent(in)    :: label              !! Label
    class(*),          intent(out)   :: value(:)           !! Value to return
    class(*),optional, intent(in)    :: default(:)         !! Default value to return in case label is not found
    ! Work variables
    integer :: entry_ID
    integer :: ierr=0

    ! Get ID of label (or 0 if not found)
    entry_ID = this%FetchLabelID(label)

    if (entry_ID.eq.0.and..not.present(default)) then
      print*,'Error in Parser: '// trim(adjustl(label)) //' not found'
      stop

    else if (entry_ID.eq.0.and.present(default)) then
      ! Check default and value are of the same type
      call this%AssignDefault(value,default)

    else
      select type (value)
      type is (logical)
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (real(leapDP))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (real(leapSP))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (integer(leapI4))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (integer(leapI8))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      type is (character(len=*))
        read(this%entries(entry_ID)%value,*,iostat=ierr) value
      class default
          stop "Error in Parser: unknown variable type"
      end select
    end if

    if (ierr.ne.0) then
      print*,'Error in Parser: An error occured while reading the value of: '// trim(adjustl(label))
      stop
    end if

    return
  end subroutine parser_obj_read1D

  subroutine parser_obj_AssignDefault0D(value,default)
    !> Assing default to value: 0D version
    implicit none
    class(*),          intent(out)   :: value              !! Value to return
    class(*),          intent(in)    :: default            !! Default value to return in case label is not found
    ! Work variables
    logical :: ok

    ok=.true.
    select type(default)
    type is (logical)
      select type(value)
      type is (logical)
        value=default
      class default
        ok=.false.
      end select

    type is (real(leapDP))
      select type(value)
      type is (real(leapDP))
        value=default
      class default
        ok=.false.
      end select

    type is (real(leapSP))
      select type(value)
      type is (real(leapSP))
        value=default
      class default
        ok=.false.
      end select

    type is (integer(leapI4))
      select type(value)
      type is (integer(leapI4))
        value=default
      class default
        ok=.false.
      end select

    type is (integer(leapI8))
      select type(value)
      type is (integer(leapI8))
        value=default
      class default
        ok=.false.
      end select

    type is (character(len=*))
      select type(value)
      type is (character(len=*))
        value=default
      class default
        ok=.false.
      end select

    class default
        stop "Error in Parser: unknown default variable type"
    end select
    if (.not.ok) then
      stop "Error in Parser: default and value must be of the same type"
    end if

    return
  end subroutine parser_obj_AssignDefault0D

  subroutine parser_obj_AssignDefault1D(value,default)
    !> Assing default to value: 1D version
    implicit none
    class(*),          intent(out)   :: value(:)           !! Value to return
    class(*),          intent(in)    :: default(:)         !! Default value to return in case label is not found
    ! Work variables
    logical :: ok

    ok=.true.
    select type(default)
    type is (logical)
      select type(value)
      type is (logical)
        value=default
      class default
        ok=.false.
      end select

    type is (real(leapDP))
      select type(value)
      type is (real(leapDP))
        value=default
      class default
        ok=.false.
      end select

    type is (real(leapSP))
      select type(value)
      type is (real(leapSP))
        value=default
      class default
        ok=.false.
      end select

    type is (integer(leapI4))
      select type(value)
      type is (integer(leapI4))
        value=default
      class default
        ok=.false.
      end select

    type is (integer(leapI8))
      select type(value)
      type is (integer(leapI8))
        value=default
      class default
        ok=.false.
      end select

    type is (character(len=*))
      select type(value)
      type is (character(len=*))
        value=default
      class default
        ok=.false.
      end select

    class default
        stop "Error in Parser: unknown default variable type"
    end select
    if (.not.ok) then
      stop "Error in Parser: default and value must be of the same type"
    end if

    return
  end subroutine parser_obj_AssignDefault1D
end module leapParser
