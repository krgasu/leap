!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapMonitor
  !>--------------------------------------------------------------------------
  ! Module: leapMonitor
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! A module that writes data computed on-the-fly to the stdout and/or
  ! ascii files.
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParallel
  use leapUtils, only : hashtbl_obj, leap_create_directory
  implicit none
  private
  public :: monitor_set

  integer, parameter :: colSize = 16                       !! Size of each column
  integer, parameter :: monSize = 64                       !! Size of the monitor name
  character(len=*), parameter :: baseDirName="./monitors"  !! base directory name
  character(len=*), parameter :: baseFileName=""           !! base file name

  type :: column_obj                                       !! Column object
    character(colSize) :: label                            !! Column label
    character(colSize) :: value                            !! Value
  end type column_obj

  type :: monitor_obj                                      !! Monitor object
    type(column_obj), allocatable :: col(:)                !! Columns
    integer            :: fid                              !! Monitor IO unit
    character(monSize) :: name                             !! Name of the monitor file
    logical            :: stdout                           !! Print to stdout
    character(len=1)   :: sep                              !! Column seperator
    character(len=16)  :: FMT(4)                           !! Format specifiers
    contains
      procedure :: Initialize => monitor_obj_Init          !! Intialize a monitor object
      procedure :: Finalize   => monitor_obj_Final         !! Finalize a monitor object
      procedure :: Print      => monitor_obj_Print         !! Print the content of this monitor
      procedure :: Formats    => monitor_obj_Formats       !! Define the format specifiers
      procedure :: SetVal     => monitor_obj_SetVal        !! Set the value
  end type monitor_obj

  type :: monitor_set
    class(monitor_obj), allocatable :: m(:)                !! Array of monitors
    type(parallel_obj), pointer     :: parallel=>null()    !! Associated parallel structure
    type(hashtbl_obj),  private     :: tbl                 !! Hash table
    contains
      procedure :: Initialize => monitor_set_Init          !! Initialize the structure
      procedure :: Finalize   => monitor_set_Final         !! Finalize the structure
      procedure :: Expand     => monitor_set_Expand        !! Add a new monitor slot
      procedure :: Create     => monitor_set_Create        !! Create a new monitor
      procedure :: Set        => monitor_set_Set           !! Set the label or value of a monitor
      procedure :: GetIndex   => monitor_set_GetIndex      !! Return the index of a monitor
      procedure :: Print      => monitor_set_Print         !! Print the content of the monitors
      procedure :: Info       => monitor_set_Info          !! Print a number of diagnostics information
  end type monitor_set

  ! default hash table size
  integer, parameter :: MONITOR_SET_HTBL_SIZE = 20
  contains
    subroutine monitor_obj_SetVal(this,n,value)
      !> Finalize the monitor object
      implicit none
      class(monitor_obj),intent(inout) :: this             !! A monitor object
      integer,           intent(in)    :: n                !! Column index
      class(*),          intent(in)    :: value            !! Value

      select type (value)
      type is (logical)
        write(this%col(n)%value,fmt=this%FMT(3)) value
      type is (real(leapDP))
        write(this%col(n)%value,fmt=this%FMT(2)) value
      type is (real(leapSP))
        write(this%col(n)%value,fmt=this%FMT(2)) value
      type is (integer(leapI4))
        write(this%col(n)%value,fmt=this%FMT(1)) value
      type is (integer(leapI8))
        write(this%col(n)%value,fmt=this%FMT(1)) value
      class default
        stop "Cannot pass anything other than logical or real or integer numbers to the monitor"
      end select
      return
    end subroutine monitor_obj_SetVal

    subroutine monitor_obj_Init(this,name,ncol,sep)
      !> Intializes a single monitor
      implicit none
      class(monitor_obj), intent(inout)    :: this         !! A monitor object
      character(len=*),   intent(in)       :: name         !! Name of the monitor
      integer,            intent(in)       :: ncol         !! Number of columns
      character(len=*),optional,intent(in) :: sep          !! Number of columns

      ! Set the name
      this%name=trim(name)

      ! Set the column seperator
      if (present(sep)) then
        if(len(sep).ne.1) stop "Cannot use a separtor that is larger than one character"
        this%sep=sep
      else
        this%sep=" "
      end if

      ! Set the number of columns
      if( ncol.le.0) stop "Cannot initialize a monitor with less than 1 column"
      allocate(this%col(ncol))

      ! Initialize labels to a default value
      this%col(:)%label="UNASSIGNED"

      ! Define formats
      call this%formats

      ! Do we print to stdout or file
      if (.not.this%stdout) then
        ! Create monitor directory if it doesn't exist
        call leap_create_directory(baseDirName)
        ! Open file
        open(newunit=this%fid, file=baseDirName//'/'//baseFileName//trim(adjustl(this%name)),status="replace")
      else
        ! Connect to stdout
        this%fid=6
      end if

      return
    end subroutine monitor_obj_Init

    subroutine monitor_obj_Final(this)
      !> Finalize the monitor object
      implicit none
      class(monitor_obj), intent(inout) :: this            !! A monitor object
      deallocate(this%col)
      if (.not.this%stdout) close(this%fid)

      return
    end subroutine monitor_obj_Final

    subroutine monitor_obj_Formats(this)
      !> Define how to print numbers
      implicit none
      class(monitor_obj), intent(inout) :: this            !! A monitor object

      !< Format specifier for an integer
      write(this%FMT(1),fmt="(a,I0,a)") "(I",colSize,")"
      !< Format specifier for a real
      write(this%FMT(2),fmt="(a,I0,a)") "(ES",colSize,".6)"
      !< Format specifier for a logical
      write(this%FMT(3),fmt="(a,I0,a)") "(L",colSize,")"
      !< Format specifier for a full line
      write(this%FMT(4),fmt='(a,I0,a,I0,a)') "(",size(this%col),"(a",colSize,",a))"

      return
    end subroutine monitor_obj_Formats

    subroutine monitor_obj_Print(this,print_labels)
      !> Prints to file or stdout content of the monitor
      implicit none
      class(monitor_obj), intent(inout) :: this            !! A monitor object
      logical,            intent(in)    :: print_labels    !! True if we want to print the labels
      integer :: n

      if (print_labels) then
        ! Write output
        write (this%fid,fmt=this%FMT(4))  (trim(adjustl(this%col(n)%label)),this%sep, n=1,size(this%col))
      else
        write (this%fid,fmt=this%FMT(4))  (trim(adjustl(this%col(n)%value)),this%sep, n=1,size(this%col))
      end if
      flush(this%fid)

      return
    end subroutine monitor_obj_Print

    subroutine monitor_set_Init(this,parallel)
      !> Initialize a set of monitors
      implicit none
      class(monitor_set), intent(inout)     :: this        !! A set of monitor objects
      type(parallel_obj), target,intent(in) :: parallel    !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel

      ! Initialize hash table, with default size of 20
      call this%tbl%initialize(MONITOR_SET_HTBL_SIZE)

      return
    end subroutine monitor_set_Init

    subroutine monitor_set_Final(this)
      implicit none
      class(monitor_set), intent(inout) :: this            !! A set of monitor objects
      integer :: n                                         !! Iterator

      ! Nullify pointer
      this%parallel => null()

      ! Deallocate arrays
      if (allocated(this%m)) then
        do n=1, size(this%m)
          call this%m(n)%finalize
        end do
        deallocate(this%m)
      end if

      ! Clear hash table
      call this%tbl%finalize

      return
    end subroutine monitor_set_Final

    subroutine monitor_set_Expand(this)
      !> Changes the size of an array of monitors
      implicit none
      class(monitor_set), intent(inout) :: this            !! Set of monitor objects
      ! Work variables
      type(monitor_obj), allocatable    :: new(:)          !! Larger set
      integer :: n


      if (.not.allocated(this%m)) then
        allocate(this%m(1))
      else
        ! Create a larger array
        n=size(this%m)
        allocate(new(n+1))
        ! Copy old entries
        new(1:n)=this%m(1:n)
        ! move allocation
        call move_alloc(new,this%m)
      end if

      return
    end subroutine monitor_set_Expand

    subroutine monitor_set_Create(this,name,ncol,stdout,sep)
      !> Creates a new monitor
      implicit none
      class(monitor_set), intent(inout) :: this            !! Set of monitor objects
      character(len=*),   intent(in)    :: name            !! Name of the monitor
      integer,            intent(in)    :: ncol            !! Number of columns
      logical,optional,   intent(in)    :: stdout          !! Switch to write to the stdout
      character(len=*),optional,intent(in) :: sep          !! Number of columns
      ! Work variables
      integer :: n

      associate(mpi => this%parallel)
        if (mpi%rank%mine.eq.mpi%rank%root) then
          ! Add one monitor slot
          call this%expand
          n=size(this%m)

          ! Add to hash table
          call this%tbl%put(key=this%tbl%HashString(name),val=n)

          ! Do we write to the stdout or a file
          if (present(stdout)) then
            this%m(n)%stdout=stdout
          else
            this%m(n)%stdout=.false.
          end if

          call this%m(n)%initialize(name,ncol,sep)
        end if
      end associate
      return
    end subroutine monitor_set_Create

    subroutine monitor_set_Set(this,name,colID,value,label)
      !> Set the label or value of a column of
      ! a certain monitor
      implicit none
      class(monitor_set),       intent(inout) :: this      !! Set of monitor objects
      character(len=*),         intent(in)    :: name      !! Name of the monitor
      integer,                  intent(in)    :: colID     !! Number of columns
      character(len=*),optional,intent(in)    :: label     !! Column label
      class(*),          optional,intent(in)  :: value     !! Value
      ! Work variables
      integer :: n

      associate(mpi => this%parallel)
        if (mpi%rank%mine.eq.mpi%rank%root) then
          If (.not.present(label).and..not.present(value)) &
            call this%parallel%stop("Need to pass at least a value")

          ! Find ID of monitor
          n=this%GetIndex(name)

          ! Check column number is valid
          if (colID.gt.size(this%m(n)%col)) &
            call this%parallel%stop("Invalid column number")

          ! Set label or value
          if (present(label)) this%m(n)%col(colID)%label=label
          if (present(value)) call this%m(n)%setVal(colID,value)
        end if
      end associate
      return
    end subroutine monitor_set_Set

    function monitor_set_GetIndex(this,name) result(loc)
      !> Returns the ID of a monitor identified by name
      implicit none
      class(monitor_set), intent(inout) :: this            !! Set of monitor objects
      character(len=*),   intent(in)    :: name            !! Name of the monitor
      integer :: loc                                       !! Monitor ID

      call this%tbl%get(key=this%tbl%HashString(name),val=loc)

      if (loc.eq.-1)  call this%parallel%stop("Unable to find monitor: "//trim(adjustl(name)))
      return
    end function monitor_set_GetIndex

    subroutine monitor_set_Print(this,print_labels)
      !> Prints content of monitors
      implicit none
      class(monitor_set), intent(inout) :: this            !! Set of monitor objects
      logical, optional,  intent(in)    :: print_labels    !! True if we want to print the labels
      ! Work variables
      integer :: n

      associate(mpi => this%parallel)
        if (mpi%rank%mine.eq.mpi%rank%root) then
          if (present(print_labels)) then
            do n=1,size(this%m)
              call this%m(n)%print(print_labels)
            end do
          else
            do n=1,size(this%m)
              call this%m(n)%print(.false.)
            end do
          end if
        end if
      end associate
      return
    end subroutine monitor_set_Print

    subroutine monitor_set_Info(this)
      !> Print a number of diagnostics information
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(monitor_set), intent(inout) :: this            !! Set of monitor objects
      ! Work variables
      integer :: n

      associate(mpi => this%parallel)
        if (mpi%rank%mine.eq.mpi%rank%root) then
          write(stdout,*) 'Number of monitors = ', size(this%m)
          do n=1,size(this%m)
            write(stdout,*) 'Name = ',     this%m(n)%name
            write(stdout,*) 'FMT  = ',     this%m(n)%FMT
            write(stdout,*) 'Sep  = ', '"',this%m(n)%sep,'"'
            end do
        end if
      end associate
      return
    end subroutine monitor_set_Info
end module leapMonitor
