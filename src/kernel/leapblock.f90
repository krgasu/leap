!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapBlock
  !>--------------------------------------------------------------------------
  ! Module: leapBlock
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Defines structured blocks
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParallel
  use mpi_f08
  implicit none

  private
  public :: block_obj

  type :: axis_obj
    !> Defines a 1D axis
    !     xm(lo)               xm(hi)
    !       |                   |
    !    |-----|-----|------|------|
    !    |     <-dxm->             |
    !  x(lo)                     x(hi+1)
    integer :: lo                                          !! Lower bound
    integer :: hi                                          !! Higher bound
    integer :: ngc                                         !! Number of ghostcells
    real(wp), pointer, contiguous :: x(:)  =>null()        !! Nodal points
    real(wp), pointer, contiguous :: xm(:) =>null()        !! Mid points
    real(wp), pointer, contiguous :: dxm(:)=>null()        !! Cell spacings
    contains
      procedure :: Initialize        => axis_obj_Init
      procedure :: Finalize          => axis_obj_Final
  end type axis_obj

  type :: block_obj
    !> A block object
    type(parallel_obj), pointer   :: parallel=>null()      !! Associated parallel structure
    integer                       :: lo(3)                 !! Array lower bound
    integer                       :: hi(3)                 !! Array upper bound
    real(wp)                      :: xlo(3)                !! Coordinate of the bottom left corner
    real(wp)                      :: xhi(3)                !! Coordinate of the top right corner
    integer                       :: ngc=2                 !! Number of ghostcells
    type(axis_obj)                :: axis(3)               !! Axes in x1, x2, and x3 directions
    real(wp), pointer, contiguous :: x(:)   =>null()       !! Convenience pointer to x-nodal points
    real(wp), pointer, contiguous :: y(:)   =>null()       !! Convenience pointer to y-nodal points
    real(wp), pointer, contiguous :: z(:)   =>null()       !! Convenience pointer to z-nodal points
    real(wp), pointer, contiguous :: xm(:)  =>null()       !! Convenience pointer to x-mid points
    real(wp), pointer, contiguous :: ym(:)  =>null()       !! Convenience pointer to y-mid points
    real(wp), pointer, contiguous :: zm(:)  =>null()       !! Convenience pointer to z-mid points
    real(wp), pointer, contiguous :: dxm(:) =>null()       !! Convenience pointer to x-cell spacings
    real(wp), pointer, contiguous :: dym(:) =>null()       !! Convenience pointer to y-cell spacings
    real(wp), pointer, contiguous :: dzm(:) =>null()       !! Convenience pointer to z-cell spacings
    logical                       :: periods(3)    =.false.!! Periodicity in each direction
    logical                       :: is_partitioned=.false.!! Flag for parallel partitioning
    type(axis_obj)                :: axis_partition(3)     !! Axes of the main partition, (spacing equal to 1 sub-block)
    real(wp)                      :: dx(3)                 !! Minimum mesh spacing in each direction
    real(wp)                      :: pmin(3),pmax(3)       !! Min and max locations across all blocks
    real(wp)                      :: xmin,xmax             !! Min and max locations across all blocks
    real(wp)                      :: ymin,ymax             !! Min and max locations across all blocks
    real(wp)                      :: zmin,zmax             !! Min and max locations across all blocks
    type(MPI_Datatype)            :: gc_slab_r(3) = MPI_DATATYPE_NULL
                                                           !! MPI derived type for wp real gc
    type(MPI_Datatype)            :: gc_slab_i(3) = MPI_DATATYPE_NULL
                                                           !! MPI derived type for integer gc
    contains
      generic   :: Initialize             => block_obj_Init, block_obj_Init2
      procedure :: Finalize               => block_obj_Final
      procedure :: SetConveniencePointers => block_obj_SetConveniencePointers
      procedure :: SetPeriodicity         => block_obj_SetPeriodicity
      procedure :: SetupUniformGrid       => block_obj_SetupUniformGrid
      generic   :: UpdateGridGhostCells   => block_obj_UpdateGridGhostCells, &
                                             block_obj_UpdateGridGhostCells2
      procedure :: UpdateExtents          => block_obj_UpdateExtents
      procedure :: UpdateMidPoints        => block_obj_UpdateMidPoints
      procedure :: UpdateSpacing          => block_obj_UpdateSpacing
      procedure :: SetupMPITypes          => block_obj_SetupMPITypes
      procedure :: Partition              => block_obj_Partition
      procedure, nopass :: SubDivideBlock => block_obj_SubDivideBlock
      procedure :: Write                  => block_obj_Write
      procedure :: Read                   => block_obj_Read
      procedure :: Locate                 => block_obj_Locate
      procedure :: Info                   => block_obj_Info
      procedure, private :: block_obj_Init
      procedure, private :: block_obj_Init2
      procedure, private :: block_obj_UpdateGridGhostCells
      procedure, private :: block_obj_UpdateGridGhostCells2
  end type block_obj
  contains
    pure subroutine axis_obj_Init(this,lo,hi,ngc)
      !> Initialize axis
      class(axis_obj), intent(inout) :: this              !! A axis object
      integer,         intent(in)    :: lo                !! Array lower bound
      integer,         intent(in)    :: hi                !! Array higher bound
      integer,         intent(in)    :: ngc               !! Number of ghost cells

      this%lo  = lo
      this%hi  = hi
      this%ngc = ngc

      ! Allocate arrays
      if (associated(this%x  )) deallocate(this%x  )
      if (associated(this%xm )) deallocate(this%xm )
      if (associated(this%dxm)) deallocate(this%dxm)

      allocate(this%x  (lo-ngc:hi+1+ngc)); this%x   = 0.0_wp
      allocate(this%xm (lo-ngc:hi  +ngc)); this%xm  = 0.0_wp
      allocate(this%dxm(lo-ngc:hi  +ngc)); this%dxm = 0.0_wp

      return
    end subroutine axis_obj_Init
    pure subroutine axis_obj_Final(this)
      class(axis_obj), intent(inout) :: this              !! A axis object
      ! Free data
      if (associated(this%x  )) deallocate(this%x  )
      if (associated(this%xm )) deallocate(this%xm )
      if (associated(this%dxm)) deallocate(this%dxm)
      return
    end subroutine axis_obj_Final
    impure subroutine block_obj_Init(this,ngc,parallel)
      !> Initialize block object
      class(block_obj), intent(inout) :: this             !! A block object
      integer,          intent(in)    :: ngc              !! Number of ghostcells
      type(parallel_obj), target, &
                        intent(in)    :: parallel         !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel

      ! Store number of ghostcells
      this%ngc = ngc
      return
    end subroutine block_obj_Init
    impure subroutine block_obj_Init2(this,xlo,xhi,lo,hi,ngc,parallel)
      !> [DEPRECATED] Initialize block object
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      real(wp),         intent(in)    :: xlo(3)           !! Coordinates of the bottom left corner
      real(wp),         intent(in)    :: xhi(3)           !! Coordinates of the top right corner
      integer,          intent(in)    :: lo(3)            !! Array lower bound
      integer,          intent(in)    :: hi(3)            !! Array upper bound
      integer,          intent(in)    :: ngc              !! Number of ghostcells
      type(parallel_obj), target, intent(in):: parallel   !! parallel structure from main program

      ! Point to the master objects
      this%parallel => parallel

      ! Setup ghostcells
      this%ngc = ngc

      ! Set up the grid
      call this%SetupUniformGrid(xlo,xhi,lo,hi)

      return
    end subroutine block_obj_Init2
    impure subroutine block_obj_Final(this)
      !> Finalize the block object
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer :: dir
      integer :: ierr

      ! Finalize axes
      do dir=1,3
        call this%axis(dir)%Finalize()
      end do

      ! Nullify the convenience pointers
      ! nodal points  ; midp points      ;  spacing
      this%x => null(); this%xm => null(); this%dxm => null()
      this%y => null(); this%ym => null(); this%dym => null()
      this%z => null(); this%zm => null(); this%dzm => null()

      ! Nullify pointer to master parallel object
      this%parallel => null()

      ! Finalize partition axes
      do dir=1,3
        call this%axis_partition(dir)%Finalize()
      end do

      ! Restore defaults
      this%ngc            = 2
      this%periods(3)     = .false.
      this%is_partitioned = .false.

      ! Free up MPI derived types
      do dir=1,3
        if (this%gc_slab_r(dir).ne.MPI_DATATYPE_NULL) &
          call MPI_TYPE_FREE(this%gc_slab_r(dir),ierr)

        if (this%gc_slab_i(dir).ne.MPI_DATATYPE_NULL) &
          call MPI_TYPE_FREE(this%gc_slab_i(dir),ierr)
      end do

      return
    end subroutine block_obj_Final
    pure subroutine block_obj_SetConveniencePointers(this)
      !> Associate the convenience pointers
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! - nodal points
      this%x => this%axis(1)%x
      this%y => this%axis(2)%x
      this%z => this%axis(3)%x
      ! - midp points
      this%xm => this%axis(1)%xm
      this%ym => this%axis(2)%xm
      this%zm => this%axis(3)%xm
      ! - spacing
      this%dxm => this%axis(1)%dxm
      this%dym => this%axis(2)%dxm
      this%dzm => this%axis(3)%dxm
      return
    end subroutine block_obj_SetConveniencePointers
    pure subroutine block_obj_SetPeriodicity(this,periods)
      !> Set block periodicity in each direction
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      logical,          intent(in)    :: periods(3)       !! Periodicity
      this%periods = periods
      return
    end subroutine block_obj_SetPeriodicity
    impure subroutine block_obj_SetupUniformGrid(this,xlo,xhi,lo,hi)
      !> Initialize a uniform grid on this block
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      real(wp),         intent(in)    :: xlo(3)           !! Coordinates of the bottom left corner
      real(wp),         intent(in)    :: xhi(3)           !! Coordinates of the top right corner
      integer,          intent(in)    :: lo(3)            !! Array lower bound
      integer,          intent(in)    :: hi(3)            !! Array upper bound
      ! Work variables
      integer :: i,dir
      real(wp):: dl

      ! Setup bounds
      this%lo = lo
      this%hi = hi

      ! Initialize axes
      do dir=1,3
        call this%axis(dir)%Initialize(this%lo(dir),this%hi(dir),this%ngc)
      end do

      ! Associate pointers
      call this%SetConveniencePointers

      ! Create a uniform grid on this block
      associate (axis => this%axis)
        ! Define the nodal grid points
        do concurrent (dir=1:3)
          axis(dir)%x=0.0_wp
          dl = (xhi(dir)-xlo(dir))/real(hi(dir)-lo(dir)+1,wp)
          do concurrent (i=lo(dir):hi(dir)+1)
            axis(dir)%x(i)= xlo(dir)+ (i-lo(dir))*dl
          end do
        end do
      end associate

      ! Update values within ghostcells
      call this%UpdateGridGhostCells()

      ! Update mid points (xm)
      call this%UpdateMidPoints()

      ! Update spacing (dxm)
      call this%UpdateSpacing()

      ! Create MPI type for ghostcell communication
      call this%SetupMPITypes()

      return
    end subroutine block_obj_SetupUniformGrid
    impure subroutine block_obj_UpdateGridGhostCells(this)
      !> Updates the ghostcell values of local grid owned by
      ! the current MPI rank. Note that each MPI rank stores
      ! only its portion of the grid, thus needs to have
      ! proper ghostcell values.
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer           :: sendR, sendL
      integer           :: recvR, recvL
      real(wp)          :: L
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer           :: i,dir
      integer           :: ierr

      ! Update block dimensions
      call this%UpdateExtents()

      associate (lo => this%lo, hi => this%hi, ngc=> this%ngc, &
         axis => this%axis, parallel => this%parallel)

        do dir=1,3

          if ( (hi(dir)-lo(dir)+1).le.ngc .or. (.not.this%is_partitioned)) then

            ! Treatment for unpartitioned, 2D/1D,
            ! and pseudo-2D/1D blocks:
            ! simply extend grid into ghostcells
            if (this%periods(dir).eqv..true.) then
              L = this%pmax(dir)-this%pmin(dir)
              do i=1,ngc
                axis(dir)%x(lo(dir)-i  ) = axis(dir)%x(hi(dir)+1-i) - L
                axis(dir)%x(hi(dir)+1+i) = axis(dir)%x(lo(dir)+i)   + L
              end do
            else
              do i=1,ngc
                axis(dir)%x(lo(dir)-i  ) = axis(dir)%x(lo(dir))   - real(i,wp)*(axis(dir)%x(lo(dir)+1)-axis(dir)%x(lo(dir)))
                axis(dir)%x(hi(dir)+1+i) = axis(dir)%x(hi(dir)+1) + real(i,wp)*(axis(dir)%x(hi(dir)+1)-axis(dir)%x(hi(dir)))
              end do
            end if

          else
            ! Treatment for partitioned blocks

            ! Address of first element in buffer
            recvL= lo(dir)-ngc;  recvR=hi(dir)+2
            sendL= lo(dir)+1;    sendR=hi(dir)+1-ngc

            ! Post receives from Left and right ranks
            call MPI_IRECV( axis(dir)%x(recvL  ), ngc, parallel%REAL_WP, &
                   parallel%rank%L(dir)-1, 0, parallel%comm%g, requests(1), ierr)
            call MPI_IRECV( axis(dir)%x(recvR  ), ngc, parallel%REAL_WP, &
                   parallel%rank%R(dir)-1, 0, parallel%comm%g, requests(2), ierr)
            ! Send buffers to left and right ranks
            call MPI_ISEND( axis(dir)%x(sendR  ), ngc, parallel%REAL_WP, &
                   parallel%rank%R(dir)-1, 0, parallel%comm%g, requests(3), ierr)
            call MPI_ISEND( axis(dir)%x(sendL  ), ngc, parallel%REAL_WP, &
                   parallel%rank%L(dir)-1, 0, parallel%comm%g, requests(4), ierr)
            ! Synchronize
            call MPI_WAITALL( 4, requests, statuses, ierr )

            ! Treatment of border block
            if (this%periods(dir).eqv..true.) then
              ! Adjust grid for periodicity
              L = this%pmax(dir)-this%pmin(dir)
              ! Left border block
              if (parallel%rank%dir(dir).eq.1) axis(dir)%x(lo(dir)-ngc:lo(dir)-1) = axis(dir)%x(lo(dir)-ngc:lo(dir)-1) - L
              ! Right border block
              if (parallel%rank%dir(dir).eq.parallel%np(dir)) axis(dir)%x(hi(dir)+2:hi(dir)+1+ngc) = axis(dir)%x(hi(dir)+2:hi(dir)+1+ngc) + L
            else
              ! Extend axis in non-periodic directions
              ! Left border block
              if (parallel%rank%dir(dir).eq.1) then
                do i=1,ngc
                  axis(dir)%x(lo(dir)-i  ) = axis(dir)%x(lo(dir))   - real(i,wp)*(axis(dir)%x(lo(dir)+1)-axis(dir)%x(lo(dir)))
                end do
              end if
              ! Right border block
              if (parallel%rank%dir(dir).eq.parallel%np(dir)) then
                do i=1,ngc
                  axis(dir)%x(hi(dir)+1+i) = axis(dir)%x(hi(dir)+1) + real(i,wp)*(axis(dir)%x(hi(dir)+1)-axis(dir)%x(hi(dir)))
                end do
              end if
            end if

          end if
        end do
      end associate
      return
    end subroutine block_obj_UpdateGridGhostCells
    subroutine block_obj_UpdateGridGhostCells2(this,axis,idir)
      !> [DEPRECATED] Updates the ghostcell values of local grid owned by
      ! the current MPI rank.
      ! Note that each MPI rank stores only its
      ! portion of the grid, thus needs to have
      ! proper ghostcell values. Although SetupUniformGrid
      ! fills the ghostcells of x/y/z, it does it
      ! assuming fixed grid spacing, which may not
      ! be the correct if a non-uniform grid is used.
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      real(wp),         intent(inout) :: axis(:)
      integer,          intent(in)    :: idir
      ! Work variables
      integer :: sendR, sendL
      integer :: recvR, recvL
      integer :: npoints
      real(wp):: L
      type(MPI_STATUS)  :: statuses(4)
      type(MPI_REQUEST) :: requests(4)
      integer :: i
      integer :: ierr

      associate (lo => this%lo, hi => this%hi,   &
        ngc=> this%ngc, mpi => this%parallel)

        ! Number of grid points (excluding ghostcell)
        npoints = hi(idir)-lo(idir)+1

        if (npoints.le.ngc .or. (.not.this%is_partitioned)) then

          ! Treatment for unpartitioned, 2D/1D,
          ! and pseudo-2D/1D blocks:
          ! simply extend grid into ghostcells
          if (this%periods(idir).eqv..true.) then
            L = this%pmax(idir)-this%pmin(idir)
            do i=1,ngc
              axis(i)   = axis(size(axis)-2*ngc+(i-1)) - L
              axis(size(axis)-ngc+i) = axis(ngc+i+1)   + L
            end do
          else
            do i=1,ngc
              axis(i)   = axis(ngc+1)   - real(ngc+1-i,wp)*(axis(ngc+2)-axis(ngc+1))
              axis(size(axis)-ngc+i) = axis(size(axis)-ngc) + real(i,wp)*(axis(size(axis)-ngc)-axis(size(axis)-ngc-1))
            end do
          end if

        else
          ! Treatment for partitioned blocks

          ! Address of first element in buffer
          recvL= 1;     recvR=npoints+ngc+1
          sendL= ngc+1; sendR=npoints+ngc+1-ngc

          ! Post receives from Left and right ranks
          call MPI_IRECV( axis(recvL  ), ngc, mpi%REAL_WP, &
                          mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(1), ierr)
          call MPI_IRECV( axis(recvR  ), ngc, mpi%REAL_WP, &
                          mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(2), ierr)
          ! Send buffers to left and right ranks
          call MPI_ISEND( axis(sendR  ), ngc, mpi%REAL_WP, &
                          mpi%rank%R(idir)-1, 0, mpi%comm%g, requests(3), ierr)
          call MPI_ISEND( axis(sendL  ), ngc, mpi%REAL_WP, &
                          mpi%rank%L(idir)-1, 0, mpi%comm%g, requests(4), ierr)
          ! Synchronize
          call MPI_WAITALL( 4, requests, statuses, ierr )

          ! Treatment for periodicity
          if (this%periods(idir).eqv..true.) then
            L = this%pmax(idir)-this%pmin(idir)
            ! Left border block
            if (mpi%rank%dir(idir).eq.1) axis(recvL:recvL+ngc-1) = axis(recvL:recvL+ngc-1) - L
            ! Right border block
            if (mpi%rank%dir(idir).eq.mpi%np(idir)) axis(recvR:recvR+ngc-1) = axis(recvR:recvR+ngc-1) + L
          end if

          ! Extend axis for non-periodic boundary ends
          if (mpi%rank%L(idir)-1.eq.MPI_PROC_NULL) then
            do i=1,ngc
              axis(i)   = axis(ngc+1)   - real(ngc+1-i,wp)*(axis(ngc+2)-axis(ngc+1))
            end do
          end if

          if (mpi%rank%R(idir)-1.eq.MPI_PROC_NULL) then
            do i=1,ngc
              axis(size(axis)-ngc+i) = axis(size(axis)-ngc) + real(i,wp)*(axis(size(axis)-ngc)-axis(size(axis)-ngc-1))
            end do
          end if
        end if
      end associate
      return
    end subroutine block_obj_UpdateGridGhostCells2
    impure subroutine block_obj_UpdateExtents(this)
      !> Updates the dimensional extents of the block
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer       :: dir
      integer       :: hi_b,lo_b,i_b
      integer       :: Nblock(3)
      integer       :: nsize
      real(wp),     &
        allocatable :: buff(:)

      associate (lo => this%lo,  hi => this%hi, ngc=> this%ngc,&
        axis => this%axis)
        do dir=1,3
          this%xhi(dir) = axis(dir)%x(hi(dir)+1)
          this%xlo(dir) = axis(dir)%x(lo(dir)  )
        end do
      end associate

      if (this%is_partitioned) then
        do dir=1,3
          ! High/low bounds and index of current block
          lo_b = 1
          hi_b = this%parallel%np(dir)
          i_b  = this%parallel%rank%dir(dir)

          ! Initialize partition axis
          call this%axis_partition(dir)%Initialize(lo_b,hi_b,ngc=0)

          ! Add values
          ! - Low bound
          this%axis_partition(dir)%x(i_b) = this%xlo(dir)
          ! - High bound
          if (i_b.eq.hi_b) this%axis_partition(dir)%x(i_b+1) = this%xhi(dir)

          Nblock      = this%parallel%np
          Nblock(dir) = 1
          nsize       = Nblock(1)*Nblock(2)*Nblock(3)

          allocate(buff(hi_b + 1))
          call this%parallel%sum(this%axis_partition(dir)%x,buff)
          this%axis_partition(dir)%x=buff/nsize
          deallocate(buff)
        end do
      else
        do dir=1,3
          ! High/low bounds and index of current block
          lo_b = 1
          hi_b = 1
          i_b  = 1

          ! Initialize partition axis
          call this%axis_partition(dir)%Initialize(lo_b,hi_b,ngc=0)

          this%axis_partition(dir)%x(i_b)   = this%xlo(dir)
          this%axis_partition(dir)%x(i_b+1) = this%xhi(dir)
        end do
      end if

      ! Global min and max
      do dir=1,3
        this%pmin(dir) = minval(this%axis_partition(dir)%x(:))
        this%pmax(dir) = maxval(this%axis_partition(dir)%x(:))
      end do

      ! Convenience min and max
      this%xmin = this%pmin(1); this%xmax = this%pmax(1)
      this%ymin = this%pmin(2); this%ymax = this%pmax(2)
      this%zmin = this%pmin(3); this%zmax = this%pmax(3)
      return
    end subroutine block_obj_UpdateExtents
    pure subroutine block_obj_UpdateMidPoints(this)
      !> Update mid points
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer :: i,dir

      associate (lo => this%lo,  hi => this%hi, ngc=> this%ngc,&
        axis => this%axis)

        do concurrent (dir=1:3)
          do concurrent (i=lo(dir)-ngc:hi(dir)+ngc)
            axis(dir)%xm(i) = 0.5_wp*(axis(dir)%x(i)+axis(dir)%x(i+1))
          end do
        end do

      end associate
      return
    end subroutine block_obj_UpdateMidPoints
    impure subroutine block_obj_UpdateSpacing(this)
      !> Update grid spacing arrays
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer :: i,dir
      real(wp):: buffr(3)

      associate (lo => this%lo,  hi => this%hi, ngc=> this%ngc,&
        axis => this%axis)

        do concurrent (dir=1:3)
          do concurrent (i=lo(dir)-ngc:hi(dir)+ngc)
            axis(dir)%dxm(i) = axis(dir)%x(i+1)-axis(dir)%x(i)
          end do
        end do

      end associate

      ! Compute minimum mesh spacing
      do concurrent (dir=1:3)
        this%dx(dir) = minval(abs(this%axis(dir)%dxm))
      end do

      call this%parallel%min(this%dx,buffr); this%dx=buffr

      return
    end subroutine block_obj_UpdateSpacing
    impure subroutine block_obj_SetupMPITypes(this)
      !> Define MPI derived type for communicating ghostcells
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      ! Work variables
      integer :: Ng(3)
      integer :: count(3)
      integer :: length(3)
      integer :: stride(3)
      integer :: dir
      integer :: ierr

      ! Start by removing old datatypes
      do dir=1,3
        if (this%gc_slab_r(dir).ne.MPI_DATATYPE_NULL) &
          call MPI_TYPE_FREE(this%gc_slab_r(dir),ierr)

        if (this%gc_slab_i(dir).ne.MPI_DATATYPE_NULL) &
          call MPI_TYPE_FREE(this%gc_slab_i(dir),ierr)
      end do

      associate (lo => this%lo, hi => this%hi,ngc=> this%ngc,&
        parallel => this%parallel )
        ! Number of grid points (including ghostcells)
        Ng=hi-lo+1+2*ngc

        count  = [Ng(2)*Ng(3), Ng(3),        1                ]
        length = [ngc        , Ng(1)*ngc,    Ng(2)*Ng(1)*ngc  ]
        stride = [Ng(1)      , Ng(1)*Ng(2),  Ng(1)*Ng(2)*Ng(3)]

        do dir=1,3
          call MPI_TYPE_VECTOR(count(dir), length(dir), stride(dir), parallel%REAL_WP, this%gc_slab_r(dir),ierr)
          call MPI_TYPE_COMMIT(this%gc_slab_r(dir),ierr)

          call MPI_TYPE_VECTOR(count(dir), length(dir), stride(dir), parallel%INTEGER, this%gc_slab_i(dir),ierr)
          call MPI_TYPE_COMMIT(this%gc_slab_i(dir),ierr)
        end do
      end associate

      return
    end subroutine block_obj_SetupMPITypes
    pure subroutine block_obj_SubDivideBlock(coord,Nb,minlo,maxhi,sublo,subhi)
      !> Computes the bounds of the sub-block form
      ! the bounds of the parent block. Each sub-block
      ! gets about the same number of grid points in each
      ! direction.
      implicit none
      integer, intent(in) :: coord(3)                     !! Coordinates of MPI rank on MPI grid
      integer, intent(in) :: Nb(3)                        !! Number of blocks in each direction
      integer, intent(in) :: minlo(3)                     !! Global lo bounds
      integer, intent(in) :: maxhi(3)                     !! Global hi bounds
      integer, intent(out):: sublo(3)                     !! lo bounds of sub-block
      integer, intent(out):: subhi(3)                     !! hi bounds of sub-block
      ! Work variables
      integer :: q(3)
      integer :: r(3)
      integer :: dir

      ! Number of grid points per sub-block in each direction
      q  = (maxhi - minlo + 1)/Nb
      r  = mod(maxhi - minlo + 1,Nb)

      ! Set the lo and hi bounds
      do concurrent (dir=1:3)
        if (coord(dir).le.r(dir)) then
          sublo(dir) = minlo(dir)   + (coord(dir)-1)*(q(dir)+1)
          subhi(dir) = sublo(dir)-1 + q(dir)+1
        else
          sublo(dir) = minlo(dir)   + (coord(dir)-1)*q(dir)+r(dir)
          subhi(dir) = sublo(dir)-1 + q(dir)
        end if
      end do
      return
    end subroutine block_obj_SubDivideBlock
    impure subroutine block_obj_Partition(this,Nb)
      !> Partition a parent block into sub-blocks
      ! based on a given decomposition Nb(3) for parallel simulations.
      ! This will also define the partition axes, and update
      ! local bounds/extents, and global bounds/extents.
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      integer,          intent(in)    :: Nb(3)            !! Number of blocks in each direction
      ! Work variables
      integer :: i,dir
      integer :: minlo(3)
      integer :: maxhi(3)
      type(axis_obj):: axis_global(3)

      if (.not.associated(this%parallel)) return

      ! Store global axis prior to subdividing
      do dir=1,3
        call axis_global(dir)%Initialize(this%lo(dir),this%hi(dir),this%ngc)
        axis_global(dir)%x = this%axis(dir)%x
      end do
      minlo = this%lo
      maxhi = this%hi

      ! Switch flag on
      this%is_partitioned=.true.

      ! Total number of blocks must match total number of MPI ranks
      if (this%parallel%nproc.ne.Nb(1)*Nb(2)*Nb(3)) &
        call this%parallel%stop('Unable to partition block due to mismatch in parallel decomposition')

      ! Generate an MPI cartesian topology
      call this%parallel%topology(this%periods,maxhi-minlo+1,Nb)

      ! Get bounds of sub-block ownded by this MPI rank
      call this%SubDivideBlock(this%parallel%rank%dir,Nb,minlo,maxhi,this%lo,this%hi)

      ! Initialize axes
      do dir=1,3
        call this%axis(dir)%Finalize()
        call this%axis(dir)%Initialize(this%lo(dir),this%hi(dir),this%ngc)
      end do

      ! Associate pointers
      call this%SetConveniencePointers

      associate (lo => this%lo, hi => this%hi, axis=>this%axis)
        ! Get local axes coordinates from global axes
        do concurrent (dir=1:3)
          do concurrent (i=lo(dir):hi(dir)+1)
            axis(dir)%x(i) = axis_global(dir)%x(i)
          end do
        end do
      end associate

      ! Remove global arrays
      do dir=1,3
        call axis_global(dir)%Finalize
      end do

      ! Update values within ghostcells
      call this%UpdateGridGhostCells()

      ! Update mid points (xm)
      call this%UpdateMidPoints()

      ! Update spacing (dxm)
      call this%UpdateSpacing()

      ! Create MPI type for ghostcell communication
      call this%SetupMPITypes()

      return
    end subroutine block_obj_Partition
    impure subroutine block_obj_Write(this,name)
      !> Write block data using HDF5
      use leapIO_hdf5
      implicit none
      class(block_obj), intent(in) :: this                !! A block object
      character(len=*), intent(in) :: name                !! Name of file to write
      ! Work variables
      type(hdf5_obj)  :: hdf5
      integer         :: Ng(3)
      integer         :: maxhi(3)
      integer         :: minlo(3)
      character(len=2):: label
      integer         :: nsize
      integer         :: dir
      integer         :: Nblock(3)
      real(wp),       &
          allocatable :: xg(:)
      real(wp),     &
          allocatable :: buff(:)

      associate (lo => this%lo, hi => this%hi, x => this%x, y => this%y, &
         z =>this%z, periods => this%periods, rank => this%parallel%rank,&
         np => this%parallel%np )

        ! Get number of grid points in each direction
        call this%parallel%max(hi,maxhi)
        call this%parallel%min(lo,minlo)

        ! Total number of grid points
        Ng = maxhi-minlo+1

        ! Initialize file and write header info
        call hdf5%Initialize(trim(adjustl(name)),'W',this%parallel)
        call hdf5%WriteAttributes('/','Periodicity',periods)
        call hdf5%WriteAttributes('/','Ng',         Ng     )

        do dir=1,3
          ! Build global coordinate array
          allocate(xg  (Ng(dir)+1))
          allocate(buff(Ng(dir)+1))

          xg = 0.0_wp
          xg(lo(dir):hi(dir)) = this%axis(dir)%x(lo(dir):hi(dir))

          if (rank%dir(dir).eq.np(dir)) xg(hi(dir)+1) = this%axis(dir)%x(hi(dir)+1)

          Nblock      = np
          Nblock(dir) = 1
          nsize       = Nblock(1)*Nblock(2)*Nblock(3)

          call this%parallel%sum(xg,buff); xg=buff/nsize

          ! Form label for this axis: x1, x2, x3
          write(label,fmt='(a,i1)') 'x',dir

          ! Write coordinates along this axis
          call hdf5%WriteCoord('/', label, xg)

          deallocate(xg)
          deallocate(buff)
        end do

        ! Finalize file
        call hdf5%Finalize
      end associate
      return
    end subroutine block_obj_Write
    impure subroutine block_obj_Read(this,name)
      !> Read block data using HDF5
      use leapIO_hdf5
      implicit none
      class(block_obj), intent(inout) :: this             !! A block object
      character(len=*), intent(in)    :: name          !! Name of file to write
      ! Work variables
      type(hdf5_obj)  :: hdf5
      integer         :: Ng(3)
      integer         :: dir
      character(len=2):: label

      ! Open file
      call hdf5%Initialize(trim(adjustl(name)),'R',this%parallel)

      associate (lo => this%lo,  hi => this%hi, periods => this%periods)
        ! Get periodicity and number of grid points
        call hdf5%ReadAttributes('/','Periodicity',periods)
        call hdf5%ReadAttributes('/','Ng',         Ng     )

        ! Set bounds
        lo = 1
        hi = Ng

        ! Initialize axes
        do dir=1,3
          call this%axis(dir)%Initialize(this%lo(dir),this%hi(dir),this%ngc)
        end do

        ! Associate pointers
        call this%SetConveniencePointers

        ! Read coordinates from file
        do dir=1,3
          ! Form label for this axis: x1, x2, x3
          write(label,fmt='(a,i1)') 'x',dir

          ! Read coordinates
          call hdf5%ReadCoord('/',label,this%axis(dir)%x(lo(dir):hi(dir)+1))
        end do

        call hdf5%Finalize
      end associate

      ! Update values within ghostcells
      call this%UpdateGridGhostCells()

      ! Update mid points (xm)
      call this%UpdateMidPoints()

      ! Update spacing (dxm)
      call this%UpdateSpacing()

      ! Create MPI type for ghostcell communication
      call this%SetupMPITypes()

      return
    end subroutine block_obj_Read
    impure function block_obj_Locate(this,p) result (rank)
      !> Return block ID and rank of the block
      ! where the point is located using a binary search
      ! alogirthm. Note that this function assumes that
      ! the point is within the domain, i.e., (pmin <= p <= pmax)
      ! and that any treatment for periodicity has been
      ! previously applied.
      use mpi_f08
      implicit none
      class(block_obj), intent(in) :: this                 !! A block object
      real(wp),         intent(in) :: p(3)                 !! Position to locate
      integer                      :: rank                 !! MPI rank
      ! Work variables
      integer :: dir
      integer :: ind(3)
      integer :: i_lo
      integer :: i_hi
      integer :: ierr

      associate(axis_p => this%axis_partition, np => this%parallel%np)

        do dir=1,3
          ! Find block index using binary search
          i_lo = axis_p(dir)%lo
          i_hi = axis_p(dir)%hi + 1

          do while (i_hi-i_lo.ge.2)
            ind(dir) = (i_hi+i_lo)/2

            if (p(dir).ge.axis_p(dir)%x( ind(dir) )) then
              i_lo = ind(dir)
            else
              i_hi = ind(dir)
            end if

          end do

          ind(dir) = i_lo
        end do

        ! Get rank from the MPI cartesian topology
        call MPI_CART_RANK(this%parallel%comm%g,ind-1,rank,ierr)

        ! Adjust rank to be 1-based (instead of 0-based ranks returned by MPI)
        rank = rank + 1
      end associate
      return
    end function block_obj_Locate
    impure subroutine block_obj_Info(this)
      !> Print to stdout information about this block
      use iso_fortran_env, only : stdout => output_unit
      implicit none
      class(block_obj), intent(in) :: this                 !! A block object

      write(stdout,"(a20,a,3i16)"   ) 'lo',                   repeat('-',19)//'>', this%lo
      write(stdout,"(a20,a,3i16)"   ) 'hi',                   repeat('-',19)//'>', this%hi
      write(stdout,"(a20,a,3f16.10)") 'xlo',                  repeat('-',19)//'>', this%xlo
      write(stdout,"(a20,a,3f16.10)") 'xhi',                  repeat('-',19)//'>', this%xhi
      write(stdout,"(a20,a, i16)"   ) 'ngc',                  repeat('-',19)//'>', this%ngc
      write(stdout,"(a20,a,3f16.10)") 'dx',                   repeat('-',19)//'>', this%dx
      write(stdout,"(a20,a, l16)"   ) 'associated(parallel)', repeat('-',19)//'>', associated(this%parallel)
      write(stdout,"(a20,a, l16)"   ) 'associated(x)',        repeat('-',19)//'>', associated(this%x)
      write(stdout,"(a20,a, l16)"   ) 'associated(xm)',       repeat('-',19)//'>', associated(this%xm)
      write(stdout,"(a20,a, l16)"   ) 'associated(y)',        repeat('-',19)//'>', associated(this%y)
      write(stdout,"(a20,a, l16)"   ) 'associated(ym)',       repeat('-',19)//'>', associated(this%ym)
      write(stdout,"(a20,a, l16)"   ) 'associated(z)',        repeat('-',19)//'>', associated(this%z)
      write(stdout,"(a20,a, l16)"   ) 'associated(zm)',       repeat('-',19)//'>', associated(this%zm)
      return
    end subroutine block_obj_Info
end module leapBlock
