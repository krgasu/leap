!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module leapIO_silo
  !>--------------------------------------------------------------------------
  ! Module: leapIO_silo
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Write (only) data with the SILO library
  ! --------------------------------------------------------------------------
  use leapKinds
  use leapParallel
  use leapBlock
  use leapUtils, only: leap_create_directory
  use mpi_f08
  implicit none
  include "silo_f9x.inc"

  private
  public :: silo_obj

  type :: silo_obj
    type(parallel_obj), pointer :: parallel=>null()        !! Associated parallel structure
    integer(leapI4)             :: fid_DAT                 !! File identifier for silo group files
    integer(leapI4)             :: fid_VDB                 !! File identifier for silo VDB files
    integer(leapI4)             :: fid_VisIt               !! File identifier for VisIt file
    character(len=str64)        :: filepath                !! Path to VDB and group files
    character(len=str64)        :: filename                !! Base name
    character(len=str64)        :: vdbname                 !! Name of silo VDB file
    character(len=str64)        :: siloname                !! Name of silo group file
    character(len=5)            :: dirname                 !! Name of directory within group file
    character(len=str64)        :: visitname               !! Name of VisIt file
    character(len=:),allocatable:: access_flag             !! Access flag
    integer                     :: nproc_node              !! Number of procs per silo
    integer, allocatable        :: group_ids(:)            !! ID for silo groups
    type(MPI_Comm)              :: silo_comm
    integer                     :: silo_rank
    contains
      procedure :: Initialize         => silo_Init
      procedure :: CreateGroups        => silo_CreateGroups
      procedure :: Finalize           => silo_Final
      procedure :: SetupGroupFiles    => silo_SetupGroupFiles
      procedure :: WriteGrid          => silo_WriteGrid
      procedure :: NewTimeStep        => silo_NewTimeStep
      generic   :: Write              => silo_WriteScalar3D
      procedure, private :: silo_WriteScalar3D
  end type

  character(len=*),parameter :: SILOinfo     = "SILO database created with LEAP"
  integer, parameter         :: SILOdriver   = DB_HDF5     !! Driver for writing
  integer, parameter         :: SILOchannels = 4           !! Default IO channels: One file per SILOchannels MPI ranks
  integer, parameter         :: SILOstr      = 40          !! Length of strings passed to Silo
  character(len=*),parameter :: SILOdir      = "Silo"      !! Name of directory

  contains
    subroutine silo_Init(this,filename,access_flag,parallel,nproc_node)
      !> Initialize structure
      implicit none
      class(silo_obj),  intent(inout)     :: this          !! A silo object
      character(len=*), intent(in)        :: filename      !! File to read/write
      character(len=*), intent(in)        :: access_flag   !! File access mode
      type(parallel_obj),target,intent(in):: parallel      !! parallel structure from main program
      integer, optional,intent(in)        :: nproc_node    !! Number of procs per silo
      ! Work variables
      integer :: pos
      integer :: ierr
      logical :: file_exists

      ! Point to the master objects
      this%parallel => parallel

      ! Set the file name (remove any extension)
      pos=scan(filename,".",BACK=.true.)
      if (pos.gt.0) then
        this%filename=trim(adjustl(filename(1:pos-1)))
      else
        this%filename=trim(adjustl(filename))
      end if

      ! Collective IO Init
      ! -------------------------- !
      ! If present, set number of procs per SILO or else
      ! use default value
      this%nproc_node=SILOchannels
      if (present(nproc_node)) this%nproc_node=nproc_node
      call this%CreateGroups

      ! Set the access flag
      this%access_flag=trim(adjustl(access_flag))

      ! Create Silo folder
      if (this%parallel%rank%mine.eq.this%parallel%rank%root) &
        call leap_create_directory(SILOdir)

      ! Create Visit file
      write(this%visitname,fmt='(4a)') SILOdir,'/',trim(adjustl(this%filename)),'.visit'
      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        inquire (file=this%visitname, exist=file_exists)
        if (.not.file_exists) then
          ! Create file
          open(newunit=this%fid_VisIt,file=trim(adjustl(this%visitname)),action='write',status='new',iostat=ierr)
          if (ierr.ne.0) call this%parallel%stop('Unable to create VisIt file')
        else
          ! Open existing file
          open(newunit=this%fid_VisIt,file=trim(adjustl(this%visitname)),action='readwrite',access='sequential',form='formatted',status='old',iostat=ierr)
          if (ierr.ne.0) call this%parallel%stop('Unable to create VisIt file')
        end if
      end if
      return
    end subroutine silo_Init
    subroutine silo_CreateGroups(this)
      !> Set up silo groups for poor man's IO.
      ! Split MPI ranks into groups of size nproc_node.
      ! Each group will write squentially to its own file.
      implicit none
      class(silo_obj),  intent(inout) :: this              !! A silo object
      !Work variables
      integer :: ierr,n

      ! Set the IDs of the different silo groups
      allocate(this%group_ids(this%parallel%nproc))

      do n=1,this%parallel%nproc
        this%group_ids(n)=ceiling(real(n,wp)/real(this%nproc_node,wp))
      end do

      ! Create a new MPI communicator for each silo group
      call MPI_COMM_SPLIT(this%parallel%comm%g,this%group_ids(this%parallel%rank%mine),0,this%silo_comm,ierr)

      ! Return rank within this communicator
      call MPI_COMM_RANK (this%silo_comm,this%silo_rank,ierr)

      ! Adjust rank numbering Fortran style
      this%silo_rank=this%silo_rank+1

      return
    end subroutine silo_CreateGroups
    subroutine silo_Final(this)
      !> Finalize structure
      implicit none
      class(silo_obj),  intent(inout) :: this              !! A silo object
      ! Work variables
      integer :: ierr

      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        ! Close the VDB file
        ierr = DBclose(this%fid_VDB)
        ! Close the VisIt
        close(this%fid_VisIt)
      end if

      ! Deallocate arrays
      if (allocated(this%group_ids))   deallocate(this%group_ids)
      if (allocated(this%access_flag)) deallocate(this%access_flag)

      ! Nullify pointers
      this%parallel => null()

      return
    end subroutine silo_Final
    subroutine silo_NewTimeStep(this,time)
      !> Create a new Silo virtual data base (VDB)
      ! for this timestep
      implicit none
      class(silo_obj),  intent(inout) :: this              !! A silo object
      !type(block_obj),  intent(in)    :: block             !! Block object
      !integer,          intent(in)    :: iter              !! Iteration for this time step
      real(wp),         intent(in)    :: time              !! Time value for this time step
      ! Work variables
      integer :: ierr
      character(SILOstr):: record
      logical :: finished
      real(wp) :: my_time

      ! Append new timestep to Visit file
      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        ! Skip existing older time steps
        finished=.false.
        do while (.not.finished)
          read(this%fid_VisIt,fmt='(a)',iostat=ierr) record
          if (ierr.ne.0) then
            finished=.true.
            backspace(this%fid_VisIt)
          else
            read(record(6:17),*) my_time
            if (my_time.ge.time) then
              finished=.true.
              backspace(this%fid_VisIt)
            end if
          end if
        end do
        ! Add timestep
        write(this%fid_VisIt,fmt='(a,ES12.6,4a)') 'time_',time,'/',trim(adjustl(this%filename)),'.silo'
      end if

      ! Create Silo directory for this timestep
      write(this%filepath,fmt='(2a,ES12.6,a)') SILOdir,'/time_',time,'/'

      ! Set name of VDB file
      this%vdbname=trim(adjustl(this%filepath))//trim(adjustl(this%filename))//'.silo'

      ! MPI root creates the VDB file
      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then

        ! Create new folder for this time step
        call leap_create_directory(trim(adjustl(this%filepath)))

        ! Create the VDB file
        ierr = DBcreate(this%vdbname,len_trim(this%vdbname),DB_CLOBBER,DB_LOCAL, &
               SILOinfo,len_trim(SILOinfo),SILOdriver,this%fid_VDB)
        if (this%fid_VDB.eq.-1) call this%parallel%stop('Unable to create SILO file')

        ! Set length of names
        ierr= DBset2dstrlen(SILOstr)
      end if

      ! Synchronize
      call MPI_BARRIER(this%parallel%comm%g)

      ! Create group files
      call this%SetupGroupFiles(this%access_flag)

      return
    end subroutine silo_NewTimeStep
    subroutine silo_SetupGroupFiles(this,flag)
      !> Create silo files and their internal structure
      implicit none
      class(silo_obj),  intent(inout) :: this              !! A silo object
      character(len=*), intent(in)    :: flag              !! IO flag
      ! Work variables
      character(len=5)          :: dirname
      integer :: n
      integer :: ntask
      integer :: status
      integer :: ierr

      ! Create SILO directories
      call MPI_COMM_SIZE(this%silo_comm,ntask,ierr)

      select case(trim(adjustl(flag)))
      case ("W")
        ! Group DB name
        write(this%siloname, '(3A,I5.5,A)') trim(adjustl(this%filepath)),trim(adjustl(this%filename)),'_g_',this%group_ids(this%parallel%rank%mine),'.silo'

        ! Group root creates SILO database
        If (this%silo_rank.eq.1) then
          ! Create database
          ierr = DBcreate(this%siloname,len_trim(this%siloname),DB_CLOBBER,DB_LOCAL, &
                 SILOinfo,len_trim(SILOinfo),SILOdriver,this%fid_DAT)

          if (this%fid_DAT.eq.-1) call this%parallel%stop('Unable to create SILO file')

          ! Create directories for the different MPI tasks
          do n=1,ntask
            write(dirname, '(I5.5)') n
            ierr = DBmkdir(this%fid_DAT,dirname,len_trim(dirname),status)
          end do

          ! Close group file
          ierr =DBclose(this%fid_DAT)
          if (ierr.ne.0) call this%parallel%stop('Error closing SILO file')
        end if

        ! Store directory name for this silo_rank
        write(this%dirname, '(I5.5)') this%silo_rank

      case default
        call this%parallel%stop("SILO files can only be opened in W mode")
      end select

      ! Wait on all rank within this SILO group
      call MPI_BARRIER(this%silo_comm)

      return
    end subroutine silo_SetupGroupFiles
    subroutine silo_WriteGrid(this,block,iter,time)
      !> Write the grid attributes
      implicit none
      class(silo_obj),  intent(inout) :: this              !! A silo object
      type(block_obj),  intent(in)    :: block             !! Block object
      integer,          intent(in)    :: iter
      real(wp),         intent(in)    :: time
      ! Work variables
      integer :: ierr,status
      character(len=SILOstr), allocatable :: names(:)
      integer, allocatable :: lnames(:),types(:)
      integer :: i,n,optlist
      integer :: Ng(3)
      character(len=6) :: mesh(4)

      Ng = block%hi-block%lo+1

      ! Write using poor man's IO: sequential (serial) writes among a silo group
      do n=1,this%nproc_node
        if (n.eq.this%silo_rank) then
          ! Open database
          ierr=DBopen(this%siloname,len_trim(this%siloname),SILOdriver,DB_APPEND,this%fid_DAT)
          if (ierr.ne.0) call this%parallel%stop('Error opening SILO file')

          ! Switch to appropriate directory
          ierr = DBsetdir(this%fid_DAT,this%dirname,len_trim(this%dirname))

          ! Write your portion of grid
          ierr = dbputqm(this%fid_DAT,"mesh_0",6,"x",1,"y",1,"z",1,      &
                 real(block%x (block%lo(1):block%hi(1)+1),leapSP),       &
                 real(block%y (block%lo(2):block%hi(2)+1),leapSP),       &
                 real(block%z (block%lo(3):block%hi(3)+1),leapSP),       &
                 Ng+1,3,DB_FLOAT,DB_COLLINEAR, DB_F77NULL, status)

          ierr = dbputqm(this%fid_DAT,"mesh_1",6,"x",1,"y",1,"z",1,      &
                 real(block%xm(block%lo(1)-1:block%hi(1)+1),leapSP),     &
                 real(block%y (block%lo(2)  :block%hi(2)+1),leapSP),     &
                 real(block%z (block%lo(3)  :block%hi(3)+1),leapSP),     &
                 Ng+1,3,DB_FLOAT,DB_COLLINEAR, DB_F77NULL, status)

          ierr = dbputqm(this%fid_DAT,"mesh_2",6,"x",1,"y",1,"z",1,      &
                 real(block%x (block%lo(1)  :block%hi(1)+1),leapSP),     &
                 real(block%ym(block%lo(2)-1:block%hi(2)+1),leapSP),     &
                 real(block%z (block%lo(3)  :block%hi(3)+1),leapSP),     &
                 Ng+1,3,DB_FLOAT,DB_COLLINEAR, DB_F77NULL, status)

          ierr = dbputqm(this%fid_DAT,"mesh_3",6,"x",1,"y",1,"z",1,      &
                 real(block%x (block%lo(1)  :block%hi(1)+1),leapSP),     &
                 real(block%y (block%lo(2)  :block%hi(2)+1),leapSP),     &
                 real(block%zm(block%lo(3)-1:block%hi(3)+1),leapSP),     &
                 Ng+1,3,DB_FLOAT,DB_COLLINEAR, DB_F77NULL, status)

          ! Close file so that next silo_rank can open it
          ierr =DBclose(this%fid_DAT)
          if (ierr.ne.0) call this%parallel%stop('Error closing SILO file')
        end if

        ! Synchronize SILO ranks
        call MPI_BARRIER(this%silo_comm)
      end do

      ! Add mesh meta data
      mesh(1)="mesh_0"
      mesh(2)="mesh_1"
      mesh(3)="mesh_2"
      mesh(4)="mesh_3"

      allocate (names(this%parallel%nproc))
      allocate (lnames(this%parallel%nproc))
      allocate (types(this%parallel%nproc))

      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        do n=1,size(mesh)
          do i=1,this%parallel%nproc
            write(names(i), '(2A,I5.5,A,I5.5,2A)') trim(adjustl(this%filename)),'_g_',&
                                                   this%group_ids(i),'.silo:',mod(i-1,this%nproc_node)+1,'/',mesh(n)
            lnames(i)=len_trim(names(i))
            types(i)=DB_QUAD_RECT
          end do
          ierr = DBmkoptlist(2,optlist)
          ierr = DBaddiopt(optlist,DBOPT_DTIME,time)
          ierr = DBaddiopt(optlist,DBOPT_CYCLE,iter)
          ierr = DBputmmesh(this%fid_VDB, mesh(n), len_trim(mesh(n)), this%parallel%nproc,names,lnames,types,optlist,ierr)
          ierr = DBfreeoptlist(optlist)
        end do
      end if

      return
    end subroutine silo_WriteGrid
    subroutine silo_WriteScalar3D(this,name,array,lo,hi,staggering)
      !> Write Eulerian/3D data to a hdf5 file with silo
      implicit none
      class(silo_obj), intent(inout) :: this               !! A silo object
      class(*),         intent(in)   :: array(:,:,:)       !! 3-D data array
      character(len=*), intent(in)   :: name               !! Variable name
      integer,          intent(in)   :: lo(3)              !! Low bounds
      integer,          intent(in)   :: hi(3)              !! High bounds
      integer,          intent(in)   :: staggering         !! Staggering value
      ! Work variables
      integer :: ierr,status
      integer :: n
      character(len=6) :: mesh
      character(len=SILOstr), allocatable :: names(:)
      integer, allocatable :: lnames(:),types(:)

      select case (staggering)
      case (0)
        mesh="mesh_0"
      case (1)
        mesh="mesh_1"
      case (2)
        mesh="mesh_2"
      case (3)
        mesh="mesh_3"
      end select

      ! Write using poor man's IO: sequential (serial) writes among a silo group
      do n=1,this%nproc_node
        if (n.eq.this%silo_rank) then

          ! Open file
          ierr=DBopen(this%siloname,len_trim(this%siloname),SILOdriver,DB_APPEND,this%fid_DAT)
          if (ierr.ne.0) call this%parallel%stop('Error opening SILO file')

          ! Switch to appropriate directory
          ierr = DBsetdir(this%fid_DAT,trim(this%dirname),len_trim(this%dirname))

          ! Write data in Single Precision
          select type(array)
          type is (real(leapDP))
            ierr = DBputqv1(this%fid_DAT,name,len_trim(name), mesh, 6, real(array,leapSP), &
               hi-lo+1, 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT, DB_F77NULL, status)
          type is (real(leapSP))
            ierr = DBputqv1(this%fid_DAT,name,len_trim(name), mesh, 6, real(array,leapSP), &
               hi-lo+1, 3, DB_F77NULL, 0, DB_FLOAT, DB_ZONECENT, DB_F77NULL, status)
          type is (integer)
            ierr = DBputqv1(this%fid_DAT,name,len_trim(name), mesh, 6, int(array,leapI4), &
               hi-lo+1, 3, DB_F77NULL, 0, DB_INT,   DB_ZONECENT, DB_F77NULL, status)
          type is (integer(leapI8))
            ierr = DBputqv1(this%fid_DAT,name,len_trim(name), mesh, 6, int(array,leapI4), &
               hi-lo+1, 3, DB_F77NULL, 0, DB_INT,   DB_ZONECENT, DB_F77NULL, status)
          end select

          ! Close file so that next silo_rank can open it
          ierr =DBclose(this%fid_DAT)
          if (ierr.ne.0) call this%parallel%stop('Error closing SILO file')
        end if

        call MPI_BARRIER(this%silo_comm)
      end do

      allocate (names(this%parallel%nproc))
      allocate (lnames(this%parallel%nproc))
      allocate (types(this%parallel%nproc))

      if (this%parallel%rank%mine.eq.this%parallel%rank%root) then
        do n=1,this%parallel%nproc
          write(names(n), '(2A,I5.5,A,I5.5,2A)') trim(adjustl(this%filename)),'_g_',&
                                                 this%group_ids(n),'.silo:',mod(n-1,this%nproc_node)+1,'/',trim(name)
          lnames(n) = len_trim(names(n))
          types(n)  = DB_QUADVAR
        end do
        ierr = DBputmvar(this%fid_VDB, name, len_trim(name), this%parallel%nproc,names,lnames,types,DB_F77NULL,ierr)
      end if

      deallocate(names)
      deallocate(lnames)
      deallocate(types)
      return
    end subroutine silo_WriteScalar3D
end module leapIO_silo
