!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
submodule (particles_point) nga_smod

  ! Tools to read raw NGA
  ! part files into LEAP point
  ! particles object
  ! -------------------------- !
  type ngapart_obj
    type(parallel_obj), pointer :: parallel=>null()        !! Associated parallel structure
    type(MPI_File)              :: fid                     !! File identifier
    character(len=str64)        :: filename                !! File to read/write
    real(leapDP)                :: time                    !! Time in file
    real(leapDP)                :: dt                      !! Time step in file
    integer                     :: count                   !! Total count of particles in file
    type(MPI_Info)              :: MPI_INFO                !! MPI info
    integer                     :: MPI_SIZE                !! MPI size of a NGA particle
    type(MPI_DATATYPE)          :: MPI_TYPE                !! MPI variable type of a NGA particle
    contains
      procedure :: Initialize      => ngapart_Init
      procedure :: Finalize        => ngapart_Final
      procedure :: Open            => ngapart_Open
      procedure :: Close           => ngapart_Close
      procedure :: GetAttributes   => ngapart_GetAttributes
      procedure :: CreateMPIType   => ngapart_CreateMPIType
      procedure :: Read            => ngapart_Read
  end type ngapart_obj

  type :: nga_part_obj
    !> Particle type as definied in NGA
    integer(leapI8)        :: id                           !! Identifying number (inactive if <0)
    real(WP)               :: x                            !! position
    real(WP)               :: y                            !! position
    real(WP)               :: z                            !! position
    real(WP)               :: u                            !! velocity
    real(WP)               :: v                            !! velocity
    real(WP)               :: w                            !! velocity
    real(WP)               :: wx                           !! angular velocity
    real(WP)               :: wy                           !! angular velocity
    real(WP)               :: wz                           !! angular velocity
    real(WP)               :: d                            !! diameter
    real(WP)               :: dt                           !! time step size
    real(WP),dimension(3)  :: Acol                         !! Particle collision force
    real(WP),dimension(3)  :: Tcol                         !! Particle collision torque
    integer                :: i                            !! nearest cell
    integer                :: j                            !! nearest cell
    integer                :: k                            !! nearest cell
    integer                :: stop                         !! control parameter
  end type nga_part_obj

  contains

    module subroutine particle_set_ReadNGA(this,iter,time)
      !> Read particle data from file in parallel
      implicit none
      class(particle_set), intent(inout) :: this           !! Lagrangian array to dump
      integer,             intent(out)   :: iter           !! Iteration at write
      real(wp),            intent(out)   :: time           !! Time at write
      ! Work variables
      type(ngapart_obj) :: ngapart                         !! Raw NGA part file

      ! Open the file
      ! - Initialize
      ! - Open file
      call ngapart%Initialize(trim(adjustl(this%read_file)),"R",this%parallel)

      ! Return time of this snapshot
      time = ngapart%time
      iter = 0              ! Unavailable in NGA part files

      ! Read data
      call ngapart%Read(this)

      ! Finalize ngapart structure
      call ngapart%Finalize

      ! Send particles to their owning MPI ranks
      call this%communicate

      return
    end subroutine particle_set_ReadNGA

    ! NGAPART methods
    ! -------------------------- !
    subroutine ngapart_Init(this,filename,access_flag,parallel)
      !> Initialize structuree
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object
      character(len=*),   intent(in)        :: filename    !! File to read/write
      character(len=*),   intent(in)        :: access_flag !! File access mode
      type(parallel_obj), target, intent(in):: parallel    !! Parallel structure

      ! Point to the master objects
      this%parallel => parallel

      ! Set the file name
      this%filename = filename

      ! Define an MPI type for NGA particles
      call this%CreateMPIType

      ! Open file
      call this%Open(access_flag)

      ! Initialize MPI info
      this%mpi_info = MPI_INFO_NULL

      return
    end subroutine ngapart_Init
    subroutine ngapart_Final(this)
      !> Finalize structure
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object

      ! Close file
      call this%Close

      ! Nullify pointer
      this%parallel => null()
      return
    end subroutine ngapart_Final
    subroutine ngapart_CreateMPIType(this)
      !> Define MPI Type and size for a NGA particle
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object

      ! Work variable
      type(MPI_Datatype), allocatable  :: types(:)         !! Array of types
      integer, allocatable             :: lengths(:)       !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
                           allocatable :: displacement(:)  !! Array of displacements
      integer(kind=MPI_ADDRESS_KIND)   :: extent           !! Extent of derived type
      integer(kind=MPI_ADDRESS_KIND)   :: lb               !! Lower bound of derived type
      integer              :: N                            !! Number of components in derived type
      type(nga_part_obj)   :: sample
      integer :: ierr
      type(MPI_DATATYPE) :: type_tmp

      associate(mpi=>this%parallel)
        ! Create the MPI structure
        N=18
        allocate(types(N),displacement(N),lengths(N))
        types( 1) = mpi%INT8    ; lengths( 1) = 1                ! id     --  integer
        types( 2) = mpi%REAL_WP ; lengths( 2) = 1                ! x      --  real
        types( 3) = mpi%REAL_WP ; lengths( 3) = 1                ! y      --  real
        types( 4) = mpi%REAL_WP ; lengths( 4) = 1                ! z      --  real
        types( 5) = mpi%REAL_WP ; lengths( 5) = 1                ! u      --  real
        types( 6) = mpi%REAL_WP ; lengths( 6) = 1                ! v      --  real
        types( 7) = mpi%REAL_WP ; lengths( 7) = 1                ! w      --  real
        types( 8) = mpi%REAL_WP ; lengths( 8) = 1                ! wx     --  real
        types( 9) = mpi%REAL_WP ; lengths( 9) = 1                ! wy     --  real
        types(10) = mpi%REAL_WP ; lengths(10) = 1                ! wz     --  real
        types(11) = mpi%REAL_WP ; lengths(11) = 1                ! d      --  real
        types(12) = mpi%REAL_WP ; lengths(12) = 1                ! dt     --  real
        types(13) = mpi%REAL_WP ; lengths(13) = size(sample%Acol)! Acol   --  real*3
        types(14) = mpi%REAL_WP ; lengths(14) = size(sample%Tcol)! Acol   --  real*3
        types(15) = mpi%INTEGER ; lengths(15) = 1                ! i      --  integer
        types(16) = mpi%INTEGER ; lengths(16) = 1                ! j      --  integer
        types(17) = mpi%INTEGER ; lengths(17) = 1                ! k      --  integer
        types(18) = mpi%INTEGER ; lengths(18) = 1                ! stop   --  integer

        ! Count the displacement for this structure
        call MPI_GET_ADDRESS(sample%id     ,  displacement( 1),ierr)
        call MPI_GET_ADDRESS(sample%x      ,  displacement( 2),ierr)
        call MPI_GET_ADDRESS(sample%y      ,  displacement( 3),ierr)
        call MPI_GET_ADDRESS(sample%z      ,  displacement( 4),ierr)
        call MPI_GET_ADDRESS(sample%u      ,  displacement( 5),ierr)
        call MPI_GET_ADDRESS(sample%v      ,  displacement( 6),ierr)
        call MPI_GET_ADDRESS(sample%w      ,  displacement( 7),ierr)
        call MPI_GET_ADDRESS(sample%wx     ,  displacement( 8),ierr)
        call MPI_GET_ADDRESS(sample%wy     ,  displacement( 9),ierr)
        call MPI_GET_ADDRESS(sample%wz     ,  displacement(10),ierr)
        call MPI_GET_ADDRESS(sample%d      ,  displacement(11),ierr)
        call MPI_GET_ADDRESS(sample%dt     ,  displacement(12),ierr)
        call MPI_GET_ADDRESS(sample%Acol(1),  displacement(13),ierr)
        call MPI_GET_ADDRESS(sample%Tcol(1),  displacement(14),ierr)
        call MPI_GET_ADDRESS(sample%i      ,  displacement(15),ierr)
        call MPI_GET_ADDRESS(sample%j      ,  displacement(16),ierr)
        call MPI_GET_ADDRESS(sample%k      ,  displacement(17),ierr)
        call MPI_GET_ADDRESS(sample%stop   ,  displacement(18),ierr)
        displacement = displacement - displacement(1)
      end associate

      ! Create MPI type
      call MPI_TYPE_CREATE_STRUCT(size(lengths),lengths,displacement,types,type_tmp,ierr)
      if (ierr.ne.0) call this%parallel%stop("Problem with MPI_TYPE")

      ! Get actual lower bound and extent of this type
      call MPI_TYPE_GET_EXTENT(type_tmp,lb,extent,ierr)
      if (ierr.ne.0) call this%parallel%stop("Problem with MPI_TYPE")

      ! Adjust upper and lower bounds
      call MPI_TYPE_CREATE_RESIZED(type_tmp,lb,extent,this%MPI_TYPE,ierr)
      call MPI_TYPE_COMMIT(this%MPI_TYPE,ierr)
      if (ierr.ne.0) call this%parallel%stop("Problem with MPI_TYPE")

      ! Get the size of this type
      call MPI_TYPE_SIZE(this%MPI_TYPE,this%MPI_SIZE,ierr)

      ! Deallocate variables
      deallocate(types,displacement,lengths)
      return
    end subroutine ngapart_CreateMPIType
    subroutine ngapart_Open(this,flag)
      !> Open raw NGA part file
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object
      character(len=*),   intent(in)        :: flag        !! File access mode
      ! Work variables
      integer :: ACCESS_FLAG
      integer :: ierr

      select case (trim(adjustl(flag)))
      case ("RW")
        ACCESS_FLAG = MPI_MODE_RDWR
      case ("W" )
        ACCESS_FLAG = MPI_MODE_WRONLY
      case ("R" )
        ACCESS_FLAG = MPI_MODE_RDONLY
      case default
        ACCESS_FLAG = MPI_MODE_RDONLY
      end select


      call MPI_FILE_OPEN(this%parallel%comm%g,""//trim(adjustl(this%filename)), &
                         ACCESS_FLAG,this%MPI_INFO,this%fid,ierr)

      if (ierr.ne.0) call this%parallel%stop("Error opening file: "//trim(adjustl(this%filename)))

      ! Get attributes
      if (trim(adjustl(flag)).eq. "R") call this%GetAttributes

      return
    end subroutine ngapart_Open
    subroutine ngapart_Close(this)
      !> Close file with MPI
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object
      ! Work variables
      integer :: ierr

      call MPI_FILE_CLOSE(this%fid,ierr)

      return
    end subroutine ngapart_Close
    subroutine ngapart_GetAttributes(this)
      !> Read file attributes
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object
      ! Work variables
      integer            :: ierr
      type(MPI_Status)   :: status
      integer            :: part_size

      ! Rewind file
      call MPI_FILE_SEEK(this%fid,int(0,kind=MPI_OFFSET_KIND),MPI_SEEK_SET,ierr)

      ! Read header information: total count of particles/MPI part type size/dt/time
      call MPI_FILE_READ_ALL(this%fid,this%count,  1,this%parallel%INTEGER,status,ierr)
      call MPI_FILE_READ_ALL(this%fid,part_size,   1,this%parallel%INTEGER,status,ierr)
      call MPI_FILE_READ_ALL(this%fid,this%dt,     1,this%parallel%REAL_DP,status,ierr)
      call MPI_FILE_READ_ALL(this%fid,this%time,   1,this%parallel%REAL_DP,status,ierr)

      if (part_size.ne. this%MPI_SIZE) call this%parallel%stop("Error, unreadable particle type in: "//trim(adjustl(this%filename)))

      return
    end subroutine ngapart_GetAttributes
    subroutine ngapart_Read(this,pp)
      !> Read NGA particles, convert and store them in
      ! LEAP particle structure
      implicit none
      class(ngapart_obj), intent(inout)     :: this        !! NGApart object
      class(particle_set),intent(inout)     :: pp          !! LEAP point-particle set
      ! Work variables
      type(nga_part_obj) :: nga_particle                   !! Value read from NGA part file
      integer            :: offset                         !! File offset
      integer            :: n                              !! Iterator
      integer            :: ierr
      type(MPI_Status)   :: status

      ! Delete old data
      call pp%resize(0)

      ! Get total number of point particles
      pp%count = this%count

      associate(mpi=>this%parallel)
        ! Distribute equally the data on each mpi rank
        pp%count_=int(pp%count/mpi%nproc)
        if (mpi%rank%mine.le.mod(this%count,mpi%nproc)) pp%count_= pp%count_+1

        ! Update count of particles per proc
        call MPI_ALLGATHER(pp%count_,1,mpi%INTEGER,pp%count_proc,1,mpi%INTEGER,mpi%comm%g,ierr)
      end associate

      ! Resize the data array to contain read Lagrangian objects
      call pp%resize(pp%count_)

      ! Compute offset (header info + particles read by previous MPI ranks)
      offset = 2*kind(this%count)+2*kind(this%dt)   + &
               this%MPI_SIZE *sum(pp%count_proc(1:this%parallel%rank%mine-1))

      ! Move file pointer to correct offset
      call MPI_FILE_SEEK(this%fid,int(offset,MPI_OFFSET_KIND),MPI_SEEK_SET,ierr)

      ! Read particles one-by-one
      do n=1,pp%count_
        call MPI_FILE_READ(this%fid,nga_particle, 1,this%MPI_TYPE,status,ierr)

        ! Deactivate particles that fall outside the domain
        if (nga_particle%x.lt.pp%block%pmin(1) .or. &
            nga_particle%x.gt.pp%block%pmax(1) .or. &
            nga_particle%y.lt.pp%block%pmin(2) .or. &
            nga_particle%y.gt.pp%block%pmax(2) .or. &
            nga_particle%z.lt.pp%block%pmin(3) .or. &
            nga_particle%z.gt.pp%block%pmax(3))     &
            nga_particle%id=-1

        ! Convert NGA particle to LEAP particle
        select type (pp_part=>pp%p(n))
        type is (particle_obj)
          pp_part%id  = nga_particle%id
          pp_part%p(1)= nga_particle%x
          pp_part%p(2)= nga_particle%y
          pp_part%p(3)= nga_particle%z
          pp_part%c(1)= nga_particle%i
          pp_part%c(2)= nga_particle%j
          pp_part%c(3)= nga_particle%k
          pp_part%v(1)= nga_particle%u
          pp_part%v(2)= nga_particle%v
          pp_part%v(3)= nga_particle%w
          pp_part%d   = nga_particle%d
          pp_part%rho = 0 ! 0 because it doesn't exist in NGA raw data, needs to be read from parser
          pp_part%s   = 0
        end select
      end do

      return
    end subroutine ngapart_Read
end submodule nga_smod
