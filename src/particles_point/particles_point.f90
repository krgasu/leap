!>   LEAP - Highly scalable, Reusable, and Extendable CFD Tools.
!>   Copyright (C) 2019-2024 Mohamed Houssem Kasbaoui <houssem.kasbaoui@asu.edu>
!>
!>   This program is free software: you can redistribute it and/or modify
!>   it under the terms of the GNU General Public License as published by
!>   the Free Software Foundation, either version 3 of the License, or
!>   (at your option) any later version.
!>
!>   This program is distributed in the hope that it will be useful,
!>   but WITHOUT ANY WARRANTY; without even the implied warranty of
!>   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!>   GNU General Public License for more details.
!>
!>   You should have received a copy of the GNU General Public License
!>   along with this program.  If not, see <https://www.gnu.org/licenses/>.
module particles_point
  !>--------------------------------------------------------------------------
  ! Module: particles_point
  ! Author: Mohamed Houssem Kasbaoui
  !
  ! Numerical methods for lagrangian point particles
  ! --------------------------------------------------------------------------
  use leapIO
  use leapKinds
  use leapLagrangian
  use leapEulerian
  use leapBlock
  use leapParallel
  use mpi_f08
  implicit none
  private
  public :: particle_obj, particle_BH_obj, particle_set

  ! Particle types supported
  integer, parameter :: PP_TYPE_DEFAULT = 1                !! Default type
  integer, parameter :: PP_TYPE_BASSET  = 2                !! Type used when computing Basset History force

  type, extends (lagrangian_obj) :: particle_obj
    !> An extended Lagrangian object that represents a
    ! Lagrangian solid particle or liquid droplet
    integer  :: s                                          !! A tag
    real(WP) :: d                                          !! Diameter of the particle
    real(WP) :: rho                                        !! Particle density
    real(WP) :: v(3)                                       !! Particle velocity
    real(WP) :: w(3)                                       !! Particle velocity
    real(WP) :: Fh(3)                                      !! Hydrodynamic force applied on particle
    real(WP) :: Th(3)                                      !! Hydrodynamic torque applied on particle
    real(WP) :: Fc(3)                                      !! Collision force applied on particle
    real(WP) :: Tc(3)                                      !! Collision torque applied on particle
    real(WP) :: pold(3)                                    !! Old particle position
    real(WP) :: vold(3)                                    !! Old particle velocity
    real(WP) :: wold(3)                                    !! Old particle angular velocity
    real(WP) :: Fhold(3)                                   !! Old hydrodynamic force
    real(WP) :: Thold(3)                                   !! Old hydrodynamic torque
    real(WP) :: Fcold(3)                                   !! Old collision force
    real(WP) :: Tcold(3)                                   !! Old collision torque
    contains
      procedure :: assign          => particle_obj_assign
  end type particle_obj

  type,extends(lagrangian_set) :: particle_set
    !> Array of particles
    integer  :: part_type = PP_TYPE_DEFAULT                !! Particle type to use
    contains
      generic   :: Initialize          => particle_set_Init
      procedure :: Read                => particle_set_Read
      procedure :: ReadNGA             => particle_set_ReadNGA
      procedure :: Write               => particle_set_Write
      procedure :: Filter              => particle_set_Filter
      procedure :: SetMPIDataTypeParams=> particle_set_SetMPIDataTypeParams
      procedure :: SetObjectType       => particle_SetObjectType
      procedure, private :: particle_set_Init 
  end type particle_set

  ! Parameters of the Basset History force
  integer,parameter :: BH_N=5                              !! Number of entries to track in time history
  integer,parameter :: BH_E=10                             !! Number of exponential functions used to approximate tail of BH kernel
  real(wp),parameter ::   &
   BH_T(BH_E)=[0.1_wp   , &
               0.3_wp   , &
               1.0_wp   , &
               3.9_wp   , &
               10.0_wp  , &
               40.0_wp  , &
               190.0_wp , &
               1000.0_wp, &
               6500.0_wp, &
               50000.0_wp]                                 !! Non-dimensional time in BH kernel
  real(wp),parameter ::             &
   BH_A(BH_E)=[0.23477481312586_wp, &
               0.28549576238194_wp, &
               0.28479416718255_wp, &
               0.26149775537574_wp, &
               0.32056200511938_wp, &
               0.35354490689146_wp, &
               0.39635904496921_wp, &
               0.42253908596514_wp, &
               0.48317384225265_wp, &
               0.63661146557001_wp]                        !! Non-dimensional weights in BH kernel

  type, extends(particle_obj) :: particle_BH_obj
    !> A particle type that supports the computation
    ! of Basset History force
    real(WP) :: Fb(3)                                      !! Basset force
    real(WP) :: vs(3)                                      !! Slip velocity
    real(WP) :: gx(BH_N+1)                                 !! Time series of slip velocity derivatives (x-dir)
    real(WP) :: gy(BH_N+1)                                 !! Time series of slip velocity derivatives (y-dir)
    real(WP) :: gz(BH_N+1)                                 !! Time series of slip velocity derivatives (z-idr)
    real(WP) :: Fix(BH_E)                                  !! Exponential tails of Basset kernel (x-dir)
    real(WP) :: Fiy(BH_E)                                  !! Exponential tails of Basset kernel (y-dir)
    real(WP) :: Fiz(BH_E)                                  !! Exponential tails of Basset kernel (z-idr)
    contains
      procedure :: assign            => particle_BH_obj_assign
  end type particle_BH_obj


  interface
    module subroutine particle_set_ReadNGA(this,iter,time)
      implicit none
      class(particle_set), intent(inout) :: this           !! Lagrangian array to dump
      integer,             intent(out)   :: iter           !! Iteration at write
      real(wp),            intent(out)   :: time           !! Time at write
    end subroutine
  end interface

  contains
    ! particle_obj methods
    ! ------------------------------------------------------
    subroutine particle_obj_assign(this,val)
      !> Assignment
      implicit none
      class(particle_obj), intent(inout) :: this           !! An particle_obj object
      class(lagrangian_obj),  intent(in) :: val            !! An particle_obj object
      select type (val)
      type is (particle_obj)
        this%id    = val%id
        this%p     = val%p
        this%c     = val%c
        this%s     = val%s
        this%d     = val%d
        this%rho   = val%rho
        this%v     = val%v
        this%w     = val%w
        this%Fh    = val%Fh
        this%Th    = val%Th
        this%Fc    = val%Fc
        this%Tc    = val%Tc
        this%pold  = val%pold
        this%vold  = val%vold
        this%wold  = val%wold
        this%Fhold = val%Fhold
        this%Thold = val%Thold
        this%Fcold = val%Fcold
        this%Tcold = val%Tcold
      end select
      return
    end subroutine particle_obj_assign
    subroutine particle_BH_obj_assign(this,val)
      !> Assignment
      implicit none
      class(particle_BH_obj), intent(inout) :: this        !! An particle_obj object
      class(lagrangian_obj),  intent(in) :: val            !! An particle_obj object
      select type (val)
      type is (particle_BH_obj)
        this%id    = val%id
        this%p     = val%p
        this%c     = val%c
        this%s     = val%s
        this%d     = val%d
        this%rho   = val%rho
        this%v     = val%v
        this%w     = val%w
        this%Fh    = val%Fh
        this%Th    = val%Th
        this%Fc    = val%Fc
        this%Tc    = val%Tc
        this%pold  = val%pold
        this%vold  = val%vold
        this%wold  = val%wold
        this%Fhold = val%Fhold
        this%Thold = val%Thold
        this%Fcold = val%Fcold
        this%Tcold = val%Tcold
        this%Fb    = val%Fb
        this%vs    = val%vs
        this%gx    = val%gx
        this%gy    = val%gy
        this%gz    = val%gz
        this%Fix   = val%Fix
        this%Fiy   = val%Fiy
        this%Fiz   = val%Fiz
      end select
      return
    end subroutine particle_BH_obj_assign
    ! particle_set methods
    ! ------------------------------------------------------
    subroutine particle_set_Init(this,name,block,parallel,type)
      !> Alternate initialization to be used when
      ! particle type is present
      implicit none
      class(particle_set),      intent(inout) :: this      !! Set of particles
      character(len=*),         intent(in)    :: name      !! Name of variable
      type(block_obj),   target,intent(in)    :: block     !! A block object
      type(parallel_obj),target,intent(in)    :: parallel  !! parallel structure from main program
      character(len=*),         intent(in)    :: type      !! particle type

      ! Point to the master objects
      this%parallel => parallel
      this%block    => block

      ! Select appropriate type
      select case (trim(adjustl(type)))
      case ('Default','default','DEFAULT')
        this%part_type = PP_TYPE_DEFAULT
      case ('Basset', 'basset', 'BASSET' )
        this%part_type = PP_TYPE_BASSET
      case default
        call this%parallel%stop ("Unknown particle type")
      end select

      ! Allocate arrays
      allocate(this%count_proc(this%parallel%nproc))
      this%count_proc(:)=0

      ! Set name of variable
      this%name=trim(adjustl(name))

      ! Set sample type
       call this%SetObjectType()

      ! Initialize array with length zero
      this%count_ = 0
      call this%resize(this%count_)

      ! Create MPI type
      call this%CreateMPIType
      return
    end subroutine particle_set_Init
    subroutine particle_SetObjectType(this)
      !> Set the sample type used in allocation
      ! of polymorphic variables
      implicit none
      class(particle_set), intent(inout) :: this           !! Lagrangian array to dump
      ! Work variables
      type(particle_obj)    :: sample                      !! My sample
      type(particle_BH_obj) :: sample_BH                   !! My sample

      select case (this%part_type)
      case (PP_TYPE_DEFAULT)
        allocate(this%sample,source=sample   )
      case (PP_TYPE_BASSET )
        allocate(this%sample,source=sample_BH)
      case default
        call this%parallel%stop("Unknown particle type")
      end select
      return
    end subroutine particle_SetObjectType
    subroutine particle_set_Read(this,iter,time)
      !> Read particle data from file in parallel
      implicit none
      class(particle_set), intent(inout) :: this           !! Lagrangian array to dump
      integer,             intent(out)   :: iter           !! Iteration at write
      real(wp),            intent(out)   :: time           !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer,          allocatable :: buffi(:)
      integer(leapI8),  allocatable :: buffi8(:)
      real(wp),         allocatable :: buffr(:)

      ! Open the file
      call h5%Initialize(trim(adjustl(this%read_file)),"R",this%parallel)

      ! Jump to last step
      call h5%LastTimeStep(iter,time)

      ! Get total number of point objects
      call h5%getnpoints(this%count)

      ! Distribute equally the data on each mpi rank
      associate(mpi=>this%parallel)
        this%count_=int(this%count/mpi%nproc)
        if (mpi%rank%mine.le.mod(this%count,mpi%nproc)) this%count_=this%count_+1
      end associate

      ! Resize the data array to contain read Lagrangian objects
      call this%resize(this%count_)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))
      ! Read the base Lagrangian object structure
      if (this%count_.eq.0) then
        call h5%read("id",  buffi8)
        call h5%read("x",   buffr )
        call h5%read("y",   buffr )
        call h5%read("z",   buffr )
        call h5%read("i",   buffi )
        call h5%read("j",   buffi )
        call h5%read("k",   buffi )
        call h5%read("vx",  buffr )
        call h5%read("vy",  buffr )
        call h5%read("vz",  buffr )
        call h5%read("wx",  buffr )
        call h5%read("wy",  buffr )
        call h5%read("wz",  buffr )
        call h5%read("d",   buffr )
        call h5%read("s",   buffi )
        call h5%read("rho", buffr )
        call h5%read("Fhx", buffr )
        call h5%read("Fhy", buffr )
        call h5%read("Fhz", buffr )
        call h5%read("Thx", buffr )
        call h5%read("Thy", buffr )
        call h5%read("Thz", buffr )
        call h5%read("Fcx", buffr )
        call h5%read("Fcy", buffr )
        call h5%read("Fcz", buffr )
        call h5%read("Tcx", buffr )
        call h5%read("Tcy", buffr )
        call h5%read("Tcz", buffr )
      else
        select type (particles=>this%p)
        class is (particle_obj)
          call h5%read("id",  buffi8); particles(:)%id   = buffi8
          call h5%read("x",   buffr ); particles(:)%p(1) = buffr
          call h5%read("y",   buffr ); particles(:)%p(2) = buffr
          call h5%read("z",   buffr ); particles(:)%p(3) = buffr
          call h5%read("i",   buffi ); particles(:)%c(1) = buffi
          call h5%read("j",   buffi ); particles(:)%c(2) = buffi
          call h5%read("k",   buffi ); particles(:)%c(3) = buffi
          call h5%read("vx",  buffr ); particles(:)%v(1) = buffr
          call h5%read("vy",  buffr ); particles(:)%v(2) = buffr
          call h5%read("vz",  buffr ); particles(:)%v(3) = buffr
          call h5%read("wx",  buffr ); particles(:)%w(1) = buffr
          call h5%read("wy",  buffr ); particles(:)%w(2) = buffr
          call h5%read("wz",  buffr ); particles(:)%w(3) = buffr
          call h5%read("d",   buffr ); particles(:)%d    = buffr
          call h5%read("s",   buffi ); particles(:)%s    = buffi
          call h5%read("rho", buffr ); particles(:)%rho  = buffr
          call h5%read("Fhx", buffr ); particles(:)%Fh(1)= buffr
          call h5%read("Fhy", buffr ); particles(:)%Fh(2)= buffr
          call h5%read("Fhz", buffr ); particles(:)%Fh(3)= buffr
          call h5%read("Thx", buffr ); particles(:)%Th(1)= buffr
          call h5%read("Thy", buffr ); particles(:)%Th(2)= buffr
          call h5%read("Thz", buffr ); particles(:)%Th(3)= buffr
          call h5%read("Fcx", buffr ); particles(:)%Fc(1)= buffr
          call h5%read("Fcy", buffr ); particles(:)%Fc(2)= buffr
          call h5%read("Fcz", buffr ); particles(:)%Fc(3)= buffr
          call h5%read("Tcx", buffr ); particles(:)%Tc(1)= buffr
          call h5%read("Tcy", buffr ); particles(:)%Tc(2)= buffr
          call h5%read("Tcz", buffr ); particles(:)%Tc(3)= buffr
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%Finalize

      return
    end subroutine particle_set_Read

    subroutine particle_set_Write(this,iter,time)
      !> Write particle data to file in parallel
      implicit none
      class(particle_set), intent(inout) :: this           !! Lagrangian array to dump
      integer,             intent(in)    :: iter           !! Iteration at write
      real(wp),            intent(in)    :: time           !! Time at write
      ! Work variables
      type(h5hut_obj) :: h5                                !! H5hut structure
      integer,          allocatable :: buffi(:)
      integer(leapI8),  allocatable :: buffi8(:)
      real(wp),         allocatable :: buffr(:)

      ! Nothing to write, if empty
      call this%UpdateCount()
      if (this%count.eq.0) return

      ! Open the file
      if (this%overwrite) then
        call h5%Initialize(trim(adjustl(this%write_file)),"W",this%parallel)
      else
        call h5%Initialize(trim(adjustl(this%write_file)),"RW",this%parallel)
      end if

      ! Create a new time step
      call h5%NewTimeStep(iter,time)

      ! Allocate buffers
      allocate(buffi8(this%count_),buffi(this%count_),buffr(this%count_))

      ! Write the particle data
      if (this%count_.eq.0) then
        call h5%write("id",  buffi8)
        call h5%write("x",   buffr )
        call h5%write("y",   buffr )
        call h5%write("z",   buffr )
        call h5%write("i",   buffi )
        call h5%write("j",   buffi )
        call h5%write("k",   buffi )
        call h5%write("vx",  buffr )
        call h5%write("vy",  buffr )
        call h5%write("vz",  buffr )
        call h5%write("wx",  buffr )
        call h5%write("wy",  buffr )
        call h5%write("wz",  buffr )
        call h5%write("d",   buffr )
        call h5%write("s",   buffi )
        call h5%write("rho", buffr )
        call h5%write("Fhx", buffr )
        call h5%write("Fhy", buffr )
        call h5%write("Fhz", buffr )
        call h5%write("Thx", buffr )
        call h5%write("Thy", buffr )
        call h5%write("Thz", buffr )
        call h5%write("Fcx", buffr )
        call h5%write("Fcy", buffr )
        call h5%write("Fcz", buffr )
        call h5%write("Tcx", buffr )
        call h5%write("Tcy", buffr )
        call h5%write("Tcz", buffr )
      else
        select type (particles=>this%p)
        class is (particle_obj)
          buffi8=particles(1:this%count_)%id   ; call h5%write("id",  buffi8)
          buffr =particles(1:this%count_)%p(1) ; call h5%write("x",   buffr )
          buffr =particles(1:this%count_)%p(2) ; call h5%write("y",   buffr )
          buffr =particles(1:this%count_)%p(3) ; call h5%write("z",   buffr )
          buffi =particles(1:this%count_)%c(1) ; call h5%write("i",   buffi )
          buffi =particles(1:this%count_)%c(2) ; call h5%write("j",   buffi )
          buffi =particles(1:this%count_)%c(3) ; call h5%write("k",   buffi )
          buffr =particles(1:this%count_)%v(1) ; call h5%write("vx",  buffr )
          buffr =particles(1:this%count_)%v(2) ; call h5%write("vy",  buffr )
          buffr =particles(1:this%count_)%v(3) ; call h5%write("vz",  buffr )
          buffr =particles(1:this%count_)%w(1) ; call h5%write("wx",  buffr )
          buffr =particles(1:this%count_)%w(2) ; call h5%write("wy",  buffr )
          buffr =particles(1:this%count_)%w(3) ; call h5%write("wz",  buffr )
          buffr =particles(1:this%count_)%d    ; call h5%write("d",   buffr )
          buffi =particles(1:this%count_)%s    ; call h5%write("s",   buffi )
          buffr =particles(1:this%count_)%rho  ; call h5%write("rho", buffr )
          buffr =particles(1:this%count_)%Fh(1); call h5%write("Fhx", buffr )
          buffr =particles(1:this%count_)%Fh(2); call h5%write("Fhy", buffr )
          buffr =particles(1:this%count_)%Fh(3); call h5%write("Fhz", buffr )
          buffr =particles(1:this%count_)%Th(1); call h5%write("Thx", buffr )
          buffr =particles(1:this%count_)%Th(2); call h5%write("Thy", buffr )
          buffr =particles(1:this%count_)%Th(3); call h5%write("Thz", buffr )
          buffr =particles(1:this%count_)%Fc(1); call h5%write("Fcx", buffr )
          buffr =particles(1:this%count_)%Fc(2); call h5%write("Fcy", buffr )
          buffr =particles(1:this%count_)%Fc(3); call h5%write("Fcz", buffr )
          buffr =particles(1:this%count_)%Tc(1); call h5%write("Tcx", buffr )
          buffr =particles(1:this%count_)%Tc(2); call h5%write("Tcy", buffr )
          buffr =particles(1:this%count_)%Tc(3); call h5%write("Tcz", buffr )
        end select
      end if

      ! Deallocate buffers
      deallocate(buffi8,buffi,buffr)

      call h5%Finalize

      return
    end subroutine particle_set_Write
    subroutine particle_set_Filter(this,var,field)
      !> Filter a quantity to the Eulerian grid
      implicit none
      class(particle_set), intent(inout) :: this           !! Set of Lagrangian objects
      character(len=*),    intent(in)    :: var            !! Variable to compute
      type(eulerian_obj_r),intent(inout) :: field          !! Filtered quantity
      ! Work variables
      real(wp),allocatable :: bump(:,:,:)
      real(wp):: coef
      integer :: slo(3),shi(3)
      integer :: n,i,j,k
      real(wp),parameter :: Pi=4.0_wp*atan(1.0_wp) 

      ! Initialize field
      field%cell=0.0_wp
      select type(particles=>this%p)
      class is (particle_obj)
        select case(trim(adjustl(var)))
        case ('volume fraction')
          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=particles(n)%c-this%stib/2
            shi=particles(n)%c+this%stib/2
            call particles(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=Pi*particles(n)%d**3/6.0_wp
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do

        case ('V1')
          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=particles(n)%c-this%stib/2
            shi=particles(n)%c+this%stib/2
            call particles(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=Pi*particles(n)%d**3/6.0_wp*particles(n)%v(1)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do

        case ('V2')
          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=particles(n)%c-this%stib/2
            shi=particles(n)%c+this%stib/2
            call particles(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=Pi*particles(n)%d**3/6.0_wp*particles(n)%v(2)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do

        case ('V3')
          do n=1,this%count_
            ! Get a bump function centered
            ! on the marker
            ! -------------------------- !
            slo=particles(n)%c-this%stib/2
            shi=particles(n)%c+this%stib/2
            call particles(n)%extrapolate(this%l_filter,slo,shi,this%block,this%g1ex,bump)

            ! Scale the bump function
            ! -------------------------- !
            do k=slo(3),shi(3)
              do j=slo(2),shi(2)
                do i=slo(1),shi(1)
                  coef=Pi*particles(n)%d**3/6.0_wp*particles(n)%v(3)
                  field%cell(i,j,k) = field%cell(i,j,k)+ coef*bump(i,j,k)
                end do
              end do
            end do
          end do
        case default
          call this%parallel%stop("Unkown quantity to filter")
        end select
      end select

      ! Free memory
      if (allocated(bump))      deallocate(bump)

      ! Update ghostcells
      call field%AddUpGhostCells

      return
    end subroutine particle_set_Filter

    ! Deferred lagrangian_set methods
    ! ------------------------------------------------------
    subroutine particle_set_SetMPIDataTypeParams(this,types,lengths,displacement)
      !> Set up parameters used when creating the MPI derived type
      implicit none
      class(particle_set), intent(inout) :: this           !! Set of Lagrangian objects
      type(MPI_Datatype), allocatable, &
                           intent(out)   :: types(:)       !! Array of types
      integer, allocatable,intent(out)   :: lengths(:)     !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
               allocatable,intent(out)   :: displacement(:)!! Array of displacements

      select case (this%part_type)
      case (PP_TYPE_DEFAULT)
        call particle_set_SetMPIDataTypeParams_default(this,types,lengths,displacement)
      case (PP_TYPE_BASSET )
        call particle_set_SetMPIDataTypeParams_BH     (this,types,lengths,displacement)
      case default
        call this%parallel%stop("Unknown particle type")
      end select
      return
    end subroutine particle_set_SetMPIDataTypeParams
    subroutine particle_set_SetMPIDataTypeParams_default(this,types,lengths,displacement)
      !> Set up parameters used when creating the MPI derived type
      implicit none
      class(particle_set), intent(inout) :: this           !! Set of Lagrangian objects
      type(MPI_Datatype), allocatable, &
                           intent(out)   :: types(:)       !! Array of types
      integer, allocatable,intent(out)   :: lengths(:)     !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
               allocatable,intent(out)   :: displacement(:)!! Array of displacements
      ! Work variable
      integer :: N
      type(particle_obj) :: sample
      integer :: ierr

      associate(mpi=>this%parallel)
        ! Create the MPI structure
        N=19
        allocate(types(N),displacement(N),lengths(N))
        types( 1) = mpi%INT8    ; lengths( 1) = 1                    ! id     --  integer8
        types( 2) = mpi%REAL_WP ; lengths( 2) = size(sample%p)       ! p      --  real*3
        types( 3) = mpi%INTEGER ; lengths( 3) = size(sample%c)       ! c      --  integer*3
        types( 4) = mpi%INTEGER ; lengths( 4) = 1                    ! s      --  integer
        types( 5) = mpi%REAL_WP ; lengths( 5) = 1                    ! d      --  real
        types( 6) = mpi%REAL_WP ; lengths( 6) = 1                    ! rho    --  real
        types( 7) = mpi%REAL_WP ; lengths( 7) = size(sample%v)       ! v      --  real*3
        types( 8) = mpi%REAL_WP ; lengths( 8) = size(sample%w)       ! w      --  real*3
        types( 9) = mpi%REAL_WP ; lengths( 9) = size(sample%Fh)      ! Fh     --  real*3
        types(10) = mpi%REAL_WP ; lengths(10) = size(sample%Th)      ! Th     --  real*3
        types(11) = mpi%REAL_WP ; lengths(11) = size(sample%Fc)      ! Fc     --  real*3
        types(12) = mpi%REAL_WP ; lengths(12) = size(sample%Tc)      ! Tc     --  real*3
        types(13) = mpi%REAL_WP ; lengths(13) = size(sample%pold)    ! pold   --  real*3
        types(14) = mpi%REAL_WP ; lengths(14) = size(sample%vold)    ! vold   --  real*3
        types(15) = mpi%REAL_WP ; lengths(15) = size(sample%wold)    ! wold   --  real*3
        types(16) = mpi%REAL_WP ; lengths(16) = size(sample%Fhold)   ! Fhold  --  real*3
        types(17) = mpi%REAL_WP ; lengths(17) = size(sample%Thold)   ! Thold  --  real*3
        types(18) = mpi%REAL_WP ; lengths(18) = size(sample%Fcold)   ! Fcold  --  real*3
        types(19) = mpi%REAL_WP ; lengths(19) = size(sample%Tcold)   ! Tcold  --  real*3
        ! Count the displacement for this structure
        call MPI_GET_ADDRESS(sample%id,      displacement( 1),ierr)
        call MPI_GET_ADDRESS(sample%p(1),    displacement( 2),ierr)
        call MPI_GET_ADDRESS(sample%c(1),    displacement( 3),ierr)
        call MPI_GET_ADDRESS(sample%s,       displacement( 4),ierr)
        call MPI_GET_ADDRESS(sample%d,       displacement( 5),ierr)
        call MPI_GET_ADDRESS(sample%rho,     displacement( 6),ierr)
        call MPI_GET_ADDRESS(sample%v(1),    displacement( 7),ierr)
        call MPI_GET_ADDRESS(sample%w(1),    displacement( 8),ierr)
        call MPI_GET_ADDRESS(sample%Fh(1),   displacement( 9),ierr)
        call MPI_GET_ADDRESS(sample%Th(1),   displacement(10),ierr)
        call MPI_GET_ADDRESS(sample%Fc(1),   displacement(11),ierr)
        call MPI_GET_ADDRESS(sample%Tc(1),   displacement(12),ierr)
        call MPI_GET_ADDRESS(sample%pold(1), displacement(13),ierr)
        call MPI_GET_ADDRESS(sample%vold(1), displacement(14),ierr)
        call MPI_GET_ADDRESS(sample%wold(1), displacement(15),ierr)
        call MPI_GET_ADDRESS(sample%Fhold(1),displacement(16),ierr)
        call MPI_GET_ADDRESS(sample%Thold(1),displacement(17),ierr)
        call MPI_GET_ADDRESS(sample%Fcold(1),displacement(18),ierr)
        call MPI_GET_ADDRESS(sample%Tcold(1),displacement(19),ierr)
        displacement = displacement - displacement( 1)
      end associate
      return
    end subroutine particle_set_SetMPIDataTypeParams_default
    subroutine particle_set_SetMPIDataTypeParams_BH(this,types,lengths,displacement)
      !> Set up parameters used when creating the MPI derived type
      implicit none
      class(particle_set), intent(inout) :: this           !! Set of Lagrangian objects
      type(MPI_Datatype), allocatable, &
                           intent(out)   :: types(:)       !! Array of types
      integer, allocatable,intent(out)   :: lengths(:)     !! Array of lengths
      integer(kind=MPI_ADDRESS_KIND),  &
               allocatable,intent(out)   :: displacement(:)!! Array of displacements
      ! Work variable
      integer :: N
      type(particle_BH_obj) :: sample
      integer :: ierr

      associate(mpi=>this%parallel)
        ! Create the MPI structure
        N=27
        allocate(types(N),displacement(N),lengths(N))
        types( 1) = mpi%INT8    ; lengths( 1) = 1                    ! id     --  integer8
        types( 2) = mpi%REAL_WP ; lengths( 2) = size(sample%p)       ! p      --  real*3
        types( 3) = mpi%INTEGER ; lengths( 3) = size(sample%c)       ! c      --  integer*3
        types( 4) = mpi%INTEGER ; lengths( 4) = 1                    ! s      --  integer
        types( 5) = mpi%REAL_WP ; lengths( 5) = 1                    ! d      --  real
        types( 6) = mpi%REAL_WP ; lengths( 6) = 1                    ! rho    --  real
        types( 7) = mpi%REAL_WP ; lengths( 7) = size(sample%v)       ! v      --  real*3
        types( 8) = mpi%REAL_WP ; lengths( 8) = size(sample%w)       ! w      --  real*3
        types( 9) = mpi%REAL_WP ; lengths( 9) = size(sample%Fh)      ! Fh     --  real*3
        types(10) = mpi%REAL_WP ; lengths(10) = size(sample%Th)      ! Th     --  real*3
        types(11) = mpi%REAL_WP ; lengths(11) = size(sample%Fc)      ! Fc     --  real*3
        types(12) = mpi%REAL_WP ; lengths(12) = size(sample%Tc)      ! Tc     --  real*3
        types(13) = mpi%REAL_WP ; lengths(13) = size(sample%pold)    ! pold   --  real*3
        types(14) = mpi%REAL_WP ; lengths(14) = size(sample%vold)    ! vold   --  real*3
        types(15) = mpi%REAL_WP ; lengths(15) = size(sample%wold)    ! wold   --  real*3
        types(16) = mpi%REAL_WP ; lengths(16) = size(sample%Fhold)   ! Fhold  --  real*3
        types(17) = mpi%REAL_WP ; lengths(17) = size(sample%Thold)   ! Thold  --  real*3
        types(18) = mpi%REAL_WP ; lengths(18) = size(sample%Fcold)   ! Fcold  --  real*3
        types(19) = mpi%REAL_WP ; lengths(19) = size(sample%Tcold)   ! Tcold  --  real*3
        types(20) = mpi%REAL_WP ; lengths(20) = size(sample%Fb)      ! Fb     --  real*3
        types(21) = mpi%REAL_WP ; lengths(21) = size(sample%vs)      ! vs     --  real*3
        types(22) = mpi%REAL_WP ; lengths(22) = size(sample%gx)      ! gx     --  real*3
        types(23) = mpi%REAL_WP ; lengths(23) = size(sample%gy)      ! gy     --  real*3
        types(24) = mpi%REAL_WP ; lengths(24) = size(sample%gz)      ! gz     --  real*3
        types(25) = mpi%REAL_WP ; lengths(25) = size(sample%Fix)     ! Fix    --  real*3
        types(26) = mpi%REAL_WP ; lengths(26) = size(sample%Fiy)     ! Fiy    --  real*3
        types(27) = mpi%REAL_WP ; lengths(27) = size(sample%Fiz)     ! Fiz    --  real*3
        ! Count the displacement for this structure
        call MPI_GET_ADDRESS(sample%id,      displacement( 1),ierr)
        call MPI_GET_ADDRESS(sample%p(1),    displacement( 2),ierr)
        call MPI_GET_ADDRESS(sample%c(1),    displacement( 3),ierr)
        call MPI_GET_ADDRESS(sample%s,       displacement( 4),ierr)
        call MPI_GET_ADDRESS(sample%d,       displacement( 5),ierr)
        call MPI_GET_ADDRESS(sample%rho,     displacement( 6),ierr)
        call MPI_GET_ADDRESS(sample%v(1),    displacement( 7),ierr)
        call MPI_GET_ADDRESS(sample%w(1),    displacement( 8),ierr)
        call MPI_GET_ADDRESS(sample%Fh(1),   displacement( 9),ierr)
        call MPI_GET_ADDRESS(sample%Th(1),   displacement(10),ierr)
        call MPI_GET_ADDRESS(sample%Fc(1),   displacement(11),ierr)
        call MPI_GET_ADDRESS(sample%Tc(1),   displacement(12),ierr)
        call MPI_GET_ADDRESS(sample%pold(1), displacement(13),ierr)
        call MPI_GET_ADDRESS(sample%vold(1), displacement(14),ierr)
        call MPI_GET_ADDRESS(sample%wold(1), displacement(15),ierr)
        call MPI_GET_ADDRESS(sample%Fhold(1),displacement(16),ierr)
        call MPI_GET_ADDRESS(sample%Thold(1),displacement(17),ierr)
        call MPI_GET_ADDRESS(sample%Fcold(1),displacement(18),ierr)
        call MPI_GET_ADDRESS(sample%Tcold(1),displacement(19),ierr)
        call MPI_GET_ADDRESS(sample%Fb(1),   displacement(20),ierr)
        call MPI_GET_ADDRESS(sample%vs(1),   displacement(21),ierr)
        call MPI_GET_ADDRESS(sample%gx(1),   displacement(22),ierr)
        call MPI_GET_ADDRESS(sample%gy(1),   displacement(23),ierr)
        call MPI_GET_ADDRESS(sample%gz(1),   displacement(24),ierr)
        call MPI_GET_ADDRESS(sample%Fix(1),  displacement(25),ierr)
        call MPI_GET_ADDRESS(sample%Fiy(1),  displacement(26),ierr)
        call MPI_GET_ADDRESS(sample%Fiz(1),  displacement(27),ierr)
        displacement = displacement - displacement( 1)
      end associate
      return
    end subroutine particle_set_SetMPIDataTypeParams_BH
end module particles_point
